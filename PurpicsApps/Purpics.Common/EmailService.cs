﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Purpics.Common
{
    public static class EmailService
    {
        public static void SendEmail(string Subject, string Body, string MailTo)
        {
            try
            {
                if (!(string.IsNullOrWhiteSpace(MailTo)))
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                    string fromEmailId = ConfigurationManager.AppSettings["FromEmailId"];
                    mail.From = new MailAddress(fromEmailId, fromEmailId);
                    mail.To.Add(MailTo);
                    mail.Subject = Subject;
                    mail.Body = Body;
                    mail.IsBodyHtml = true;
                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUsername"], ConfigurationManager.AppSettings["SMTPPassword"]);
                    SmtpServer.EnableSsl = true;
                    SmtpServer.Send(mail);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
