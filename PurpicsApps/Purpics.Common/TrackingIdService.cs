using System.Security.Cryptography;
using System.Text;

namespace Purpics.Common
{
    /// <summary>
    /// This class generates a unique short tracking id for Purpics given the VolunteerId and the CampaignUploadId
    /// </summary>
    public class TrackingIdService
    {
        // Define other methods and classes here
        // define characters allowed in passcode.  set length so divisible into 256
        static readonly char[] ValidChars = {'2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F','G','H',
            'J','K','L','M','N','P','Q',
            'R','S','T','U','V','W','X','Y','Z'}; // len=32

        private const string Hashkey = "03af67fa-cb26-4101-b5c3-83180593eb8b"; // unique key for HMAC function
        const int Codelength = 6; // length of passcode

        // Code adopted from http://stackoverflow.com/a/6810497/1183778
        public static string NewId(int testVolunteerId, int testUploadId)
        {
            byte[] hash;
            var stringToHash = string.Format("{0}|{1}", testVolunteerId, testUploadId);

            using (var sha1 = new HMACSHA1(Encoding.ASCII.GetBytes(Hashkey)))
                hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(stringToHash));
            
            var startpos = hash[hash.Length - 1] % (hash.Length - Codelength);
            
            var passbuilder = new StringBuilder();
            for (var i = startpos; i < startpos + Codelength; i++)
                passbuilder.Append(ValidChars[hash[i] % ValidChars.Length]);
            
            return passbuilder.ToString();
        }
    }
}