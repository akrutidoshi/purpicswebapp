﻿# Purpics.Common README

Author: [Ken Taylor](mailto:ktaylor@gotechark.com?subject=Purpics.Common%20Library)  
Date Created: 2016-05-05  
Last Updated: 2016-05-05  

## Summary

This library contains common classes used across the Purpics project.
It is primarily for untility classes.


## Installation

Use this library by including the csproj file in your solution. 
This should be part of the Purpics master solution file.

## Testing

Please refer to the MSTest test library -> `Purpics.Common.Tests`

---