﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Purpics.Instagram.Tests
{
    [TestClass]
    public class InstagramServiceTestOLD
    {
        private const int OneHundredLikes = 100;
        private const string TestMediaId = "341503663076960026_25108224";

        // tracking ID is in the caption
        // service also needs the tracking ID to look for likes
        // should we still gather likes for expired campaigns?
        // we will need to get all campaigns that have not expired and pass them in

        [TestMethod]
        public void GetCurrentLikeCountByMediaId_GivenTestParams_ReturnsAValues()
        {
            // Arrange
            var service = new Mock<IInstagramService>();
            service.Setup(x => x.GetCurrentLikeCountByMediaId(TestMediaId)).Returns(OneHundredLikes);
            // Act
            var likes = service.Object.GetCurrentLikeCountByMediaId(TestMediaId);
            // Assert
            likes.Should().Be(OneHundredLikes);
        }

        [TestMethod]
        public void GetMediaId_GivenTestParams_ReturnsAValue()
        {
            // Arrange
            var instagramUserId = "testId";
            var instagramToken = "testToken";
            var trackingId = "testTrackingId";
            var service = new Mock<IInstagramService>();
            service.Setup(x => x.GetMediaId(instagramUserId, instagramToken, trackingId)).Returns(TestMediaId);
            // Act
            var mediaId = service.Object.GetMediaId(instagramUserId, instagramToken, trackingId);
            // Assert
            mediaId.Should().NotBeNullOrEmpty();
        }
    }

    public interface IInstagramService
    {
        int GetCurrentLikeCountByMediaId(string mediaId);
        string GetMediaId(string instagramUserId, string instagramToken, string trackingId);
    }
}
