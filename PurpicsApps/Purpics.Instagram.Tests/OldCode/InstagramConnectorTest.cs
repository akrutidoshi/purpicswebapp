﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// THIS WILL BE DEPRECATED!
namespace Purpics.Instagram.Tests
{
    [TestClass]
    public class InstagramConnectorTest
    {
        private string _clientId = "433528627562442ebc4d768a45d48c48";
        private string _clientSecret = "812425692a8e4f4e8349d7fde2fd0487";
        private string _websiteUrl = "	http://www.purpics.com";
        private string _redirectUri = "	http://www.purpics.com";


        [TestMethod]
        public void Login_GivenProperConfig_ReturnsRedirectLink()
        {
            var connector = new InstagramConnector(_clientId, _clientSecret, _websiteUrl, _redirectUri);

            var link = connector.GetLoginLink();

            link.Should().NotBeNullOrEmpty();
        }
    }
}
