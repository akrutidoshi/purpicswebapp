﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Purpics.Instagram.Tests
{
    [TestClass]
    public class InstagramServiceTest
    {
        private readonly Mock<IInstagramUploadRepository> _repo = new Mock<IInstagramUploadRepository>();
        private readonly Mock<IInstagramApiClient> _client = new Mock<IInstagramApiClient>();
        private const string TestTrackingId = "test-trackingId";
        private const string TestMediaId = "test-mediaId";
        private const int TestLikeCount = 999;
        private readonly InstagramUpload _testUploadNoMediaId = new InstagramUpload { TrackingId = TestTrackingId };
        private InstagramUpload _testUpload = new InstagramUpload {TrackingId = TestTrackingId, MediaId = TestMediaId};
        private InstagramUpload _testUploadWithLikes = new InstagramUpload { TrackingId = TestTrackingId, MediaId = TestMediaId, LikeCount = TestLikeCount };

        [TestMethod]
        public void UpdateMediaId_GivenTrackingId_RetrievesUpload()
        {
            // Arrange
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateMediaId(TestTrackingId);
            // Assert
            _repo.Verify(x => x.GetUploadByTrackingId(TestTrackingId), Times.Once);
        }

        [TestMethod]
        public void UpdateMediaId_GivenTrackingId_CallsInstagramClient()
        {
            // Arrange
            _repo.Setup(x => x.GetUploadByTrackingId(TestTrackingId)).Returns(_testUploadNoMediaId);
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateMediaId(TestTrackingId);
            // Assert
            _client.Verify(x => x.GetMediaId(_testUploadNoMediaId), Times.Once);
        }

        [TestMethod]
        public void UpdateMediaId_GivenTrackingId_UpdatesInstagramUpload()
        {
            // Arrange
            _repo.Setup(x => x.GetUploadByTrackingId(TestTrackingId)).Returns(_testUploadNoMediaId);
            _client.Setup(x => x.GetMediaId(_testUploadNoMediaId)).Returns(_testUpload);
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateMediaId(TestTrackingId);
            // Assert
            _repo.Verify(x => x.UpdateMediaId(_testUpload), Times.Once);
        }

        [TestMethod]
        public void UpdateMediaIds_WhenCalled_RetrievesUploads()
        {
            // Arrange
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateMediaIds();
            // Assert
            _repo.Verify(x => x.GetUploadsWithNoMediaId(), Times.Once);
        }

        [TestMethod]
        public void UpdateMediaIds_WhenCalled_CallsInstagramClient()
        {
            // Arrange
            List<InstagramUpload> testUploadsNoMediaId = new List<InstagramUpload> { _testUploadNoMediaId };
            _repo.Setup(x => x.GetUploadsWithNoMediaId()).Returns(testUploadsNoMediaId);
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateMediaIds();
            // Assert
            _client.Verify(x => x.GetMediaId(_testUploadNoMediaId));
        }

        [TestMethod]
        public void UpdateMediaIds_WhenCalled_SavesFoundMediaId()
        {
            // Arrange
            List<InstagramUpload> _testUploadsNoMediaId = new List<InstagramUpload> { _testUploadNoMediaId };
            _client.Setup(x => x.GetMediaId(_testUploadNoMediaId)).Returns(_testUpload);
            _repo.Setup(x => x.GetUploadsWithNoMediaId()).Returns(_testUploadsNoMediaId);
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateMediaIds();
            // Assert
            _repo.Verify(x => x.UpdateMediaId(_testUpload));
        }

        [TestMethod]
        public void UpdateLikeCounts_WhenCalled_RetrievesUploads()
        {
            // Arrange
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateLikeCounts();
            // Assert
            _repo.Verify(x => x.GetUploadsWithMediaId(), Times.Once);
        }

        [TestMethod]
        public void UpdateLikeCounts_WhenCalled_CallsInstagramClient()
        {
            // Arrange
            List<InstagramUpload> testUploads = new List<InstagramUpload> { _testUpload };
            _repo.Setup(x => x.GetUploadsWithMediaId()).Returns(testUploads);
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateLikeCounts();
            // Assert
           _client.Verify(x => x.GetLikeCount(_testUpload), Times.AtLeastOnce);
        }

        [TestMethod]
        public void UpdateLikeCounts_WhenCalled_SavesLikeCount()
        {
            // Arrange
            _client.Setup(x => x.GetLikeCount(_testUpload)).Returns(_testUploadWithLikes);
            List<InstagramUpload> testUploads = new List<InstagramUpload> { _testUpload };
            _repo.Setup(x => x.GetUploadsWithMediaId()).Returns(testUploads);
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateLikeCounts();
            // Assert
            _repo.Verify(x => x.UpdateLikeCount(_testUploadWithLikes), Times.AtLeastOnce);
        }

        [TestMethod]
        public void UpdateLikeCountByMediaId_WhenCalled_RetrievesUpload()
        {
            // Arrange
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateLikeCountByMediaId(TestMediaId);
            // Assert
            _repo.Verify(x => x.GetUploadByMediaId(TestMediaId), Times.Once);
        }

        [TestMethod]
        public void UpdateLikeCountByMediaId_WhenCalled_CallsInstagramClient()
        {
             // Arrange
            _repo.Setup(x => x.GetUploadByMediaId(TestMediaId)).Returns(_testUpload);
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateLikeCountByMediaId(TestMediaId);
            // Assert
            _client.Verify(x => x.GetLikeCount(_testUpload), Times.Once);
        }

        [TestMethod]
        public void UpdateLikeCountByMediaId_WhenCalled_SavesLikeCount()
        {
            // Arrange
            _repo.Setup(x => x.GetUploadByMediaId(TestMediaId)).Returns(_testUpload);
            _client.Setup(x => x.GetLikeCount(_testUpload)).Returns(_testUploadWithLikes);
            var service = new InstagramService(_repo.Object, _client.Object);
            // Act
            service.UpdateLikeCountByMediaId(TestMediaId);
            // Assert
            _repo.Verify(x => x.UpdateLikeCount(_testUploadWithLikes), Times.AtLeastOnce);
        }
    }
}
