﻿using SendGrid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace PurpicsWeb.Helper
{
    public static class EmailHelper
    {
        public static bool sendEmail(string fstToEmail, string fstFromEmail, string fstSubject, string fstBody, string fstCategory, Hashtable hashtable)
        {
            try
            {
                var loMessage = new SendGridMessage();
                IEnumerable<string> lstEmailTo = fstToEmail.Split(',').ToArray();
                loMessage.AddTo(lstEmailTo);

                loMessage.From = new MailAddress(fstFromEmail, "Purpics");
                loMessage.Subject = fstSubject;
                loMessage.Html = fstBody;
                loMessage.SetCategory(fstCategory);
                loMessage.EnableGravatar();
                loMessage.EnableClickTracking(true);
                // Create credentials, specifying your user name and password.
                var loCredentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);

                // Create a Web transport for sending email.
                var loTransportWeb = new Web(loCredentials);

                // Send the email.
                if (loTransportWeb != null)
                {
                    //loTransportWeb.DeliverAsync(loMessage);
                    loTransportWeb.DeliverAsync(loMessage);
                    return true;
                }
            }
            catch (Exception ex)
            {
                writeEmailLog(ex.Message);
                return false;
            }
            return false;
        }

        public static void writeEmailLog(string fsMessage)
        {
            try
            {
                string stLogFile = "SendEmail" + DateTime.Now.ToString("ddMMyy") + ".txt";
                string stLogFolder = "Logs";

                if (!System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~//" + stLogFolder + "//" + stLogFile)))
                {
                    var stFile = System.IO.File.Create(System.Web.HttpContext.Current.Server.MapPath(@"~//" + stLogFolder + "//" + stLogFile));
                    stFile.Close();
                }
                StreamWriter loStreamWriter = new StreamWriter(System.Web.HttpContext.Current.Server.MapPath(@"~//" + stLogFolder + "//" + stLogFile), true);
                if (fsMessage != null)
                    loStreamWriter.Write(DateTime.Now.ToString() + ": " + fsMessage + "\r\n");
                loStreamWriter.Dispose();
                loStreamWriter.Close();
            }
            catch (Exception ex)
            {
            }
        }
    }
}