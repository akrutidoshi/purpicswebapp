﻿using Hangfire;
using PurpicsWeb.Controllers;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
namespace PurpicsWeb.Helper
{
    public class CommonFunctions
    {
        [AutomaticRetry(Attempts = 3)]
        public static void NotifyAdminForNewRegistration(string UserId, string NewRegistrationEmailHTML)
        {
            var row = Repository<AspNetUsers>.GetEntityListForQuery(null).Where(x => x.Id == UserId).FirstOrDefault();

            if (row != null)
            {
                string lsToMails = row.Email;
                string lsSubject = "New User Registration";
                string stEmailBody = string.Empty;
                stEmailBody = NewRegistrationEmailHTML;

                PurpicsWeb.Services.CommonFunctions.EmailHelper.SendEmail(lsSubject, stEmailBody,lsToMails);
            }
        }

        [AutomaticRetry(Attempts = 3)]
        public static void sendCompanyStatusMail(string fsName, int fiStatus, string fsToEmail)
        {
            string lsFrom = ConfigurationManager.AppSettings["FromEmail"].ToString(); ;
            string lsToMails = fsToEmail;
            string lsSubject = "Company Registration Status";
            string stEmailBody = string.Empty;

            if (fiStatus == (int)PurpicsWeb.Services.CommonFunctions.CommonEnums.UserStatus.Approved)
            {
                lsSubject = "Your Purpics registration is approved!";
                //stEmailBody = "Your Registration for " + fsName + " has been Approved";
                stEmailBody = new SystemEmailController().CompanyRegistrationApproval(fsName);
            }
            else
            {
                lsSubject = "Your Purpics registration was rejected.";
                //stEmailBody = "Your Registration for " + fsName + " has been Declined";
                stEmailBody = new SystemEmailController().CompanyRegistrationRejected();
            }


            Services.CommonFunctions.EmailHelper.SendEmail(lsSubject, stEmailBody, lsToMails);
        }

        [AutomaticRetry(Attempts = 3)]
        public static void sendCharityStatusMail(string fsName, int fiStatus, string fsToEmail)
        {
            string lsFrom = ConfigurationManager.AppSettings["FromEmail"].ToString(); ;
            string lsToMails = fsToEmail;
            string lsSubject = "Charity Registration Status";
            string stEmailBody = string.Empty;

            if (fiStatus == (int)PurpicsWeb.Services.CommonFunctions.CommonEnums.UserStatus.Approved)
            {
                //stEmailBody = "Your Registration for " + fsName + " has been Approved";
                lsSubject = "Your Purpics registration is approved!";
                stEmailBody = new SystemEmailController().CharityRegistrationApproval(fsName);
            }
            else
            {
                //stEmailBody = "Your Registration for " + fsName + " has been Declined";
                lsSubject = "Your Purpics registration was rejected.";
                stEmailBody = new SystemEmailController().CharityRegistrationRejected();
            }
            Services.CommonFunctions.EmailHelper.SendEmail(lsSubject, stEmailBody, lsToMails);
        }

        [AutomaticRetry(Attempts = 3)]
        public static void sendSOStatusMail(string fsName, int fiStatus, string fsToEmail)
        {
            string lsFrom = ConfigurationManager.AppSettings["FromEmail"].ToString(); ;
            string lsToMails = fsToEmail;
            string lsSubject = "Student Organization Registration Status";
            string stEmailBody = string.Empty;

            if (fiStatus == (int)PurpicsWeb.Services.CommonFunctions.CommonEnums.UserStatus.Approved)
            {
                //stEmailBody = "Your Registration for " + fsName + " has been Approved";
                lsSubject = "Your Purpics registration is approved!";
                stEmailBody = new SystemEmailController().SORegistrationApproval(fsName);
            }
            else
            {
                //stEmailBody = "Your Registration for " + fsName + " has been Declined";
                lsSubject = "Your Purpics registration was rejected.";
                stEmailBody = new SystemEmailController().SORegistrationRejected();
            }
            Services.CommonFunctions.EmailHelper.SendEmail(lsSubject, stEmailBody, lsToMails);
        }

        [AutomaticRetry(Attempts = 3)]
        public static void sendCampaignStatusMail(string fsName, int fiStatus, string fsToEmail)
        {
            string lsFrom = ConfigurationManager.AppSettings["FromEmail"].ToString(); ;
            string lsToMails = fsToEmail;
            string lsSubject = "Campaign Registration Status";
            string stEmailBody = string.Empty;

            if (fiStatus == (int)PurpicsWeb.Services.CommonFunctions.CommonEnums.UserStatus.Approved)
            {
                //stEmailBody = "Your Registration for " + fsName + " has been Approved";
                lsSubject = "Your Purpics campaign (" + fsName + ") is approved!";
                stEmailBody = new SystemEmailController().CampaignRegistrationApproval(fsName);
            }
            else
            {
                lsSubject = "Your Purpics campaign (" + fsName + ") was rejected.";
                stEmailBody = new SystemEmailController().CompanyRegistrationRejected();
            }
            Services.CommonFunctions.EmailHelper.SendEmail(lsSubject, stEmailBody, lsToMails);
        }

        [AutomaticRetry(Attempts = 3)]
        public static void sendGiftCardMailToVolunteer(string fsToEmail)
        {
            string lsFrom = ConfigurationManager.AppSettings["FromEmail"].ToString(); ;
            string lsToMails = fsToEmail;
            string lsSubject = "Your gift card is on it's way!";
            string stEmailBody = string.Empty;
            
            stEmailBody = new SystemEmailController().SendGiftCardEmail();
            
            Services.CommonFunctions.EmailHelper.SendEmail(lsSubject, stEmailBody, lsToMails);
        }
    }
}