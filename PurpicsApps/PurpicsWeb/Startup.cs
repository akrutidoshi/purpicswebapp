﻿using Microsoft.Owin;
using Owin;
using Hangfire;
using GlobalConfiguration = Hangfire.GlobalConfiguration;

[assembly: OwinStartupAttribute(typeof(PurpicsWeb.Startup))]
namespace PurpicsWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            PurpicsWeb.Hangfire.ConfigureHangfire(app);
            PurpicsWeb.Hangfire.InitializeJobs();
        }
    }
}
