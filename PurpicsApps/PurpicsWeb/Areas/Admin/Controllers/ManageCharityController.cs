﻿using Hangfire;
using PurpicsWeb.Areas.Admin.ViewModels.Charity;
using PurpicsWeb.Services;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.IO;
using ImageResizer;
using System.Drawing;
using PurpicsWeb.Models;
using System.Configuration;
using PurpicsWeb.Controllers;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManageCharityController : Controller
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Admin/ManageCharity
        public ActionResult Index()
        {
            CharityViewModel foRequest = new CharityViewModel();
            foRequest.stSortColumn = "Id ASC";
            CharityModel loCharityListModel = getCharityList(foRequest);
            return View("~/Areas/Admin/Views/ManageCharity/ManageCharity.cshtml", loCharityListModel);
        }

        public CharityModel getCharityList(CharityViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CharityModel objCharityViewModel = new CharityModel();
            int liRecordCount = 0;
            Func<IQueryable<Charities>, IOrderedQueryable<Charities>> orderingFunc =
            query => query.OrderBy(x => x.Id);

            Expression<Func<Charities, bool>> expression = null;

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.UserStatusID)) && foRequest.UserStatusID != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.UserStatusID == foRequest.UserStatusID && x.CharityName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.UserStatusID)) && foRequest.UserStatusID != 0)
                expression = x => x.UserStatusID == foRequest.UserStatusID && x.IsDeleted == false;
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CharityName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else
                expression = x => x.IsDeleted == false;
            List<Expression<Func<Charities, Object>>> Charitiesincludes = new List<Expression<Func<Charities, object>>>();
            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "CharityName DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CharityName);
                        break;
                    case "CharityName ASC":
                        orderingFunc = q => q.OrderBy(s => s.CharityName);
                        break;
                    default:
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }
            }
            List<Charities> objCampaign = new List<Charities>();
            objCampaign = Repository<Charities>.GetEntityListForQuery(expression, orderingFunc, Charitiesincludes, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.UserStatusID)) && foRequest.UserStatusID != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.CharityName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.UserStatusID == foRequest.UserStatusID).Count();
            }
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.CharityName.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.UserStatusID)) && foRequest.UserStatusID != 0)
            {
                liRecordCount = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.UserStatusID == foRequest.UserStatusID).Count();
            }
            else
            {
                liRecordCount = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false).Count();
            }

            if (objCampaign.Count > 0)
            {
                if (objCampaign != null && objCampaign.Count > 0)
                {
                    foreach (var pac in objCampaign)
                    {
                        CharityViewModel objComp = new CharityViewModel
                        {
                            Id = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsDeleted = pac.IsDeleted,
                            ModifiedBy = pac.ModifiedBy,
                            ModifiedOn = pac.ModifiedOn,
                            CharityName = pac.CharityName,
                            UserStatusID = pac.UserStatusID
                        };

                        objCharityViewModel.loCharityList.Add(objComp);
                    }
                }
            }
            CharityModel loCompanyModel = new CharityModel();
            loCompanyModel.inRecordCount = liRecordCount;
            loCompanyModel.loCharityList = objCharityViewModel.loCharityList;
            loCompanyModel.inPageIndex = foRequest.inPageIndex;
            loCompanyModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCompanyModel;
        }
        public ActionResult searchCharity(CharityViewModel foSearchRequest)
        {
            CharityModel loCharityModel = getCharityList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCharity/_ManageCharity.cshtml", loCharityModel);
        }
        public ActionResult searchCharities(int inPageIndex, int inPageSize, string stSortColumn, string stSearch, int STATUS)
        {
            CharityViewModel foSearchRequest = new CharityViewModel();
            foSearchRequest.inPageIndex = inPageIndex;
            foSearchRequest.inPageSize = inPageSize;
            foSearchRequest.stSortColumn = stSortColumn;
            foSearchRequest.stSearch = stSearch.Replace("%20", " ");
            foSearchRequest.UserStatusID = STATUS;
            CharityModel loCharityModel = getCharityList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCharity/_ManageCharity.cshtml", loCharityModel);
        }
        public ActionResult CharityDetail(int fiCharityId)
        {
            Charities lowebstats = Repository<Charities>.GetEntityListForQuery(x => x.Id == fiCharityId).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();

            CharityViewModel loCompanyViewModel = new CharityViewModel();
            loCompanyViewModel.Id = fiCharityId;
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;
            loCompanyViewModel.UserStatusID = lowebstats.UserStatusID;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.Company = "-";
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser == null ? "" : loUser.UserName;
            loCompanyViewModel.LogoFileName = lowebstats.LogoFileName;
            loCompanyViewModel.BraintreeCustomerId = lowebstats.BraintreeCustomerId;

            return View(loCompanyViewModel);
        }

        public ActionResult approveCharity(int Id)
        {
            Charities lowebstats = Repository<Charities>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            AspNetUsers loLoginUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == User.Identity.Name).FirstOrDefault();
            CharityViewModel loCompanyViewModel = new CharityViewModel();
            string userid = loLoginUser.Id;
            try
            {
                AppSettings appSettings = Repository<AppSettings>.GetEntityListForQuery(x => (x.Name == "CharityRegistrationAmount" && x.IsDeleted == false)).FirstOrDefault();
                if (!string.IsNullOrEmpty(appSettings.Value) && Convert.ToDecimal(appSettings.Value) == 0)
                {
                    UpdateCharityAsApproved(Id);
                    loCompanyViewModel.UserStatusID = (int)CommonEnums.UserStatus.Approved;
                }
                else
                {
                    var lsResult = new BraintreePayments().PayCharityRegistrationAmount(lowebstats.Id, userid);
                    if (lsResult != null)
                    {
                        UpdateCharityAsApproved(Id);
                        loCompanyViewModel.UserStatusID = (int)CommonEnums.UserStatus.Approved;
                    }
                    else
                    {
                        TempData["ErrorMsg"] = "Something wrong in payment.";
                    }
                }
            }
            catch (Exception)
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
            }

            loCompanyViewModel.Id = Id;
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;

            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.Company = "-";
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser == null ? "" : loUser.UserName;
            loCompanyViewModel.LogoFileName = lowebstats.LogoFileName;

            return PartialView("~/Areas/Admin/Views/ManageCharity/_CharityDetail.cshtml", loCompanyViewModel);
        }

        public ActionResult approveCharityWithoutPayment(int Id)
        {
            Charities lowebstats = Repository<Charities>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            AspNetUsers loLoginUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == User.Identity.Name).FirstOrDefault();
            CharityViewModel loCompanyViewModel = new CharityViewModel();
            string userid = loLoginUser.Id;
            try
            {
                UpdateCharityAsApproved(Id);
                loCompanyViewModel.UserStatusID = (int)CommonEnums.UserStatus.Approved;
            }
            catch (Exception)
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
            }

            loCompanyViewModel.Id = Id;
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;

            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.Company = "-";
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser == null ? "" : loUser.UserName;
            loCompanyViewModel.LogoFileName = lowebstats.LogoFileName;

            return PartialView("~/Areas/Admin/Views/ManageCharity/_CharityDetail.cshtml", loCompanyViewModel);
        }

        private void UpdateCharityAsApproved(int Id)
        {
            Charities lowebstats = Repository<Charities>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            lowebstats.UserStatusID = (int)CommonEnums.UserStatus.Approved;
            Repository<Charities>.UpdateEntity(lowebstats, (entity) => { return entity.Id; });
            if (loUser != null)
            {
                Helper.CommonFunctions.sendCharityStatusMail(lowebstats.LegalEntityName, (int)CommonEnums.UserStatus.Approved, loUser.Email);
            }
            TempData["SuccessMsg"] = "Charity has been Approved.";
        }

        public ActionResult declineCharityStatus(int Id)
        {
            Charities lowebstats = Repository<Charities>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();

            try
            {
                lowebstats.UserStatusID = (int)CommonEnums.UserStatus.Denied;
                Repository<Charities>.UpdateEntity(lowebstats, (entity) => { return entity.Id; });
                if (loUser != null)
                {
                    Helper.CommonFunctions.sendCharityStatusMail(lowebstats.LegalEntityName, (int)CommonEnums.UserStatus.Denied, loUser.Email);
                }
                TempData["SuccessMsg"] = "Charity has been Declined.";
            }
            catch (Exception)
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
            }

            CharityViewModel loCompanyViewModel = new CharityViewModel();
            loCompanyViewModel.Id = Id;
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;
            loCompanyViewModel.UserStatusID = lowebstats.UserStatusID;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.Company = "-";
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser == null ? "" : loUser.UserName;
            loCompanyViewModel.LogoFileName = lowebstats.LogoFileName;

            //Helper.CommonFunctions.sendCharityStatusMail(lowebstats.LegalEntityName, 3, loUser.Email);

            return PartialView("~/Areas/Admin/Views/ManageCharity/_CharityDetail.cshtml", loCompanyViewModel);
        }

        #region Manage Charity Cateogy
        // GET: Admin/ManagePackage
        public ActionResult ManageCharityCategories()
        {
            CharityCategoryViewModel foRequest = new CharityCategoryViewModel();
            foRequest.stSortColumn = "Name ASC";
            CharityCategoriesViewModel model = getCharityCategoryList(foRequest);
            return View("~/Areas/Admin/Views/ManageCharity/ManageCharityCategories.cshtml", model);
        }

        public CharityCategoriesViewModel getCharityCategoryList(CharityCategoryViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CharityCategoriesViewModel model = new CharityCategoriesViewModel();
            int liRecordCount = 0;
            Func<IQueryable<CharityCategories>, IOrderedQueryable<CharityCategories>> orderingFunc =
            query => query.OrderBy(x => x.Name);
            Expression<Func<CharityCategories, bool>> expression = null;

            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                foRequest.stSearch = HttpUtility.UrlDecode(foRequest.stSearch);
                expression = x => x.Name.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            }
            else
                expression = x => x.IsDeleted == false;

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Name DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Name);
                        break;
                    case "Name ASC":
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;
                    default:  // Name ascending 
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;
                }
            }

            List<CharityCategories> charityCategories = new List<CharityCategories>();
            charityCategories = Repository<CharityCategories>.GetEntityListForQuery(expression, orderingFunc, null, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();

            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<PurpicsWeb_DL.Entities.CharityCategories>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.Name.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            }
            else
            {
                liRecordCount = Repository<PurpicsWeb_DL.Entities.CharityCategories>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false).Count();
            }

            if (charityCategories.Count > 0)
            {
                if (charityCategories != null && charityCategories.Count > 0)
                {
                    foreach (var pac in charityCategories)
                    {
                        CharityCategoryViewModel categorymodel = new CharityCategoryViewModel
                        {
                            ID = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsActive = pac.IsActive,
                            IsDeleted = pac.IsDeleted,
                            Name = pac.Name
                        };

                        model.loCharityCategoryList.Add(categorymodel);
                    }
                }
            }
            CharityCategoriesViewModel loPackageModel = new CharityCategoriesViewModel();
            loPackageModel.inRecordCount = liRecordCount;
            loPackageModel.inPageIndex = foRequest.inPageIndex;
            loPackageModel.loCharityCategoryList = model.loCharityCategoryList;
            loPackageModel.Pager = new Services.Pager(liRecordCount, foRequest.inPageIndex);
            return loPackageModel;
        }

        public ActionResult searchCharityCategory(CharityCategoryViewModel foSearchRequest)
        {
            CharityCategoriesViewModel charityCategories = getCharityCategoryList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCharity/_ManageCharityCategories.cshtml", charityCategories);
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddEditCharityCategory(PurpicsWeb.Areas.Admin.ViewModels.Charity.CharityCategoryViewModel loPackageViewModel)
        {
            if (loPackageViewModel != null)
            {
                if (loPackageViewModel.ID == 0)
                {
                    CharityCategories charityCategories = new CharityCategories();
                    charityCategories.Name = loPackageViewModel.Name;
                    charityCategories.IsActive = loPackageViewModel.IsActive;
                    charityCategories.CreatedBy = User.Identity.GetUserId();
                    charityCategories.CreatedOn = DateTime.Now;
                    Repository<CharityCategories>.InsertEntity(charityCategories, entity => { return entity.Id; });
                    TempData["SuccessMsg"] = "Category has been added successfully";
                }
                else
                {
                    CharityCategories charityCategories = Repository<CharityCategories>.GetEntityListForQuery(x => x.Id == loPackageViewModel.ID).FirstOrDefault();

                    charityCategories.Name = loPackageViewModel.Name;
                    charityCategories.IsActive = loPackageViewModel.IsActive;
                    charityCategories.ModifiedBy = User.Identity.GetUserId();
                    charityCategories.ModifiedOn = DateTime.Now;
                    Repository<CharityCategories>.UpdateEntity(charityCategories, (entity) => { return entity.Id; });
                    TempData["SuccessMsg"] = "Category has been updated successfully";
                }
            }
            return RedirectToAction("ManageCharityCategories", "ManageCharity");
        }

        [HttpGet]
        public ActionResult AddEditCharityCategory(int? id)
        {
            CharityCategoryViewModel objPackageViewModel = new CharityCategoryViewModel();

            if (id != null && id > 0)
            {
                CharityCategories charityCategories = Repository<CharityCategories>.GetEntityListForQuery(x => x.Id == id).FirstOrDefault();

                objPackageViewModel = new CharityCategoryViewModel
                {
                    ID = charityCategories.Id,
                    Name = charityCategories.Name,
                };
            }

            return View("~/Areas/Admin/Views/ManageCharity/AddEditCharityCategory.cshtml", objPackageViewModel);
        }

        public ActionResult DeleteCharityCategory(CharityCategoryViewModel foRequest)
        {
            int liSuccess = 0;
            string lsMessage = string.Empty;

            Expression<Func<Campaigns, bool>> expression = x => x.IsDeleted == false && x.Charities.CategoryID == foRequest.ID;

            //Include 
            List<Expression<Func<Campaigns, Object>>> Campaignsincludes = new List<Expression<Func<Campaigns, object>>>();
            Expression<Func<Campaigns, object>> campaignCharity = (campaignCharityName) => campaignCharityName.Charities;
            Campaignsincludes.Add(campaignCharity);

            int cntCharity = Repository<Campaigns>.GetEntityListForQuery(expression, null, Campaignsincludes, null, null).Count();
            List<Campaigns> obj = Repository<Campaigns>.GetEntityListForQuery(expression, null, Campaignsincludes, null, null).ToList();

            //Campaigns loCampaign = Repository<Campaigns>.GetEntityListForQuery(x => x.PackageID == foRequest.ID && x.CampaignStatusID == 2 && x.IsDeleted == false && x.IsActive == true).FirstOrDefault();
            if (cntCharity > 0)
            {
                TempData["ErrorMsg"] = "You can not delete this category as it is associated with campaigns.";
            }
            else if (foRequest.ID != null && foRequest.ID > 0)
            {
                CharityCategories charityCategories = Repository<CharityCategories>.GetEntityListForQuery(x => x.Id == foRequest.ID).FirstOrDefault();
                charityCategories.ModifiedBy = User.Identity.GetUserId();
                charityCategories.ModifiedOn = DateTime.Now;
                charityCategories.IsDeleted = true;
                Repository<CharityCategories>.UpdateEntity(charityCategories, (entity) => { return entity.Id; });
                TempData["SuccessMsg"] = "Category has been deleted successfully";
            }
            return this.Json(new CharityCategoryViewModel { ID = liSuccess });
        }
        #endregion

        #region Add Edit Charity

        [HttpGet]
        public ActionResult AddEditCharity(int? id)
        {
            CharityRegistration model = new CharityRegistration();

            if (id != null && id > 0)
            {
                Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == id).FirstOrDefault();
                model.CharityId = charity.Id;
                model.LegalEntityName = charity.LegalEntityName;
                model.Number501_C3 = charity.C501C3Number;
                model.AboutCharity = charity.About;

                model.CharityName = charity.CharityName;
                model.Address = charity.Address;
                model.City = charity.City;
                model.State = charity.State;
                model.Zip = charity.Zip;
                model.ContactName = charity.ContactPerson;
                model.CategoryID = charity.CategoryID;

                string[] Phone = charity.ContactPhone.Split('-');
                model.PhoneNumber1 = Phone[0];
                model.PhoneNumber2 = Phone[1];
                model.PhoneNumber3 = Phone[2];
                model.AspNetUserId = charity.AspNetUserID;
                model.IsActive = charity.IsActive;

                AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == charity.AspNetUserID).FirstOrDefault();
                model.EmailAddress = user.Email;
                model.Password = "Dummy@123";

                List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
                CharityCategories objcategory = new CharityCategories();
                SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
                model.Categories = objmodeldata.ToList();

                model.States = CommonFunctions.GetStateList();

                if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"] + charity.LogoFileName)))
                {
                    FileInfo file = new FileInfo(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"] + charity.LogoFileName));
                    model.CharityLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"].Remove(0, 1) + charity.LogoFileName + "?" + file.LastAccessTime.ToString("ddMMyyHHmmss");
                }
                else
                {
                    model.CharityLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"].Remove(0, 1) + "NoPhoto.jpg";
                }
            }
            else
            {
                model.CharityId = 0;

                List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
                CharityCategories objcategory = new CharityCategories();
                SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
                model.Categories = objmodeldata.ToList();

                model.States = CommonFunctions.GetStateList();
            }

            return View("~/Areas/Admin/Views/ManageCharity/AddEditCharity.cshtml", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> AddEditCharity(CharityRegistration objCharityRegistration)
        {
            try
            {
                string userId = User.Identity.GetUserId();

                List<AspNetUsers> users = new List<AspNetUsers>();
                if (objCharityRegistration.CharityId > 0)
                {
                    var charity = Repository<Charities>.GetEntityListForQuery(r => r.Id == objCharityRegistration.CharityId).FirstOrDefault();
                    users = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == objCharityRegistration.EmailAddress.Trim() && x.Id != charity.AspNetUserID).ToList();

                }
                else
                {
                    users = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == objCharityRegistration.EmailAddress.Trim()).ToList();
                }

                if (users.Count > 0)
                {
                    TempData["Message"] = "The email address " + objCharityRegistration.EmailAddress + " is already registered.";
                    List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
                    CharityCategories objcategory = new CharityCategories();
                    SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
                    objCharityRegistration.Categories = objmodeldata.ToList();
                    objCharityRegistration.States = CommonFunctions.GetStateList();
                    return View("AddEditCharity", objCharityRegistration);
                }
                if (objCharityRegistration.CharityId > 0)
                {
                    var charity = Repository<Charities>.GetEntityListForQuery(r => r.Id == objCharityRegistration.CharityId).FirstOrDefault();
                    charity.LegalEntityName = objCharityRegistration.LegalEntityName;
                    charity.C501C3Number = objCharityRegistration.Number501_C3;
                    charity.CharityName = objCharityRegistration.CharityName;
                    charity.Address = objCharityRegistration.Address;
                    charity.City = objCharityRegistration.City;
                    charity.State = objCharityRegistration.State;
                    charity.Zip = objCharityRegistration.Zip;
                    charity.ContactPerson = objCharityRegistration.ContactName;
                    charity.ContactPhone = objCharityRegistration.PhoneNumber1 + "-" + objCharityRegistration.PhoneNumber2 + "-" + objCharityRegistration.PhoneNumber3;
                    charity.About = objCharityRegistration.AboutCharity;
                    charity.IsActive = objCharityRegistration.IsActive;
                    charity.CategoryID = objCharityRegistration.CategoryID;

                    charity.ModifiedOn = DateTime.Now;
                    charity.ModifiedBy = userId;
                    await Repository<Charities>.UpdateEntity(charity, entity => { return entity.Id; });

                    //Update Logo
                    if (objCharityRegistration.CharityLogo != null)
                    {
                        string lsUploadPath = Server.MapPath(ConfigurationManager.AppSettings["CharityImagePath"]);
                        string savepath = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["CharityImagePath"]), charity.Id.ToString());
                        if (!string.IsNullOrEmpty(charity.LogoFileName))
                        {
                            if (System.IO.File.Exists(Path.Combine(lsUploadPath, charity.LogoFileName)))
                            {
                                System.IO.File.Delete(Path.Combine(lsUploadPath, charity.LogoFileName));
                            }
                        }
                        charity.LogoFileName = UploadCharityLogo(objCharityRegistration.CharityLogo, charity.Id, savepath);
                        await Repository<Charities>.UpdateEntity(charity, entity => { return entity.Id; });
                    }
                    //Update Email ID
                    var user = await UserManager.FindByIdAsync(charity.AspNetUserID);
                    if (user.Email.Trim() != objCharityRegistration.EmailAddress.Trim())
                    {
                        user.Email = objCharityRegistration.EmailAddress;
                        user.UserName = objCharityRegistration.EmailAddress;
                        var result = await UserManager.UpdateAsync(user);
                    }

                    //Update Password
                    ApplicationDbContext context = new ApplicationDbContext();
                    UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
                    String hashedNewPassword = UserManager.PasswordHasher.HashPassword(objCharityRegistration.Password);
                    ApplicationUser cUser = await store.FindByIdAsync(user.Id);
                    await store.SetPasswordHashAsync(cUser, hashedNewPassword);
                    await store.UpdateAsync(cUser);
                    return RedirectToAction("Index", "ManageCharity");
                }
                else
                {
                    // Create Entry in ASPNetUsers table
                    var user = new ApplicationUser() { UserName = objCharityRegistration.EmailAddress, Email = objCharityRegistration.EmailAddress, PhoneNumber = objCharityRegistration.PhoneNumber1 + "-" + objCharityRegistration.PhoneNumber2 + "-" + objCharityRegistration.PhoneNumber3 };
                    var userresult = await UserManager.CreateAsync(user, objCharityRegistration.Password);
                    if (userresult.Succeeded)
                    {
                        string strChariyRole = CommonConstants.RoleNames.Charity;
                        await UserManager.AddToRoleAsync(user.Id, strChariyRole);

                        Charities charities = new Charities();
                        charities.LegalEntityName = objCharityRegistration.LegalEntityName;
                        charities.C501C3Number = objCharityRegistration.Number501_C3;
                        charities.CharityName = objCharityRegistration.CharityName;
                        charities.Address = objCharityRegistration.Address;
                        charities.City = objCharityRegistration.City;
                        charities.State = objCharityRegistration.State;
                        charities.Zip = objCharityRegistration.Zip;
                        charities.ContactPerson = objCharityRegistration.ContactName;
                        charities.ContactPhone = objCharityRegistration.PhoneNumber1 + "-" + objCharityRegistration.PhoneNumber2 + "-" + objCharityRegistration.PhoneNumber3;
                        charities.About = objCharityRegistration.AboutCharity;
                        charities.AspNetUserID = user.Id;
                        charities.IsActive = true;
                        charities.CategoryID = objCharityRegistration.CategoryID;

                        charities.CreatedOn = DateTime.Now;
                        charities.CreatedBy = user.Id;
                        charities.UserStatusID = (int)CommonEnums.UserStatus.Approved;
                        await Repository<Charities>.InsertEntity(charities, entity => { return entity.Id; });

                        string lsUploadPath = Server.MapPath(ConfigurationManager.AppSettings["CharityImagePath"]);
                        int charityid = charities.Id;

                        string savepath = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["CharityImagePath"]), charityid.ToString());

                        if (!string.IsNullOrEmpty(charities.LogoFileName))
                        {
                            if (System.IO.File.Exists(Path.Combine(lsUploadPath, charities.LogoFileName)))
                            {
                                System.IO.File.Delete(Path.Combine(lsUploadPath, charities.LogoFileName));
                            }
                        }

                        var charity = Repository<Charities>.GetEntityListForQuery(r => r.Id == charityid).FirstOrDefault();
                        charity.LogoFileName = UploadCharityLogo(objCharityRegistration.CharityLogo, charityid, savepath);
                        await Repository<Charities>.UpdateEntity(charity, entity => { return entity.Id; });

                        CharityCategories category = Repository<CharityCategories>.GetEntityListForQuery(x => x.Id == objCharityRegistration.CategoryID).FirstOrDefault();
                        string domainURL = ConfigurationManager.AppSettings["domainURL"] ?? "http://www.Purpics.com/";

                        string objRegistrationEmailBody = new SystemEmailController().CharityRegistration(objCharityRegistration, charity.Id);

                        string mailTo = Utility.ExecuteScalar("sp_GetAllAdminEmailId").ToString();
                        try
                        {
                            BackgroundJob.Enqueue(() => EmailHelper.SendEmail("New Charity Registration", objRegistrationEmailBody, mailTo));
                        }
                        catch { }
                        return RedirectToAction("Index", "ManageCharity");
                    }
                    else
                    {
                        TempData["Message"] = "The email address " + objCharityRegistration.EmailAddress + " is already registered.";
                        return View(objCharityRegistration);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return View();
        }


        public string UploadCharityLogo(HttpPostedFileBase image, int charityId, string savePath)
        {
            string newfileName = "";
            if (image != null)
            {
                Dictionary<string, string> loVersions = new Dictionary<string, string>();
                loVersions.Add("", "maxwidth=250&maxheight=300&AutoRotate=true");

                //Generate each version
                foreach (var loKey in loVersions.Keys)
                {
                    Image img = Image.FromStream(image.InputStream);
                    var ExIfImage = img.PropertyItems.Where(x => x.Id == 0x0112).FirstOrDefault();

                    if (ExIfImage != null)
                    {
                        int orientationValue = img.GetPropertyItem(0x0112).Value[0];
                        RotateFlipType rotateFlipType = CommonFunctions.GetOrientationToFlipType(orientationValue);
                        img.RotateFlip(rotateFlipType);
                    }

                    ImageBuilder.Current.Build(
                             new ImageJob(
                                 img,
                                 savePath,
                                 new Instructions(loVersions[loKey]),
                                 false,
                                 true));
                    //                    string lsFileExt = Path.GetExtension(image.FileName);
                    string newext = "";
                    if (ImageFormat.Jpeg.Equals(img.RawFormat))
                    {
                        newext = ".JPG";
                    }
                    else if (ImageFormat.Png.Equals(img.RawFormat))
                    {
                        newext = ".PNG";
                    }
                    else if (ImageFormat.Gif.Equals(img.RawFormat))
                    {
                        newext = ".GIF";
                    }
                    newfileName = charityId.ToString() + newext;
                    return newfileName;
                }

            }
            return "";
        }
        #endregion
    }

}