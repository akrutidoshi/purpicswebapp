﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Areas.Admin.ViewModels.AppSetting;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AppSettingsController : Controller
    {
        // GET: Admin/AppSettings
        public ActionResult Index()
        {
            AppSettigsViewModel objAppSettigsViewModel = new AppSettigsViewModel();

            AppSettings loAppSettingsCampaignPer = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignPer").FirstOrDefault();
            AppSettings loAppSettingsCampaignPointAwarded = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignPointAwarded").FirstOrDefault();
            AppSettings loAppSettingsCampaignUploadPoint = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignUploadPoints").FirstOrDefault();
            AppSettings loAppSettingsRedemptionPoints = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedemptionPoints").FirstOrDefault();
            AppSettings loAppSettingsCharityRegAmount = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CharityRegistrationAmount").FirstOrDefault();
            AppSettings loCharitySubscriptionNotificationDays = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CharitySubscriptionNotificationDays").FirstOrDefault();
            //AppSettings loRedeemGiftCardMinimumPoints = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedeemGiftCardMinimumPoints").FirstOrDefault();

            objAppSettigsViewModel.CampaignPer = loAppSettingsCampaignPer != null ? loAppSettingsCampaignPer.Value : string.Empty;
            objAppSettigsViewModel.CampaignPointAwarded = loAppSettingsCampaignPointAwarded != null ? loAppSettingsCampaignPointAwarded.Value : string.Empty;
            objAppSettigsViewModel.RedemptionPoints = loAppSettingsRedemptionPoints != null ? loAppSettingsRedemptionPoints.Value : string.Empty;
            objAppSettigsViewModel.CharityRegistrationAmount = loAppSettingsCharityRegAmount != null ? loAppSettingsCharityRegAmount.Value : string.Empty;
            objAppSettigsViewModel.CampaignUploadPointAwarded = loAppSettingsCampaignUploadPoint != null ? loAppSettingsCampaignUploadPoint.Value : string.Empty;
            objAppSettigsViewModel.CharitySubscriptionNotificationDays = loCharitySubscriptionNotificationDays != null ? loCharitySubscriptionNotificationDays.Value : string.Empty;

            //objAppSettigsViewModel.RedeemGiftCardMinimumPoints = loRedeemGiftCardMinimumPoints != null ? loRedeemGiftCardMinimumPoints.Value : string.Empty;


            return View("~/Areas/Admin/Views/AppSettings/AppSetting.cshtml", objAppSettigsViewModel);
        }
        //public ActionResult AppSettings()
        //{
        //    AppSettingViewModel foRequest = new AppSettingViewModel();
        //    foRequest.stSortColumn = "Name ASC";
        //    AppSettingModel loPatientListModel = getUserList(foRequest);
        //    return View("~/Areas/Admin/Views/AppSettings/AppSettings.cshtml", loPatientListModel);
        //}

        public AppSettingModel getUserList(AppSettingViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            AppSettingModel objPackagesViewModel = new AppSettingModel();
            int liRecordCount = 0;
            List<AppSettings> objPackages = Repository<AppSettings>.GetEntityListForQuery(null).OrderBy(x => x.Name).Where(x => x.IsDeleted == false).ToList();
            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                objPackages = objPackages.Where(s => s.Name.ToLower().Contains(foRequest.stSearch.ToLower())).ToList();
            }
            liRecordCount = objPackages.Count();
            if (objPackages.Count > 0)
            {
                if (!string.IsNullOrEmpty(foRequest.stSortColumn))
                {
                    switch (foRequest.stSortColumn)
                    {
                        case "Name DESC":
                            objPackages = objPackages.OrderByDescending(s => s.Name).ToList();
                            break;
                        case "Name ASC":
                            objPackages = objPackages.OrderBy(s => s.Name).ToList();
                            break;
                        case "Value DESC":
                            objPackages = objPackages.OrderByDescending(s => s.Value).ToList();
                            break;
                        case "Value ASC":
                            objPackages = objPackages.OrderByDescending(s => s.Value).ToList();
                            break;
                    }
                }
                if (foRequest.inPageIndex > 0)
                    objPackages = objPackages.Skip((foRequest.inPageIndex - 1) * foRequest.inPageSize).Take(foRequest.inPageSize).ToList();
                if (objPackages != null && objPackages.Count > 0)
                {
                    foreach (var pac in objPackages)
                    {
                        AppSettingViewModel objPackage = new AppSettingViewModel
                        {
                            Id = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsDeleted = pac.IsDeleted,
                            ModifiedBy = pac.ModifiedBy,
                            ModifiedOn = pac.ModifiedOn,
                            Name = pac.Name,
                            Value = pac.Value
                        };

                        objPackagesViewModel.loAppSettingList.Add(objPackage);
                    }
                }
            }
            AppSettingModel loPackageModel = new AppSettingModel();
            loPackageModel.inRecordCount = liRecordCount;
            loPackageModel.loAppSettingList = objPackagesViewModel.loAppSettingList;
            return loPackageModel;
        }
        public ActionResult searchPackages(AppSettingViewModel foSearchRequest)
        {
            AppSettingModel loUserModel = getUserList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/AppSettings/_AppSettings.cshtml", loUserModel);
        }


        [HttpPost]
        public ActionResult AddEditAppSettings(AppSettingViewModel foRequest)
        {
            if (foRequest != null)
            {
                if (foRequest.Id == 0)
                {
                    AppSettings loAppSetting = new AppSettings();
                    loAppSetting.Name = foRequest.Name;
                    loAppSetting.Value = foRequest.Value;
                    loAppSetting.IsDeleted = false;
                    loAppSetting.CreatedBy = "1";
                    loAppSetting.CreatedOn = DateTime.Now;
                    Repository<AppSettings>.InsertEntity(loAppSetting, entity => { return entity.Id; });
                }
                else
                {
                    AppSettings loAppSettings = Repository<AppSettings>.GetEntityListForQuery(x => x.Id == foRequest.Id).FirstOrDefault();

                    loAppSettings.Name = foRequest.Name;
                    loAppSettings.Value = foRequest.Value;
                    loAppSettings.ModifiedBy = "1";
                    loAppSettings.ModifiedOn = DateTime.Now;

                    Repository<AppSettings>.UpdateEntity(loAppSettings, (entity) => { return entity.Id; });
                }
            }
            return RedirectToAction("AppSettings", "AppSettings");
        }

        [HttpGet]
        public ActionResult AddEditAppSettings(int? id)
        {
            AppSettingViewModel objAppSettingViewModel = new AppSettingViewModel();

            if (id != null && id > 0)
            {
                AppSettings objPackages = Repository<AppSettings>.GetEntityListForQuery(x => x.Id == id).FirstOrDefault();

                objAppSettingViewModel = new AppSettingViewModel
                {
                    Id = objPackages.Id,
                    CreatedBy = objPackages.CreatedBy,
                    CreatedOn = objPackages.CreatedOn,
                    IsDeleted = objPackages.IsDeleted,
                    ModifiedBy = objPackages.ModifiedBy,
                    ModifiedOn = objPackages.ModifiedOn,
                    Name = objPackages.Name,
                    Value = objPackages.Value
                };
            }

            return View("~/Areas/Admin/Views/AppSettings/AddEditAppSettings.cshtml", objAppSettingViewModel);
        }

        public ActionResult DeletePackage(AppSettingViewModel foRequest)
        {
            int liSuccess = 0;
            string lsMessage = string.Empty;
            if (foRequest.Id != null && foRequest.Id > 0)
            {
                AppSettings objPackages = Repository<AppSettings>.GetEntityListForQuery(x => x.Id == foRequest.Id).FirstOrDefault();
                objPackages.ModifiedBy = "1";
                objPackages.ModifiedOn = DateTime.Now;
                objPackages.IsDeleted = true;
                Repository<AppSettings>.UpdateEntity(objPackages, (entity) => { return entity.Id; });

            }
            TempData["AlertMessage"] = lsMessage;
            //PackagesViewModel loUserModel = getUserList(foRequest);
            return this.Json(new AppSettingViewModel { Id = liSuccess });
        }


        public ActionResult AppSetting()
        {
            AppSettigsViewModel objAppSettigsViewModel = new AppSettigsViewModel();

            AppSettings loAppSettingsCampaignPer = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignPer").FirstOrDefault();
            AppSettings loAppSettingsCampaignPointAwarded = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignPointAwarded").FirstOrDefault();
            AppSettings loAppSettingsRedemptionPoints = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedemptionPoints").FirstOrDefault();
            AppSettings loAppSettingsCharityRegAmount = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CharityRegistrationAmount").FirstOrDefault();
            AppSettings loCharitySubscriptionNotificationDays = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CharitySubscriptionNotificationDays").FirstOrDefault();
            //AppSettings loRedeemGiftCardMinimumPoints = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedeemGiftCardMinimumPoints").FirstOrDefault();

            objAppSettigsViewModel.CampaignPer = loAppSettingsCampaignPer != null ? loAppSettingsCampaignPer.Value : string.Empty;
            objAppSettigsViewModel.CampaignPointAwarded = loAppSettingsCampaignPointAwarded != null ? loAppSettingsCampaignPointAwarded.Value : string.Empty;
            objAppSettigsViewModel.RedemptionPoints = loAppSettingsRedemptionPoints != null ? loAppSettingsRedemptionPoints.Value : string.Empty;
            objAppSettigsViewModel.CharityRegistrationAmount = loAppSettingsCharityRegAmount != null ? loAppSettingsCharityRegAmount.Value : string.Empty;
            objAppSettigsViewModel.CharitySubscriptionNotificationDays = loCharitySubscriptionNotificationDays != null ? loCharitySubscriptionNotificationDays.Value : string.Empty;
            //objAppSettigsViewModel.RedeemGiftCardMinimumPoints = loRedeemGiftCardMinimumPoints != null ? loRedeemGiftCardMinimumPoints.Value : string.Empty;

            return View("~/Areas/Admin/Views/AppSettings/AppSetting.cshtml", objAppSettigsViewModel);
        }

        [HttpPost]
        public ActionResult UpdateAppSettings(AppSettigsViewModel objAppSettingsViewModel)
        {
            try
            {
                AppSettings loAppSettingsCampaignPer = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignPer").FirstOrDefault();

                if (loAppSettingsCampaignPer != null)
                {
                    loAppSettingsCampaignPer.Value = objAppSettingsViewModel.CampaignPer;
                    loAppSettingsCampaignPer.ModifiedBy = User.Identity.Name;
                    loAppSettingsCampaignPer.ModifiedOn = DateTime.Now;

                    Repository<AppSettings>.UpdateEntity(loAppSettingsCampaignPer, (entity) => { return entity.Id; });
                }
                else
                {
                    loAppSettingsCampaignPer = new AppSettings
                    {
                        Name = "CampaignPer",
                        Value = objAppSettingsViewModel.CampaignPer,
                        CreatedBy = User.Identity.Name,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    Repository<AppSettings>.InsertEntity(loAppSettingsCampaignPer, (entity) => { return entity.Id; });
                }

                AppSettings loAppSettingsCampaignPointAwarded = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignPointAwarded").FirstOrDefault();
                if (loAppSettingsCampaignPointAwarded != null)
                {
                    loAppSettingsCampaignPointAwarded.Value = objAppSettingsViewModel.CampaignPointAwarded;
                    loAppSettingsCampaignPointAwarded.ModifiedBy = User.Identity.Name;
                    loAppSettingsCampaignPointAwarded.ModifiedOn = DateTime.Now;
                    Repository<AppSettings>.UpdateEntity(loAppSettingsCampaignPointAwarded, (entity) => { return entity.Id; });
                }
                else
                {
                    loAppSettingsCampaignPointAwarded = new AppSettings
                    {
                        Name = "CampaignPointAwarded",
                        Value = objAppSettingsViewModel.CampaignPointAwarded,
                        CreatedBy = User.Identity.Name,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    Repository<AppSettings>.InsertEntity(loAppSettingsCampaignPointAwarded, (entity) => { return entity.Id; });
                }

                AppSettings loAppSettingsRedemptionPoints = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedemptionPoints").FirstOrDefault();
                if (loAppSettingsRedemptionPoints != null)
                {
                    loAppSettingsRedemptionPoints.Value = objAppSettingsViewModel.RedemptionPoints;
                    loAppSettingsRedemptionPoints.ModifiedBy = User.Identity.Name;
                    loAppSettingsRedemptionPoints.ModifiedOn = DateTime.Now;
                    Repository<AppSettings>.UpdateEntity(loAppSettingsRedemptionPoints, (entity) => { return entity.Id; });
                }
                else
                {
                    loAppSettingsRedemptionPoints = new AppSettings
                    {
                        Name = "RedemptionPoints",
                        Value = objAppSettingsViewModel.RedemptionPoints,
                        CreatedBy = User.Identity.Name,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    Repository<AppSettings>.InsertEntity(loAppSettingsRedemptionPoints, (entity) => { return entity.Id; });
                }

                AppSettings loAppSettingsCharityRegAmount = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CharityRegistrationAmount").FirstOrDefault();
                if (loAppSettingsCharityRegAmount != null)
                {
                    loAppSettingsCharityRegAmount.Value = objAppSettingsViewModel.CharityRegistrationAmount;
                    loAppSettingsCharityRegAmount.ModifiedBy = User.Identity.Name;
                    loAppSettingsCharityRegAmount.ModifiedOn = DateTime.Now;
                    Repository<AppSettings>.UpdateEntity(loAppSettingsCharityRegAmount, (entity) => { return entity.Id; });
                }
                else
                {
                    loAppSettingsCharityRegAmount = new AppSettings
                    {
                        Name = "CharityRegistrationAmount",
                        Value = objAppSettingsViewModel.CharityRegistrationAmount,
                        CreatedBy = User.Identity.Name,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    Repository<AppSettings>.InsertEntity(loAppSettingsCharityRegAmount, (entity) => { return entity.Id; });
                }
                AppSettings loAppSettingsCampaignUploadPoint = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignUploadPoints").FirstOrDefault();
                if (loAppSettingsCampaignUploadPoint != null)
                {
                    loAppSettingsCampaignUploadPoint.Value = objAppSettingsViewModel.CampaignUploadPointAwarded;
                    loAppSettingsCampaignUploadPoint.ModifiedBy = User.Identity.Name;
                    loAppSettingsCampaignUploadPoint.ModifiedOn = DateTime.Now;
                    Repository<AppSettings>.UpdateEntity(loAppSettingsCampaignUploadPoint, (entity) => { return entity.Id; });
                }
                else
                {
                    loAppSettingsCampaignUploadPoint = new AppSettings
                    {
                        Name = "CampaignUploadPoints",
                        Value = objAppSettingsViewModel.CampaignUploadPointAwarded,
                        CreatedBy = User.Identity.Name,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    Repository<AppSettings>.InsertEntity(loAppSettingsCharityRegAmount, (entity) => { return entity.Id; });
                }

                AppSettings loCharitySubscriptionNotificationDays = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CharitySubscriptionNotificationDays").FirstOrDefault();
                if (loCharitySubscriptionNotificationDays != null)
                {
                    loCharitySubscriptionNotificationDays.Value = objAppSettingsViewModel.CharitySubscriptionNotificationDays;
                    loCharitySubscriptionNotificationDays.ModifiedBy = User.Identity.Name;
                    loCharitySubscriptionNotificationDays.ModifiedOn = DateTime.Now;
                    Repository<AppSettings>.UpdateEntity(loCharitySubscriptionNotificationDays, (entity) => { return entity.Id; });
                }
                else
                {
                    loCharitySubscriptionNotificationDays = new AppSettings
                    {
                        Name = "CharitySubscriptionNotificationDays",
                        Value = objAppSettingsViewModel.CharitySubscriptionNotificationDays,
                        CreatedBy = User.Identity.Name,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                    };

                    Repository<AppSettings>.InsertEntity(loAppSettingsCharityRegAmount, (entity) => { return entity.Id; });
                }

                //AppSettings loRedeemGiftCardMinimumPoints = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedeemGiftCardMinimumPoints").FirstOrDefault();
                //if (loRedeemGiftCardMinimumPoints != null)
                //{
                //    loRedeemGiftCardMinimumPoints.Value = objAppSettingsViewModel.RedeemGiftCardMinimumPoints;
                //    loRedeemGiftCardMinimumPoints.ModifiedBy = User.Identity.Name;
                //    loRedeemGiftCardMinimumPoints.ModifiedOn = DateTime.Now;
                //    Repository<AppSettings>.UpdateEntity(loRedeemGiftCardMinimumPoints, (entity) => { return entity.Id; });
                //}
                //else
                //{
                //    loRedeemGiftCardMinimumPoints = new AppSettings
                //    {
                //        Name = "RedeemGiftCardMinimumPoints",
                //        Value = objAppSettingsViewModel.RedeemGiftCardMinimumPoints,
                //        CreatedBy = User.Identity.Name,
                //        CreatedOn = DateTime.Now,
                //        IsDeleted = false,
                //    };

                //    Repository<AppSettings>.InsertEntity(loRedeemGiftCardMinimumPoints, (entity) => { return entity.Id; });
                //}

                TempData["SuccessMsg"] = "App Setting Updated Successfully";
            }
            catch (Exception)
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
            }

            //return View("~/Areas/Admin/Views/AppSettings/AppSetting.cshtml", objAppSettingsViewModel);

            return RedirectToAction("Index", "AppSettings", new { area = "Admin" });
        }
    }
}