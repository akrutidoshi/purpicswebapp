﻿using Hangfire;
using PurpicsWeb.Areas.Admin.ViewModels.GiftCard;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManageGiftCardsController : Controller
    {
        // GET: Admin/ManageGiftCards
        public ActionResult Index()
        {
            GiftCardViewModel foRequest = new GiftCardViewModel();
            foRequest.stSortColumn = "Name ASC";
            GiftCardModel loGiftCardListModel = getPackageList(foRequest);
            return View("~/Areas/Admin/Views/ManageGiftCards/ManageGiftCards.cshtml", loGiftCardListModel);
        }
        //public ActionResult ManageGiftCards()
        //{
        //    return View();
        //}
        public GiftCardModel getPackageList(GiftCardViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            GiftCardModel objPackagesViewModel = new GiftCardModel();
            int liRecordCount = 0;
            Func<IQueryable<GiftCards>, IOrderedQueryable<GiftCards>> orderingFunc =
            query => query.OrderBy(x => x.Name);
            Expression<Func<GiftCards, bool>> expression = null;
            
            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                foRequest.stSearch = HttpUtility.UrlDecode(foRequest.stSearch);
                expression = x => x.Name.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            }
            else
                expression = x => x.IsDeleted == false;
            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Name DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Name);
                        break;
                    case "Name ASC":
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;
                    case "PointCost DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.PointCost);
                        break;
                    case "PointCost ASC":
                        orderingFunc = q => q.OrderBy(s => s.PointCost);
                        break;
                    case "Zipcode DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Zipcode);
                        break;
                    case "Zipcode ASC":
                        orderingFunc = q => q.OrderBy(s => s.Zipcode);
                        break;
                    case "Radius DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Radius);
                        break;
                    case "Radius ASC":
                        orderingFunc = q => q.OrderBy(s => s.Radius);
                        break;
                    default:  // Name ascending 
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;
                }
            }

            List<GiftCards> objPackages = Repository<GiftCards>.GetEntityListForQuery(expression, orderingFunc, null, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();
            
            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<PurpicsWeb_DL.Entities.GiftCards>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.Name.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            }
            else
            {
                liRecordCount = Repository<PurpicsWeb_DL.Entities.GiftCards>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false).Count();
            }

            if (objPackages.Count > 0)
            {
                if (objPackages != null && objPackages.Count > 0)
                {
                    foreach (var pac in objPackages)
                    {
                        GiftCardViewModel objPackage = new GiftCardViewModel
                        {
                            Id = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsActive = pac.IsActive,
                            IsDeleted = pac.IsDeleted,
                            ModifiedBy = pac.ModifiedBy,
                            ModifiedOn = pac.ModifiedOn,
                            Name = pac.Name,
                            PointCost = pac.PointCost,
                            Zipcode = pac.Zipcode,
                            Radius=pac.Radius
                        };

                        objPackagesViewModel.loGiftCardsList.Add(objPackage);
                    }
                }
            }
            GiftCardModel loPackageModel = new GiftCardModel();
            loPackageModel.inRecordCount = liRecordCount;
            loPackageModel.inPageIndex = foRequest.inPageIndex;
            loPackageModel.Pager = new Services.Pager(liRecordCount, foRequest.inPageIndex);
            loPackageModel.loGiftCardsList = objPackagesViewModel.loGiftCardsList;
            return loPackageModel;
        }

        public ActionResult searchGiftCard(GiftCardViewModel foSearchRequest)
        {
            GiftCardModel loPackageModel = getPackageList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageGiftCards/_ManageGiftCards.cshtml", loPackageModel);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddEditGiftCard(GiftCardViewModel loGiftCardViewModel)
        {
            if (loGiftCardViewModel != null)
            {
                try
                {
                    if (loGiftCardViewModel.IsDefault == true)
                    {
                        string lsResult = Utility.ExecuteScalar("sp_UpdGiftCardDefault").ToString();
                    }
                    if (loGiftCardViewModel.Id == 0)
                    {
                        GiftCards lowebstats = new GiftCards();
                        lowebstats.Name = loGiftCardViewModel.Name;
                        lowebstats.PointCost = loGiftCardViewModel.PointCost;
                        lowebstats.Zipcode = loGiftCardViewModel.Zipcode;
                        lowebstats.Radius = loGiftCardViewModel.Radius;
                        lowebstats.IsActive = loGiftCardViewModel.IsActive;
                        lowebstats.CreatedBy = User.Identity.Name;
                        lowebstats.IsDefault = loGiftCardViewModel.IsDefault;
                        lowebstats.CreatedOn = DateTime.Now;
                        Repository<GiftCards>.InsertEntity(lowebstats, entity => { return entity.Id; });
                        BackgroundJob.Enqueue(() => PurpicsWeb.Services.CommonFunctions.CommonFunctions.UpdateGiftcardScopeArea(lowebstats.Id));
                        TempData["SuccessMsg"] = "Giftcard has been added successfully";
                    }
                    else
                    {
                        GiftCards lowebstats = Repository<GiftCards>.GetEntityListForQuery(x => x.Id == loGiftCardViewModel.Id).FirstOrDefault();
                        string lsZipCode = lowebstats.Zipcode;
                        int? liRadius = lowebstats.Radius;
                        lowebstats.Name = loGiftCardViewModel.Name;
                        lowebstats.PointCost = loGiftCardViewModel.PointCost;
                        lowebstats.IsActive = loGiftCardViewModel.IsActive;
                        lowebstats.Zipcode = loGiftCardViewModel.Zipcode;
                        lowebstats.Radius = loGiftCardViewModel.Radius;
                        lowebstats.IsDefault = loGiftCardViewModel.IsDefault;
                        lowebstats.ModifiedBy = User.Identity.Name;
                        lowebstats.ModifiedOn = DateTime.Now;
                        Repository<GiftCards>.UpdateEntity(lowebstats, (entity) => { return entity.Id; });

                        if (loGiftCardViewModel.Zipcode != lsZipCode || loGiftCardViewModel.Radius != liRadius)
                        {
                            BackgroundJob.Enqueue(() => PurpicsWeb.Services.CommonFunctions.CommonFunctions.UpdateGiftcardScopeArea(loGiftCardViewModel.Id));
                        }
                        TempData["SuccessMsg"] = "Giftcard has been updated successfully";
                    }
                }
                catch (Exception)
                {
                    TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
                }
            }
            return RedirectToAction("", "ManageGiftCards");
        }
        [HttpGet]
        public ActionResult AddEditGiftCard(int? id)
        {
            GiftCardViewModel objPackageViewModel = new GiftCardViewModel();

            if (id != null && id > 0)
            {
                GiftCards objPackages = Repository<GiftCards>.GetEntityListForQuery(x => x.Id == id).FirstOrDefault();

                objPackageViewModel = new GiftCardViewModel
                {
                    Id = objPackages.Id,
                    CreatedBy = objPackages.CreatedBy,
                    CreatedOn = objPackages.CreatedOn,
                    IsActive = objPackages.IsActive,
                    IsDeleted = objPackages.IsDeleted,
                    ModifiedBy = objPackages.ModifiedBy,
                    ModifiedOn = objPackages.ModifiedOn,
                    Name = objPackages.Name,
                    Radius = objPackages.Radius,
                    Zipcode = objPackages.Zipcode,
                    PointCost = objPackages.PointCost,
                    IsDefault = Convert.ToBoolean(objPackages.IsDefault)
                };
            }

            return View("~/Areas/Admin/Views/ManageGiftCards/AddEditGiftCard.cshtml", objPackageViewModel);
        }
        public ActionResult DeleteGiftCard(GiftCardViewModel foRequest)
        {
            int liSuccess = 0;
            string lsMessage = string.Empty;

            Volunteers loCampaign = Repository<Volunteers>.GetEntityListForQuery(x => x.GiftCardPreference == foRequest.Id && x.Status == 1 && x.IsDeleted == false && x.IsActive == true).FirstOrDefault();
            if (loCampaign != null)
            {
                TempData["ErrorMsg"] = "You can not delete this gift as its in use!";
            }
            else if (foRequest.Id > 0)
            {
                GiftCards objPackages = Repository<GiftCards>.GetEntityListForQuery(x => x.Id == foRequest.Id).FirstOrDefault();
                objPackages.ModifiedBy = "1";
                objPackages.ModifiedOn = DateTime.Now;
                objPackages.IsDeleted = true;
                Repository<GiftCards>.UpdateEntity(objPackages, (entity) => { return entity.Id; });
                TempData["SuccessMsg"] = "Giftcard has been deleted successfully";
            }
            
            return this.Json(new GiftCardViewModel { Id = liSuccess });
        }
    }
}