﻿using PurpicsWeb.Areas.Admin.ViewModels.Company;
using PurpicsWeb.Services;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManageCompanyController : Controller
    {
        // GET: Admin/ManageCompany
        public ActionResult Index()
        {
            CompanyViewModel foRequest = new CompanyViewModel();
            foRequest.stSortColumn = "ID ASC";
            CompanyModel loCompanyListModel = getCompanyList(foRequest);
            return View("~/Areas/Admin/Views/ManageCompany/ManageCompany.cshtml", loCompanyListModel);
        }

        //public ActionResult ManageCompany()
        //{
        //    CompanyViewModel foRequest = new CompanyViewModel();
        //    foRequest.stSortColumn = "ID ASC";
        //    CompanyModel loCompanyListModel = getCompanyList(foRequest);
        //    return View("~/Areas/Admin/Views/ManageCompany/ManageCompany.cshtml", loCompanyListModel);
        //}

        public CompanyModel getCompanyList(CompanyViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CompanyModel objCompanyViewModel = new CompanyModel();
            int liRecordCount = 0;

            Func<IQueryable<Companies>, IOrderedQueryable<Companies>> orderingFunc =
            query => query.OrderBy(x => x.Id);

            Expression<Func<Companies, bool>> expression = null;
            //expression = x => x.IsDeleted == false;
            //if (!string.IsNullOrEmpty(foRequest.stSearch))
            //    expression = x => x.CompanyName.ToLower().Contains(foRequest.stSearch.ToLower());

            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                foRequest.stSearch = foRequest.stSearch.Replace("%20", " ");
            }

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.Status)) && foRequest.Status != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.Status == foRequest.Status && x.CompanyName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.Status)) && foRequest.Status != 0)
                expression = x => x.Status == foRequest.Status && x.IsDeleted == false;
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CompanyName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else
                expression = x => x.IsDeleted == false;
            
            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "ID DESC":
                        orderingFunc = q=> q.OrderByDescending(s => s.Id);
                        break;
                    case "ID ASC":
                        orderingFunc = q=> q.OrderBy(s => s.Id);
                        break;
                    case "LegalEntityName DESC":
                        orderingFunc = q=> q.OrderByDescending(s => s.CompanyName);
                        break;
                    case "LegalEntityName ASC":
                        orderingFunc = q=> q.OrderBy(s => s.CompanyName);
                        break;
                }
            }
            List<Companies> objCompany = new List<Companies>();
            objCompany = Repository<Companies>.GetEntityListForQuery(expression, orderingFunc, null, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();
            //if (!string.IsNullOrEmpty(foRequest.stSearch))
            //{
            //    liRecordCount = Repository<Companies>.GetEntityListForQuery(null).OrderBy(x => x.Id).Where(x => x.IsDeleted == false && x.CompanyName.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            //}
            //else
            //{
            //    liRecordCount = Repository<Companies>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false).Count();
            //}

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.Status)) && foRequest.Status != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<Companies>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.CompanyName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.Status == foRequest.Status).Count();
            }
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<Companies>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.CompanyName.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.Status)) && foRequest.Status != 0)
            {
                liRecordCount = Repository<Companies>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.Status == foRequest.Status).Count();
            }
            else
            {
                liRecordCount = Repository<Companies>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false).Count();
            }
            
            //liRecordCount = objCompany.Count();
            if (objCompany.Count > 0)
            {
                if (objCompany != null && objCompany.Count > 0)
                {
                    foreach (var pac in objCompany)
                    {
                        CompanyViewModel objComp = new CompanyViewModel
                        {
                            Id = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsDeleted = pac.IsDeleted,
                            ModifiedBy = pac.ModifiedBy,
                            ModifiedOn = pac.ModifiedOn,
                            LegalEntityName = pac.LegalEntityName,
                            LogoFilename = pac.LogoFilename,
                            CompanyName = pac.CompanyName,
                            Status = pac.Status,
                            ActiveCampaigns = Repository<Campaigns>.GetEntityListForQuery(null).OrderBy(x => x.Id).Where(x => x.IsDeleted == false && x.CompanyID==pac.Id).Count()
                        };

                        objCompanyViewModel.loCompanyList.Add(objComp);
                    }
                }
            }
            CompanyModel loCompanyModel = new CompanyModel();
            loCompanyModel.inRecordCount = liRecordCount;
            loCompanyModel.loCompanyList = objCompanyViewModel.loCompanyList;
            loCompanyModel.inPageIndex = foRequest.inPageIndex;
            loCompanyModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCompanyModel;
        }
        public ActionResult searchCompany(CompanyViewModel foSearchRequest)
        {
            CompanyModel loCompanyModel = getCompanyList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCompany/_ManageCompany.cshtml", loCompanyModel);
        }
        [HttpGet]
        public ActionResult CompanyDetail(int fiCompanyId=0)
        {
            Companies lowebstats = Repository<Companies>.GetEntityListForQuery(x => x.Id == fiCompanyId).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            CompanyViewModel loCompanyViewModel = new CompanyViewModel();
            loCompanyViewModel.Id = Convert.ToInt32(fiCompanyId);
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;
            loCompanyViewModel.Status = lowebstats.Status;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            //loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.CompanyName = lowebstats.CompanyName;
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser.UserName;
            loCompanyViewModel.LogoFilename = lowebstats.LogoFilename;
            return View(loCompanyViewModel);
        }

        [HttpPost]
        public ActionResult approveCompany(int fiCompanyId)
        {
            Companies lowebstats = Repository<Companies>.GetEntityListForQuery(x => x.Id == fiCompanyId).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            lowebstats.Status = (int)CommonEnums.UserStatus.Approved;
            Repository<Companies>.UpdateEntity(lowebstats, (entity) => { return entity.Id; });

            CompanyViewModel loCompanyViewModel = new CompanyViewModel();
            loCompanyViewModel.Id = fiCompanyId;
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;
            loCompanyViewModel.Status = lowebstats.Status;
            //loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.CompanyName = lowebstats.CompanyName;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.LogoFilename = lowebstats.LogoFilename;
            loCompanyViewModel.AspNetUsername = loUser.UserName;
            TempData["SuccessMsg"] = "Company has been Approved.";
            Helper.CommonFunctions.sendCompanyStatusMail(lowebstats.CompanyName, 2, loUser.Email);
            
            return PartialView("~/Areas/Admin/Views/ManageCompany/_CompanyDetail.cshtml", loCompanyViewModel);
        }
        [HttpPost]
        public ActionResult declineCompanyStatus(int fiCompanyId)
        {
            Companies lowebstats = Repository<Companies>.GetEntityListForQuery(x => x.Id == fiCompanyId).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();

            lowebstats.Status = (int)CommonEnums.UserStatus.Denied;
            Repository<Companies>.UpdateEntity(lowebstats, (entity) => { return entity.Id; });

            CompanyViewModel loCompanyViewModel = new CompanyViewModel();
            loCompanyViewModel.Id = fiCompanyId;
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;
            loCompanyViewModel.Status = lowebstats.Status;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            //loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.CompanyName = lowebstats.CompanyName;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser.UserName;
            loCompanyViewModel.LogoFilename = lowebstats.LogoFilename;
            TempData["SuccessMsg"] = "Company has been Declined.";
            Helper.CommonFunctions.sendCompanyStatusMail(lowebstats.CompanyName, 3, loUser.Email);
            return PartialView("~/Areas/Admin/Views/ManageCompany/_CompanyDetail.cshtml", loCompanyViewModel);
        }
    }
}