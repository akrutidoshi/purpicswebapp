﻿using Hangfire;
using PurpicsWeb.Areas.Admin.ViewModels.RedeemGiftCard;
using PurpicsWeb.Controllers;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Services;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManageRedeemGiftCardController : Controller
    {
        // GET: Admin/ManageRedeemGiftCard
        public ActionResult Index()
        {
            VolunteerViewModel foRequest = new VolunteerViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.inPageIndex = 1;
            RedeemGiftCardModel lovolunteersViewmodel = getRedeemGiftCardVolunteersList(foRequest);
            return View("~/Areas/Admin/Views/ManageRedeemGiftCard/ManageRedeemGiftCard.cshtml", lovolunteersViewmodel);
        }

        public RedeemGiftCardModel getRedeemGiftCardVolunteersList(VolunteerViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;
            if (foRequest.stPointSearch == "")
                foRequest.stPointSearch = null;

            RedeemGiftCardModel objRedeemGiftCardModel = new RedeemGiftCardModel();
            int liRecordCount = 0;

            Func<IQueryable<Volunteers>, IOrderedQueryable<Volunteers>> orderingFunc =
          query => query.OrderBy(x => x.Id);

            Expression<Func<Volunteers, bool>> expression = null;

            AppSettings settings = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedemptionPoints").FirstOrDefault();
            int MinimumPoints = 0;
            int SearchPoints = 0;
            if (settings != null)
            {
                MinimumPoints = Convert.ToInt32(settings.Value);
            }

            if (foRequest.stPointSearch != null)
            {
                MinimumPoints = Convert.ToInt32(foRequest.stPointSearch);
            }

            //if (!string.IsNullOrEmpty(foRequest.stSearch) && !string.IsNullOrEmpty(foRequest.stPointSearch))
            //    expression = x => x.CreditPoints >= MinimumPoints && x.CreditPoints >= SearchPoints && x.IsDeleted == false && x.Name.ToLower().Contains(foRequest.stSearch.ToLower());
            //else if (!string.IsNullOrEmpty(foRequest.stPointSearch))
            //    expression = x => x.CreditPoints >= MinimumPoints  && x.IsDeleted == false && x.CreditPoints >= SearchPoints;
            //else 
            if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CreditPoints >= MinimumPoints && x.IsDeleted == false && x.Name.ToLower().Contains(foRequest.stSearch.ToLower());
            else
                expression = x => x.CreditPoints >= MinimumPoints && x.IsDeleted == false;

            List<Volunteers> loVolunteers = Repository<Volunteers>.GetEntityListForQuery(expression, null, null, null, null).ToList();


            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "Name DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Name);
                        break;
                    case "Name ASC":
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;

                    case "CreditPoints DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CreditPoints);
                        break;
                    case "CreditPoints ASC":
                        orderingFunc = q => q.OrderBy(s => s.CreditPoints);
                        break;


                    //case "PrefCard DESC":
                    //    orderingFunc = q => q.OrderByDescending(s => s.GiftCards.);
                    //    break;
                    //case "PrefCard ASC":
                    //    orderingFunc = q => q.OrderBy(s => s.Likes);
                    //    break;
                    default:  // Name ascending 
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }
            }

            List<Volunteers> objVolunteers = new List<Volunteers>();
            objVolunteers = Repository<Volunteers>.GetEntityListForQuery(expression, orderingFunc, null, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.CreditPoints >= MinimumPoints && x.IsDeleted == false).ToList();

            //if (!string.IsNullOrEmpty(foRequest.stSearch) && !string.IsNullOrEmpty(foRequest.stPointSearch))
            //{
            //    liRecordCount = Repository<Volunteers>.GetEntityListForQuery(null, null, null).Where(x => x.CreditPoints >= MinimumPoints && x.CreditPoints >= SearchPoints && x.IsDeleted == false && x.Name != null && x.Name.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            //}
            //else if (!string.IsNullOrEmpty(foRequest.stPointSearch))
            //{
            //    liRecordCount = Repository<Volunteers>.GetEntityListForQuery(null, null, null).Where(x => x.CreditPoints >= MinimumPoints && x.IsDeleted == false && x.Name != null && x.CreditPoints >= SearchPoints).Count();
            //}
            //else 
            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<Volunteers>.GetEntityListForQuery(null, null, null).Where(x => x.CreditPoints >= MinimumPoints && x.IsDeleted == false && x.Name != null && x.Name.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            }
            else
            {
                liRecordCount = Repository<Volunteers>.GetEntityListForQuery(null).Where(x => x.CreditPoints >= MinimumPoints && x.IsDeleted == false).Count();
            }

            if (foRequest.inPageIndex > 0)
            {
                ViewBag.Currentpageindex = foRequest.inPageIndex;
            }
            if (objVolunteers.Count > 0)
            {
                foreach (var vol in objVolunteers)
                {
                    VolunteerViewModel objVol = new VolunteerViewModel
                    {
                        Id = vol.Id,
                        Name = vol.Name,
                        CreditPoints = vol.CreditPoints,
                        GiftCardPreferenceName = getGiftPreference(vol.GiftCardPreference)//Repository<GiftCards>.GetEntityListForQuery(x => x.Id == vol.GiftCardPreference).Select(x => x.Name).FirstOrDefault()
                    };

                    objRedeemGiftCardModel.loVolunteersList.Add(objVol);
                }
                //if (!string.IsNullOrEmpty(foRequest.stSortColumn))
                //{
                //    if (foRequest.stSortColumn == "GiftCardPreferenceName DESC")
                //    {
                //        objRedeemGiftCardModel.loVolunteersList = objRedeemGiftCardModel.loVolunteersList.OrderByDescending(x => x.GiftCardPreferenceName).ToList();
                //    }
                //    else if (foRequest.stSortColumn == "GiftCardPreferenceName ASC")
                //    {
                //        objRedeemGiftCardModel.loVolunteersList = objRedeemGiftCardModel.loVolunteersList.OrderBy(x => x.GiftCardPreferenceName).ToList();
                //    }
                //}
            }

            RedeemGiftCardModel loRedeemGiftCard = new RedeemGiftCardModel();
            loRedeemGiftCard.inRecordCount = liRecordCount;
            loRedeemGiftCard.loVolunteersList = objRedeemGiftCardModel.loVolunteersList;
            loRedeemGiftCard.inPageIndex = foRequest.inPageIndex;

            loRedeemGiftCard.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loRedeemGiftCard;



        }
        public string getGiftPreference(int? id)
        {
            string lsName = string.Empty;
            if (string.IsNullOrEmpty(Convert.ToString(id)))
            {
                lsName = Repository<GiftCards>.GetEntityListForQuery(x => x.IsDefault == true).Select(x => x.Name).FirstOrDefault();
            }
            else
            {
                lsName = Repository<GiftCards>.GetEntityListForQuery(x => x.Id == id).Select(x => x.Name).FirstOrDefault();
            }
                return lsName;
        }
        public ActionResult searchVolunteers(VolunteerViewModel foSearchRequest)
        {
            RedeemGiftCardModel objVolunteersViewModel = new RedeemGiftCardModel();
            objVolunteersViewModel = getRedeemGiftCardVolunteersList(foSearchRequest);

            return PartialView("~/Areas/Admin/Views/ManageRedeemGiftCard/_RedeemGiftCardDetails.cshtml", objVolunteersViewModel);
        }

        public ActionResult SendGiftCard(int VolunteersId)
        {
            if (VolunteersId != null)
            {
                Volunteers volunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == VolunteersId).FirstOrDefault();
                int GiftCardPreferenceId = 0;
                if (volunteer.GiftCardPreference != null)
                {
                    GiftCardPreferenceId = Convert.ToInt32(volunteer.GiftCardPreference);
                }

                GiftCards giftCard = Repository<GiftCards>.GetEntityListForQuery(x => x.Id == GiftCardPreferenceId).FirstOrDefault();
                if (giftCard==null)
                {
                    giftCard = Repository<GiftCards>.GetEntityListForQuery(x => x.IsDefault == true).FirstOrDefault();
                }
                if (giftCard != null)
                {
                    int PointCost = Convert.ToInt32(giftCard.PointCost);                    
                    volunteer.CreditPoints = (volunteer.CreditPoints - PointCost);
                    Repository<Volunteers>.UpdateEntity(volunteer, (entity) => { return entity.Id; });



                    GiftCardSent giftcardSent = new GiftCardSent();
                    giftcardSent.VolunteerId = VolunteersId;
                    giftcardSent.GiftCardId = GiftCardPreferenceId;
                    giftcardSent.ApprovalDate = DateTime.Now;
                    giftcardSent.Points = PointCost;
                    giftcardSent.CreatedBy = "1";
                    giftcardSent.CreatedOn = DateTime.Now;
                    Repository<GiftCardSent>.InsertEntity(giftcardSent, entity => { return entity.Id; });

                }

                string VolunteerName = string.Empty;
                string VolunteerEmail = string.Empty;

                if (!string.IsNullOrEmpty(volunteer.Name))
                {
                    VolunteerName = volunteer.Name;
                }

                if (!string.IsNullOrEmpty(volunteer.Email))
                {
                    VolunteerEmail = volunteer.Email;
                    Helper.CommonFunctions.sendGiftCardMailToVolunteer(VolunteerEmail);
                }
                
                TempData["SuccessMsg"] = "Gift Card sent successfully.";
            }
            return RedirectToAction("Index", "ManageRedeemGiftCard");
        }

        public ActionResult VolunteerDetails(int fiVolunteerId)
        {
            VolunteerViewModel loVolunteerModel = new VolunteerViewModel();
            loVolunteerModel = getVolunterDetail(fiVolunteerId);
            return View(loVolunteerModel);
        }
        public VolunteerViewModel getVolunterDetail(int fiVolunteerId)
        {
            Volunteers loVolunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == fiVolunteerId).FirstOrDefault();
            List<CampaignUploads> loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(x => x.VolunteerID == fiVolunteerId && x.IsDeleted==false).ToList();
            VolunteerViewModel loVolunteerModel = new VolunteerViewModel();
            int liLikes = 0;
            AppSettings loAppSettingsCampaignPointAwarded = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignPointAwarded").FirstOrDefault();
            AppSettings loAppSettingsRedemptionPoints = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedemptionPoints").FirstOrDefault();
            int liPointAwarded = 0;
            int liMinRedeemPoint = 0;
            if (loAppSettingsCampaignPointAwarded != null)
            {
                liPointAwarded = Convert.ToInt32(loAppSettingsCampaignPointAwarded.Value);
            }
            if (loAppSettingsRedemptionPoints != null)
            {
                liMinRedeemPoint = Convert.ToInt32(loAppSettingsRedemptionPoints.Value);
            }
            if (loCampaignUpload.Count > 0)
            {
                foreach (var loItem in loCampaignUpload)
                {
                    VolunteersCampaign objCamp = new VolunteersCampaign
                    {
                        Campaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == loItem.CampaignID).Select(x => x.Caption).FirstOrDefault(),
                        Likes = loItem.Likes,
                        RedeemPoint = Convert.ToInt32(loItem.Likes * liPointAwarded)
                    };
                    loVolunteerModel.loVolunteerCampaignList.Add(objCamp);
                    liLikes = liLikes + loItem.Likes;
                }
            }

            int liTotalRedeempoints = Convert.ToInt32(loVolunteer.CreditPoints);//Convert.ToInt32(liLikes * liPointAwarded);
            loVolunteerModel.TotalRedeem = liTotalRedeempoints;

            GiftCards loGiftCard = new GiftCards();
            if (loVolunteer.GiftCardPreference != null && loVolunteer.GiftCardPreference > 0)
            {
                loGiftCard = Repository<GiftCards>.GetEntityListForQuery(x => x.Id == loVolunteer.GiftCardPreference).FirstOrDefault();
                if (loGiftCard != null)
                {
                    loVolunteerModel.GiftCard = loGiftCard.Name;
                    loVolunteerModel.GiftCardPreference = loGiftCard.Id;
                }
            }
            else
            {
                loGiftCard = Repository<GiftCards>.GetEntityListForQuery(x => x.IsDefault == true).FirstOrDefault();
                if (loGiftCard != null)
                {
                    loVolunteerModel.GiftCard = loGiftCard.Name;
                    loVolunteerModel.GiftCardPreference = loGiftCard.Id;
                }
            }

            if (liTotalRedeempoints >= liMinRedeemPoint && loGiftCard != null && loGiftCard.PointCost <= liTotalRedeempoints)
                loVolunteerModel.IsDisplaySendGiftCardButton = true;

            loVolunteerModel.Name = loVolunteer.Name;
            loVolunteerModel.TotalLikes = liLikes;
            loVolunteerModel.Id = loVolunteer.Id;
            return loVolunteerModel;
        }
    }


}