﻿using Hangfire;
using LinqKit;
using PurpicsWeb.Areas.Admin.ViewModels.Volunteer;
using PurpicsWeb.Controllers;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManageVolunteerController : Controller
    {
        // GET: Admin/ManageVolunteer
        public ActionResult Index()
        {
            VolunteerViewModel foRequest = new VolunteerViewModel();
            foRequest.stSortColumn = "Id ASC";
            VolunteerModel loCharityListModel = getVolunteersList(foRequest);
            loCharityListModel.loSOs = CommonFunctions.GetStudentOrganizations("");

            return View("~/Areas/Admin/Views/ManageVolunteer/ManageVolunteer.cshtml", loCharityListModel);
        }

        public VolunteerModel getVolunteersList(VolunteerViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            VolunteerModel objCharityViewModel = new VolunteerModel();
            int liRecordCount = 0;
            int liTotalFollowers = 0;
            Func<IQueryable<Volunteers>, IOrderedQueryable<Volunteers>> orderingFunc =
            query => query.OrderBy(x => x.Name);

            Expression<Func<Volunteers, bool>> expression = null;

            if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.Name.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else
                expression = x => x.IsDeleted == false;

            if (!string.IsNullOrEmpty(foRequest.stSelectedSOs))
            {
                if (foRequest.stSelectedSOs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length > 0)
                {
                    IEnumerable<int> soIds = foRequest.stSelectedSOs.Split(',').Select(str => int.Parse(str));

                    Expression<Func<Volunteers, bool>> expSO = x => x.VolunteersSOes.Any(y => soIds.Contains(y.StudentOrganizationId));
                    expression = PredicateBuilder.And(expression, expSO);
                }
            }

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "Name DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Name);
                        break;
                    case "Name ASC":
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;
                    case "FollowerCount DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.FollowerCount);
                        break;
                    case "FollowerCount ASC":
                        orderingFunc = q => q.OrderBy(s => s.FollowerCount);
                        break;
                }
            }
            List<Expression<Func<Volunteers, Object>>> Volunteersincludes = new List<Expression<Func<Volunteers, object>>>();

            Expression<Func<Volunteers, object>> volunteersSO = (so) => so.VolunteersSOes;
            Volunteersincludes.Add(volunteersSO);

            Expression<Func<Volunteers, object>> StudentOrg = (so) => so.VolunteersSOes.Select(x => x.StudentOrganizations);
            Volunteersincludes.Add(StudentOrg);

            List<Volunteers> objCampaign = new List<Volunteers>();
            objCampaign = Repository<Volunteers>.GetEntityListForQuery(expression, orderingFunc, Volunteersincludes, foRequest.inPageIndex, foRequest.inPageSize).Where(s => s.IsDeleted == false).ToList();

            liRecordCount = Repository<Volunteers>.GetEntityListForQuery(expression, null, Volunteersincludes, null, null).Count();
            
            VolunteerModel loCompanyModel = new VolunteerModel();
            loCompanyModel.inRecordCount = liRecordCount;
            if (!string.IsNullOrEmpty(foRequest.stSelectedSOs))
            {
                liTotalFollowers = Convert.ToInt32(Repository<Volunteers>.GetEntityListForQuery(expression, null, Volunteersincludes, null, null).Where(s => s.IsDeleted == false).Sum(y => y.FollowerCount));
                loCompanyModel.inTotalFolowers = liTotalFollowers;
            }
            
            loCompanyModel.loVolunteerList = objCampaign;
            loCompanyModel.inPageIndex = foRequest.inPageIndex;
            loCompanyModel.Pager = new Services.Pager(liRecordCount, foRequest.inPageIndex);
            return loCompanyModel;
        }
        public ActionResult searchVolunteer(VolunteerViewModel foSearchRequest)
        {
            VolunteerModel loCharityModel = getVolunteersList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageVolunteer/_ManageVolunteer.cshtml", loCharityModel);
        }
        public ActionResult VolunteerDetails(int fiVolunteerId)
        {
            VolunteerViewModel loVolunteerModel = new VolunteerViewModel();
            loVolunteerModel = getVolunterDetail(fiVolunteerId);
            return View(loVolunteerModel);
        }
        public VolunteerViewModel getVolunterDetail(int fiVolunteerId)
        {
            Volunteers loVolunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == fiVolunteerId).FirstOrDefault();
            List<CampaignUploads> loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(x => x.VolunteerID == fiVolunteerId && x.IsDeleted == false).ToList();
            VolunteerViewModel loVolunteerModel = new VolunteerViewModel();
            int liLikes = 0;
            AppSettings loAppSettingsCampaignPointAwarded = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "CampaignPointAwarded").FirstOrDefault();
            AppSettings loAppSettingsRedemptionPoints = Repository<AppSettings>.GetEntityListForQuery(x => x.Name.ToLower() == "RedemptionPoints").FirstOrDefault();
            int liPointAwarded = 0;
            int liMinRedeemPoint = 0;
            if (loAppSettingsCampaignPointAwarded != null)
            {
                liPointAwarded = Convert.ToInt32(loAppSettingsCampaignPointAwarded.Value);
            }
            if (loAppSettingsRedemptionPoints != null)
            {
                liMinRedeemPoint = Convert.ToInt32(loAppSettingsRedemptionPoints.Value);
            }
            if (loCampaignUpload.Count > 0)
            {
                foreach (var loItem in loCampaignUpload)
                {
                    VolunteersCampaign objCamp = new VolunteersCampaign
                    {
                        Campaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == loItem.CampaignID).Select(x => x.CampaignName).FirstOrDefault(),
                        Likes = loItem.Likes,
                        RedeemPoint = Convert.ToInt32(loItem.Likes * liPointAwarded)
                    };
                    loVolunteerModel.loVolunteerCampaignList.Add(objCamp);
                    liLikes = liLikes + loItem.Likes;
                }
            }

            int liTotalRedeempoints = Convert.ToInt32(loVolunteer.CreditPoints);//Convert.ToInt32(liLikes * liPointAwarded);
            loVolunteerModel.TotalRedeem = liTotalRedeempoints;

            GiftCards loGiftCard = new GiftCards();
            if (loVolunteer.GiftCardPreference != null && loVolunteer.GiftCardPreference > 0)
            {
                loGiftCard = Repository<GiftCards>.GetEntityListForQuery(x => x.Id == loVolunteer.GiftCardPreference).FirstOrDefault();
                if (loGiftCard != null)
                {
                    loVolunteerModel.GiftCard = loGiftCard.Name;
                    loVolunteerModel.GiftCardPreference = loGiftCard.Id;
                }
            }
            else
            {
                loGiftCard = Repository<GiftCards>.GetEntityListForQuery(x => x.IsDefault == true).FirstOrDefault();
                if (loGiftCard != null)
                {
                    loVolunteerModel.GiftCard = loGiftCard.Name;
                    loVolunteerModel.GiftCardPreference = loGiftCard.Id;
                }
            }

            if (liTotalRedeempoints >= liMinRedeemPoint && loGiftCard != null && loGiftCard.PointCost <= liTotalRedeempoints)
                loVolunteerModel.IsDisplaySendGiftCardButton = true;

            loVolunteerModel.Name = loVolunteer.Name;
            loVolunteerModel.TotalLikes = liLikes;
            loVolunteerModel.Id = loVolunteer.Id;
            return loVolunteerModel;
        }
        public ActionResult sendGiftcard(int Id, int GiftId)
        {
            VolunteerViewModel loVolunteerModel = new VolunteerViewModel();
            Volunteers loVolunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            GiftCards loGiftCard = Repository<GiftCards>.GetEntityListForQuery(x => x.Id == GiftId).FirstOrDefault();
            if (loGiftCard != null && loVolunteer != null)
            {
                if (loVolunteer.Email != null)
                {
                    try
                    {
                        #region Send email to volunteer
                        string lsToEmailId = loVolunteer.Email;
                        string objGiftCardVolunteerEmailBody = new SystemEmailController().GiftCardVolunteerNotification(string.Empty, DateTime.Now.ToShortDateString(), (7).ToString());
                        BackgroundJob.Enqueue(() => EmailHelper.SendEmail("Your gift card is on it's way!", objGiftCardVolunteerEmailBody, lsToEmailId));
                        #endregion

                        #region Send email to admin
                        string objGiftCardAdminEmailBody = new SystemEmailController().GiftCardCompanyAdminNotification(loVolunteer.Id.ToString(), loVolunteer.Name, loGiftCard.Name);
                        string mailTo = Utility.ExecuteScalar("sp_GetAllAdminEmailId").ToString();
                        BackgroundJob.Enqueue(() => EmailHelper.SendEmail("Giftcard redeemed by " + loVolunteer.Name, objGiftCardAdminEmailBody, mailTo));
                        #endregion
                    }
                    catch { }
                }
                VolunteerGiftCards loVGC = new VolunteerGiftCards();
                loVGC.GiftCardId = GiftId;
                loVGC.VolunteerId = Id;
                loVGC.GiftCardName = loGiftCard.Name;
                loVGC.PointCost = loGiftCard.PointCost;
                loVGC.RedeemptionDate = DateTime.Now;
                loVGC.CreatedBy = User.Identity.Name;
                loVGC.CreatedOn = DateTime.Now;
                Repository<VolunteerGiftCards>.InsertEntity(loVGC, (entity) => { return entity.Id; });

                loVolunteer.CreditPoints = (loVolunteer.CreditPoints - loGiftCard.PointCost);
                Repository<Volunteers>.UpdateEntity(loVolunteer, (entity) => { return entity.Id; });
            }
            VolunteerViewModel loVolunteerViewModel = new VolunteerViewModel();
            loVolunteerViewModel = getVolunterDetail(Id);
            return PartialView("~/Areas/Admin/Views/ManageVolunteer/_VolunteerDetail.cshtml", loVolunteerViewModel);

        }
    }
}