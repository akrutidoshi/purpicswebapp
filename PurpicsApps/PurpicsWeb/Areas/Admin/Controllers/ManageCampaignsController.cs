﻿using PurpicsWeb.Areas.Admin.ViewModels.Campaign;
using PurpicsWeb.Services;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManageCampaignsController : Controller
    {
        // GET: Admin/ManageCampaigns
        public ActionResult Index()
        {
            CampaignViewModel foRequest = new CampaignViewModel();
            foRequest.stSortColumn = "Id ASC";
            CampaignModel loCampaignListModel = getCampaignList(foRequest);
            return View("~/Areas/Admin/Views/ManageCampaigns/ManageCampaigns.cshtml", loCampaignListModel);
        }

        public CampaignModel getCampaignList(CampaignViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                foRequest.stSearch = foRequest.stSearch.Replace("%20", " ");
            }

            CampaignModel objCampaignViewModel = new CampaignModel();
            int liRecordCount = 0;
            Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderingFunc =
            query => query.OrderBy(x => x.Id);

            Expression<Func<Campaigns, bool>> expression = null;

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.CampaignStatusID)) && foRequest.CampaignStatusID != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CampaignStatusID == foRequest.CampaignStatusID && x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.CampaignStatusID)) && foRequest.CampaignStatusID != 0)
                expression = x => x.CampaignStatusID == foRequest.CampaignStatusID && x.IsDeleted == false;
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else
                expression = x => x.IsDeleted == false;

            //Include 
            List<Expression<Func<Campaigns, Object>>> Campaignsincludes = new List<Expression<Func<Campaigns, object>>>();

            Expression<Func<Campaigns, object>> campaignCharity = (campaignCharityName) => campaignCharityName.Charities;
            Campaignsincludes.Add(campaignCharity);

            Expression<Func<Campaigns, object>> campaignSO = (campaignSOName) => campaignSOName.StudentOrganizations;
            Campaignsincludes.Add(campaignSO);

            Expression<Func<Campaigns, object>> companyName = (companyname) => companyname.Companies;
            Campaignsincludes.Add(companyName);

            Expression<Func<Campaigns, object>> campaignUploads = (campaignuploads) => campaignuploads.CampaignUploads;
            Campaignsincludes.Add(campaignUploads);

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "LegalCampaignName DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignName);
                        break;
                    case "LegalCampaignName ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignName);
                        break;
                    case "Company DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Companies.CompanyName);
                        break;
                    case "Company ASC":
                        orderingFunc = q => q.OrderBy(s => s.Companies.CompanyName);
                        break;
                    case "Charity DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Charities.CharityName);
                        break;
                    case "Charity ASC":
                        orderingFunc = q => q.OrderBy(s => s.Charities.CharityName);
                        break;
                    case "StudentOrganization DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.StudentOrganizations.Name);
                        break;
                    case "StudentOrganization ASC":
                        orderingFunc = q => q.OrderBy(s => s.StudentOrganizations.Name);
                        break;
                    case "Status ASC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignStatusID == 2).ThenBy(s => s.CampaignStatusID == 1).ThenBy(s => s.CampaignStatusID == 3);
                        break;
                    case "Status DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignStatusID == 1).ThenBy(s => s.CampaignStatusID == 2).ThenBy(s => s.CampaignStatusID == 3);
                        break;
                    default:
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }
            }

            List<Campaigns> objCampaign = new List<Campaigns>();
            objCampaign = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.CampaignStatusID)) && foRequest.CampaignStatusID != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.CampaignStatusID == foRequest.CampaignStatusID).Count();
            }
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.CampaignStatusID)) && foRequest.CampaignStatusID != 0)
            {
                liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.CampaignStatusID == foRequest.CampaignStatusID).Count();
            }
            else
            {
                liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false).Count();
            }

            if (objCampaign.Count > 0)
            {
                if (objCampaign != null && objCampaign.Count > 0)
                {
                    foreach (var pac in objCampaign)
                    {
                        CampaignViewModel objComp = new CampaignViewModel
                        {
                            Id = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsDeleted = pac.IsDeleted,
                            ModifiedBy = pac.ModifiedBy,
                            ModifiedOn = pac.ModifiedOn,
                            Caption = pac.Caption,
                            CampaignName = pac.CampaignName,
                            CharityID = pac.Charities.Id,
                            Charity = pac.Charities.CharityName,
                            CompanyID = pac.Companies.Id,
                            Company = pac.Companies.CompanyName,
                            CampaignStatusID = pac.CampaignStatusID,
                            Likes = pac.CampaignUploads.Sum(x => x.Likes),
                            IsFeaturedCampaign = pac.IsFeaturedCampaign,
                            StudentOrganizationId = pac.StudentOrganizationId != null ? (int)pac.StudentOrganizationId : pac.StudentOrganizationId,
                            StudentOrganizationName = pac.StudentOrganizations == null ? "" : pac.StudentOrganizations.Name
                        };

                        objCampaignViewModel.loCampaignList.Add(objComp);
                    }
                }
            }
            CampaignModel loCompanyModel = new CampaignModel();
            loCompanyModel.inRecordCount = liRecordCount;
            loCompanyModel.loCampaignList = objCampaignViewModel.loCampaignList;
            loCompanyModel.inPageIndex = foRequest.inPageIndex;
            loCompanyModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCompanyModel;
        }

        public ActionResult searchCampaign(CampaignViewModel foSearchRequest)
        {
            CampaignModel loCampaignModel = getCampaignList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCampaigns/_ManageCampaigns.cshtml", loCampaignModel);
        }
        public ActionResult CampaignDetail(int fiCampaignId)
        {
            Campaigns loCampaign = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.Id == fiCampaignId).FirstOrDefault();

            CampaignViewModel loCampaignModel = new CampaignViewModel();
            loCampaignModel = getCampaignDetail(loCampaign);
            return View(loCampaignModel);
        }
        public ActionResult getVolunteerList(CampaignViewModel loCampaign)
        {
            if (loCampaign.inPageSize <= 0)
                loCampaign.inPageSize = 10;

            if (loCampaign.inPageIndex <= 0)
                loCampaign.inPageIndex = 1;
            Func<IQueryable<CampaignUploads>, IOrderedQueryable<CampaignUploads>> orderingFunc =
            query => query.OrderBy(x => x.Id);
            Expression<Func<CampaignUploads, bool>> expression = null;

            expression = x => x.CampaignID == loCampaign.Id;

            //Include 
            List<Expression<Func<CampaignUploads, Object>>> CampaignUploadsincludes = new List<Expression<Func<CampaignUploads, object>>>();

            Expression<Func<CampaignUploads, object>> volunteersEntity = (volunteers) => volunteers.Volunteers;
            CampaignUploadsincludes.Add(volunteersEntity);
            CampaignViewModel loCampaignModel = new CampaignViewModel();
            List<CampaignUploads> loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(expression, orderingFunc, CampaignUploadsincludes, loCampaign.inPageIndex, loCampaign.inPageSize).ToList();

            loCampaignModel.TotalVolunteers = Repository<CampaignUploads>.GetEntityListForQuery(null).Where(x => x.CampaignID == loCampaign.Id && x.IsDeleted == false).Count();
            if (loCampaignUpload != null && loCampaignUpload.Count > 0)
            {
                foreach (var pac in loCampaignUpload)
                {
                    VolunteerModel objComp = new VolunteerModel
                    {
                        Id = pac.Volunteers.Id,
                        Name = pac.Volunteers.Name,
                        Likes = loCampaignUpload == null ? 0 : pac.Likes,
                        ZipCode = pac.Volunteers.ZipCode
                    };

                    loCampaignModel.loVolunteerList.Add(objComp);
                }

            }
            loCampaignModel.inPageIndex = loCampaign.inPageIndex;
            return PartialView("~/Areas/Admin/Views/ManageCampaigns/_CampaignVolunteer.cshtml", loCampaignModel);
        }
        public CampaignViewModel getCampaignDetail(Campaigns loCampaign)
        {
            Companies loCompany = Repository<Companies>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.CompanyID).FirstOrDefault();
            Charities loCharity = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.CharityID).FirstOrDefault();
            StudentOrganizations loSO = Repository<StudentOrganizations>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.StudentOrganizationId).FirstOrDefault();
            Packages loPackage = Repository<Packages>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.PackageID).FirstOrDefault();
            //List<Volunteers> loVolunteerList = Repository<Volunteers>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.Id).ToList();

            Expression<Func<CampaignUploads, bool>> expression = null;

            expression = x => x.CampaignID == loCampaign.Id && x.IsDeleted == false;

            //Include 
            List<Expression<Func<CampaignUploads, Object>>> CampaignUploadsincludes = new List<Expression<Func<CampaignUploads, object>>>();

            Expression<Func<CampaignUploads, object>> volunteersEntity = (volunteers) => volunteers.Volunteers;
            CampaignUploadsincludes.Add(volunteersEntity);

            List<CampaignUploads> loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(expression, null, CampaignUploadsincludes, null, null).OrderBy(x => x.VolunteerID).ToList();

            CampaignViewModel loCampaignModel = new CampaignViewModel();
            loCampaignModel.Id = loCampaign.Id;
            loCampaignModel.Caption = loCampaign.Caption;
            loCampaignModel.CampaignName = loCampaign.CampaignName;
            loCampaignModel.Status = loCampaign.CampaignStatusID;
            loCampaignModel.Company = loCompany == null ? "-" : loCompany.CompanyName;
            loCampaignModel.ExpirationDate = loCampaign.ExpirationDate;
            loCampaignModel.ImageFile = loCampaign.ImageFile;
            loCampaignModel.Charity = loCharity == null ? "-" : loCharity.CharityName;
            if (loSO != null)
            {
                loCampaignModel.StudentOrganizationName = loSO == null ? "-" : loSO.Name;
            }
            //loCampaignModel.Likes = loCampaign.PackageLikes;
            //loCampaignModel.PackagePrice = loCampaign.PackagePrice;

            int liLikeCount = loCampaignUpload.Sum(x=>x.Likes);
            int liVolunteerCount = 0;

            if (loCampaignUpload != null && loCampaignUpload.Count > 0)
            {
                foreach (var pac in loCampaignUpload)
                {
                    VolunteerModel objComp = new VolunteerModel
                    {
                        Id = pac.Volunteers.Id,
                        Name = pac.Volunteers.Name,
                        Likes = loCampaignUpload == null ? 0 : pac.Likes,
                        ZipCode = pac.Volunteers.ZipCode
                    };
                    //liLikeCount = liLikeCount + (loCampaignUpload == null ? 0 : pac.Likes);
                    if (loCampaignModel.loVolunteerList.Count < 10)
                        loCampaignModel.loVolunteerList.Add(objComp);
                }
                liVolunteerCount = loCampaignUpload.Count();
            }
            loCampaignModel.TotalLikes = liLikeCount;
            loCampaignModel.TotalVolunteers = liVolunteerCount;
            loCampaignModel.Status = loCampaign.CampaignStatusID;
            loCampaignModel.Charity = loCharity.CharityName == null ? "" : loCharity.CharityName;
            loCampaignModel.PackageLikes = loCampaign.PackageLikes;
            loCampaignModel.PackagePrice = loCampaign.PackagePrice;
            loCampaignModel.PackageName = (Repository<Packages>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.PackageID).Select(x => x.Name).FirstOrDefault()) == null ? "" : Repository<Packages>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.PackageID).Select(x => x.Name).FirstOrDefault();

            string lsConverRate = string.Empty;
            if (loCampaign.CouponCode != null)
            {
                decimal ConversionRate = 0;
                var totalvolCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(x => (x.CampaignID == loCampaign.Id && x.IsDeleted == false));
                var totalLikes = Repository<Campaigns>.GetEntityListForQuery(x => (x.Id == loCampaign.Id)).FirstOrDefault();
                var volRedeemedCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(x => (x.CampaignID == loCampaign.Id && x.IsDeleted == false && x.RedeemptionDate != null));
                if (totalLikes.Likes > 0)
                {
                    ConversionRate = ((Convert.ToDecimal(volRedeemedCoupons.Count()) / Convert.ToDecimal(totalLikes.Likes)) * 100);
                    ConversionRate = Math.Round(ConversionRate, 2);
                }

                lsConverRate = ConversionRate.ToString() + "%";
                loCampaignModel.TotalCoupons = totalvolCoupons.Count();
                loCampaignModel.RedeemedCoupons = volRedeemedCoupons.Count();
            }
            else
            {
                lsConverRate = "N/A";
                loCampaignModel.TotalCoupons = 0;
                loCampaignModel.RedeemedCoupons = 0;
            }
            loCampaignModel.CouponCode = lsConverRate;
            loCampaignModel.inPageIndex = 1;
            return loCampaignModel;
        }

        public ActionResult approveCampaignStatus(int Id)
        {
            Campaigns loCampaign = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.Id == Id).FirstOrDefault();
            try
            {
                AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == HttpContext.User.Identity.Name).FirstOrDefault();
                string userid = loUser.Id;
                var lsResult = new BraintreePayments().PayCreateCampaignAmount(loCampaign.CompanyID, loCampaign.PackageID, userid);
                if (lsResult.IsSuccess() == true)
                {
                    loCampaign.CampaignStatusID = 2;
                    loCampaign.CampaignStartDate = DateTime.Now;
                    Repository<Campaigns>.UpdateEntity(loCampaign, (entity) => { return entity.Id; });

                    CampaignPaymentHistory loCampPay = Repository<CampaignPaymentHistory>.GetEntityListForQuery(x => x.CompanyID == loCampaign.CompanyID && x.CampaignID == loCampaign.Id).FirstOrDefault();

                    if (loCampPay == null)
                    {
                        Packages package = Repository<Packages>.GetEntityListForQuery(x => x.Id == loCampaign.PackageID).FirstOrDefault();
                        decimal PackageAmount = Convert.ToDecimal(package.Price);
                        PackageAmount = Math.Round(PackageAmount, 2);
                        CampaignPaymentHistory paymentHistory = new CampaignPaymentHistory();
                        paymentHistory.AspNetUserID = userid;
                        paymentHistory.CampaignID = loCampaign.Id;
                        paymentHistory.CompanyID = loCampaign.CompanyID;
                        paymentHistory.RefernceNumber = lsResult.Target.ProcessorAuthorizationCode;
                        paymentHistory.Notes = "debit for create campaign";
                        paymentHistory.Amount = Convert.ToDouble(PackageAmount);
                        paymentHistory.CreatedBy = userid;
                        paymentHistory.CreatedOn = DateTime.Now;
                        Repository<CampaignPaymentHistory>.InsertEntity(paymentHistory, entity => { return entity.Id; });
                    }
                    TempData["SuccessMsg"] = "Campaign has been Approved.";
                }
                else
                {
                    TempData["ErrorMsg"] = lsResult.Message;//"Something wrong in payment.";
                }
            }
            catch
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
            }

            Companies loCompany = Repository<Companies>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.CompanyID).FirstOrDefault();
            CampaignViewModel loCampaignModel = new CampaignViewModel();

            loCampaignModel = getCampaignDetail(loCampaign);
            if (loCompany != null)
            {
                AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == loCompany.AspNetUserID).FirstOrDefault();
                Helper.CommonFunctions.sendCampaignStatusMail(loCampaign.CampaignName, 2, loUser.Email);
            }

            return PartialView("~/Areas/Admin/Views/ManageCampaigns/_CampaignDetail.cshtml", loCampaignModel);
        }
        public ActionResult declineCampaignStatus(int Id)
        {
            Campaigns loCampaign = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.Id == Id).FirstOrDefault();
            loCampaign.CampaignStatusID = 3;
            Repository<Campaigns>.UpdateEntity(loCampaign, (entity) => { return entity.Id; });
            TempData["SuccessMsg"] = "Campaign has been Declined.";
            Companies loCompany = Repository<Companies>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.CompanyID).FirstOrDefault();
            CampaignViewModel loCampaignModel = new CampaignViewModel();
            loCampaignModel = getCampaignDetail(loCampaign);
            if (loCompany != null)
            {
                AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == loCompany.AspNetUserID).FirstOrDefault();
                Helper.CommonFunctions.sendCampaignStatusMail(loCampaign.CampaignName, 3, loUser.Email);
            }
            return PartialView("~/Areas/Admin/Views/ManageCampaigns/_CampaignDetail.cshtml", loCampaignModel);
        }

        public ActionResult CampaignDashBoard(int fiCampaignId)
        {
            Campaigns loCampaign = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.Id == fiCampaignId).FirstOrDefault();
            Packages loPackage = Repository<Packages>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.PackageID).FirstOrDefault();
            List<Volunteers> loVolunteerList = Repository<Volunteers>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.Id).ToList();
            CampaignUploads loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(null).Where(x => x.CampaignID == loCampaign.Id && x.IsDeleted == false).FirstOrDefault();
            Charities loCharity = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.CharityID).FirstOrDefault();
            CampaignViewModel loCampaignModel = new CampaignViewModel();
            int liLikeCount = 0;
            int liVolunteerCount = 0;

            if (loVolunteerList != null && loVolunteerList.Count > 0)
            {
                foreach (var pac in loVolunteerList)
                {
                    VolunteerModel objComp = new VolunteerModel
                    {
                        Id = pac.Id,
                        Name = pac.Name,
                        Likes = loCampaignUpload == null ? 0 : loCampaignUpload.Likes,
                        ZipCode = pac.ZipCode
                    };
                    liLikeCount = liLikeCount + (loCampaignUpload == null ? 0 : loCampaignUpload.Likes);
                    loCampaignModel.loVolunteerList.Add(objComp);
                }
                liVolunteerCount = loVolunteerList.Count();
            }
            loCampaignModel.Id = fiCampaignId;
            loCampaignModel.TotalLikes = liLikeCount;
            loCampaignModel.TotalVolunteers = liVolunteerCount;
            loCampaignModel.Status = loCampaign.CampaignStatusID;
            loCampaignModel.Charity = loCharity.CharityName == null ? "" : loCharity.CharityName;
            loCampaignModel.PackageLikes = loPackage.Likes;
            loCampaignModel.PackagePrice = loPackage.Price;
            string lsConverRate = string.Empty;
            if (loCampaign.CouponCode != null)
            {
                decimal ConversionRate = 0;
                var totalvolCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(x => (x.CampaignID == fiCampaignId && x.IsDeleted == false));
                var totalLikes = Repository<Campaigns>.GetEntityListForQuery(x => (x.Id == loCampaign.Id)).FirstOrDefault();
                var volRedeemedCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(x => (x.CampaignID == fiCampaignId && x.IsDeleted == false && x.RedeemptionDate != null));
                if (totalLikes.Likes > 0)
                {
                    ConversionRate = ((Convert.ToDecimal(volRedeemedCoupons.Count()) / Convert.ToDecimal(totalLikes.Likes)) * 100);
                    ConversionRate = Math.Round(ConversionRate, 2);
                }

                lsConverRate = ConversionRate.ToString() + "%";

                loCampaignModel.TotalCoupons = totalvolCoupons.Count();
                loCampaignModel.RedeemedCoupons = volRedeemedCoupons.Count();
            }
            else
            {
                lsConverRate = "N/A";
                loCampaignModel.TotalCoupons = 0;
                loCampaignModel.RedeemedCoupons = 0;
            }

            loCampaignModel.CouponCode = lsConverRate;
            return View(loCampaignModel);
        }

        public ActionResult approveDashboardCampaign(int Id)
        {
            Campaigns loCampaign = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.Id == Id).FirstOrDefault();
            try
            {
                AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == HttpContext.User.Identity.Name).FirstOrDefault();
                string userid = loUser.Id;
                var lsResult = new BraintreePayments().PayCreateCampaignAmount(loCampaign.CompanyID, loCampaign.PackageID, userid);
                if (lsResult.IsSuccess() == true)
                {
                    loCampaign.CampaignStatusID = 2;
                    Repository<Campaigns>.UpdateEntity(loCampaign, (entity) => { return entity.Id; });

                    CampaignPaymentHistory loCampPay = Repository<CampaignPaymentHistory>.GetEntityListForQuery(x => x.CompanyID == loCampaign.CompanyID && x.CampaignID == loCampaign.Id).FirstOrDefault();

                    if (loCampPay == null)
                    {
                        Packages package = Repository<Packages>.GetEntityListForQuery(x => x.Id == loCampaign.PackageID).FirstOrDefault();
                        decimal PackageAmount = Convert.ToDecimal(package.Price);
                        PackageAmount = Math.Round(PackageAmount, 2);
                        CampaignPaymentHistory paymentHistory = new CampaignPaymentHistory();
                        paymentHistory.AspNetUserID = userid;
                        paymentHistory.CampaignID = loCampaign.Id;
                        paymentHistory.CompanyID = loCampaign.CompanyID;
                        paymentHistory.RefernceNumber = lsResult.Target.ProcessorAuthorizationCode;
                        paymentHistory.Notes = "debit for create campaign";
                        paymentHistory.Amount = Convert.ToDouble(PackageAmount);
                        paymentHistory.CreatedBy = userid;
                        paymentHistory.CreatedOn = DateTime.Now;
                        Repository<CampaignPaymentHistory>.InsertEntity(paymentHistory, entity => { return entity.Id; });
                    }
                }
            }
            catch
            {

            }

            Packages loPackage = Repository<Packages>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.PackageID).FirstOrDefault();
            List<Volunteers> loVolunteerList = Repository<Volunteers>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.Id).ToList();
            CampaignUploads loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(null).Where(x => x.CampaignID == loCampaign.Id && x.IsDeleted == false).FirstOrDefault();
            Charities loCharity = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.CharityID).FirstOrDefault();
            CampaignViewModel loCampaignModel = new CampaignViewModel();

            int liLikeCount = 0;
            int liVolunteerCount = 0;
            if (loVolunteerList != null && loVolunteerList.Count > 0)
            {
                foreach (var pac in loVolunteerList)
                {
                    VolunteerModel objComp = new VolunteerModel
                    {
                        Id = pac.Id,
                        Name = pac.Name,
                        Likes = loCampaignUpload == null ? 0 : loCampaignUpload.Likes,
                        ZipCode = pac.ZipCode
                    };
                    liLikeCount = liLikeCount + (loCampaignUpload == null ? 0 : loCampaignUpload.Likes);
                    loCampaignModel.loVolunteerList.Add(objComp);
                }
                liVolunteerCount = loVolunteerList.Count();
            }
            loCampaignModel.Id = Id;
            loCampaignModel.TotalLikes = liLikeCount;
            loCampaignModel.TotalVolunteers = liVolunteerCount;
            loCampaignModel.Status = loCampaign.CampaignStatusID;
            loCampaignModel.Charity = loCharity.CharityName == null ? "" : loCharity.CharityName;
            loCampaignModel.PackageLikes = loPackage.Likes;
            loCampaignModel.PackagePrice = loPackage.Price;

            return PartialView("~/Areas/Admin/Views/ManageCampaigns/_CampaignDashboard.cshtml", loCampaignModel);
        }

        public ActionResult declineDashboardCampaignStatus(int Id)
        {
            Campaigns loCampaign = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.Id == Id).FirstOrDefault();
            loCampaign.CampaignStatusID = 3;
            Repository<Campaigns>.UpdateEntity(loCampaign, (entity) => { return entity.Id; });

            Packages loPackage = Repository<Packages>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.PackageID).FirstOrDefault();
            List<Volunteers> loVolunteerList = Repository<Volunteers>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.Id).ToList();
            CampaignUploads loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(null).Where(x => x.CampaignID == loCampaign.Id && x.IsDeleted == false).FirstOrDefault();
            Charities loCharity = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.Id == loCampaign.CharityID).FirstOrDefault();
            CampaignViewModel loCampaignModel = new CampaignViewModel();

            int liLikeCount = 0;
            int liVolunteerCount = 0;
            if (loVolunteerList != null && loVolunteerList.Count > 0)
            {
                foreach (var pac in loVolunteerList)
                {
                    VolunteerModel objComp = new VolunteerModel
                    {
                        Id = pac.Id,
                        Name = pac.Name,
                        Likes = loCampaignUpload == null ? 0 : loCampaignUpload.Likes,
                        ZipCode = pac.ZipCode
                    };
                    liLikeCount = liLikeCount + (loCampaignUpload == null ? 0 : loCampaignUpload.Likes);
                    loCampaignModel.loVolunteerList.Add(objComp);
                }
                liVolunteerCount = loVolunteerList.Count();
            }
            loCampaignModel.Id = Id;
            loCampaignModel.TotalLikes = liLikeCount;
            loCampaignModel.TotalVolunteers = liVolunteerCount;
            loCampaignModel.Status = loCampaign.CampaignStatusID;
            loCampaignModel.Charity = loCharity.CharityName == null ? "" : loCharity.CharityName;
            loCampaignModel.PackageLikes = loPackage.Likes;
            loCampaignModel.PackagePrice = loPackage.Price;

            return PartialView("~/Areas/Admin/Views/ManageCampaigns/_CampaignDashboard.cshtml", loCampaignModel);
        }
        public ActionResult changeFeaturedCampaign(int Id, bool isFeaturedCampaign)
        {
            Campaigns campaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            campaign.IsFeaturedCampaign = isFeaturedCampaign;
            campaign.ModifiedBy = User.Identity.Name;
            campaign.ModifiedOn = DateTime.Now;

            Repository<Campaigns>.UpdateEntity(campaign, (entity) => { return entity.Id; });

            return Json(1, JsonRequestBehavior.AllowGet);
        }
    }
}