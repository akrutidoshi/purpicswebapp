﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Areas.Admin.ViewModels.Package;
using System.Linq.Expressions;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManagePackageController : Controller
    {
        // GET: Admin/ManagePackage
        public ActionResult Index()
        {           
            PackageViewModel foRequest = new PackageViewModel();
            foRequest.stSortColumn = "Name ASC";
            PackagesViewModel loPackageListModel = getPackageList(foRequest);
            //return View(loPatientListModel);
            return View("~/Areas/Admin/Views/ManagePackage/ManagePackage.cshtml", loPackageListModel);
        }

        //public ActionResult ManagePackage()
        //{
        //    //PackagesViewModel objPackagesViewModel = new PackagesViewModel();

        //    //List<PurpicsWeb_DL.Entities.Packages> objPackages = Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(null).OrderBy(x => x.Name).Where(x => x.IsDeleted == false).ToList();

        //    //if (objPackages != null && objPackages.Count > 0)
        //    //{
        //    //    foreach (var pac in objPackages)
        //    //    {
        //    //        PackageViewModel objPackage = new PackageViewModel
        //    //        {
        //    //            ID = pac.ID,
        //    //            CreatedBy = pac.CreatedBy,
        //    //            CreatedOn = pac.CreatedOn,
        //    //            IsActive = pac.IsActive,
        //    //            IsDeleted = pac.IsDeleted,
        //    //            Likes = pac.Likes,
        //    //            ModifiedBy = pac.ModifiedBy,
        //    //            ModifiedOn = pac.ModifiedOn,
        //    //            Name = pac.Name,
        //    //            Price = pac.Price
        //    //        };

        //    //        objPackagesViewModel.loPackagesList.Add(objPackage);
        //    //    }
        //    //}
        //    PackageViewModel foRequest = new PackageViewModel();
        //    foRequest.stSortColumn = "Name ASC";
        //    PackagesViewModel loPackageListModel = getPackageList(foRequest);
        //    //return View(loPatientListModel);
        //    return View("~/Areas/Admin/Views/ManagePackage/ManagePackage.cshtml", loPackageListModel);
        //}

        public PackagesViewModel getPackageList(PackageViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            PackagesViewModel objPackagesViewModel = new PackagesViewModel();
            int liRecordCount = 0;
            Func<IQueryable<Packages>, IOrderedQueryable<Packages>> orderingFunc =
            query => query.OrderBy(x => x.Name);
            Expression<Func<Packages, bool>> expression = null;
            
            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                foRequest.stSearch = HttpUtility.UrlDecode(foRequest.stSearch);
                expression = x => x.Name.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            }
            else
                expression = x => x.IsDeleted == false;

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Name DESC":
                        orderingFunc = q=>q.OrderByDescending(s => s.Name);
                        break;
                    case "Name ASC":
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;
                    case "Likes DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Likes);
                        break;
                    case "Likes ASC":
                        orderingFunc = q => q.OrderBy(s => s.Likes);
                        break;
                    case "Price DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Price);
                        break;
                    case "Price ASC":
                        orderingFunc = q => q.OrderBy(s => s.Price);
                        break;
                    default:  // Name ascending 
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;
                }
            }

            List<Packages> objPackages = new List<Packages>();
            objPackages = Repository<Packages>.GetEntityListForQuery(expression, orderingFunc, null, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();
                
            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.Name.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            }
            else
            {
                liRecordCount = Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false).Count();
            }

            if (objPackages.Count > 0)
            {
                if (objPackages != null && objPackages.Count > 0)
                {
                    foreach (var pac in objPackages)
                    {
                        PackageViewModel objPackage = new PackageViewModel
                        {
                            ID = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsActive = pac.IsActive,
                            IsDeleted = pac.IsDeleted,
                            Likes = pac.Likes,
                            ModifiedBy = pac.ModifiedBy,
                            ModifiedOn = pac.ModifiedOn,
                            Name = pac.Name,
                            Price = pac.Price
                        };

                        objPackagesViewModel.loPackagesList.Add(objPackage);
                    }
                }
            }
            PackagesViewModel loPackageModel = new PackagesViewModel();
            loPackageModel.inRecordCount = liRecordCount;
            loPackageModel.inPageIndex = foRequest.inPageIndex;
            loPackageModel.loPackagesList = objPackagesViewModel.loPackagesList;
            loPackageModel.Pager = new Services.Pager(liRecordCount,foRequest.inPageIndex);
            return loPackageModel;
        }

        public ActionResult searchPackages(PackageViewModel foSearchRequest)
        {
            PackagesViewModel loPackageModel = getPackageList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManagePackage/_ManagePackages.cshtml", loPackageModel);
        }
        [ValidateInput(false)] 
        [HttpPost]
        public ActionResult AddEditPackage(PurpicsWeb.Areas.Admin.ViewModels.Package.PackageViewModel loPackageViewModel)
        {
            if (loPackageViewModel != null)
            {
                if (loPackageViewModel.ID == 0)
                {
                    Packages lowebstats = new Packages();
                    lowebstats.Name = loPackageViewModel.Name;
                    lowebstats.Price = loPackageViewModel.Price;
                    lowebstats.Likes = loPackageViewModel.Likes;
                    lowebstats.IsActive = loPackageViewModel.IsActive;
                    lowebstats.CreatedBy = "1";
                    lowebstats.CreatedOn = DateTime.Now;
                    Repository<Packages>.InsertEntity(lowebstats, entity => { return entity.Id; });
                    TempData["SuccessMsg"] = "Package has been added successfully";
                }
                else
                {
                    Packages lowebstats = Repository<Packages>.GetEntityListForQuery(x => x.Id == loPackageViewModel.ID).FirstOrDefault();

                    lowebstats.Name = loPackageViewModel.Name;
                    lowebstats.Price = loPackageViewModel.Price;
                    lowebstats.Likes = loPackageViewModel.Likes;
                    lowebstats.IsActive = loPackageViewModel.IsActive;
                    lowebstats.ModifiedBy = "1";
                    lowebstats.ModifiedOn = DateTime.Now;
                    Repository<Packages>.UpdateEntity(lowebstats, (entity) => { return entity.Id; });
                    TempData["SuccessMsg"] = "Package has been updated successfully";
                }
            }
            return RedirectToAction("", "ManagePackage");
        }

        [HttpGet]
        public ActionResult AddEditPackage(int? id)
        {
            PackageViewModel objPackageViewModel = new PackageViewModel();

            if (id != null && id > 0)
            {
                Packages objPackages = Repository<Packages>.GetEntityListForQuery(x => x.Id == id).FirstOrDefault();

                objPackageViewModel = new PackageViewModel
                {
                    ID = objPackages.Id,
                    CreatedBy = objPackages.CreatedBy,
                    CreatedOn = objPackages.CreatedOn,
                    IsActive = objPackages.IsActive,
                    IsDeleted = objPackages.IsDeleted,
                    Likes = objPackages.Likes,
                    ModifiedBy = objPackages.ModifiedBy,
                    ModifiedOn = objPackages.ModifiedOn,
                    Name = objPackages.Name,
                    Price = objPackages.Price
                };
            }

            return View("~/Areas/Admin/Views/ManagePackage/AddEditPackage.cshtml", objPackageViewModel);
        }
        
        public ActionResult DeletePackage(PackageViewModel foRequest)
        {
            int liSuccess = 0;
            string lsMessage = string.Empty;
            Campaigns loCampaign = Repository<Campaigns>.GetEntityListForQuery(x => x.PackageID == foRequest.ID && x.CampaignStatusID == 2 && x.IsDeleted == false && x.IsActive == true).FirstOrDefault();
            if (loCampaign != null)
            {
                TempData["ErrorMsg"] = "You can not delete this package as it is in use!";
            }
            else if (foRequest.ID != null && foRequest.ID > 0)
            {
                Packages objPackages = Repository<Packages>.GetEntityListForQuery(x => x.Id == foRequest.ID).FirstOrDefault();
                objPackages.ModifiedBy = "1";
                objPackages.ModifiedOn = DateTime.Now;
                objPackages.IsDeleted = true;
                Repository<Packages>.UpdateEntity(objPackages, (entity) => { return entity.Id; });
                TempData["SuccessMsg"] = "Package has been deleted successfully";
            }
            //PackagesViewModel loUserModel = getUserList(foRequest);
            return this.Json(new PackageViewModel { ID = liSuccess });
        }
    }
}