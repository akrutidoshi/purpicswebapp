﻿using PurpicsWeb.Areas.Admin.ViewModels.CharityPayment;
using PurpicsWeb.Services;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    public class ManageCharityPaymentController : Controller
    {
        // GET: Admin/ManageCharityPayment
        public ActionResult Index()
        {
            CharityPaymentViewModel foRequest = new CharityPaymentViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.IsPaid = false;
            CharityPaymentModel loCampaignListModel = getCharityPaymentList(foRequest);
            return View("~/Areas/Admin/Views/ManageCharityPayment/ManageCharityPayment.cshtml", loCampaignListModel);
        }

        public CharityPaymentModel getCharityPaymentList(CharityPaymentViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CharityPaymentModel objCharityViewModel = new CharityPaymentModel();
            int liRecordCount = 0;
            Func<IQueryable<CharityPaymentHistory>, IOrderedQueryable<CharityPaymentHistory>> orderingFunc =
            query => query.OrderBy(x => x.Id);

            Expression<Func<CharityPaymentHistory, bool>> expression = null;
            string lsSearch = HttpUtility.UrlDecode(foRequest.stSearch);
            if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.IsPaid == foRequest.IsPaid && x.Charities.CharityName.ToLower().Contains(lsSearch.ToLower());
            else 
                expression = x => x.IsPaid == foRequest.IsPaid;

            List<Expression<Func<CharityPaymentHistory, Object>>> Campaignsincludes = new List<Expression<Func<CharityPaymentHistory, object>>>();

            Expression<Func<CharityPaymentHistory, object>> campaignCharity = (campaignCharityName) => campaignCharityName.Charities;
            Campaignsincludes.Add(campaignCharity);

            Expression<Func<CharityPaymentHistory, object>> companyName = (companyname) => companyname.Companies;
            Campaignsincludes.Add(companyName);

            Expression<Func<CharityPaymentHistory, object>> campaignName = (campaignname) => campaignname.Campaigns;
            Campaignsincludes.Add(campaignName);

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "Campaign DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Campaigns.Caption);
                        break;
                    case "Campaign ASC":
                        orderingFunc = q => q.OrderBy(s => s.Campaigns.Caption);
                        break;
                    case "Company DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Companies.CompanyName);
                        break;
                    case "Company ASC":
                        orderingFunc = q => q.OrderBy(s => s.Companies.CompanyName);
                        break;
                    case "Date DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignEndDate);
                        break;
                    case "Date ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignEndDate);
                        break;
                    case "Charity DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Charities.CharityName);
                        break;
                    case "Charity ASC":
                        orderingFunc = q => q.OrderBy(s => s.Charities.CharityName);
                        break;
                    default:
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }
            }

            List<CharityPaymentHistory> objCampaign = new List<CharityPaymentHistory>();
            objCampaign = Repository<CharityPaymentHistory>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, foRequest.inPageIndex, foRequest.inPageSize).ToList();
            //if (!string.IsNullOrEmpty(Convert.ToString(foRequest.stSearch)))
            //{
            //    liRecordCount = Repository<CharityPaymentHistory>.GetEntityListForQuery(null).Where(x => x.IsPaid == foRequest.IsPaid && x.Charities.CharityName.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            //}
            //else
            //{
            //    liRecordCount = Repository<CharityPaymentHistory>.GetEntityListForQuery(null).Where(x => x.IsPaid == foRequest.IsPaid).Count();
            //}
            liRecordCount = Repository<CharityPaymentHistory>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, 1, int.MaxValue).Count();
            
            if (objCampaign.Count > 0)
            {
                if (objCampaign != null && objCampaign.Count > 0)
                {
                    foreach (var pac in objCampaign)
                    {
                        CharityPaymentViewModel objComp = new CharityPaymentViewModel
                        {
                            Id = pac.Id,
                            Charity = pac.Charities.CharityName,
                            CharityID = pac.CharityID,
                            Campaign = pac.Campaigns.Caption,
                            CampaignID = pac.CampaignID,
                            CampaignEndDate = pac.CampaignEndDate,
                            EstAmount = pac.EstAmount,
                            IsPaid = Convert.ToBoolean(pac.IsPaid)
                        };

                        objCharityViewModel.loCharityPaymentList.Add(objComp);
                    }
                }
            }
            CharityPaymentModel loCompanyModel = new CharityPaymentModel();
            loCompanyModel.inRecordCount = liRecordCount;
            loCompanyModel.loCharityPaymentList = objCharityViewModel.loCharityPaymentList;
            loCompanyModel.inPageIndex = foRequest.inPageIndex;
            loCompanyModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCompanyModel;
        }

        public ActionResult searchCharityPaymentList(int inPageIndex, int inPageSize, string stSortColumn, string stSearch, int STATUS)
        {
            CharityPaymentViewModel foSearchRequest = new CharityPaymentViewModel();
            foSearchRequest.inPageIndex = inPageIndex;
            foSearchRequest.inPageSize = inPageSize;
            foSearchRequest.stSortColumn = stSortColumn;
            foSearchRequest.stSearch = stSearch;
            foSearchRequest.IsPaid = Convert.ToBoolean(STATUS);
            CharityPaymentModel loCharityModel = getCharityPaymentList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCharityPayment/_ManageCharityPayment.cshtml", loCharityModel);
        }
        public ActionResult PayCharityPayment(int ID, string fsVal, int inPageIndex, int inPageSize, string stSortColumn, string stSearch, int STATUS)
        {
            string lsMessage = string.Empty;

            CharityPaymentHistory loCampaign = Repository<CharityPaymentHistory>.GetEntityListForQuery(x => x.Id == ID).FirstOrDefault();
            if (loCampaign != null)
            {
                loCampaign.EstAmount = Convert.ToDouble(fsVal);
                if(loCampaign.IsPaid==false)
                {
                    loCampaign.IsPaid = true;
                }
                else
                {
                    loCampaign.IsPaid = false;
                }
                Repository<CharityPaymentHistory>.UpdateEntity(loCampaign, (entity) => { return entity.Id; });
                TempData["SuccessMsg"] = "Payment status updated successfully";
            }

            CharityPaymentViewModel foSearchRequest = new CharityPaymentViewModel();
            foSearchRequest.inPageIndex = inPageIndex;
            foSearchRequest.inPageSize = inPageSize;
            foSearchRequest.stSortColumn = stSortColumn;
            foSearchRequest.stSearch = stSearch;
            foSearchRequest.IsPaid = Convert.ToBoolean(STATUS);
            CharityPaymentModel loCharityModel = getCharityPaymentList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCharityPayment/_ManageCharityPayment.cshtml", loCharityModel);
            //return this.Json(new CharityPaymentViewModel { Id = liSuccess });
        }
        public ActionResult CharityPayment(int ID, string fsVal, int inPageIndex, int inPageSize, string stSortColumn, string stSearch, int STATUS)
        {
            int liSuccess = 0;
            string lsMessage = string.Empty;

            CharityPaymentHistory loCampaign = Repository<CharityPaymentHistory>.GetEntityListForQuery(x => x.Id == ID).FirstOrDefault();
            if (loCampaign != null)
            {
                loCampaign.EstAmount = Convert.ToDouble(fsVal);
                Repository<CharityPaymentHistory>.UpdateEntity(loCampaign, (entity) => { return entity.Id; });
                TempData["SuccessMsg"] = "updated successfully";
            }
            CharityPaymentViewModel foSearchRequest = new CharityPaymentViewModel();
            foSearchRequest.inPageIndex = inPageIndex;
            foSearchRequest.inPageSize = inPageSize;
            foSearchRequest.stSortColumn = stSortColumn;
            foSearchRequest.stSearch = stSearch;
            foSearchRequest.IsPaid = Convert.ToBoolean(STATUS);
            CharityPaymentModel loCharityModel = getCharityPaymentList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCharityPayment/_ManageCharityPayment.cshtml", loCharityModel);
            //return this.Json(new CharityPaymentViewModel { Id = liSuccess });
        }
    }
}