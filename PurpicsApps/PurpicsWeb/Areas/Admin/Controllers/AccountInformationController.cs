﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Areas.Admin.ViewModels.AccountInformation;
using Microsoft.AspNet.Identity;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using PurpicsWeb.Services.CommonFunctions;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Text;


namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AccountInformationController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        // GET: Admin/AccountInformation
        public ActionResult Index()
        {
            AccountEmail objAccountInformtion = new AccountEmail();
            string userid = HttpContext.User.Identity.GetUserId();
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == userid).FirstOrDefault();
            objAccountInformtion.EmailAddress = user.Email;
            objAccountInformtion.OldPassword = "********";
            //objAccountInformtion.Password = user.PasswordHash;
            return View("~/Areas/Admin/Views/AccountInformation/AccountInformation.cshtml", objAccountInformtion);
        }
        //public ActionResult AccountInformation()
        //{
        //    AccountInformation objAccountInformtion = new AccountInformation();
        //    string userid = HttpContext.User.Identity.GetUserId();
        //    AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == userid).FirstOrDefault();
        //    objAccountInformtion.EmailAddress = user.Email;
        //    //objAccountInformtion.Password = user.PasswordHash;
        //    return View(objAccountInformtion);
        //}
        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        public async Task<ActionResult> ChangePassword(AccountInformation model)
        {
            //AccountEmail objAccountInformtion = new AccountEmail();
            //string userid = HttpContext.User.Identity.GetUserId();
            //AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == userid).FirstOrDefault();
            //objAccountInformtion.EmailAddress = user.Email;
            //model.EmailAddress = user.Email;

            //if (!ModelState.IsValid)
            //{
            //    return View("~/Areas/Admin/Views/AccountInformation/AccountInformation.cshtml", objAccountInformtion);
            //}
            //var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            //if (result.Succeeded)
            //{
            //    TempData["SuccessConfirmationMessage"] = "Your password has been changed.";
            //    return RedirectToAction("", "AccountInformation");
            //}
            //AddErrors(result);
            //return View("~/Areas/Admin/Views/AccountInformation/AccountInformation.cshtml", objAccountInformtion);



            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return Json(-2, JsonRequestBehavior.AllowGet);//Invalid AspNetUserId.

            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return Json(new AccontStatus { Status = 1, Message = "Password changed successfully" }, JsonRequestBehavior.AllowGet);//Password changed
            }
            else
            {
                StringBuilder errorMessages = new StringBuilder();
                foreach( var message in result.Errors)
                {
                    errorMessages.Append(message);
                }

                return Json(new AccontStatus { Status=-1 ,Message=errorMessages.ToString() }, JsonRequestBehavior.AllowGet);//Old password is not valid.
            }
        }
        public ActionResult EditEmail()
        {
            return View();
        }
        public ActionResult EditPassword()
        {
            AccountInformation objAccountInformtion = new AccountInformation();
            string userid = HttpContext.User.Identity.GetUserId();
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == userid).FirstOrDefault();
            objAccountInformtion.EmailAddress = user.Email;
            //objAccountInformtion.Password = user.PasswordHash;
            return View("~/Areas/Admin/Views/AccountInformation/EditPassword.cshtml", objAccountInformtion);
        }


        //
        // POST: /Manage/Email
        [HttpPost]
        public async Task<ActionResult> ChangeEmail(AccountEmail model)
        {
            AccountEmail objAccountInformtion = new AccountEmail();
            string userid = HttpContext.User.Identity.GetUserId();

            bool lsIsExist = IsEmailAlreadyExist(model.EmailAddress, userid);
            if (lsIsExist == false)
            {
                try
                {
                    //ApplicationDbContext context = new ApplicationDbContext();
                    //UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
                    //ApplicationUser cUser = await store.FindByIdAsync(userid);
                    //cUser.UserName = model.EmailAddress;
                    //cUser.Email = model.EmailAddress;
                    //await store.UpdateAsync(cUser);
                    //await SignInManager.SignInAsync(cUser, true, true);
                    
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    user.Email = model.EmailAddress;
                    user.UserName = model.EmailAddress;
                    var result = await UserManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, true, true);
                        return Json(new AccontStatus { Status=1 ,Message="Your email has been changed." }, JsonRequestBehavior.AllowGet);//Password changed
                    }
                    else
                    {
                        StringBuilder errorMessages = new StringBuilder();
                        foreach (var message in result.Errors)
                        {
                            errorMessages.Append(message);
                        }
                        return Json(new AccontStatus { Status = 0, Message = errorMessages.ToString() }, JsonRequestBehavior.AllowGet);//Password changed
                    }

                    //TempData["SuccessConfirmationMessage"] = "Your email has been changed.";
                }
                catch
                {
                    //TempData["ErrorMsg"] = "An error occured while updating email.";
                    return Json(new AccontStatus { Status = -1, Message = "An error occured while updating email." }, JsonRequestBehavior.AllowGet);//Password changed
                }
            }
            else
            {
                //TempData["ErrorMsg"] = "Email is already exist.";
                return Json(new AccontStatus { Status = -1, Message = "Email is already exist." }, JsonRequestBehavior.AllowGet);//Password changed

            }
            //return RedirectToAction("", "AccountInformation");
            //return View("~/Areas/Admin/Views/AccountInformation/AccountInformation.cshtml", objAccountInformtion);
        }
        private bool IsEmailAlreadyExist(string fsEmail, string fsId)
        {
            bool lsIsExist = false;
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Email == fsEmail && x.Id != fsId).FirstOrDefault();
            if (user != null)
                lsIsExist = true;

            return lsIsExist;
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}