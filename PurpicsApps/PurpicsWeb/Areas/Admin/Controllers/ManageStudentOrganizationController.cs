﻿using Hangfire;
using PurpicsWeb.Areas.Admin.ViewModels.StudentOrganization;
using PurpicsWeb.Services;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PurpicsWeb.Areas.StudentOrganization.Models;
namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManageStudentOrganizationController : Controller
    {
        // GET: Admin/ManageStudentOrganization
        public ActionResult Index()
        {
            StudentOrganizationViewModel foRequest = new StudentOrganizationViewModel();
            foRequest.stSortColumn = "Id ASC";
            StudentOrganizationModel loSOListModel = getSOList(foRequest);
            return View("~/Areas/Admin/Views/ManageStudentOrganization/ManageStudentOrganization.cshtml", loSOListModel);
        }

        public StudentOrganizationModel getSOList(StudentOrganizationViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                foRequest.stSearch = foRequest.stSearch.Replace("%20", " ");
            }

            StudentOrganizationModel objStudentOrganizationViewModel = new StudentOrganizationModel();
            int liRecordCount = 0;
            Func<IQueryable<StudentOrganizations>, IOrderedQueryable<StudentOrganizations>> orderingFunc =
            query => query.OrderBy(x => x.Id);

            Expression<Func<StudentOrganizations, bool>> expression = null;

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.UserStatusID)) && foRequest.UserStatusID != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.UserStatusID == foRequest.UserStatusID && x.Name.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.UserStatusID)) && foRequest.UserStatusID != 0)
                expression = x => x.UserStatusID == foRequest.UserStatusID && x.IsDeleted == false;
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.Name.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else
                expression = x => x.IsDeleted == false;
            List<Expression<Func<StudentOrganizations, Object>>> inclStudentOrganizations = new List<Expression<Func<StudentOrganizations, object>>>();
            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "CharityName DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Name);
                        break;
                    case "CharityName ASC":
                        orderingFunc = q => q.OrderBy(s => s.Name);
                        break;
                    default:
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }
            }
            List<StudentOrganizations> objCampaign = new List<StudentOrganizations>();
            objCampaign = Repository<StudentOrganizations>.GetEntityListForQuery(expression, orderingFunc, inclStudentOrganizations, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.UserStatusID)) && foRequest.UserStatusID != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<StudentOrganizations>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.Name.ToLower().Contains(foRequest.stSearch.ToLower()) && x.UserStatusID == foRequest.UserStatusID).Count();
            }
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                liRecordCount = Repository<StudentOrganizations>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.Name.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.UserStatusID)) && foRequest.UserStatusID != 0)
            {
                liRecordCount = Repository<StudentOrganizations>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.UserStatusID == foRequest.UserStatusID).Count();
            }
            else
            {
                liRecordCount = Repository<StudentOrganizations>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false).Count();
            }

            if (objCampaign.Count > 0)
            {
                if (objCampaign != null && objCampaign.Count > 0)
                {
                    foreach (var pac in objCampaign)
                    {
                        StudentOrganizationViewModel objComp = new StudentOrganizationViewModel
                        {
                            Id = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsDeleted = pac.IsDeleted,
                            ModifiedBy = pac.ModifiedBy,
                            ModifiedOn = pac.ModifiedOn,
                            Name = pac.Name,
                            UserStatusID = pac.UserStatusID
                        };

                        objStudentOrganizationViewModel.loStudentOrganizationList.Add(objComp);
                    }
                }
            }
            StudentOrganizationModel loCompanyModel = new StudentOrganizationModel();
            loCompanyModel.inRecordCount = liRecordCount;
            loCompanyModel.loStudentOrganizationList = objStudentOrganizationViewModel.loStudentOrganizationList;
            loCompanyModel.inPageIndex = foRequest.inPageIndex;
            loCompanyModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCompanyModel;
        }
        public ActionResult searchStudentOrganization(StudentOrganizationViewModel foSearchRequest)
        {
            StudentOrganizationModel loStudentOrganizationModel = getSOList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageStudentOrganization/_ManageStudentOrganization.cshtml", loStudentOrganizationModel);
        }
        public ActionResult searchStudentOrganizations(int inPageIndex, int inPageSize, string stSortColumn, string stSearch, int STATUS)
        {

            StudentOrganizationViewModel foSearchRequest = new StudentOrganizationViewModel();
            foSearchRequest.inPageIndex = inPageIndex;
            foSearchRequest.inPageSize = inPageSize;
            foSearchRequest.stSortColumn = stSortColumn;
            foSearchRequest.stSearch = stSearch;
            foSearchRequest.UserStatusID = STATUS;
            StudentOrganizationModel loStudentOrganizationModel = getSOList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageStudentOrganization/_ManageStudentOrganization.cshtml", loStudentOrganizationModel);
        }
        public ActionResult StudentOrganizationDetail(int fiSOId)
        {
            StudentOrganizationViewModel loCompanyViewModel = GetSOModel(fiSOId);
            return View(loCompanyViewModel);
        }

        private StudentOrganizationViewModel GetSOModel(int fiSOId)
        {
            StudentOrganizations lowebstats = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.Id == fiSOId).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();

            StudentOrganizationViewModel loCompanyViewModel = new StudentOrganizationViewModel();
            loCompanyViewModel.Id = fiSOId;
            loCompanyViewModel.UserStatusID = lowebstats.UserStatusID;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.Company = "-";
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser.UserName;
            loCompanyViewModel.LogoFileName = lowebstats.LogoFileName;
            loCompanyViewModel.Name = lowebstats.Name;
            loCompanyViewModel.SOCharities = getSOCharityList(loCompanyViewModel.Id);
            return loCompanyViewModel;
        }
        public List<SOCharities> getSOCharityList(int Id)
        {

            Expression<Func<Charities, bool>> expression = x => x.IsActive == true && x.IsDeleted == false && x.UserStatusID == (int)CommonEnums.CharityApprovalStatus.Approved;

            //Include 
            List<Expression<Func<Charities, Object>>> includeSO = new List<Expression<Func<Charities, object>>>();

            //Sort
            Func<IQueryable<Charities>, IOrderedQueryable<Charities>> orderingFunc = query => query.OrderBy(x => x.CharityName);

            List<Charities> loSOCharities = Repository<Charities>.GetEntityListForQuery(expression, null, includeSO, null, null).ToList();
            List<SOCharities> model = new List<SOCharities>();
            if (loSOCharities != null && loSOCharities.Count > 0)
            {
                foreach (var charity in loSOCharities)
                {
                    StudentOrganizationsCharities studentOrganizationsCharities = Repository<StudentOrganizationsCharities>.GetEntityListForQuery(x => x.CharityId == charity.Id && x.StudentOrganizationId == Id).FirstOrDefault();

                    SOCharities objComp = new SOCharities();
                    objComp.CharityId = charity.Id;
                    objComp.CharityName = charity.CharityName;
                    objComp.IsSelected = studentOrganizationsCharities == null ? false : true;
                    model.Add(objComp);
                }
            }

            return model;
        }
        public ActionResult approveStudentOrganization(int Id)
        {
            StudentOrganizations lowebstats = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            AspNetUsers loLoginUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == User.Identity.Name).FirstOrDefault();
            string userid = loLoginUser.Id;
            StudentOrganizationViewModel model = GetSOModel(Id);
            try
            {
                UpdateSOAsApproved(Id);
                model.UserStatusID = (int)CommonEnums.UserStatus.Approved;
            }
            catch (Exception)
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
            }

            return PartialView("~/Areas/Admin/Views/ManageStudentOrganization/_StudentOrganizationDetail.cshtml", model);
        }

        private void UpdateSOAsApproved(int Id)
        {
            StudentOrganizations lowebstats = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            lowebstats.UserStatusID = (int)CommonEnums.UserStatus.Approved;
            Repository<StudentOrganizations>.UpdateEntity(lowebstats, (entity) => { return entity.Id; });
            Helper.CommonFunctions.sendSOStatusMail(lowebstats.Name, (int)CommonEnums.UserStatus.Approved, loUser.Email);
            TempData["SuccessMsg"] = "Student Organization has been Approved.";
        }

        public ActionResult declineSOStatus(int Id)
        {
            StudentOrganizations lowebstats = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            StudentOrganizationViewModel model = GetSOModel(Id);
            try
            {
                lowebstats.UserStatusID = (int)CommonEnums.UserStatus.Denied;
                Repository<StudentOrganizations>.UpdateEntity(lowebstats, (entity) => { return entity.Id; });
                Helper.CommonFunctions.sendSOStatusMail(lowebstats.Name, (int)CommonEnums.UserStatus.Denied, loUser.Email);
                TempData["SuccessMsg"] = "Student Organization has been Declined.";
            }
            catch (Exception)
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
            }

            return PartialView("~/Areas/Admin/Views/ManageStudentOrganization/_StudentOrganizationDetail.cshtml", model);
        }
    }
}