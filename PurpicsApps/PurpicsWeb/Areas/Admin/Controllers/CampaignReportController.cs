﻿using PurpicsWeb.Areas.Admin.ViewModels.CampaignReport;
using PurpicsWeb.Services;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Services.CommonFunctions;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CampaignReportController : Controller
    {
        // GET: Admin/CampaignReport
        public ActionResult Index()
        {
            CampaignViewModel foRequest = new CampaignViewModel();
            foRequest.stSortColumn = "Company ASC";
            CampaignReportModel loCampaignListModel = getCampaignList(foRequest);
            return View("~/Areas/Admin/Views/CampaignReport/CampaignReport.cshtml", loCampaignListModel);
        }

        public CampaignReportModel getCampaignList(CampaignViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CampaignReportModel objCampaignViewModel = new CampaignReportModel();
            int liRecordCount = 0;
            Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderingFunc =
            query => query.OrderBy(x => x.Id);

            Expression<Func<Campaigns, bool>> expression = null;


            DateTime? ldFromDate = null;
            if (!string.IsNullOrEmpty(foRequest.lsFromDate))
                ldFromDate = Convert.ToDateTime(foRequest.lsFromDate + " 00:00:01");

            DateTime? ldToDate = null;
            if (!string.IsNullOrEmpty(foRequest.lsToDate))
                ldToDate = Convert.ToDateTime(foRequest.lsToDate + " 23:59:59");


            if(!string.IsNullOrEmpty(foRequest.lsFromDate) && !string.IsNullOrEmpty(foRequest.lsToDate) && foRequest.selectedCompanyID != 0)
                expression = x => x.CompanyID == foRequest.selectedCompanyID && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignStartDate >= ldFromDate && x.CampaignEndDate <= ldToDate;
            else if (!string.IsNullOrEmpty(foRequest.lsFromDate) && !string.IsNullOrEmpty(foRequest.lsToDate))
                expression = x => x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignStartDate >= ldFromDate && x.CampaignEndDate <= ldToDate;


            else if(!string.IsNullOrEmpty(foRequest.lsFromDate) && foRequest.selectedCompanyID != 0)
                expression = x => x.CompanyID == foRequest.selectedCompanyID && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignStartDate >= ldFromDate;

            else if (!string.IsNullOrEmpty(foRequest.lsToDate) && foRequest.selectedCompanyID != 0)
                expression = x => x.CompanyID == foRequest.selectedCompanyID && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignEndDate <= ldToDate;

            else if (!string.IsNullOrEmpty(foRequest.lsFromDate))
                expression = x =>  x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignStartDate >= ldFromDate;
            else if (!string.IsNullOrEmpty(foRequest.lsToDate))
                expression = x =>  x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignEndDate <= ldToDate;

            else if (foRequest.selectedCompanyID != 0)
                expression = x => x.CompanyID == foRequest.selectedCompanyID && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved;           
            else
                expression = x => x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved;

            //Include 
            List<Expression<Func<Campaigns, Object>>> Campaignsincludes = new List<Expression<Func<Campaigns, object>>>();

            Expression<Func<Campaigns, object>> companyName = (companyname) => companyname.Companies;
            Campaignsincludes.Add(companyName);

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "Company DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Companies.CompanyName).ThenBy(s => s.Id);
                        break;
                    case "Company ASC":
                        orderingFunc = q => q.OrderBy(s => s.Companies.CompanyName).ThenBy(s => s.Id);
                        break;
                    case "Likes DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Likes);
                        break;
                    case "Likes ASC":
                        orderingFunc = q => q.OrderBy(s => s.Likes);
                        break;
                    case "PackageLikes DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.PackageLikes);
                        break;
                    case "PackageLikes ASC":
                        orderingFunc = q => q.OrderBy(s => s.PackageLikes);
                        break;
                    case "CampaignStartDate DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignStartDate);
                        break;
                    case "CampaignStartDate ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignStartDate);
                        break;
                    case "CampaignEndDate DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignEndDate);
                        break;
                    case "CampaignEndDate ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignEndDate);
                        break;
                    case "CampaignStatusID ASC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignStatusID).ThenByDescending(s => s.CampaignEndDate);
                        break;
                    case "CampaignStatusID DESC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignStatusID).ThenBy(s => s.CampaignEndDate);
                        break;
                    case "TimeToReachTarget ASC":
                        orderingFunc = q => q.OrderBy(s =>  (System.Data.Entity.DbFunctions.DiffDays(s.CampaignStartDate, s.CampaignEndDate)));
                        break;
                    case "TimeToReachTarget DESC":
                        orderingFunc = q => q.OrderByDescending(s => (System.Data.Entity.DbFunctions.DiffDays(s.CampaignStartDate, s.CampaignEndDate)));
                        break;
                    default:
                        orderingFunc = q => q.OrderBy(s => s.Companies.CompanyName).ThenBy(s => s.Id);
                        break;
                }
            }

            //List<Campaigns> objCampaign = new List<Campaigns>();
            //objCampaign = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();


            List<Expression<Func<Campaigns, Object>>> campaigns = new List<Expression<Func<Campaigns, object>>>();


            List<Campaigns> objallCampaigns = new List<Campaigns>();
            objallCampaigns = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, foRequest.inPageIndex, foRequest.inPageSize).ToList();
            liRecordCount = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, campaigns).Count();
            if (objallCampaigns.Count > 0)
            {
                foreach (var campaign in objallCampaigns)
                {
                    CampaignViewModel objcampaign = new CampaignViewModel
                    {
                        Company = campaign.Companies.CompanyName,
                        CompanyID = campaign.CompanyID,
                        CampaignStatusID = campaign.CampaignStatusID,
                        Id = campaign.Id,                        
                        Likes = campaign.Likes,
                        PackageLikes = campaign.PackageLikes,
                        CampaignStartDate = campaign.CampaignStartDate,
                        CampaignEndDate = campaign.CampaignEndDate,                        
                        Days = getCampaignTargetReachDays(campaign.Id)
                    };
                    objCampaignViewModel.loCampaignList.Add(objcampaign);

                }
            }




            //if (objCampaign.Count > 0)
            //{
            //    if (objCampaign != null && objCampaign.Count > 0)
            //    {
            //        foreach (var pac in objCampaign)
            //        {
            //            CampaignViewModel objComp = new CampaignViewModel
            //            {
                          
            //                CompanyID = pac.Companies.Id,
            //                Company = pac.Companies.CompanyName,
            //                selectedCompanyID = pac.CompanyID,
            //                CampaignStatusID = pac.CampaignStatusID
            //            };

            //            objCampaignViewModel.loCampaignList.Add(objComp);
            //        }
            //    }
            //}
            CampaignReportModel loCompanyModel = new CampaignReportModel();
            loCompanyModel.inRecordCount = liRecordCount;
            loCompanyModel.loCampaignList = objCampaignViewModel.loCampaignList;
            loCompanyModel.inPageIndex = foRequest.inPageIndex;
            loCompanyModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);

            List<Companies> loCompanyNames = new List<Companies>();
            loCompanyNames = Repository<Companies>.GetEntityListForQuery(x => x.IsDeleted == false).OrderBy(x => x.CompanyName).ToList();
            loCompanyModel.loCompanyName = loCompanyNames;
            return loCompanyModel;
        }

        public ActionResult searchCampaign(CampaignViewModel foSearchRequest)
        {
            CampaignReportModel loCampaignModel = getCampaignList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/CampaignReport/_CampaignReport.cshtml", loCampaignModel);
        }
        public int getCampaignTargetReachDays(int fiCampaignId)
        {
            int liDays = 0;
            Campaigns campaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == fiCampaignId).FirstOrDefault();
            if (campaign != null && campaign.CampaignEndDate != null && campaign.CampaignStartDate != null)
            {
                liDays = Convert.ToInt32((Convert.ToDateTime(campaign.CampaignEndDate) - Convert.ToDateTime(campaign.CampaignStartDate)).TotalDays);
            }
            return liDays;
        }
    }

    
}