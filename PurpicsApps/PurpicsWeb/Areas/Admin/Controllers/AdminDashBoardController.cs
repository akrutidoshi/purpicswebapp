﻿using PurpicsWeb.Areas.Admin.ViewModels;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminDashBoardController : Controller
    {
        // GET: Admin/AdminDashBoard
        public ActionResult Index()
        {
            DashboardViewModel foRequest = new DashboardViewModel();
            foRequest.stSortColumn = "Id ASC";
            DashboardModel loCampaignListModel = getCampaignList(foRequest);
            return View("~/Areas/Admin/Views/AdminDashBoard/DashBoard.cshtml", loCampaignListModel);
        }

        public DashboardModel getCampaignList(DashboardViewModel foRequest)
        {
            foRequest.inPageSize = 5;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            DashboardModel objCampaignViewModel = new DashboardModel();
            int liRecordCount = 0;
            Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderingFunc =
            query => query.OrderByDescending(x => x.Id);
     
            Expression<Func<Campaigns, bool>> expression = null;
            expression = x => x.IsDeleted == false && x.IsActive == true && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved;

            List<Expression<Func<Campaigns, Object>>> Campaignsincludes = new List<Expression<Func<Campaigns, object>>>();

            Expression<Func<Campaigns, object>> campaignCharity = (campaignCharityName) => campaignCharityName.Charities;
            Campaignsincludes.Add(campaignCharity);

            Expression<Func<Campaigns, object>> companyName = (companyname) => companyname.Companies;
            Campaignsincludes.Add(companyName);


            List<Campaigns> objCampaign = new List<Campaigns>();
            objCampaign = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();

            int liCharities = Repository<Charities>.GetEntityListForQuery(null).OrderBy(x => x.Id).Where(x => x.IsDeleted == false && x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved).Count();

            List<Volunteers> loVolunteerList = Repository<Volunteers>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.IsActive == true).ToList();
            liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.IsDeleted == false && x.IsActive == true && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved).Count();
            if (objCampaign.Count > 0)
            {
                if (objCampaign != null && objCampaign.Count > 0)
                {
                    foreach (var pac in objCampaign)
                    {
                        DashboardViewModel objComp = new DashboardViewModel
                        {
                            Id = pac.Id,
                            CreatedBy = pac.CreatedBy,
                            CreatedOn = pac.CreatedOn,
                            IsDeleted = pac.IsDeleted,
                            ModifiedBy = pac.ModifiedBy,
                            ModifiedOn = pac.ModifiedOn,
                            Caption = pac.Caption,
                            CampaignName=pac.CampaignName,
                            CharityID = pac.Charities.Id,
                            Charity = pac.Charities.CharityName,
                            CompanyID = pac.Companies.Id,
                            Company = pac.Companies.CompanyName,
                            CampaignStatusID = pac.CampaignStatusID,
                            Likes = pac.Likes
                        };

                        objCampaignViewModel.loCampaignList.Add(objComp);
                    }
                }
            }
     
            DashboardModel loCompanyModel = new DashboardModel();
            loCompanyModel.inRecordCount = liRecordCount;
            loCompanyModel.TotalVolunteers = loVolunteerList.Count();
            loCompanyModel.TotalCharities = liCharities;
            loCompanyModel.loCampaignList = objCampaignViewModel.loCampaignList;
            return loCompanyModel;
        }

        public ActionResult searchCampaign(DashboardViewModel foSearchRequest)
        {
            DashboardModel loCampaignModel = getCampaignList(foSearchRequest);
            return PartialView("~/Areas/Admin/Views/ManageCampaigns/_ManageCampaigns.cshtml", loCampaignModel);
        }
    }
}