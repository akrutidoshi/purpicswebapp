﻿$(document).ready(function () {
    //SetPaging();
    setSortingArrow();
    $('#txtSearch').focus();
});
function userSearch(e) {

    if (e.which == 13) {
        debugger
        e.preventDefault();
        $(this).blur();
        $('#btnSearch').focus().click();
    }
}

function search() {
    var stSearch = "";
    var stToDate = "";
    if ($('#txtSearch').val() != "") {
        stSearch = $('#txtSearch').val();
    }
    // Do some operation
    refeshList(stSearch);
}
function statusSearch(val) {
    $('#hdnStatus').val(val);
    refeshList("load");
}
function refeshList(foId, pageIndex) {
    debugger
    if (pageIndex == undefined)
        pageIndex = 0;
    
    //var pageIndex = 0;
    var stSortField = "";
    var lsStatus = parseInt($('#hdnStatus').val());
    var lsSearch = $('#txtSearch').val().trim();
    var lsOriginalSearch = $('#txtSearch').val().trim();
    var lsSearch = lsSearch.replace(/'/g, "''");
    lsSearch = encodeURIComponent(lsSearch);
    if (foId == "aHrefNext") {
        //pageIndex = parseInt($('#hdnPageIndex').val()) + 1;
        $('#hdnPageIndex').val(pageIndex);
    }
    else if (foId == "aHrefPrev") {
        //pageIndex = parseInt($('#hdnPageIndex').val()) - 1;
        $('#hdnPageIndex').val(pageIndex);
    }
    else if (foId == "load") {
        pageIndex = 1;
    }
    else if (foId == "Id ASC" || foId == "Id DESC" || foId == "CharityName ASC" || foId == "CharityName DESC") {
        pageIndex = parseInt($('#hdnPageIndex').val());
    }
    //else {
    //    pageIndex = 1;
    //}
    
    $.ajax({
        type: "POST",
        url: lsgetCharityList,
        content: "application/json; charset=utf-8",
        dataType: "html",
        data: {
            inPageIndex: pageIndex,
            inPageSize: 10,
            stSortColumn: $('#hdnOrder').val(),
            stSearch: lsSearch,
            STATUS: lsStatus
        },
        success: function (lodata) {
            
            if (lodata != null) {
                var divSOReleased = $('#companylist');
                divSOReleased.html('');
                divSOReleased.html(lodata);
                $('#hdnPageIndex').val(pageIndex);
                //SetPaging();
                $('.dashboard-menu').height('100%');
                $('.dashboard-menu').height($(document).height());
                setSortingArrow();
                $('#txtSearch').val(lsOriginalSearch);
                $('#txtSearch').focus();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            
            $('#txtSearch').val(lsOriginalSearch);
            $('#txtSearch').focus();
            if (errorThrown == "abort") {
                return;
            }
            else {
                alert(errorThrown);
            }
        }
    });
}
function getOrderbyCompanyList(foOrderedField, tdID) {
    var foFieldwithOrder = "";
    $('#hdnSortingOnColumn').val(tdID);

    if ($('#hdnOrder').val() == "") {
        $('#hdnOrder').val(foOrderedField + " ASC");
        foFieldwithOrder = foOrderedField + " ASC";
        $('#hdnOrder').val(foFieldwithOrder);
        $('#hdnSortingDirection').val("ASC");

    }
    else {
        if ($('#hdnOrder').val() == (foOrderedField + " ASC")) {
            foFieldwithOrder = foOrderedField + " DESC";
            $('#hdnOrder').val(foFieldwithOrder);
            $('#hdnSortingDirection').val("DESC");
        }
        else {
            foFieldwithOrder = foOrderedField + " ASC";
            $('#hdnOrder').val(foFieldwithOrder);
            $('#hdnSortingDirection').val("ASC");
        }
    }
    refeshList(foFieldwithOrder);
}

//function SetPaging() {

//    var inPagesize = 10;

//    $('#aHrefPrev').parent("li").attr('class', 'disabled');
//    $('#aHrefPrev').removeAttr("onclick", "javascript:void(0);");

//    pageIndex = $('#hdnPageIndex').val();
//    totalRec = $('#hdnTotalRec').val();

//    if (pageIndex == 1) {
//        $('#aHrefPrev').parent("li").attr('class', 'disabled');
//        $('#aHrefPrev').attr("onclick", "javascript:void(0);");
//    }
//    else {
//        $('#aHrefPrev').parent("li").removeAttr('class', 'disabled');
//        $('#aHrefPrev').attr("onclick", "refeshList(this.id)");
//    }
//    if (pageIndex * inPagesize >= totalRec) {
//        $('#aHrefNext').parent("li").attr('class', 'disabled');
//        $('#aHrefNext').attr("onclick", "javascript:void(0);");
//    }
//    if (inPagesize >= totalRec) {
//        $('#Patientpaging').css('visibility', 'hidden');
//    }
//    else {
//        $('#Patientpaging').css('visibility', 'visible');
//    }
//}

function setSortingArrow() {

    var sortTD = $('#hdnSortingOnColumn').val();
    var sortDirection = $('#hdnSortingDirection').val();

    if (sortTD != "") {
        $('#' + sortTD).removeClass("sorting");
        if (sortDirection != null) {
            if (sortDirection.toUpperCase() == "ASC") {
                $('#' + sortTD).addClass("sorting_asc");
            }
            else if (sortDirection.toUpperCase() == "DESC") {
                $('#' + sortTD).addClass("sorting_desc");
            }
        }
    }
    else {
        $('#sortId').removeClass("sorting");
        $('#sortId').addClass("sorting_asc");
        $('#hdnOrder').val("Id ASC");
    }
}
