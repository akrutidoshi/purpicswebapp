﻿using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Admin.ViewModels.RedeemGiftCard
{
    public class RedeemGiftCardModel
    {
        public RedeemGiftCardModel()
        {
            loVolunteersList = new List<VolunteerViewModel>();
        }

        public List<VolunteerViewModel> loVolunteersList { get; set; }
        public Int64 inRecordCount { get; set; }
        public int inPageIndex { get; set; }
        
        public Pager Pager { get; set; }
    }
    public class VolunteerViewModel
    {
        public int CampaignId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string GiftCardPreferenceName { get; set; }
        public int CreditPoints { get; set; }

        public string stPointSearch { get; set; }
        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public VolunteerViewModel()
        {
            loVolunteerCampaignList = new List<VolunteersCampaign>();
        }
        public List<VolunteersCampaign> loVolunteerCampaignList { get; set; }
        public int? GiftCardPreference { get; set; } // GiftCardPreference
        public string GiftCard { get; set; }
        public bool IsDisplaySendGiftCardButton { get; set; }
        public int TotalLikes { get; set; }
        public int TotalRedeem { get; set; }
    }

    public class VolunteersCampaign
    {
        public string Campaign { get; set; }
        public int Likes { get; set; }
        public int RedeemPoint { get; set; }
    }


}