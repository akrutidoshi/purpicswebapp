﻿using PurpicsWeb.Areas.StudentOrganization.Models;
using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Admin.ViewModels.StudentOrganization
{
    public class StudentOrganizationModel
    {
        public StudentOrganizationModel()
        {
            loStudentOrganizationList = new List<StudentOrganizationViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public List<StudentOrganizationViewModel> loStudentOrganizationList { get; set; }
        public int inPageIndex { get; set; }
        public Pager Pager { get; set; }	

    }
    public class StudentOrganizationViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // CharityName
        public string Address { get; set; } // Address
        public string ContactPerson { get; set; } // ContactPerson
        public string ContactPhone { get; set; } // ContactPhone
        public string LogoFileName { get; set; } // LogoFileName
        public string AspNetUserID { get; set; } // AspNetUserID
        public string AspNetUsername { get; set; }
        public int UserStatusID { get; set; } // UserStatusID
        public int CategoryID { get; set; } // CategoryID
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string stSortColumn { get; set; }
        public string stSearch { get; set; }
        public string Company { get; set; }
        public string StudetnOrganization { get; set; }
        public string Status { get; set; }
        public int inPageSize { get; set; }
        public int inPageIndex { get; set; }
        public List<SOCharities> SOCharities { get; set; }
    }
 

    public class NonProfitPartnersViewModel
    {
        public NonProfitPartnersViewModel()
        {
            loNonProfitPartnerList = new List<NonProfitPartnerViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public int inPageIndex { get; set; }
        public List<NonProfitPartnerViewModel> loNonProfitPartnerList { get; set; }
        public Pager Pager { get; set; }
    }
    public class NonProfitPartnerViewModel
    {
        public int ID { get; set; } // ID (Primary key)
        [Required(ErrorMessage = "Non Profit Partner Name should not be empty.")]
        public string Name { get; set; } // Name
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
    }
}