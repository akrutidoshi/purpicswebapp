﻿using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Admin.ViewModels.GiftCard
{
    public class GiftCardModel
    {
        public GiftCardModel()
        {
            loGiftCardsList = new List<GiftCardViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public int inPageIndex { get; set; }
        public List<GiftCardViewModel> loGiftCardsList { get; set; }
        public Pager Pager { get; set; }
    }
    public class GiftCardViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        [Required(ErrorMessage = "Giftcard name should not be empty.")]
        public string Name { get; set; } // Name
        [Required(ErrorMessage = "pointcost should not be empty.")]
        public int PointCost { get; set; } // PointCost
        public System.Data.Entity.Spatial.DbGeography Latitude { get; set; } // Latitude
        public System.Data.Entity.Spatial.DbGeography Longitude { get; set; } // Longitude
        [Required(ErrorMessage = "Please select Miles.")]
        public int? Radius { get; set; } // Radius
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        [Required(ErrorMessage = "Zipcode should not be empty.")]
        public string Zipcode { get; set; } // Zipcode
        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public bool IsDefault { get; set; } // Radius
    }

}