﻿using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Admin.ViewModels.CharityPayment
{
    public class CharityPaymentModel
    {
        public CharityPaymentModel()
        {
            loCharityPaymentList = new List<CharityPaymentViewModel>(); 
        }
        public Int64 inRecordCount { get; set; }
        public List<CharityPaymentViewModel> loCharityPaymentList { get; set; }
        public int inPageIndex { get; set; }
        public Pager Pager { get; set; }
    }
    public class CharityPaymentViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public int? CharityID { get; set; } // CharityID
        public int? CompanyID { get; set; } // CompanyID
        public int? CampaignID { get; set; } // CampaignID
        public DateTime? CampaignEndDate { get; set; } // CampaignEndDate
        public string Notes { get; set; } // Notes
        public double? EstAmount { get; set; } // EstAmount
        public bool IsPaid { get; set; } // IsPaid
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime? CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string stSortColumn { get; set; }
        public string stSearch { get; set; }
        public int inPageSize { get; set; }
        public int inPageIndex { get; set; }
        public string Company { get; set; }
        public string Charity { get; set; }
        public string Campaign { get; set; }
        
    }
}