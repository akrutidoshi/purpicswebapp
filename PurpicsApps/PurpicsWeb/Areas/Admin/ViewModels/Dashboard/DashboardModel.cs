﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Admin.ViewModels
{
    public class DashboardModel
    {
        public DashboardModel()
        {
            loCampaignList = new List<DashboardViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public List<DashboardViewModel> loCampaignList { get; set; }
        public int TotalLikes { get; set; }
        public int TotalVolunteers { get; set; }
        public int TotalCharities { get; set; }
    }
    public class DashboardViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public int CharityID { get; set; } // CharityID
        public int CompanyID { get; set; } // CompanyID
        public int PackageID { get; set; } // PackageID
        public int CampaignStatusID { get; set; } // CampaignStatusID
        public string Caption { get; set; } // Caption
        public string ImageFile { get; set; } // ImageFile
        public string CouponCode { get; set; } // CouponCode
        public DateTime? ExpirationDate { get; set; } // ExpirationDate
        public System.Data.Entity.Spatial.DbGeography Latitude { get; set; } // Latitude
        public System.Data.Entity.Spatial.DbGeography Longitude { get; set; } // Longitude
        public int Radius { get; set; } // Radius
        public int Likes { get; set; } // Likes
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string stSortColumn { get; set; }
        public string stSearch { get; set; }
        public string Company { get; set; }
        public string CampaignName { get; set; }

        public string Charity { get; set; }
        public int Status { get; set; }
        public int inPageSize { get; set; }
        public int inPageIndex { get; set; }
        public double PackagePrice { get; set; }
        public int PackageLikes { get; set; }

        public DashboardViewModel()
        {
            loVolunteerList = new List<VolunteerModel>();
        }
        public List<VolunteerModel> loVolunteerList { get; set; }

    }
    public class VolunteerModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; }
        public int Likes { get; set; }
        public string ZipCode { get; set; }
    }
}