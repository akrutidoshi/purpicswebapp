﻿using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Admin.ViewModels.Charity
{
    public class CharityModel
    {
        public CharityModel()
        {
            loCharityList = new List<CharityViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public List<CharityViewModel> loCharityList { get; set; }
        public int inPageIndex { get; set; }
        public Pager Pager { get; set; }	

    }
    public class CharityViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string LegalEntityName { get; set; } // LegalEntityName
        public string C501C3Number { get; set; } // 501C3Number
        public string CharityName { get; set; } // CharityName
        public string Address { get; set; } // Address
        public string ContactPerson { get; set; } // ContactPerson
        public string ContactPhone { get; set; } // ContactPhone
        public string About { get; set; } // About
        public string LogoFileName { get; set; } // LogoFileName
        public string AspNetUserID { get; set; } // AspNetUserID
        public string AspNetUsername { get; set; }
        public int UserStatusID { get; set; } // UserStatusID
        public int CategoryID { get; set; } // CategoryID
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string stSortColumn { get; set; }
        public string stSearch { get; set; }
        public string Company { get; set; }
        public string Charity { get; set; }
        public string Status { get; set; }
        public int inPageSize { get; set; }
        public int inPageIndex { get; set; }
        public int? STATUS { get; set; } // STATUS
        public string BraintreeCustomerId { get; set; } // LegalEntityName
    }
    public class CharityPaymentHistoryViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string AspNetUserID { get; set; } // AspNetUserID
        public int? CharityID { get; set; } // CharityID
        public string ReferenceNumber { get; set; } // ReferenceNumber
        public string Notes { get; set; } // Notes
        public double? Amount { get; set; } // Amount
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime? CreatedOn { get; set; } // CreatedOn
    }

    public class CharityCategoriesViewModel
    {
        public CharityCategoriesViewModel()
        {
            loCharityCategoryList = new List<CharityCategoryViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public int inPageIndex { get; set; }
        public List<CharityCategoryViewModel> loCharityCategoryList { get; set; }
        public Pager Pager { get; set; }
    }
    public class CharityCategoryViewModel
    {
        public int ID { get; set; } // ID (Primary key)
        [Required(ErrorMessage = "Category name should not be empty.")]
        public string Name { get; set; } // Name
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
    }

    public class CharityRegistration
    {
        [Required(ErrorMessage = "Please enter Username.")]
        [EmailAddress(ErrorMessage = "Please enter valid Email Address.")]
        public string EmailAddress { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter Password.")]
        [RegularExpression("^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*0-9]{6,}$", ErrorMessage = "Password must have:<br/>Minimum 6 characters<br/>At least one special character<br/>At least one digit (0-9)<br/>At least one upper case letter")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter Contact Name.")]
        public string ContactName { get; set; }
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter Phone Number.")]
        public string PhoneNumber1 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        public string PhoneNumber2 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(4, ErrorMessage = "You must enter 4 numbers.", MinimumLength = 4)]
        public string PhoneNumber3 { get; set; }


        [Required(ErrorMessage = "Please enter details about Charity.")]
        public string AboutCharity { get; set; }
        [Required(ErrorMessage = "Please enter Legal Entity Name.")]
        public string LegalEntityName { get; set; }
        [Required(ErrorMessage = "Please enter 501-C3 Number.")]
        public string Number501_C3 { get; set; }
        [Required(ErrorMessage = "Please enter Charity Address.")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please enter City.")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please select State.")]
        public string State { get; set; }
        [Required(ErrorMessage = "Please enter zip.")]
        public string Zip { get; set; }
        [Required(ErrorMessage = "Please select Category.")]
        public int? CategoryID { get; set; }
        [Required(ErrorMessage = "Please enter Charity Name.")]
        public string CharityName { get; set; }
        public HttpPostedFileBase CharityLogo { get; set; }

        public List<SelectListItem> Categories { get; set; }
        public List<SelectListItem> States { get; set; }
        public string AspNetUserId { get; set; }
        public bool IsActive { get; set; }
        public string CharityLogoFullPath { get; set; }
        public int CharityId { get; set; }

    }
}