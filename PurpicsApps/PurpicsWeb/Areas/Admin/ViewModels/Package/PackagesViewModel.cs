﻿using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Admin.ViewModels.Package
{
    public class PackagesViewModel
    {
        public PackagesViewModel()
        {
            loPackagesList = new List<PackageViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public int inPageIndex { get; set; }
        public List<PackageViewModel> loPackagesList { get; set; }
        public Pager Pager { get; set; }
    }
    public class PackageViewModel
    {
        public int ID { get; set; } // ID (Primary key)

        [Required(ErrorMessage = "Package name should not be empty.")]
        public string Name { get; set; } // Name

        [Required(ErrorMessage = "Price should not be empty.")]
        public double Price { get; set; } // Price

        [Required(ErrorMessage = "Likes should not be empty.")]
        public int Likes { get; set; } // Likes

        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn

        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        
    }
}