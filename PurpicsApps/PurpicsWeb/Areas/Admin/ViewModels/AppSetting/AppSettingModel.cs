﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Admin.ViewModels.AppSetting
{
    public class AppSettingModel
    {
        public AppSettingModel()
        {
            loAppSettingList = new List<AppSettingViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public List<AppSettingViewModel> loAppSettingList { get; set; }
    }
    public class AppSettingViewModel
    {
        public int Id { get; set; } // Id (Primary key)

        [Required(ErrorMessage = "required")]
        public string Name { get; set; } // Name

        [Required(ErrorMessage = "required")]
        public string Value { get; set; } // Value
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string ModifiedBy { get; set; } // ModifiedBy

        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
    }

    public class AppSettigsViewModel
    {
        [Required(ErrorMessage="Campaign percentage should not be empty.")]
        public string CampaignPer { get; set; }
        [Required(ErrorMessage = "Campaign awarded points should not be empty.")]
        public string CampaignPointAwarded { get; set; }
        [Required(ErrorMessage = "Campaign Upload awarded points should not be empty.")]
        public string CampaignUploadPointAwarded { get; set; }
        [Required(ErrorMessage = "Redemption points should not be empty.")]
        public string RedemptionPoints { get; set; }
        [Required(ErrorMessage = "Charity Registration Amount should not be empty.")]
        public string CharityRegistrationAmount { get; set; }

        [Required(ErrorMessage = "Charity Subscription Notification Days should not be empty.")]
        public string CharitySubscriptionNotificationDays { get; set; }

        [Required(ErrorMessage = "Redeem Gift Card Minimum Points should not be empty.")]
        public string RedeemGiftCardMinimumPoints { get; set; }
    }
}