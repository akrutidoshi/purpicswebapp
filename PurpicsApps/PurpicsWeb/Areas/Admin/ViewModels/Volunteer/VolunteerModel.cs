﻿using PurpicsWeb.Services;
using PurpicsWeb_DL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Admin.ViewModels.Volunteer
{
    public class VolunteerModel
    {
        public VolunteerModel()
        {
            loVolunteerList = new List<Volunteers>();
        }
        public Int64 inRecordCount { get; set; }
        public int? inTotalFolowers { get; set; }
        public int inPageIndex { get; set; }
        public List<Volunteers> loVolunteerList { get; set; }
        public Pager Pager { get; set; }
        public List<SelectListItem> loSOs { get; set; }
        public int SoId { get; set; }


    }

    public class VolunteerViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string CurrentGrade { get; set; } // CurrentGrade
        public string ZipCode { get; set; } // ZipCode
        public int CreditPoints { get; set; } // CreditPoints
        public string InstagramUserId { get; set; } // InstagramUserId
        public string AspNetUserID { get; set; } // AspNetUserID
        public int? GiftCardPreference { get; set; } // GiftCardPreference
        public string ImageFile { get; set; } // ImageFile
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public bool IsDisplaySendGiftCardButton { get; set; } // IsDisplaySendGiftCardButton
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string Name { get; set; } // Name
        public string Email { get; set; } // Email
        public int? Status { get; set; } // Status
        public string GiftCard { get; set; } 
        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public VolunteerViewModel()
        {
            loVolunteerCampaignList = new List<VolunteersCampaign>();
        }
        public List<VolunteersCampaign> loVolunteerCampaignList { get; set; }
        public int TotalLikes { get; set; }
        public int TotalRedeem { get; set; }
        public int? TotalFollowers { get; set; }
        public List<VolunteersSO> loVolunteersSO { get; set; }
        public string stSelectedSOs { get; set; }
    }
    public class VolunteerGiftCardViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public int? VolunteerId { get; set; } // VolunteerId
        public int? GiftCardId { get; set; } // GiftCardId
        public string GiftCardName { get; set; } // GiftCardName
        public int? PointCost { get; set; } // PointCost
        public DateTime? RedeemptionDate { get; set; } // RedeemptionDate
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime? CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
    }
    public class VolunteersCampaign
    {
        public string Campaign { get; set; }
        public int Likes { get; set; }
        public int RedeemPoint { get; set; }
    }
}