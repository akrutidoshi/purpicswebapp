﻿using System.Web.Mvc;

namespace PurpicsWeb.Areas.StudentOrganization
{
    public class StudentOrganizationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "StudentOrganization";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "StudentOrganization_default",
                "StudentOrganization/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}