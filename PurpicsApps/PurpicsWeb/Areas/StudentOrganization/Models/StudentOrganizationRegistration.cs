﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PurpicsWeb_DL.Entities;
using PurpicsWeb.Helper;
using PurpicsWeb.Areas.StudentOrganization.Models.Charity;

namespace PurpicsWeb.Areas.StudentOrganization.Models
{
    public class StudentOrganizationRegistration
    {
        public StudentOrganizationRegistration()
        {
            CharityRegistration = new CharityRegistration();
        }
        [Required(ErrorMessage = "Please enter Username.")]
        [EmailAddress(ErrorMessage = "Please enter valid Email Address.")]
        public string EmailAddress { get; set; }
         [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter Password.")]
        [RegularExpression("^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*0-9]{6,}$", ErrorMessage = "Password must have:<br/>Minimum 6 characters<br/>At least one special character<br/>At least one digit (0-9)<br/>At least one upper case letter")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter Contact Name.")]
        public string ContactName { get; set; }
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter Phone Number.")]
        public string PhoneNumber1 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        public string PhoneNumber2 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(4, ErrorMessage = "You must enter 4 numbers.", MinimumLength = 4)]
        public string PhoneNumber3 { get; set; }

        [Required(ErrorMessage = "Please enter Address.")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please enter City.")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please select State.")]
        public string State { get; set; }
        [Required(ErrorMessage = "Please enter zip.")]
        public string Zip { get; set; }
        [Required(ErrorMessage = "Please enter Student Organization Name.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please select Student Organization Logo.")]
        public HttpPostedFileBase SOLogo { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public List<SelectListItem> States { get; set; }
        public List<Charities> Charities { get; set; }
        public string[] CharityIds { get; set; }

        public CharityRegistration CharityRegistration { get; set; }
    }
}