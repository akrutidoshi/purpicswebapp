﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.StudentOrganization.Models.Charity
{
    public class CharityRegistration
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        [Required(ErrorMessage = "Please enter Charity Name.")]
        public string CharityName { get; set; }
        public List<SelectListItem> States { get; set; }
        public int CharityId { get; set; }
    }
}