﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.StudentOrganization.Models.Dashboard
{
    public class DashboardViewModel
    {
        public DashboardViewModel()
        {
            loCampaignsList = new List<CampaignViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public List<CampaignViewModel> loCampaignsList { get; set; }
    }

    public class CampaignViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public int CharityID { get; set; } // CharityID
        public int CompanyID { get; set; } // CompanyID
        public int PackageID { get; set; } // PackageID
        public int CampaignStatusID { get; set; } // CampaignStatusID
        public string Caption { get; set; } // Caption
        public string CharitySponsor { get; set; } // Caption
        public string ImageFile { get; set; } // ImageFile
        public string CampaignCouponImageFile { get; set; } // ImageFile
        public string CouponCode { get; set; } // CouponCode
        public DateTime? ExpirationDate { get; set; } // ExpirationDate
        public int Radius { get; set; } // Radius
        public int Likes { get; set; } // Likes
        public bool IsActive { get; set; } // IsActive
        public double PackagePrice { get; set; } // Price
        public int PackageLikes { get; set; } // Likes
        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public string Status { get; set; }
    }
}