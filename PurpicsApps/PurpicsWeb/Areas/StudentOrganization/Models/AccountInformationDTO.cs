﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PurpicsWeb.Areas.StudentOrganization.Models.Charity;


namespace PurpicsWeb.Areas.StudentOrganization.Models
{
    public class AccountInformationDTO        
    {
        public AccountInformationDTO()
        {
            CharityRegistration = new CharityRegistration();
        }
        public string EmailAddress { get; set; }        
        [Required(ErrorMessage = "Please enter Contact Name.")]
        public string ContactName { get; set; }
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter Phone Number.")]
        public string PhoneNumber1 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        public string PhoneNumber2 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(4, ErrorMessage = "You must enter 4 numbers.", MinimumLength = 4)]
        public string PhoneNumber3 { get; set; }
      
        [Required(ErrorMessage = "Please enter Address.")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please enter City.")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please select State.")]
        public string State { get; set; }
        [Required(ErrorMessage = "Please enter zip.")]
        public string Zip { get; set; }
        [Required(ErrorMessage = "Please enter Student Organization.")]
        public string Name { get; set; }
        public HttpPostedFileBase SOLogo { get; set; }
        public string SOLogoFullPath { get; set; }
        public List<SOCharities> SOCharities { get; set; }
        public List<SelectListItem> States { get; set; }
        public int StudentOrganizationId { get; set; }
        public string[] CharityIds { get; set; }
        public CharityRegistration CharityRegistration { get; set; }
    }
}
