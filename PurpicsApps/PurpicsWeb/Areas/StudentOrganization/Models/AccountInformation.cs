﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.StudentOrganization.Models
{
    public class AccountInformation
    {
        public AccountInformation()
        {
            StudentOrganizationId = 0;
            EmailAddress = string.Empty;
            Password = string.Empty;
            ContactName = string.Empty;
            PhoneNumber = string.Empty;
            Address = string.Empty;
            Name = string.Empty;
            Logo = null;
            CategoryName = string.Empty;
            StudentOrgLogoFullPath = string.Empty;
            CategoryId = 0;
            Categories = new List<SelectListItem>();
            Status = string.Empty;
        }

        public int StudentOrganizationId { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Name { get; set; }
        public HttpPostedFileBase Logo { get; set; }
        public string CategoryName { get; set; }
        public string StudentOrgLogoFullPath { get; set; }
        public int CategoryId { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public string Status { get; set; }
        public List<SOCharities> SOCharities { get; set; }

    }

    public class SOCharities
    {
        public SOCharities()
        {
        }

        public int CharityId { get; set; }
        public string CharityName { get; set; }
        public bool IsSelected { get; set; }
    }
}