﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Areas.StudentOrganization.Models.Company;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Linq.Expressions;

namespace PurpicsWeb.Areas.StudentOrganization.Controllers
{
    [Authorize(Roles = "StudentOrganization")]
    public class ManageCompanyController : Controller
    {
        // GET: StudentOrganization/ManageCompany
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CompanyDetail(int CompanyId = 0, int page = 1, bool IsFromDashboard = false)
        {
            CompanyViewModel loCompanyViewModel = new CompanyViewModel();
            ViewBag.Currentpageindex = page;
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.Id == CompanyId).FirstOrDefault();
            if (company != null)
            {
                AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == company.AspNetUserID).FirstOrDefault();
                if (user != null)
                {
                    loCompanyViewModel.Id = Convert.ToInt32(CompanyId);
                    loCompanyViewModel.LegalEntityName = company.LegalEntityName;
                    loCompanyViewModel.Status = company.Status;
                    loCompanyViewModel.ContactPerson = company.ContactPerson;
                    loCompanyViewModel.CompanyName = company.CompanyName;
                    loCompanyViewModel.ContactPhone = company.ContactPhone;
                    loCompanyViewModel.About = company.About;
                    loCompanyViewModel.Address = company.Address;
                    loCompanyViewModel.AspNetUsername = user.UserName;
                    loCompanyViewModel.LogoFilename = company.LogoFilename;
                    loCompanyViewModel.IsManageDashboard = IsFromDashboard;
                }
            }
            return View(loCompanyViewModel);
        }
    }
}