﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Areas.StudentOrganization.Models;
using PurpicsWeb.Models;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Threading.Tasks;
using PurpicsWeb.Services.CommonFunctions;
using AutoMapper;
using Braintree;
using PurpicsWeb.Services;
using System.Drawing;
using ImageResizer;
using System.Drawing.Imaging;
using System.Linq.Expressions;

namespace PurpicsWeb.Areas.StudentOrganization.Controllers
{
    [Authorize(Roles = "StudentOrganization")]
    public class AccountInformationController : Controller
    {
        // GET: StudentOrganization/AccountInformation
        public ActionResult Index()
        {
            AccountInformation objAccountInformtion = new AccountInformation();
            string userid = HttpContext.User.Identity.GetUserId();
            StudentOrganizations studentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == studentOrganizations.AspNetUserID).FirstOrDefault();
            objAccountInformtion.StudentOrganizationId = studentOrganizations.Id;
            objAccountInformtion.EmailAddress = user.UserName;
            objAccountInformtion.ContactName = studentOrganizations.ContactPerson;
            objAccountInformtion.PhoneNumber = studentOrganizations.ContactPhone;
            objAccountInformtion.Name = studentOrganizations.Name;
            objAccountInformtion.Address = studentOrganizations.Address;
            objAccountInformtion.City = studentOrganizations.City;
            objAccountInformtion.State = studentOrganizations.State;
            objAccountInformtion.Zip = studentOrganizations.Zip;

            objAccountInformtion.SOCharities = getSOCharityList(studentOrganizations.Id);

            if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"] + studentOrganizations.LogoFileName)))
            {
                FileInfo file = new FileInfo(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"] + studentOrganizations.LogoFileName));
                objAccountInformtion.StudentOrgLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"].Remove(0, 1) + studentOrganizations.LogoFileName + "?" + file.LastAccessTime.ToString("ddMMyyHHmmss");
            }
            else
            {
                objAccountInformtion.StudentOrgLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"].Remove(0, 1) + "NoPhoto.jpg";
            }

            CommonEnums.UserStatus enumUserStatus = (CommonEnums.UserStatus)studentOrganizations.UserStatusID;
            objAccountInformtion.Status = enumUserStatus.ToString();
            return View("AccountInformation", objAccountInformtion);
        }

        public ActionResult EditAccountInformation()
        {
            AccountInformationDTO objAccountInformtion = new AccountInformationDTO();
            string userid = HttpContext.User.Identity.GetUserId();
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == userid).FirstOrDefault();
            StudentOrganizations studentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            objAccountInformtion.StudentOrganizationId = studentOrganizations.Id;
            objAccountInformtion.EmailAddress = user.UserName;
            objAccountInformtion.ContactName = studentOrganizations.ContactPerson;

            string[] splitPhoneNumber = studentOrganizations.ContactPhone.Split('-');
            objAccountInformtion.PhoneNumber1 = splitPhoneNumber[0];
            objAccountInformtion.PhoneNumber2 = splitPhoneNumber[1];
            objAccountInformtion.PhoneNumber3 = splitPhoneNumber[2];

            objAccountInformtion.Name = studentOrganizations.Name;
            objAccountInformtion.Address = studentOrganizations.Address;
            objAccountInformtion.City = studentOrganizations.City;
            objAccountInformtion.State = studentOrganizations.State;
            objAccountInformtion.Zip = studentOrganizations.Zip;
            //NonProfitPartners nonProfitPartners = Repository<NonProfitPartners>.GetEntityListForQuery(x => x.Id == studentOrganizations.NonProfitPartnersID).FirstOrDefault();
            if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"] + studentOrganizations.LogoFileName)))
            {
                FileInfo file = new FileInfo(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"] + studentOrganizations.LogoFileName));
                objAccountInformtion.SOLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"].Remove(0, 1) + studentOrganizations.LogoFileName + "?" + file.LastAccessTime.ToString("ddMMyyHHmmss");
            }
            else
            {
                objAccountInformtion.SOLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"].Remove(0, 1) + "NoPhoto.jpg";
            }

            objAccountInformtion.SOCharities = getSOCharityList(studentOrganizations.Id);
            objAccountInformtion.CharityRegistration.States = CommonFunctions.GetStateList();

            objAccountInformtion.States = CommonFunctions.GetStateList();
            return View("EditAccountInfo", objAccountInformtion);
        }

        public List<SOCharities> getSOCharityList(int Id)
        {

            Expression<Func<Charities, bool>> expression = x => x.IsActive == true && x.IsDeleted == false && x.UserStatusID == (int)CommonEnums.CharityApprovalStatus.Approved;

            //Include 
            List<Expression<Func<Charities, Object>>> includeSO = new List<Expression<Func<Charities, object>>>();

            //Sort
            Func<IQueryable<Charities>, IOrderedQueryable<Charities>> orderingFunc = query => query.OrderBy(x => x.CharityName);

            List<Charities> loSOCharities = Repository<Charities>.GetEntityListForQuery(expression, null, includeSO, null, null).ToList();
            List<SOCharities> model = new List<SOCharities>();
            if (loSOCharities != null && loSOCharities.Count > 0)
            {
                foreach (var charity in loSOCharities)
                {
                    StudentOrganizationsCharities studentOrganizationsCharities = Repository<StudentOrganizationsCharities>.GetEntityListForQuery(x => x.CharityId == charity.Id && x.StudentOrganizationId == Id).FirstOrDefault();

                    SOCharities objComp = new SOCharities();
                    objComp.CharityId = charity.Id;
                    objComp.CharityName = charity.CharityName;
                    objComp.IsSelected = studentOrganizationsCharities == null ? false : true;
                    model.Add(objComp);
                }
            }

            return model;
        }

        [ValidateInput(false)]
        public async Task<ActionResult> UpateAccountInfo(AccountInformationDTO objAccountInformation)
        {
            string userid = HttpContext.User.Identity.GetUserId();
            var studentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.Id == objAccountInformation.StudentOrganizationId).FirstOrDefault();
            studentOrganizations.ContactPerson = objAccountInformation.ContactName;
            studentOrganizations.ContactPhone = objAccountInformation.PhoneNumber1 + "-" + objAccountInformation.PhoneNumber2 + "-" + objAccountInformation.PhoneNumber3; ;
            studentOrganizations.Address = objAccountInformation.Address;
            studentOrganizations.City = objAccountInformation.City;
            studentOrganizations.State = objAccountInformation.State;
            studentOrganizations.Zip = objAccountInformation.Zip;
            studentOrganizations.ContactPerson = objAccountInformation.ContactName;
            studentOrganizations.ModifiedBy = userid;
            studentOrganizations.ModifiedOn = DateTime.Now;

            string newfilename = UploadSOLogo(objAccountInformation.SOLogo, objAccountInformation.StudentOrganizationId);
            if (newfilename != "")
            {
                studentOrganizations.LogoFileName = newfilename;
            }
            await Repository<StudentOrganizations>.UpdateEntity(studentOrganizations, (entity) => { return entity.Id; });
            await CommonFunctions.SaveSOCharities(objAccountInformation.StudentOrganizationId, objAccountInformation.CharityIds, userid);
            return Redirect("Index");
        }

        public string UploadSOLogo(HttpPostedFileBase soLogo, int SOId)
        {
            string newfileName = "";
            if (soLogo != null)
            {
                var studentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.Id == SOId).FirstOrDefault();
                string savepath = Path.Combine(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"]), SOId.ToString());
                if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"]) + studentOrganizations.LogoFileName))
                {
                    System.IO.File.Delete(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["StudentOrganizationImagePath"]) + studentOrganizations.LogoFileName);
                }

                Dictionary<string, string> loVersions = new Dictionary<string, string>();
                loVersions.Add("", "maxwidth=250&maxheight=300&AutoRotate=true");

                //Generate each version
                foreach (var loKey in loVersions.Keys)
                {
                    Image img = Image.FromStream(soLogo.InputStream);
                    var ExIfImage = img.PropertyItems.Where(x => x.Id == 0x0112).FirstOrDefault();

                    if (ExIfImage != null)
                    {
                        int orientationValue = img.GetPropertyItem(0x0112).Value[0];
                        RotateFlipType rotateFlipType = CommonFunctions.GetOrientationToFlipType(orientationValue);
                        img.RotateFlip(rotateFlipType);
                    }

                    ImageBuilder.Current.Build(
                             new ImageJob(
                                 img,
                                 savepath,
                                 new Instructions(loVersions[loKey]),
                                 false,
                                 true));
                    //string lsFileExt = Path.GetExtension(charityLogo.FileName);
                    string newext = "";
                    if (ImageFormat.Jpeg.Equals(img.RawFormat))
                    {
                        newext = ".JPG";
                    }
                    else if (ImageFormat.Png.Equals(img.RawFormat))
                    {
                        newext = ".PNG";
                    }
                    else if (ImageFormat.Gif.Equals(img.RawFormat))
                    {
                        newext = ".GIF";
                    }
                    newfileName = SOId.ToString() + newext;
                    return newfileName;
                }
            }
            return "";
        }
    }
}