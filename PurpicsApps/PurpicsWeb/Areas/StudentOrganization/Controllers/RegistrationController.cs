﻿using Hangfire;
using ImageResizer;
using Microsoft.AspNet.Identity.Owin;
using PurpicsWeb.Areas.StudentOrganization.Models;
using PurpicsWeb.Areas.StudentOrganization.Models.Charity;
using PurpicsWeb.Controllers;
using PurpicsWeb.Models;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;


namespace PurpicsWeb.Areas.StudentOrganization.Controllers
{
    public class RegistrationController : Controller
    {
        string StudentOrganizationImagePath = ConfigurationManager.AppSettings["StudentOrganizationImagePath"].ToString();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            if (User != null)
            {
                if (User.IsInRole(CommonConstants.RoleNames.Admin.ToString()))
                {
                    return RedirectToAction("", "AdminDashBoard", new { area = "Admin" });
                }
                else if (User.IsInRole(CommonConstants.RoleNames.Company.ToString()))
                {
                    return RedirectToAction("", "ManageDashboard", new { area = "Company" });
                }
                else if (User.IsInRole(CommonConstants.RoleNames.Charity.ToString()))
                {
                    return RedirectToAction("", "ManageDashboard", new { area = "Charity" });
                }
                else if (User.IsInRole(CommonConstants.RoleNames.StudentOrganization.ToString()))
                {
                    return RedirectToAction("", "ManageDashboard", new { area = CommonConstants.RoleNames.StudentOrganization.ToString() });
                }
            }
            return View();
        }

        [ActionName("Step1")]
        public ActionResult Registration()
        {
            StudentOrganizationRegistration objCharityRegistration = new StudentOrganizationRegistration();
            objCharityRegistration.States = CommonFunctions.GetStateList();
            objCharityRegistration.Charities = getCharityList();
            objCharityRegistration.CharityRegistration.States = CommonFunctions.GetStateList();

            return View("Register", objCharityRegistration);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> StudentOrganizationRegistration(StudentOrganizationRegistration objCharityRegistration)
        {
            List<AspNetUsers> users = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == objCharityRegistration.EmailAddress).ToList();
            if (users.Count > 0)
            {
                TempData["Message"] = "The email address " + objCharityRegistration.EmailAddress + " is already registered. Please login.";
                objCharityRegistration.States = CommonFunctions.GetStateList();
                objCharityRegistration.Charities = getCharityList();
                objCharityRegistration.CharityRegistration.States = CommonFunctions.GetStateList();
                return View("Register", objCharityRegistration);
            }

            await SaveStudentOrganization(objCharityRegistration);
            return View("Thanks");
        }

        public async Task<ActionResult> SaveStudentOrganization(StudentOrganizationRegistration objStudentOrganizationRegistration)
        {
            try
            {
                var user = new ApplicationUser() { UserName = objStudentOrganizationRegistration.EmailAddress, Email = objStudentOrganizationRegistration.EmailAddress, PhoneNumber = objStudentOrganizationRegistration.PhoneNumber1 + "-" + objStudentOrganizationRegistration.PhoneNumber2 + "-" + objStudentOrganizationRegistration.PhoneNumber3 };
                var userresult = await UserManager.CreateAsync(user, objStudentOrganizationRegistration.Password);
                if (userresult.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, CommonConstants.RoleNames.StudentOrganization.ToString());

                    StudentOrganizations studentOrganization = new StudentOrganizations();
                    studentOrganization.Name = objStudentOrganizationRegistration.Name;
                    studentOrganization.Address = objStudentOrganizationRegistration.Address;
                    studentOrganization.City = objStudentOrganizationRegistration.City;
                    studentOrganization.State = objStudentOrganizationRegistration.State;
                    studentOrganization.Zip = objStudentOrganizationRegistration.Zip;
                    studentOrganization.ContactPerson = objStudentOrganizationRegistration.ContactName;
                    studentOrganization.ContactPhone = objStudentOrganizationRegistration.PhoneNumber1 + "-" + objStudentOrganizationRegistration.PhoneNumber2 + "-" + objStudentOrganizationRegistration.PhoneNumber3;
                    studentOrganization.AspNetUserID = user.Id;
                    studentOrganization.IsActive = true;
                    studentOrganization.CreatedOn = DateTime.Now;
                    studentOrganization.CreatedBy = user.Id;
                    studentOrganization.UserStatusID = (int)CommonEnums.UserStatus.Pending;
                    await Repository<StudentOrganizations>.InsertEntity(studentOrganization, entity => { return entity.Id; });

                    int soid = studentOrganization.Id;
                    string savepath = Path.Combine(Server.MapPath(StudentOrganizationImagePath), soid.ToString());
                    if (!string.IsNullOrEmpty(studentOrganization.LogoFileName))
                    {
                        if (System.IO.File.Exists(Path.Combine(Server.MapPath(StudentOrganizationImagePath), studentOrganization.LogoFileName)))
                        {
                            System.IO.File.Delete(Path.Combine(Server.MapPath(StudentOrganizationImagePath), studentOrganization.LogoFileName));
                        }
                    }
                    studentOrganization.LogoFileName = UploadStudentOrganizationLogo(objStudentOrganizationRegistration.SOLogo, soid, savepath);
                    await Repository<StudentOrganizations>.UpdateEntity(studentOrganization, entity => { return entity.Id; });

                    await CommonFunctions.SaveSOCharities(studentOrganization.Id, objStudentOrganizationRegistration.CharityIds, user.Id);

                    string strRegistrationEmailBody = new SystemEmailController().SORegistration(objStudentOrganizationRegistration, studentOrganization.Id);
                    string mailTo = Utility.ExecuteScalar("sp_GetAllAdminEmailId").ToString();
                    BackgroundJob.Enqueue(() => EmailHelper.SendEmail("New Student Organization Registration", strRegistrationEmailBody, mailTo));
                    return View("Thanks");
                }
                else
                {
                    objStudentOrganizationRegistration.CharityRegistration.States = CommonFunctions.GetStateList();
                    TempData["Message"] = "The email address " + objStudentOrganizationRegistration.EmailAddress + " is already registered. Please login.";
                    return View("Register", objStudentOrganizationRegistration);
                }
            }
            catch (Exception ex)
            {
                return View("~/Areas/StudentOrganization/Views/Payment/AddEditCreditCard.cshtml");
            }
        }

        public string UploadStudentOrganizationLogo(HttpPostedFileBase image, int soId, string savePath)
        {
            string newfileName = "";
            if (image != null)
            {
                Dictionary<string, string> loVersions = new Dictionary<string, string>();
                loVersions.Add("", "maxwidth=250&maxheight=300&AutoRotate=true");

                //Generate each version
                foreach (var loKey in loVersions.Keys)
                {
                    Image img = Image.FromStream(image.InputStream);
                    var ExIfImage = img.PropertyItems.Where(x => x.Id == 0x0112).FirstOrDefault();

                    if (ExIfImage != null)
                    {
                        int orientationValue = img.GetPropertyItem(0x0112).Value[0];
                        RotateFlipType rotateFlipType = CommonFunctions.GetOrientationToFlipType(orientationValue);
                        img.RotateFlip(rotateFlipType);
                    }

                    ImageBuilder.Current.Build(
                             new ImageJob(
                                 img,
                                 savePath,
                                 new Instructions(loVersions[loKey]),
                                 false,
                                 true));
                    //                    string lsFileExt = Path.GetExtension(image.FileName);
                    string newext = "";
                    if (ImageFormat.Jpeg.Equals(img.RawFormat))
                    {
                        newext = ".JPG";
                    }
                    else if (ImageFormat.Png.Equals(img.RawFormat))
                    {
                        newext = ".PNG";
                    }
                    else if (ImageFormat.Gif.Equals(img.RawFormat))
                    {
                        newext = ".GIF";
                    }
                    newfileName = soId.ToString() + newext;
                    return newfileName;
                }

            }
            return "";
        }

        public List<Charities> getCharityList()
        {
            Expression<Func<Charities, bool>> expression = x => x.IsActive == true && x.IsDeleted == false && x.UserStatusID == (int)CommonEnums.CharityApprovalStatus.Approved;
            Func<IQueryable<Charities>, IOrderedQueryable<Charities>> orderingFunc = query => query.OrderBy(x => x.CharityName);
            List<Charities> charities = Repository<Charities>.GetEntityListForQuery(expression, null, null, null, null).ToList();
            return charities;
        }

        #region Add Edit Charity

        [HttpGet]
        public ActionResult AddEditCharity(int? id)
        {
            CharityRegistration model = new CharityRegistration();
            model.States = CommonFunctions.GetStateList();
            return PartialView("~/Areas/StudentOrganization/Views/Registration/_AddCharity.cshtml", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> AddEditCharity(CharityRegistration objCharityRegistration)
        {
            try
            {
                Charities chairty = Repository<Charities>.GetEntityListForQuery(x => x.CharityName.Trim() == objCharityRegistration.CharityName.Trim() && x.IsDeleted == false).FirstOrDefault();
                if (chairty == null)
                {
                    Charities charities = new Charities();
                    charities.CharityName = objCharityRegistration.CharityName;
                    charities.Address = objCharityRegistration.Address;
                    charities.City = objCharityRegistration.City;
                    charities.State = objCharityRegistration.State;
                    charities.Zip = objCharityRegistration.Zip;
                    charities.AspNetUserID = null;
                    charities.IsActive = true;

                    charities.CreatedOn = DateTime.Now;
                    charities.CreatedBy = null;
                    charities.UserStatusID = (int)CommonEnums.UserStatus.Approved;
                    await Repository<Charities>.InsertEntity(charities, entity => { return entity.Id; });
                    return this.Json(charities.Id, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(-1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return this.Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
