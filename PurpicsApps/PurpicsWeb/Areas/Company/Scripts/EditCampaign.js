﻿function validateCampaign() {

    var form = $("#frmUpdateCampaign");
    
    form.valid();   
    


    var couponflag = true;
    
    if (document.getElementById("CouponCode").value != "" || ($('#upCouponCodeimg').attr('src') != '' && $('#upCouponCodeimg').attr('src') != "/images/default-placeholder.png")) {

        
        if ($("#ExpirationDate").val() == '') {
            $("#msgExpDate").css("display", "block");
            couponflag = false;
        }
        else {
            $("#msgExpDate").css("display", "none");
        }

        if ($("#CouponTitle").val() == '') {
            $("#msgCouponTitle").css("display", "block");
            couponflag = false;
        }
        else {
            $("#msgCouponTitle").css("display", "none");
        }

        if ($("#CouponDescription").val() == '') {
            $("#msgCouponDescription").css("display", "block");
            couponflag = false;
        }
        else {
            $("#msgCouponDescription").css("display", "none");
        }
    }
    if (couponflag == false)
        return;
    

    if (form.valid()) {
        
        form.submit();
        if ($("#loadingbar").length === 0) {
            $("body").append("<div id='loadingbar'></div>")
            $("#divLoader").css("display", "block");
            $("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
            $("#loadingbar").width((50 + Math.random() * 30) + "%");
        }
    }
    return false;
}