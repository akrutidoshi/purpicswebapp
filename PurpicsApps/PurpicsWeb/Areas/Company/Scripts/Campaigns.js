﻿$(document).ready(function () {
    //SetPaging();
    setSortingArrow();
    $('#txtSearch').focus();
});
function userSearch(e) {

    if (e.which == 13) {
        debugger
        e.preventDefault();
        $(this).blur();
        $('#btnSearch').focus().click();
    }
}

function search() {
    var stSearch = "";
    var stToDate = "";
    if ($('#txtSearch').val() != "") {
        stSearch = $('#txtSearch').val();
    }
    // Do some operation
    refeshList(stSearch);
}

function statusSearch(val) {
    $('#hdnStatus').val(val);
    refeshList("load");
}

function clearsearch() {
    $('#txtSearch').val("");
    refeshList("load");
}

function refeshList(foId, pageIndex) {
    var lsStatus = parseInt($('#hdnStatus').val());
    var stSortField = "";
    var lsSearch = $('#txtSearch').val().trim();
    var lsOriginalSearch = $('#txtSearch').val().trim();
    var lsSearch = lsSearch.replace(/'/g, "''");
    if (foId == "load") {
        pageIndex = 1;
    }
    else if (foId == "Id ASC" || foId == "Id DESC" || foId == "Caption ASC" || foId == "Caption DESC" || foId == "Likes ASC" || foId == "Likes DESC" || foId == "CharitySponsor ASC" || foId == "CharitySponsor DESC" || foId == "Status ASC" || foId == "Status DESC" || foId == "CharityName DESC" || foId == "CharityName ASC" || foId == "SOName DESC" || foId == "SOName ASC") {
        pageIndex = parseInt($('#hdnPageIndex').val());
    }
    $.ajax({
        type: "POST",
        url: lsgetUserList,
        content: "application/json; charset=utf-8",
        dataType: "html",
        data: {
            inPageIndex: pageIndex,
            inPageSize: 10,
            stSortColumn: $('#hdnOrder').val(),
            stSearch: lsSearch,
            CampaignStatusID: lsStatus
        },
        success: function (lodata) {

            if (lodata != null) {
                var divSOReleased = $('#Campaignslist');
                divSOReleased.html('');
                divSOReleased.html(lodata);
                $('#hdnPageIndex').val(pageIndex);
                //SetPaging();
                setSortingArrow();

                
                $('.dashboard-menu').height('800px');
                $('.dashboard-menu').height($(document).height());

                $('#txtSearch').val(lsOriginalSearch);
                $('#txtSearch').focus();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#txtSearch').val(lsOriginalSearch);
            $('#txtSearch').focus();
            if (errorThrown == "abort") {
                return;
            }
            else {
                alert(errorThrown);
            }
        }
    });
}
function getOrderbyUserList(foOrderedField, tdID) {

    var foFieldwithOrder = "";
    $('#hdnSortingOnColumn').val(tdID);

    if ($('#hdnOrder').val() == "") {
        $('#hdnOrder').val(foOrderedField + " ASC");
        foFieldwithOrder = foOrderedField + " ASC";
        $('#hdnOrder').val(foFieldwithOrder);
        $('#hdnSortingDirection').val("ASC");

    }
    else {
        if ($('#hdnOrder').val() == (foOrderedField + " ASC")) {
            foFieldwithOrder = foOrderedField + " DESC";
            $('#hdnOrder').val(foFieldwithOrder);
            $('#hdnSortingDirection').val("DESC");
        }
        else {
            foFieldwithOrder = foOrderedField + " ASC";
            $('#hdnOrder').val(foFieldwithOrder);
            $('#hdnSortingDirection').val("ASC");
        }
    }
    refeshList(foFieldwithOrder);
}



function setSortingArrow() {

    var sortTD = $('#hdnSortingOnColumn').val();
    var sortDirection = $('#hdnSortingDirection').val();

    if (sortTD != "") {
        $('#' + sortTD).removeClass("sorting");
        if (sortDirection.toUpperCase() == "ASC") {
            $('#' + sortTD).addClass("sorting_asc");
        }
        else if (sortDirection.toUpperCase() == "DESC") {
            $('#' + sortTD).addClass("sorting_desc");
        }
    }
    else {
        $('#sortName').removeClass("sorting");
        $('#sortName').addClass("sorting_asc");
        $('#hdnOrder').val("Caption ASC");
    }
}





function DeleteUser(fiUserId) {
    
        $.ajax({
            type: "POST",
            url: deleteUserUrl,
            content: "application/json; charset=utf-8",
            dataType: "json",
            data: {
                ID: fiUserId
            },
            success: function (lodata) {
                if (lodata != null) {
                    
                    refeshList("load");                    
                    //DisplayMessage("Campaign deleted successfully.");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (errorThrown == "abort") {
                    return;
                }
                else {
                    //alert(errorThrown);
                }
            }
        });
   
}