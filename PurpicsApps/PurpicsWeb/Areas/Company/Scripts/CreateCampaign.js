﻿
function validateCampaign() {

    var form = $("#frmAddCampaign");
    form.valid();  

    var flagvalid = true;

    if ($(':hidden#CharityID').val() == 0)
    {
        $("#msgCharityReq").css("display", "block");
        flagvalid = false;        
    }
    else {
        $("#msgCharityReq").css("display", "none");
    }

    
    if ($(':hidden#hdnPackageSelectedID').val() == '') {
        $("#msgPackageReq").css("display", "block");
        flagvalid = false;

    }
    else {
        $("#msgPackageReq").css("display", "none");
    }

   

    if (flagvalid == false)
        return;
    

   
    if (document.getElementById("CampaignImage").files.length <= 0) {
        $('#lblAlertMsgs').text("Please upload campaign image.");
        $('#modelAlertMessages').modal('show');
        return false;
    }
   
   
    var couponflag = true;
    if (document.getElementById("CouponCode").value != "") {

        if ($("#ExpirationDate").val() == '') {
            $("#msgExpDate").css("display", "block");
            couponflag = false;
        }
        else {
            $("#msgExpDate").css("display", "none");
        }

        if ($("#CouponTitle").val() == '') {
            $("#msgCouponTitle").css("display", "block");
            couponflag = false;
        }
        else {
            $("#msgCouponTitle").css("display", "none");
        }

        if ($("#CouponDescription").val() == '') {
            $("#msgCouponDescription").css("display", "block");
            couponflag = false;
        }
        else {
            $("#msgCouponDescription").css("display", "none");
        }
    }
    if (couponflag == false)
        return;

    if (form.valid()) {
        
        form.submit();
        if ($("#loadingbar").length === 0) {
            $("body").append("<div id='loadingbar'></div>")
            $("#divLoader").css("display", "block");
            $("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
            $("#loadingbar").width((50 + Math.random() * 30) + "%");
        }
    }
    return false;
}