﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Areas.Company.Models.Charity;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Linq.Expressions;

namespace PurpicsWeb.Areas.Company.Controllers
{
    [Authorize(Roles = "Company")]
    public class ManageCharityController : Controller
    {
        // GET: Company/ManageCharity
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CharityDetail(int CharityId, int page = 1, bool IsFromDashboard = false, bool showBackButton = true)
        {
            ViewBag.Currentpageindex = page;
            Charities lowebstats = Repository<Charities>.GetEntityListForQuery(x => x.Id == CharityId).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();

            CharityViewModel loCompanyViewModel = new CharityViewModel();
            loCompanyViewModel.Id = CharityId;
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;
            loCompanyViewModel.STATUS = lowebstats.UserStatusID;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.Company = "-";
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.CharityName = lowebstats.CharityName;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser == null ? "" : loUser.UserName;
            loCompanyViewModel.LogoFileName = lowebstats.LogoFileName;
            loCompanyViewModel.IsManageDashboard = IsFromDashboard;
            loCompanyViewModel.ShowBackButton = showBackButton;
            return View(loCompanyViewModel);
        }
    }
}