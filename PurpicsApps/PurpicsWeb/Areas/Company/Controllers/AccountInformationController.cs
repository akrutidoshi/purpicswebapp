﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Areas.Company.Models;
using PurpicsWeb.Models;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Threading.Tasks;
using PurpicsWeb.Services.CommonFunctions;
using AutoMapper;
using Braintree;
using PurpicsWeb.Services;
using System.Configuration;
using System.Drawing;
using ImageResizer;
using System.Drawing.Imaging;


namespace PurpicsWeb.Areas.Company.Controllers
{
    [Authorize(Roles = "Company")]
    public class AccountInformationController : Controller
    {
        string CompanyImagePath = ConfigurationManager.AppSettings["CompanyImagePath"].ToString();
        public ActionResult Index()
        {
            AccountInformation objAccountInformtion = new AccountInformation();
            string userid = HttpContext.User.Identity.GetUserId();
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == company.AspNetUserID).FirstOrDefault();
            objAccountInformtion.CompanyId = company.Id;
            objAccountInformtion.EmailAddress = user.UserName;
            objAccountInformtion.ContactName = company.ContactPerson;
            objAccountInformtion.PhoneNumber = company.ContactPhone;
            objAccountInformtion.AboutCompany = company.About;
            objAccountInformtion.LegalEntityName = company.LegalEntityName;
            //objAccountInformtion.Number501_C3 = company.C501C3Number;
            objAccountInformtion.CompanyName = company.CompanyName;
            objAccountInformtion.Address = company.Address;
            if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CompanyImagePath"] + company.LogoFilename)))
            {
                objAccountInformtion.CompanyLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["CompanyImagePath"].Remove(0, 1) + company.LogoFilename;
            }
            int value = company.Status;
            CommonEnums.UserStatus enumUserStatus = (CommonEnums.UserStatus)value;
            string stringUserStatus = enumUserStatus.ToString();
            objAccountInformtion.Status = stringUserStatus;


            string CustomerId = company.BraintreeCustomerId;
            if (!string.IsNullOrEmpty(CustomerId))
            {
                BraintreePayments payment = new BraintreePayments();
                CreditCard CreditCard = payment.GetCustomerDetails(CustomerId).CreditCards.FirstOrDefault();
                objAccountInformtion.CreditCardDetails = Mapper.DynamicMap<CreditCardDetails>(CreditCard);
            }
            return View("AccountInformation", objAccountInformtion);
        }

        public ActionResult EditAccountInformation(int CompanyId)
        {
            AccountInformationDTO objAccountInformtion = new AccountInformationDTO();
            string userid = HttpContext.User.Identity.GetUserId();
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.Id == CompanyId).FirstOrDefault();
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == company.AspNetUserID).FirstOrDefault();
            objAccountInformtion.CompanyId = company.Id;
            objAccountInformtion.EmailAddress = user.UserName;
            objAccountInformtion.ContactName = company.ContactPerson;

            string[] splitPhoneNumber = company.ContactPhone.Split('-');


            objAccountInformtion.PhoneNumber1 = splitPhoneNumber[0];
            objAccountInformtion.PhoneNumber2 = splitPhoneNumber[1];
            objAccountInformtion.PhoneNumber3 = splitPhoneNumber[2];

            objAccountInformtion.AboutCompany = company.About;
            objAccountInformtion.LegalEntityName = company.LegalEntityName;
            //objAccountInformtion.Number501_C3 = company.C501C3Number;
            objAccountInformtion.CompanyName = company.CompanyName;
            objAccountInformtion.Address = company.Address;

            if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CompanyLogo"] + company.LogoFilename)))
            {
                objAccountInformtion.CompanyLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["CompanyLogo"].Remove(0, 1) + company.LogoFilename;
            }
            else
            {
                objAccountInformtion.CompanyLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["CompanyLogo"].Remove(0, 1) + "NoPhoto.jpg";
            }
            
            return View("EditAccountInfo", objAccountInformtion);
        }

        [ValidateInput(false)] 
        public async Task<ActionResult> UpateAccountInfo(AccountInformationDTO objAccountInformation)
        {
            string userid = HttpContext.User.Identity.GetUserId();
            var company = Repository<Companies>.GetEntityListForQuery(x => x.Id == objAccountInformation.CompanyId).FirstOrDefault();
            company.ContactPerson = objAccountInformation.ContactName;
            company.ContactPhone = objAccountInformation.PhoneNumber1 + "-" + objAccountInformation.PhoneNumber2 + "-" + objAccountInformation.PhoneNumber3; ;
            company.About = objAccountInformation.AboutCompany;
            company.LegalEntityName = objAccountInformation.LegalEntityName;
            //company.C501C3Number = objAccountInformation.Number501_C3;
            company.CompanyName = objAccountInformation.CompanyName;
            company.Address = objAccountInformation.Address;
            company.ModifiedBy = userid;
            company.ModifiedOn = DateTime.Now;

            string newfilename = UploadCompanyLogo(objAccountInformation.CompanyLogo, objAccountInformation.CompanyId);
            if (newfilename != "")
            {
                company.LogoFilename = newfilename;
            }
            await Repository<Companies>.UpdateEntity(company, (entity) => { return entity.Id; });
            
            return Redirect("Index");
        }

        public string UploadCompanyLogo(HttpPostedFileBase companyLogo, int CompanyId)
        {
            string newfileName = "";
            if (companyLogo != null)
            {

                var company = Repository<Companies>.GetEntityListForQuery(x => x.Id == CompanyId).FirstOrDefault();
                string savepath = Path.Combine(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CompanyImagePath"]), CompanyId.ToString());
                if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CompanyImagePath"]) + company.LogoFilename))
                {
                    System.IO.File.Delete(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CompanyImagePath"]) + company.LogoFilename);
                }


                Dictionary<string, string> loVersions = new Dictionary<string, string>();
                loVersions.Add("", "maxwidth=250&maxheight=300&AutoRotate=true");

                //Generate each version
                foreach (var loKey in loVersions.Keys)
                {
                    Image img = Image.FromStream(companyLogo.InputStream);
                    var ExIfImage = img.PropertyItems.Where(x => x.Id == 0x0112).FirstOrDefault();

                    if (ExIfImage != null)
                    {
                        int orientationValue = img.GetPropertyItem(0x0112).Value[0];
                        RotateFlipType rotateFlipType = CommonFunctions.GetOrientationToFlipType(orientationValue);
                        img.RotateFlip(rotateFlipType);
                    }

                    ImageBuilder.Current.Build(
                             new ImageJob(
                                 img,
                                 savepath,
                                 new Instructions(loVersions[loKey]),
                                 false,
                                 true));
                    //string lsFileExt = Path.GetExtension(companyLogo.FileName);
                    string newext = "";
                    if (ImageFormat.Jpeg.Equals(img.RawFormat))
                    {
                        newext = ".JPG";
                    }
                    else if (ImageFormat.Png.Equals(img.RawFormat))
                    {
                        newext = ".PNG";
                    }
                    else if (ImageFormat.Gif.Equals(img.RawFormat))
                    {
                        newext = ".GIF";
                    }
                    newfileName = CompanyId.ToString() + newext;
                    return newfileName;
                }

            }
            return "";
        }
    }
}