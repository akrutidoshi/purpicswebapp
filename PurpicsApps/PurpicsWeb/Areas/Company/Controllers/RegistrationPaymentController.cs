﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Company.Controllers
{
    
    public class RegistrationPaymentController : Controller
    {
        //
        // GET: /Company/RegistrationPayment/
        public ActionResult RegistrationPayment()
        {
            return View();
        }

        //
        // GET: /Company/RegistrationPayment/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Company/RegistrationPayment/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Company/RegistrationPayment/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Company/RegistrationPayment/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Company/RegistrationPayment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Company/RegistrationPayment/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Company/RegistrationPayment/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
