﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Services;
using Braintree;
using PurpicsWeb.Areas.Company.Models;
using Microsoft.AspNet.Identity;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Services.CommonFunctions;
using System.Threading.Tasks;
using AutoMapper;
using System.IO;

namespace PurpicsWeb.Areas.Company.Controllers
{
    
    public class PaymentController : Controller
    {
        // GET: Company/Payment
        public ActionResult Index()
        {
            return View();
        }

        [ActionName("Step2")]
        public ActionResult ShowAddEditCreditCardView(int CompanyId, bool fromAccountInfo = false)
        {
            PaymentDTO model = GetModel();
            model.CompanyId = CompanyId;
            model.FromAccountInfo = fromAccountInfo;
            model.CreditCardExpireMonthList = CommonFunctions.getCreditCardMonth();
            model.CreditCardExpireYearList = CommonFunctions.getCreditCardYear();
            return View("~/Areas/Company/Views/Payment/AddEditCreditCard.cshtml", model);
        }

        public ActionResult AddEditCreditCard(string token)
        {
            BraintreePayments payment = new BraintreePayments();
            PaymentDTO model = new PaymentDTO();
            string userid = HttpContext.User.Identity.GetUserId();
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            if (!string.IsNullOrEmpty(token))
            {

                CreditCard card = payment.GetCustomerDetails(company.BraintreeCustomerId).CreditCards.Where(m => m.Token == token).FirstOrDefault();
                model = GetModel();
                model.CompanyId = company.Id;
                model.CreditCardDetails = Mapper.DynamicMap<CreditCardDetails>(card);
                model.FromAccountInfo = true;
                return View("~/Areas/Company/Views/Payment/AddEditCreditCard.cshtml", model);
            }
            else
            {
                model.StatesList = CommonFunctions.GetStateList();
                model.CompanyId = company.Id;
                return View("~/Areas/Company/Views/Payment/AddEditCreditCard.cshtml", model);
            }
        }


        [HttpPost]
        public async Task<ActionResult> AddEditCreditCard(FormCollection collection)
        {
            string token = collection["CreditCardDetails.Token"];
            PaymentDTO model = new PaymentDTO();

            BraintreePayments payment = new BraintreePayments();
            int companyId = Convert.ToInt32(collection["CompanyId"]);
            model.CompanyId = companyId;
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.Id == companyId).FirstOrDefault();
            string CustomerId = company.BraintreeCustomerId;
            if (!string.IsNullOrEmpty(token))
            {

                Result<PaymentMethod> result = payment.UpdateCreditCard(collection, CustomerId, token);
                if (result.IsSuccess())
                {
                    return RedirectToAction("Index", "AccountInformation");
                }
                else
                {
                    if (result.Message == "Duplicate card exists in the vault.")
                    {
                        ModelState.AddModelError("CardError", "This credit card is already on file.");
                    }
                    else
                    {
                        ModelState.AddModelError("CardError", result.Message.ToString());
                    }
                    CreditCard card = payment.GetCustomerDetails(company.BraintreeCustomerId).CreditCards.Where(m => m.Token == token).FirstOrDefault();
                    model.StatesList = CommonFunctions.GetStateList();
                    model = GetModel();
                    model.CompanyId = company.Id;
                    model.CreditCardDetails = Mapper.DynamicMap<CreditCardDetails>(card);
                    model.FromAccountInfo = true;
                    return View("~/Areas/Company/Views/Payment/AddEditCreditCard.cshtml", model);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(CustomerId)) // Create New Customer in Braintree
                {
                    Result<Customer> result = payment.CreateNewCustomer(collection);
                    if (result.IsSuccess())
                    {
                        company.BraintreeCustomerId = result.Target.Id;
                        await Repository<Companies>.UpdateEntity(company, (entity) => { return entity.Id; });

                        if (Convert.ToBoolean(collection["fromAccountInfo"]) == true)
                        {
                            return RedirectToAction("Index", "AccountInformation");
                        }
                        else
                        {
                            return View("~/Areas/Company/Views/Registration/Thanks.cshtml");
                        }
                    }
                    else
                    {
                        if (result.Message == "Duplicate card exists in the vault.")
                        {
                            ModelState.AddModelError("CardError", "This credit card is already on file.");
                        }
                        else
                        {
                            ModelState.AddModelError("CardError", result.Message.ToString());
                        }
                        model.StatesList = CommonFunctions.GetStateList();
                        model.FromAccountInfo = Convert.ToBoolean(collection["fromAccountInfo"]);
                        return View("AddEditCreditCard", model);
                    }
                }
                else
                {
                    Result<PaymentMethod> result = payment.CreateCreditCard(collection, CustomerId);
                    if (result.IsSuccess())
                    {
                        return View("AddEditCreditCard", model);
                    }
                    else
                    {
                        model = GetModel();
                        ModelState.AddModelError("CardError", "Credit card number is invalid.");
                        model.StatesList = CommonFunctions.GetStateList();
                        model.FromAccountInfo = Convert.ToBoolean(collection["fromAccountInfo"]);
                        return View("AddEditCreditCard", model);
                    }
                }
            }
        }

        public PaymentDTO GetModel()
        {
            PaymentDTO model = new PaymentDTO();
            model.StatesList = CommonFunctions.GetStateList();           
            return model;
        }

        public async Task<ActionResult> DeleteCompanyCreditCard(string token, int companyid)
        {
            try
            {
                BraintreePayments payment = new BraintreePayments();
                payment.DeleteCreditCard(token);
                Companies company = Repository<Companies>.GetEntityListForQuery(x => x.Id == companyid).FirstOrDefault();
                company.BraintreeCustomerId = null;
                await Repository<Companies>.UpdateEntity(company, (entity) => { return entity.Id; });
                return RedirectToAction("Index", "AccountInformation");
            }
            catch (Exception ex)
            {
                return Json(-1, JsonRequestBehavior.AllowGet);

            }
        }
    }
}