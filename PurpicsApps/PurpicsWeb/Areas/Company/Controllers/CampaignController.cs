﻿using PurpicsWeb.Areas.Company.Models;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Areas.Company.Models.Campaigns;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Threading.Tasks;
using System.IO;
using Hangfire;
using PurpicsWeb.Services;
using System.Text;
using PurpicsWeb.Services.CommonFunctions;
using System.Drawing;
using ImageResizer;
using PurpicsWeb.Controllers;
using PurpicsWeb.Services;
using System.Drawing.Imaging;
using PurpicsWeb.Areas.Company.Models.StudentOrganization;
using LinqKit;

namespace PurpicsWeb.Areas.Company.Controllers
{
    [Authorize(Roles = "Company")]
    public class CampaignController : Controller
    {
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        string CampaignCouponImagePath = ConfigurationManager.AppSettings["CampaignCouponImagePath"].ToString();
        string DefaultNoImagePath = ConfigurationManager.AppSettings["DefaultNoImagePath"].ToString();


        public ActionResult Index()
        {
            Campaign modelCampaign = new Campaign();
            modelCampaign = GetSearchModel("", 0, "");
            //List<Expression<Func<StudentOrganizations, object>>> include = new List<Expression<Func<StudentOrganizations, object>>>();
            //Expression<Func<StudentOrganizations, object>> includeNonProfitPartners = (status) => status.NonProfitPartners;
            //include.Add(includeNonProfitPartners);

            //var studentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved), null, include).OrderBy(x => x.Name).ToList();
            //modelCampaign.loStudentOrganizations = studentOrganizations;

            List<Packages> lstPackages = new List<Packages>();
            lstPackages = Repository<Packages>.GetEntityListForQuery(m => m.IsActive == true && m.IsDeleted == false).ToList();
            modelCampaign.loPackages = lstPackages;

            //List<NonProfitPartners> lstNonProfitPartners = new List<NonProfitPartners>();
            //lstNonProfitPartners = Repository<NonProfitPartners>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
            //modelCampaign.loNonProfitPartners = lstNonProfitPartners;

            //List<string> loSOZipCode = new List<string>();
            //loSOZipCode = Repository<StudentOrganizations>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved && x.Zip != "")).OrderBy(x => x.Zip).Select(x => x.Zip).Distinct().ToList();
            //modelCampaign.loSOZipcodes = loSOZipCode;

            return View("Index", modelCampaign);
        }

        // GET: /Company/Campaign/
        //public ActionResult Index()
        //{

        //    List<Expression<Func<Charities, object>>> include = new List<Expression<Func<Charities, object>>>();
        //    Expression<Func<Charities, object>> statusInclude = (status) => status.CharityCategories;
        //    include.Add(statusInclude);

        //    var complaintDetail = Repository<Charities>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved), null, include).OrderBy(x => x.CharityName).ToList();

        //    List<Packages> lstPackages = new List<Packages>();
        //    lstPackages = Repository<Packages>.GetEntityListForQuery(m => m.IsActive == true && m.IsDeleted == false).ToList();

        //    Campaign modelCampaign = new Campaign();
        //    modelCampaign.loCharities = complaintDetail;
        //    modelCampaign.loPackages = lstPackages;

        //    List<CharityCategories> lstCharityCategories = new List<CharityCategories>();

        //    lstCharityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
        //    modelCampaign.loCharityCategories = lstCharityCategories;

        //    List<string> loCharityZipcodes = new List<string>();
        //    loCharityZipcodes = Repository<Charities>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved && x.Zip != "")).OrderBy(x => x.Zip).Select(x => x.Zip).Distinct().ToList();
        //    modelCampaign.loZipcodes = loCharityZipcodes;

        //    return View("Index", modelCampaign);
        //}

        public ActionResult ShowEditCampaignView(int CampaignID, bool IsManageDashboard = false, int page = 1)
        {

            Campaigns campaign = Repository<Campaigns>.GetEntityListForQuery(r => r.Id == CampaignID).FirstOrDefault();
            Campaign modelCampaign = new Campaign();


            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == campaign.CharityID).FirstOrDefault();
            StudentOrganizations studentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.Id == campaign.StudentOrganizationId).FirstOrDefault();

            modelCampaign.CampaignId = CampaignID;
            modelCampaign.CharityName = charity.CharityName;
            modelCampaign.SOName = studentOrganizations == null ? "" : studentOrganizations.Name;
            Packages package = Repository<Packages>.GetEntityListForQuery(x => x.Id == campaign.PackageID).FirstOrDefault();
            modelCampaign.PackageName = package.Name;
            modelCampaign.PackagePrice = package.Price;
            modelCampaign.PackageLikes = package.Likes;
            modelCampaign.ZipCode = campaign.Zipcode;
            modelCampaign.Radius = campaign.Radius;
            if (!string.IsNullOrEmpty(campaign.ImageFile))
            {
                if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CampaignImagePath"] + campaign.ImageFile)))
                {
                    modelCampaign.CampaignImageFullPath = System.Configuration.ConfigurationManager.AppSettings["CampaignImagePath"].Remove(0, 1) + campaign.ImageFile;
                }

            }
            modelCampaign.Caption = campaign.Caption;
            modelCampaign.CampaignName = campaign.CampaignName;
            modelCampaign.Description = campaign.Description;
            if (campaign.ExpirationDate != null)
            {
                modelCampaign.ExpirationDate = campaign.ExpirationDate;
            }

            if (campaign.CouponTitle != null)
            {
                modelCampaign.CouponTitle = campaign.CouponTitle;
            }

            if (campaign.CouponDescription != null)
            {
                modelCampaign.CouponDescription = campaign.CouponDescription;
            }

            if (!string.IsNullOrEmpty(campaign.CouponCode))
            {
                if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CampaignCouponImagePath"] + campaign.CouponCode)))
                {
                    modelCampaign.CouponCodeImageFullPath = System.Configuration.ConfigurationManager.AppSettings["CampaignCouponImagePath"].Remove(0, 1) + campaign.CouponCode;
                }

            }
            else
            {
                modelCampaign.CouponCodeImageFullPath = "/images/default-placeholder.png";
            }
            modelCampaign.IsManageDashboard = IsManageDashboard;
            modelCampaign.Currentpageindex = page;
            return View("~/Areas/Company/Views/Campaign/EditCampaignDetails.cshtml", modelCampaign);
        }

        [HttpPost]
        public ActionResult getCharityDataByID(int CategoryId)
        {
            Campaign modelCampaign = new Campaign();
            List<Expression<Func<Charities, object>>> include = new List<Expression<Func<Charities, object>>>();
            Expression<Func<Charities, object>> statusInclude = (status) => status.CharityCategories;
            include.Add(statusInclude);

            //var complaintDetail = Repository<Charities>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved), null, include).Where(x => (x.CategoryID == CategoryId || CategoryId == 0)).OrderBy(x => x.CharityName).ToList();

            //modelCampaign.loCharities = complaintDetail;
            //List<CharityCategories> lstCharityCategories = new List<CharityCategories>();
            //lstCharityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
            //modelCampaign.loCharityCategories = lstCharityCategories;

            modelCampaign.loStudentOrganizationSearchModel = getSOList("Category", CategoryId, "");

            List<string> loCharityZipcodes = new List<string>();
            loCharityZipcodes = Repository<Charities>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved && x.Zip != "")).OrderBy(x => x.Zip).Select(x => x.Zip).Distinct().ToList();
            modelCampaign.loZipcodes = loCharityZipcodes;
            return PartialView("~/Areas/Company/Views/Campaign/_StudentOrganization.cshtml", modelCampaign);
        }

        [HttpPost]
        public ActionResult getCharityDataByZipcode(string zipcode)
        {
            Campaign modelCampaign = new Campaign();
            List<Expression<Func<Charities, object>>> include = new List<Expression<Func<Charities, object>>>();
            Expression<Func<Charities, object>> statusInclude = (status) => status.CharityCategories;
            include.Add(statusInclude);

            //var complaintDetail = Repository<Charities>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved), null, include).Where(x => (x.Zip == zipcode || zipcode == "")).OrderBy(x => x.CharityName).ToList();
            // modelCampaign.loCharities = complaintDetail;
            //List<CharityCategories> lstCharityCategories = new List<CharityCategories>();
            //lstCharityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
            //modelCampaign.loCharityCategories = lstCharityCategories;

            modelCampaign.loStudentOrganizationSearchModel = getSOList("ZipCode", 0, zipcode);

            List<string> loSOZipCode = new List<string>();
            loSOZipCode = Repository<StudentOrganizations>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved && x.Zip != "")).OrderBy(x => x.Zip).Select(x => x.Zip).Distinct().ToList();
            modelCampaign.loSOZipcodes = loSOZipCode;
            return PartialView("~/Areas/Company/Views/Campaign/_StudentOrganization.cshtml", modelCampaign);
        }


        public Campaign GetSearchModel(string searchBy, int Id, string zipCode)
        {
            Campaign modelCampaign = new Campaign();

            List<Expression<Func<Charities, object>>> include = new List<Expression<Func<Charities, object>>>();
            Expression<Func<Charities, object>> statusInclude = (status) => status.CharityCategories;
            include.Add(statusInclude);

            modelCampaign.loStudentOrganizationSearchModel = getSOList(searchBy, Id, zipCode);

            //List<string> loCharityZipcodes = new List<string>();
            //loCharityZipcodes = Repository<Charities>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved && x.Zip != "")).OrderBy(x => x.Zip).Select(x => x.Zip).Distinct().ToList();
            //modelCampaign.loZipcodes = loCharityZipcodes;

            List<string> loSOZipCode = new List<string>();
            loSOZipCode = Repository<StudentOrganizations>.GetEntityListForQuery(x => (x.IsActive == true && x.UserStatusID == (int)CommonEnums.UserStatus.Approved && x.Zip != "")).OrderBy(x => x.Zip).Select(x => x.Zip).Distinct().ToList();
            modelCampaign.loSOZipcodes = loSOZipCode;

            return modelCampaign;
        }
        public List<StudentOrganizationSearchModel> getSOList(string searchBy, int Id, string ZipCode)
        {

            Campaign modelCampaign = new Campaign();

            Expression<Func<StudentOrganizations, bool>> expression = x => x.IsActive == true && x.IsDeleted == false && x.UserStatusID == (int)CommonEnums.UserStatus.Approved;

            if (searchBy == "ZipCode" && !string.IsNullOrEmpty(ZipCode))
            {
                Expression<Func<StudentOrganizations, bool>> expCharity = x => x.Zip == ZipCode;
                expression = PredicateBuilder.And(expression, expCharity);
            }

            //Include 
            List<Expression<Func<StudentOrganizations, Object>>> includeSO = new List<Expression<Func<StudentOrganizations, object>>>();

            Expression<Func<StudentOrganizations, object>> includesoCharities = (so) => so.StudentOrganizationsCharities;
            includeSO.Add(includesoCharities);

            //Expression<Func<StudentOrganizations, object>> includecharities = (so) => so.StudentOrganizationsCharities.Select(x => x.Charities);
            //includeSO.Add(includecharities);

            //Expression<Func<StudentOrganizations, object>> includecharitiesCat = (so) => so.StudentOrganizationsCharities.Select(x => x.Charities.CharityCategories);
            //includeSO.Add(includecharitiesCat);

            //Sort
            Func<IQueryable<StudentOrganizations>, IOrderedQueryable<StudentOrganizations>> orderingFunc = query => query.OrderBy(x => x.Name);

            List<StudentOrganizations> loStudentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(expression, null, includeSO, null, null).ToList();

            if (loStudentOrganizations != null && loStudentOrganizations.Count > 0)
            {
                foreach (var so in loStudentOrganizations.Distinct())
                {
                    foreach (var soCharities in so.StudentOrganizationsCharities)
                    {
                        Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == soCharities.CharityId).FirstOrDefault();
                        //CharityCategories charityCategories = Repository<CharityCategories>.GetEntityListForQuery(x => x.Id == charity.CategoryID).FirstOrDefault();

                        StudentOrganizationSearchModel objComp = new StudentOrganizationSearchModel();
                        objComp.SOName = so.Name;
                        objComp.CharityName = charity.CharityName;
                        objComp.CharityId = charity.Id;
                        objComp.SoId = so.Id;
                        objComp.ZipCode = so.Zip;
                        //objComp.CategoryName = charityCategories.Name;

                        modelCampaign.loStudentOrganizationSearchModel.Add(objComp);
                    }
                }
            }

            return modelCampaign.loStudentOrganizationSearchModel;
        }
        public ActionResult ManageCampaign(int CurrpageIndex = 1)
        {
            if (CurrpageIndex != 1)
            {
                ViewBag.Currentpageindex = CurrpageIndex;

            }
            else
            {
                ViewBag.Currentpageindex = 1;
            }

            CampaignViewModel foRequest = new CampaignViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.inPageIndex = ViewBag.Currentpageindex;
            CampaignsViewModel loCampaignListModel = getCampaignList(foRequest);

            return View("~/Areas/Company/Views/Campaign/ManageCampaign.cshtml", loCampaignListModel);
        }

        public CampaignsViewModel getCampaignList(CampaignViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                foRequest.stSearch = foRequest.stSearch.Replace("%20", " ");
            }

            CampaignsViewModel objCampaignsViewModel = new CampaignsViewModel();
            int liRecordCount = 0;
            string userid = HttpContext.User.Identity.GetUserId();
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();

            Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderingFunc =
           query => query.OrderBy(x => x.Id);

            Expression<Func<Campaigns, bool>> expression = null;

            if (!string.IsNullOrEmpty(Convert.ToString(foRequest.CampaignStatusID)) && foRequest.CampaignStatusID != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CampaignStatusID == foRequest.CampaignStatusID && x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true;
            else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.CampaignStatusID)) && foRequest.CampaignStatusID != 0)
                expression = x => x.CampaignStatusID == foRequest.CampaignStatusID && x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true;
            else if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true && x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower());
            else
                expression = x => x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true;

            //Include 
            List<Expression<Func<Campaigns, Object>>> Campaignsincludes = new List<Expression<Func<Campaigns, object>>>();

            Expression<Func<Campaigns, object>> campaignStatsName = (campaignstatus) => campaignstatus.CampaignStatus;
            Campaignsincludes.Add(campaignStatsName);

            Expression<Func<Campaigns, object>> companyName = (companyname) => companyname.Companies;
            Campaignsincludes.Add(companyName);

            Expression<Func<Campaigns, object>> charityName = (charityname) => charityname.Charities;
            Campaignsincludes.Add(charityName);

            Expression<Func<Campaigns, object>> studentOrganizations = (so) => so.StudentOrganizations;
            Campaignsincludes.Add(studentOrganizations);

            List<Campaigns> objallCampaigns = Repository<Campaigns>.GetEntityListForQuery(expression, null, Campaignsincludes, null, null).ToList();

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "CampaignName DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignName);
                        break;
                    case "CampaignName ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignName);
                        break;
                    case "Likes DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Likes);
                        break;
                    case "Likes ASC":
                        orderingFunc = q => q.OrderBy(s => s.Likes);
                        break;
                    case "CharitySponsor DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Companies.CompanyName);
                        break;
                    case "CharitySponsor ASC":
                        orderingFunc = q => q.OrderBy(s => s.Companies.CompanyName);
                        break;
                    case "CharityName DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Charities.CharityName);
                        break;
                    case "CharityName ASC":
                        orderingFunc = q => q.OrderBy(s => s.Charities.CharityName);
                        break;
                    case "SOName DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.StudentOrganizations.Name);
                        break;
                    case "SOName ASC":
                        orderingFunc = q => q.OrderBy(s => s.StudentOrganizations.Name);
                        break;
                    case "Status DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignStatus.Name);
                        break;
                    case "Status ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignStatus.Name);
                        break;
                    default:  // Name ascending 
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }


                List<Campaigns> objCampaign = new List<Campaigns>();
                objCampaign = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.IsDeleted == false).ToList();

                if (!string.IsNullOrEmpty(Convert.ToString(foRequest.CampaignStatusID)) && foRequest.CampaignStatusID != 0 && !string.IsNullOrEmpty(foRequest.stSearch))
                {
                    liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true && x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower()) && x.CampaignStatusID == foRequest.CampaignStatusID).Count();
                }
                else if (!string.IsNullOrEmpty(foRequest.stSearch))
                {
                    liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true && x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(foRequest.CampaignStatusID)) && foRequest.CampaignStatusID != 0)
                {
                    liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true && x.CampaignStatusID == foRequest.CampaignStatusID).Count();
                }
                else
                {
                    liRecordCount = Repository<Campaigns>.GetEntityListForQuery(null).Where(x => x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true).Count();
                }

                if (foRequest.inPageIndex > 0)
                {
                    ViewBag.Currentpageindex = foRequest.inPageIndex;
                }

                if (objCampaign != null && objCampaign.Count > 0)
                {
                    foreach (var campaign in objCampaign)
                    {
                        CampaignViewModel objcampaign = new CampaignViewModel
                        {
                            Id = campaign.Id,
                            CreatedBy = campaign.CreatedBy,
                            CreatedOn = campaign.CreatedOn,
                            IsActive = campaign.IsActive,
                            IsDeleted = campaign.IsDeleted,
                            Likes = campaign.Likes,
                            ModifiedBy = campaign.ModifiedBy,
                            ModifiedOn = campaign.ModifiedOn,
                            Caption = campaign.Caption,
                            CampaignName = campaign.CampaignName,
                            CharitySponsor = campaign.Companies.CompanyName,
                            Status = campaign.CampaignStatus.Name,
                            CharityName = campaign.Charities.CharityName,
                            StudentOrganizationName = campaign.StudentOrganizations == null ? "" : campaign.StudentOrganizations.Name,
                            StudentOrganizationId = campaign.StudentOrganizations != null ? campaign.StudentOrganizations.Id : 0,
                            CompanyID = campaign.CompanyID,
                            CharityID = campaign.CharityID
                        };

                        objCampaignsViewModel.loCampaignsList.Add(objcampaign);
                    }
                }
            }
            CampaignsViewModel loCampaignModel = new CampaignsViewModel();
            loCampaignModel.inRecordCount = liRecordCount;
            loCampaignModel.loCampaignsList = objCampaignsViewModel.loCampaignsList;
            loCampaignModel.inPageIndex = foRequest.inPageIndex;
            loCampaignModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCampaignModel;
        }

        public ActionResult searchCampaigns(CampaignViewModel foSearchRequest)
        {
            CampaignsViewModel loCampaignModel = getCampaignList(foSearchRequest);
            return PartialView("~/Areas/Company/Views/Campaign/_ManageCampaign.cshtml", loCampaignModel);
        }

        public ActionResult searchVolunteers(VolunteerModel foSearchRequest)
        {
            CampaignViewModel objCampaignViewModel = new CampaignViewModel();
            objCampaignViewModel.loVolunteerList = getVolunteersList(foSearchRequest);
            if (foSearchRequest.inPageIndex > 0)
            {
                objCampaignViewModel.hdnVolunteerPageIndex = foSearchRequest.inPageIndex;
            }
            else
            {
                objCampaignViewModel.hdnVolunteerPageIndex = 1;
            }
            return PartialView("~/Areas/Company/Views/Shared/_VolunteersDetails.cshtml", objCampaignViewModel);
        }

        public List<VolunteerModel> getVolunteersList(VolunteerModel foRequest)
        {
            foRequest.inPageSize = 5;
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CampaignViewModel objCampaignViewModel = new CampaignViewModel();
            int liRecordCount = 0;

            Expression<Func<CampaignUploads, bool>> expression = null;

            expression = x => x.CampaignID == foRequest.CampaignId && x.IsDeleted == false;

            //Include 
            List<Expression<Func<CampaignUploads, Object>>> CampaignUploadsincludes = new List<Expression<Func<CampaignUploads, object>>>();

            Expression<Func<CampaignUploads, object>> volunteersEntity = (volunteers) => volunteers.Volunteers;
            CampaignUploadsincludes.Add(volunteersEntity);
            List<CampaignUploads> loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(expression, null, CampaignUploadsincludes, null, null).ToList();

            objCampaignViewModel.loVolunteerList = new List<VolunteerModel>();
            if (loCampaignUpload != null && loCampaignUpload.Count > 0)
            {
                foreach (var upload in loCampaignUpload)
                {
                    VolunteerModel objComp = new VolunteerModel
                    {
                        Id = upload.Volunteers.Id,
                        Name = upload.Volunteers.Name,
                        Likes = upload.Likes,
                        ZipCode = upload.Volunteers.ZipCode,
                        inRecordCount = loCampaignUpload.Count(),
                        inVoluneerPageIndex = foRequest.inPageIndex
                    };

                    objCampaignViewModel.loVolunteerList.Add(objComp);
                }
            }

            liRecordCount = objCampaignViewModel.loVolunteerList.Count();
            if (objCampaignViewModel.loVolunteerList.Count > 0)
            {
                if (!string.IsNullOrEmpty(foRequest.stSortColumn))
                {
                    switch (foRequest.stSortColumn)
                    {
                        case "Id DESC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderByDescending(s => s.Id).ToList();
                            break;
                        case "Id ASC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.Id).ToList();
                            break;
                        case "Name DESC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderByDescending(s => s.Name).ToList();
                            break;
                        case "Name ASC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.Name).ToList();
                            break;

                        case "ZipCode DESC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderByDescending(s => s.ZipCode).ToList();
                            break;
                        case "ZipCode ASC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.ZipCode).ToList();
                            break;
                        case "Likes DESC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderByDescending(s => s.Likes).ToList();
                            break;
                        case "Likes ASC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.Likes).ToList();
                            break;
                        default:  // Name ascending 
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.Id).ToList();
                            break;
                    }
                }

                if (foRequest.inPageIndex > 0)
                    objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.Skip((foRequest.inPageIndex - 1) * foRequest.inPageSize).Take(foRequest.inPageSize).ToList();
            }
            return objCampaignViewModel.loVolunteerList;
        }

        public ActionResult CampaignDetails(int Id, int page = 1)
        {
            ViewBag.Currentpageindex = page;
            CampaignViewModel objCampaignViewModel = new CampaignViewModel();
            Campaigns objCampaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();
            objCampaignViewModel = new CampaignViewModel
            {
                Id = objCampaign.Id,
                CreatedBy = objCampaign.CreatedBy,
                CreatedOn = objCampaign.CreatedOn,
                IsActive = objCampaign.IsActive,
                IsDeleted = objCampaign.IsDeleted,
                Likes = objCampaign.Likes,
                ModifiedBy = objCampaign.ModifiedBy,
                ModifiedOn = objCampaign.ModifiedOn,
                Caption = objCampaign.Caption,
                CampaignName = objCampaign.CampaignName,
                ImageFile = string.IsNullOrEmpty(objCampaign.ImageFile) ? Url.Content(CampaignImagePath) + "NoPhoto.jpg" : Url.Content(CampaignImagePath) + objCampaign.ImageFile,
                CampaignCouponImageFile = string.IsNullOrEmpty(objCampaign.CouponCode) ? Url.Content(CampaignCouponImagePath) + "NoPhoto.jpg" : Url.Content(CampaignCouponImagePath) + objCampaign.CouponCode,
                PackagePrice = objCampaign.PackagePrice, //Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(x => x.Id == objCampaign.PackageID).Select(x => x.Price).FirstOrDefault(),
                PackageLikes = objCampaign.PackageLikes,//Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(x => x.Id == objCampaign.PackageID).Select(x => x.Likes).FirstOrDefault(),
                CharityName = Repository<PurpicsWeb_DL.Entities.Charities>.GetEntityListForQuery(x => x.Id == objCampaign.CharityID).Select(x => x.CharityName).FirstOrDefault(),
                StudentOrganizationName = objCampaign.StudentOrganizationId != null && objCampaign.StudentOrganizationId > 0 ? Repository<PurpicsWeb_DL.Entities.StudentOrganizations>.GetEntityListForQuery(x => x.Id == objCampaign.StudentOrganizationId).Select(x => x.Name).FirstOrDefault() : "",
                StudentOrganizationId = objCampaign.StudentOrganizationId
            };

            VolunteerModel foRequest = new VolunteerModel();
            foRequest.stSortColumn = "CampaignName ASC";
            foRequest.inPageIndex = 1;
            foRequest.CampaignId = objCampaign.Id;
            objCampaignViewModel.loVolunteerList = getVolunteersList(foRequest);
            objCampaignViewModel.inRecordCount = objCampaignViewModel.loVolunteerList.Count();
            objCampaignViewModel.hdnVolunteerPageIndex = 1;

            if (objCampaign.CouponCode != null)
            {
                decimal ConversionRate = 0;
                var totalvolCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(x => (x.CampaignID == Id && x.IsDeleted == false));
                var totalLikes = Repository<Campaigns>.GetEntityListForQuery(x => (x.Id == Id)).FirstOrDefault();
                var volRedeemedCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(x => (x.CampaignID == Id && x.IsDeleted == false && x.RedeemptionDate != null));
                if (totalLikes.Likes > 0)
                {
                    ConversionRate = ((Convert.ToDecimal(volRedeemedCoupons.Count()) / Convert.ToDecimal(totalLikes.Likes)) * 100);
                    ConversionRate = Math.Round(ConversionRate, 2);
                }

                objCampaignViewModel.ConversionRateValue = ConversionRate.ToString() + "%";
                objCampaignViewModel.TotalCoupons = totalvolCoupons.Count();
                objCampaignViewModel.RedeemedCoupons = volRedeemedCoupons.Count();
            }
            else
            {
                objCampaignViewModel.ConversionRateValue = "N/A";
                objCampaignViewModel.TotalCoupons = 0;
                objCampaignViewModel.RedeemedCoupons = 0;
            }
            objCampaignViewModel.CouponCode = objCampaign.CouponCode;
            return View("~/Areas/Company/Views/Campaign/CampaignDetails.cshtml", objCampaignViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateCampaign(FormCollection collection, HttpPostedFileBase CampaignImage, HttpPostedFileBase CouponCode)
        {
            int campaignID = Convert.ToInt32(collection["CampaignId"]);
            int Radius = Convert.ToInt32(collection["Radius"]);
            string Zipcode = Convert.ToString(collection["ZipCode"]);
            string userid = HttpContext.User.Identity.GetUserId();
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            string caption = collection["Caption"];
            string CampaignName = collection["CampaignName"];

            var campaign = Repository<Campaigns>.GetEntityListForQuery(r => r.Id == campaignID).FirstOrDefault();

            campaign.Caption = caption;
            campaign.CampaignName = CampaignName;
            campaign.Description = collection["Description"];
            if (collection["ExpirationDate"] != "")
            {
                campaign.ExpirationDate = Convert.ToDateTime(collection["ExpirationDate"]);
            }
            campaign.CouponTitle = collection["CouponTitle"];
            campaign.CouponDescription = collection["CouponDescription"];
            campaign.ModifiedOn = DateTime.Now;
            campaign.ModifiedBy = userid;
            int existingRadius = campaign.Radius;
            campaign.Zipcode = Zipcode;
            string existingZip = campaign.Zipcode;
            campaign.Radius = Radius;
            //Save Campaign image
            if (CampaignImage != null)
            {
                string savepath = Path.Combine(Server.MapPath(CampaignImagePath), campaign.Id.ToString());
                if (!string.IsNullOrEmpty(campaign.ImageFile))
                {
                    if (System.IO.File.Exists(Path.Combine(Server.MapPath(CampaignImagePath), campaign.ImageFile)))
                    {
                        System.IO.File.Delete(Path.Combine(Server.MapPath(CampaignImagePath), campaign.ImageFile));
                    }
                }
                string newfilename = UploadImage(CampaignImage, campaign.Id, savepath);
                campaign.ImageFile = newfilename;
            }

            /// Save Coupon image
            if (CouponCode != null)
            {
                string saveCouponpath = Path.Combine(Server.MapPath(CampaignCouponImagePath), campaign.Id.ToString());

                if (!string.IsNullOrEmpty(campaign.CouponCode))
                {
                    if (System.IO.File.Exists(Path.Combine(Server.MapPath(CampaignCouponImagePath), campaign.CouponCode)))
                    {
                        System.IO.File.Delete(Path.Combine(Server.MapPath(CampaignCouponImagePath), campaign.CouponCode));
                    }
                }
                string newfilename = UploadImage(CouponCode, campaign.Id, saveCouponpath);
                campaign.CouponCode = newfilename;
            }

            await Repository<Campaigns>.UpdateEntity(campaign, entity => { return entity.Id; });

            if (existingZip != Zipcode || existingRadius != Radius)
            {
                BackgroundJob.Enqueue(() => PurpicsWeb.Services.CommonFunctions.CommonFunctions.UpdateCampaignScopeArea(campaignID));
            }

            TempData["SuccessConfirmationMessage"] = "Campaign has been updated successfully.";

            if (Convert.ToBoolean(collection["IsManageDashboard"]) == true)
            {
                return RedirectToAction("", "ManageDashboard", new { CurrpageIndex = Convert.ToInt32(collection["Currentpageindex"]) });
            }
            else
            {
                return RedirectToAction("ManageCampaign", "Campaign", new { CurrpageIndex = Convert.ToInt32(collection["Currentpageindex"]) });
            }
        }

        //
        // GET: /Company/Campaign/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Company/Campaign/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddCampaign(FormCollection collection, HttpPostedFileBase CampaignImage, HttpPostedFileBase CouponCode)
        {

            try
            {
                int CharityID = Convert.ToInt32(collection["CharityID"]);
                int StudentOrganizationId = Convert.ToInt32(collection["StudentOrganizationId"]);
                int Radius = Convert.ToInt32(collection["Radius"]);
                string Zipcode = Convert.ToString(collection["ZipCode"]);
                string userid = HttpContext.User.Identity.GetUserId();
                Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
                int CompanyId = company.Id;
                int PackageId = Convert.ToInt32(collection["hdnPackageSelectedID"]);
                string caption = collection["Caption"];
                string CampaignName = collection["CampaignName"];

                Packages package = Repository<Packages>.GetEntityListForQuery(x => x.Id == PackageId).FirstOrDefault();

                Campaigns campaign = new Campaigns();
                campaign.CharityID = CharityID;
                campaign.StudentOrganizationId = StudentOrganizationId;
                campaign.CompanyID = CompanyId;
                campaign.PackageID = PackageId;
                campaign.CampaignStatusID = 1;
                campaign.Caption = caption;
                campaign.CampaignName = CampaignName;
                campaign.Description = collection["Description"];
                if (collection["ExpirationDate"] != "")
                {
                    campaign.ExpirationDate = Convert.ToDateTime(collection["ExpirationDate"]);
                }
                campaign.CouponTitle = collection["CouponTitle"];
                campaign.CouponDescription = collection["CouponDescription"];
                campaign.CreatedOn = DateTime.Now;
                campaign.CreatedBy = userid;
                campaign.Zipcode = Zipcode;
                campaign.Radius = Radius;
                campaign.PackagePrice = package.Price;
                campaign.PackageLikes = package.Likes;

                AppSettings setting = Repository<AppSettings>.GetEntityListForQuery(x => x.Name == "CampaignPer").FirstOrDefault();
                campaign.CharityPercentage = Convert.ToDecimal(setting.Value);

                await Repository<Campaigns>.InsertEntity(campaign, entity => { return entity.Id; });
                BackgroundJob.Enqueue(() => PurpicsWeb.Services.CommonFunctions.CommonFunctions.UpdateCampaignScopeArea(campaign.Id));
                try
                {
                    var campaigndetails = Repository<Campaigns>.GetEntityListForQuery(r => r.Id == campaign.Id).FirstOrDefault();
                    //Save Campaign image
                    if (CampaignImage != null)
                    {
                        string savepath = Path.Combine(Server.MapPath(CampaignImagePath), campaign.Id.ToString());
                        string newfilename = UploadImage(CampaignImage, campaign.Id, savepath);
                        campaigndetails.ImageFile = newfilename;
                    }

                    // Save Coupon image
                    if (CouponCode != null)
                    {
                        string saveCouponpath = Path.Combine(Server.MapPath(CampaignCouponImagePath), campaign.Id.ToString());
                        string newfilename = UploadImage(CouponCode, campaign.Id, saveCouponpath);
                        campaigndetails.CouponCode = newfilename;
                    }

                    await Repository<Campaigns>.UpdateEntity(campaigndetails, entity => { return entity.Id; });
                    Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == CharityID).FirstOrDefault();
                    string objRegistrationEmailBody = new SystemEmailController().CampaignRegistration(CampaignName, company.CompanyName, campaign.Id);
                    string mailTo = Utility.ExecuteScalar("sp_GetAllAdminEmailId").ToString();
                    BackgroundJob.Enqueue(() => EmailHelper.SendEmail("New Campaign (" + campaign.CampaignName + " for " + company.CompanyName + ") awaiting approval", objRegistrationEmailBody, mailTo));

                }
                catch { }

                return RedirectToAction("ManageCampaign", "Campaign");
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after some time.";
                return RedirectToAction("Index", "Campaign");
            }
        }

        public ActionResult DeleteCampaign(CampaignViewModel foRequest)
        {
            int liSuccess = 0;

            if (foRequest.Id != null && foRequest.Id > 0)
            {
                string userid = HttpContext.User.Identity.GetUserId();
                Campaigns objCampaigns = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == foRequest.Id).FirstOrDefault();
                objCampaigns.ModifiedBy = userid;
                objCampaigns.ModifiedOn = DateTime.Now;
                objCampaigns.IsDeleted = true;
                Repository<Campaigns>.UpdateEntity(objCampaigns, (entity) => { return entity.Id; });

            }
            TempData["SuccessMsg"] = "Campaign has been deleted successfully";
            return this.Json(new CampaignViewModel { Id = liSuccess });
        }
        public string UploadImage(HttpPostedFileBase image, int CampaignId, string savePath)
        {
            string newfileName = "";
            if (image != null)
            {
                Dictionary<string, string> loVersions = new Dictionary<string, string>();
                loVersions.Add("", "maxwidth=1080&maxheight=1350&AutoRotate=true");

                //Generate each version
                foreach (var loKey in loVersions.Keys)
                {
                    Image img = Image.FromStream(image.InputStream);
                    var ExIfImage = img.PropertyItems.Where(x => x.Id == 0x0112).FirstOrDefault();

                    if (ExIfImage != null)
                    {
                        int orientationValue = img.GetPropertyItem(0x0112).Value[0];
                        RotateFlipType rotateFlipType = CommonFunctions.GetOrientationToFlipType(orientationValue);
                        img.RotateFlip(rotateFlipType);
                    }

                    ImageBuilder.Current.Build(
                             new ImageJob(
                                 img,
                                 savePath,
                                 new Instructions(loVersions[loKey]),
                                 false,
                                 true));
                    //                    string lsFileExt = Path.GetExtension(image.FileName);
                    string newext = "";
                    if (ImageFormat.Jpeg.Equals(img.RawFormat))
                    {
                        newext = ".JPG";
                    }
                    else if (ImageFormat.Png.Equals(img.RawFormat))
                    {
                        newext = ".PNG";
                    }
                    else if (ImageFormat.Gif.Equals(img.RawFormat))
                    {
                        newext = ".GIF";
                    }
                    newfileName = CampaignId.ToString() + newext;
                    return newfileName;
                }

            }
            return "";
        }
    }

    public class CampaignsWithDetails : PurpicsWeb_DL.Entities.Campaigns
    {
        public string CharitySponsor { get; set; }
        public string Status { get; set; }
        public string CharityName { get; set; }
        public string StudentOrganizationName { get; set; }

    }
}
