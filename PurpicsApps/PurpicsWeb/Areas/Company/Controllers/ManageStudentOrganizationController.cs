﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Areas.Company.Models.StudentOrganization;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Linq.Expressions;

namespace PurpicsWeb.Areas.Company.Controllers
{
    [Authorize(Roles = "Company")]
    public class ManageStudentOrganizationController : Controller
    {
        // GET: Company/ManageStudentOrganizationController
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StudentOrganizationDetail(int StudentOrganizationId, int page = 1, bool IsFromDashboard = false, bool showBackButton = true)
        {
            ViewBag.Currentpageindex = page;
            StudentOrganizations lowebstats = Repository<StudentOrganizations>.GetEntityListForQuery(x => x.Id == StudentOrganizationId).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();

            StudentOrganizationViewModel loCompanyViewModel = new StudentOrganizationViewModel();
            loCompanyViewModel.Id = StudentOrganizationId;
            loCompanyViewModel.STATUS = lowebstats.UserStatusID;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            loCompanyViewModel.Company = "-";
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser.UserName;
            loCompanyViewModel.LogoFileName = lowebstats.LogoFileName;
            loCompanyViewModel.IsManageDashboard = IsFromDashboard;
            loCompanyViewModel.ShowBackButton = showBackButton;
            return View(loCompanyViewModel);
        }
    }
}