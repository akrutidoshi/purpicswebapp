﻿using PurpicsWeb.Areas.Company.Models;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Areas.Company.Models.Campaigns;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Threading.Tasks;
using System.IO;

namespace PurpicsWeb.Areas.Company.Controllers
{
    [Authorize(Roles = "Company")]
    public class ManageDashboardController : Controller
    {
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        string CampaignCouponImagePath = ConfigurationManager.AppSettings["CampaignCouponImagePath"].ToString();
        // GET: Company/ManageDashboard
        public ActionResult Index(int CurrpageIndex = 1)
        {
            if (CurrpageIndex != 1)
            {
                ViewBag.Currentpageindex = CurrpageIndex;

            }
            else
            {
                ViewBag.Currentpageindex = 1;
            }

            CampaignViewModel foRequest = new CampaignViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.inPageIndex = ViewBag.Currentpageindex;
            CampaignsViewModel loCampaignListModel = getCampaignList(foRequest);
            loCampaignListModel.IsManageDashboard = true;

            string userid = HttpContext.User.Identity.GetUserId();
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            List<PurpicsWeb_DL.Entities.Campaigns> objallCampaigns = Repository<PurpicsWeb_DL.Entities.Campaigns>.GetEntityListForQuery(x => x.CompanyID == company.Id).OrderBy(x => x.CampaignName).Where(x => (x.IsDeleted == false && x.IsActive == true && x.CampaignStatusID == 2)).ToList();
            loCampaignListModel.TotalActiveCampaigns = objallCampaigns.Count();
            loCampaignListModel.TotalLikes = objallCampaigns.Select(x => x.Likes).Sum();
            return View("~/Areas/Company/Views/Dashboard/ManageDashboard.cshtml", loCampaignListModel);
        }

        public CampaignsViewModel getCampaignList(CampaignViewModel foRequest)
        {
            foRequest.inPageSize = 5;
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CampaignsViewModel objCampaignsViewModel = new CampaignsViewModel();
            int liRecordCount = 0;
            string userid = HttpContext.User.Identity.GetUserId();
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            Expression<Func<Campaigns, bool>> expression = null;

            expression = x => x.CompanyID == company.Id && x.IsDeleted == false && x.IsActive == true && x.CampaignStatusID == (int)PurpicsWeb.Services.CommonFunctions.CommonEnums.UserStatus.Approved;

            //Include 
            List<Expression<Func<Campaigns, Object>>> Campaignsincludes = new List<Expression<Func<Campaigns, object>>>();

            Expression<Func<Campaigns, object>> campaignStatsName = (campaignstatus) => campaignstatus.CampaignStatus;
            Campaignsincludes.Add(campaignStatsName);

            Expression<Func<Campaigns, object>> companyName = (companyname) => companyname.Companies;
            Campaignsincludes.Add(companyName);

            Expression<Func<Campaigns, object>> charityName = (charityname) => charityname.Charities;
            Campaignsincludes.Add(charityName);

            Expression<Func<Campaigns, object>> studentOrganizations = (so) => so.StudentOrganizations;
            Campaignsincludes.Add(studentOrganizations);

            List<Campaigns> objallCampaigns = Repository<Campaigns>.GetEntityListForQuery(expression, null, Campaignsincludes, null, null).ToList();
            List<CampaignsWithDetails> objCampaigns = new List<CampaignsWithDetails>();
            CampaignsWithDetails campaignWithDetails = null;
            foreach (Campaigns campaign in objallCampaigns)
            {

                campaignWithDetails = new CampaignsWithDetails();
                campaignWithDetails.Id = campaign.Id;
                campaignWithDetails.Caption = campaign.Caption;
                campaignWithDetails.CampaignName = campaign.CampaignName;
                campaignWithDetails.IsActive = campaign.IsActive;
                campaignWithDetails.Status = campaign.CampaignStatus.Name;
                campaignWithDetails.Likes = campaign.Likes;
                campaignWithDetails.CharitySponsor = campaign.Companies.CompanyName;
                campaignWithDetails.CharityName = campaign.Charities.CharityName;
                campaignWithDetails.StudentOrganizationName = campaign.StudentOrganizations == null ? "" : campaign.StudentOrganizations.Name;
                campaignWithDetails.CompanyID = campaign.CompanyID;
                campaignWithDetails.CharityID = campaign.CharityID;
                campaignWithDetails.StudentOrganizationId = campaign.StudentOrganizationId;

                objCampaigns.Add(campaignWithDetails);

            }

            if (!string.IsNullOrEmpty(foRequest.stSearch))
            {
                objCampaigns = objCampaigns.Where(s => (s.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower()) || s.CharityName.ToLower().Contains(foRequest.stSearch.ToLower()) || s.CharitySponsor.ToLower().Contains(foRequest.stSearch.ToLower()))).ToList();
            }

            liRecordCount = objCampaigns.Count();
            if (objCampaigns.Count > 0)
            {
                if (!string.IsNullOrEmpty(foRequest.stSortColumn))
                {
                    switch (foRequest.stSortColumn)
                    {
                        case "Id DESC":
                            objCampaigns = objCampaigns.OrderByDescending(s => s.Id).ToList();
                            break;
                        case "Id ASC":
                            objCampaigns = objCampaigns.OrderBy(s => s.Id).ToList();
                            break;
                        case "Caption DESC":
                            objCampaigns = objCampaigns.OrderByDescending(s => s.CampaignName).ToList();
                            break;
                        case "Caption ASC":
                            objCampaigns = objCampaigns.OrderBy(s => s.CampaignName).ToList();
                            break;
                        case "Likes DESC":
                            objCampaigns = objCampaigns.OrderByDescending(s => s.Likes).ToList();
                            break;
                        case "Likes ASC":
                            objCampaigns = objCampaigns.OrderBy(s => s.Likes).ToList();
                            break;
                        case "CharitySponsor DESC":
                            objCampaigns = objCampaigns.OrderByDescending(s => s.CharitySponsor).ToList();
                            break;
                        case "CharitySponsor ASC":
                            objCampaigns = objCampaigns.OrderBy(s => s.CharitySponsor).ToList();
                            break;

                        case "CharityName DESC":
                            objCampaigns = objCampaigns.OrderByDescending(s => s.CharityName).ToList();
                            break;
                        case "CharityName ASC":
                            objCampaigns = objCampaigns.OrderBy(s => s.CharityName).ToList();
                            break;
                        default:  // Name ascending 
                            objCampaigns = objCampaigns.OrderBy(s => s.Id).ToList();
                            break;
                    }
                }
                if (foRequest.inPageIndex > 0)
                {
                    ViewBag.Currentpageindex = foRequest.inPageIndex;
                }
                if (foRequest.inPageIndex > 0)
                    objCampaigns = objCampaigns.Skip((foRequest.inPageIndex - 1) * foRequest.inPageSize).Take(foRequest.inPageSize).ToList();
                if (objCampaigns != null && objCampaigns.Count > 0)
                {
                    foreach (var campaign in objCampaigns)
                    {
                        CampaignViewModel objCampaign = new CampaignViewModel
                        {
                            Id = campaign.Id,
                            CreatedBy = campaign.CreatedBy,
                            CreatedOn = campaign.CreatedOn,
                            IsActive = campaign.IsActive,
                            IsDeleted = campaign.IsDeleted,
                            Likes = campaign.Likes,
                            ModifiedBy = campaign.ModifiedBy,
                            ModifiedOn = campaign.ModifiedOn,
                            Caption = campaign.Caption,
                            CampaignName = campaign.CampaignName,
                            CharitySponsor = campaign.CharitySponsor,
                            Status = campaign.Status,
                            CharityName = campaign.CharityName,
                            CompanyID = campaign.CompanyID,
                            CharityID = campaign.CharityID,
                            StudentOrganizationName = campaign.StudentOrganizationName == null ? "" : campaign.StudentOrganizationName,
                            StudentOrganizationId = campaign.StudentOrganizationId
                        };

                        objCampaignsViewModel.loCampaignsList.Add(objCampaign);
                    }
                }
            }
            CampaignsViewModel loCampaignModel = new CampaignsViewModel();
            loCampaignModel.inRecordCount = liRecordCount;
            loCampaignModel.loCampaignsList = objCampaignsViewModel.loCampaignsList;
            loCampaignModel.IsManageDashboard = true;
            return loCampaignModel;
        }

        public ActionResult searchCampaigns(CampaignViewModel foSearchRequest)
        {
            CampaignsViewModel loCampaignModel = getCampaignList(foSearchRequest);
            return PartialView("~/Areas/Company/Views/Campaign/_ManageCampaign.cshtml", loCampaignModel);
        }

        public ActionResult CampaignDetails(int Id, int page = 1)
        {
            ViewBag.Currentpageindex = page;
            CampaignViewModel objCampaignViewModel = new CampaignViewModel();


            Campaigns objCampaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();

            objCampaignViewModel = new CampaignViewModel
            {
                Id = objCampaign.Id,
                CreatedBy = objCampaign.CreatedBy,
                CreatedOn = objCampaign.CreatedOn,
                IsActive = objCampaign.IsActive,
                IsDeleted = objCampaign.IsDeleted,
                Likes = objCampaign.Likes,
                ModifiedBy = objCampaign.ModifiedBy,
                ModifiedOn = objCampaign.ModifiedOn,
                Caption = objCampaign.Caption,
                CampaignName = objCampaign.CampaignName,
                ImageFile = string.IsNullOrEmpty(objCampaign.ImageFile) ? Url.Content(CampaignImagePath) + "NoPhoto.jpg" : Url.Content(CampaignImagePath) + objCampaign.ImageFile,
                CampaignCouponImageFile = string.IsNullOrEmpty(objCampaign.CouponCode) ? Url.Content(CampaignCouponImagePath) + "NoPhoto.jpg" : Url.Content(CampaignCouponImagePath) + objCampaign.CouponCode,
                PackagePrice = Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(x => x.Id == objCampaign.PackageID).Select(x => x.Price).FirstOrDefault(),
                PackageLikes = Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(x => x.Id == objCampaign.PackageID).Select(x => x.Likes).FirstOrDefault(),
                CharityName = Repository<PurpicsWeb_DL.Entities.Charities>.GetEntityListForQuery(x => x.Id == objCampaign.CharityID).Select(x => x.CharityName).FirstOrDefault(),
                StudentOrganizationName = objCampaign.StudentOrganizationId == null ? "" : Repository<PurpicsWeb_DL.Entities.StudentOrganizations>.GetEntityListForQuery(x => x.Id == objCampaign.StudentOrganizationId).Select(x => x.Name).FirstOrDefault()
            };

            VolunteerModel foRequest = new VolunteerModel();
            foRequest.stSortColumn = "CampaignName ASC";
            foRequest.inPageIndex = 1;
            foRequest.CampaignId = objCampaign.Id;
            objCampaignViewModel.loVolunteerList = getVolunteersList(foRequest);
            objCampaignViewModel.inRecordCount = objCampaignViewModel.loVolunteerList.Count();
            objCampaignViewModel.hdnVolunteerPageIndex = 1;
            objCampaignViewModel.IsManageDashboard = true;
            if (objCampaign.CouponCode != null)
            {
                decimal ConversionRate = 0;
                var totalvolCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(x => (x.CampaignID == Id && x.IsDeleted == false));
                var totalLikes = Repository<Campaigns>.GetEntityListForQuery(x => (x.Id == Id)).FirstOrDefault();
                var volRedeemedCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(x => (x.CampaignID == Id && x.IsDeleted == false && x.RedeemptionDate != null));
                if (totalLikes.Likes > 0)
                {
                    ConversionRate = ((Convert.ToDecimal(volRedeemedCoupons.Count()) / Convert.ToDecimal(totalLikes.Likes)) * 100);
                    ConversionRate = Math.Round(ConversionRate, 2);
                }

                objCampaignViewModel.ConversionRateValue = ConversionRate.ToString() + "%";
                objCampaignViewModel.TotalCoupons = totalvolCoupons.Count();
                objCampaignViewModel.RedeemedCoupons = volRedeemedCoupons.Count();
            }
            else
            {
                objCampaignViewModel.ConversionRateValue = "N/A";
                objCampaignViewModel.TotalCoupons = 0;
                objCampaignViewModel.RedeemedCoupons = 0;
            }
            objCampaignViewModel.CouponCode = objCampaign.CouponCode;
            return View("~/Areas/Company/Views/Campaign/CampaignDetails.cshtml", objCampaignViewModel);
        }

        public List<VolunteerModel> getVolunteersList(VolunteerModel foRequest)
        {
            foRequest.inPageSize = 5;
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CampaignViewModel objCampaignViewModel = new CampaignViewModel();
            int liRecordCount = 0;

            Expression<Func<CampaignUploads, bool>> expression = null;

            expression = x => x.CampaignID == foRequest.CampaignId && x.IsDeleted == false;

            //Include 
            List<Expression<Func<CampaignUploads, Object>>> CampaignUploadsincludes = new List<Expression<Func<CampaignUploads, object>>>();

            Expression<Func<CampaignUploads, object>> volunteersEntity = (volunteers) => volunteers.Volunteers;
            CampaignUploadsincludes.Add(volunteersEntity);
            List<CampaignUploads> loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(expression, null, CampaignUploadsincludes, null, null).ToList();


            objCampaignViewModel.loVolunteerList = new List<VolunteerModel>();
            if (loCampaignUpload != null && loCampaignUpload.Count > 0)
            {
                foreach (var upload in loCampaignUpload)
                {
                    VolunteerModel objComp = new VolunteerModel
                    {
                        Id = upload.Volunteers.Id,
                        Name = upload.Volunteers.Name,
                        Likes = upload.Likes,
                        ZipCode = upload.Volunteers.ZipCode,
                        inRecordCount = loCampaignUpload.Count(),
                        inVoluneerPageIndex = foRequest.inPageIndex
                    };

                    objCampaignViewModel.loVolunteerList.Add(objComp);
                }
            }


            liRecordCount = objCampaignViewModel.loVolunteerList.Count();
            if (objCampaignViewModel.loVolunteerList.Count > 0)
            {
                if (!string.IsNullOrEmpty(foRequest.stSortColumn))
                {
                    switch (foRequest.stSortColumn)
                    {
                        case "Id DESC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderByDescending(s => s.Id).ToList();
                            break;
                        case "Id ASC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.Id).ToList();
                            break;
                        case "Name DESC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderByDescending(s => s.Name).ToList();
                            break;
                        case "Name ASC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.Name).ToList();
                            break;

                        case "ZipCode DESC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderByDescending(s => s.ZipCode).ToList();
                            break;
                        case "ZipCode ASC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.ZipCode).ToList();
                            break;
                        case "Likes DESC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderByDescending(s => s.Likes).ToList();
                            break;
                        case "Likes ASC":
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.Likes).ToList();
                            break;
                        default:  // Name ascending 
                            objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.OrderBy(s => s.Id).ToList();
                            break;
                    }
                }
                if (foRequest.inPageIndex > 0)
                {
                    ViewBag.Currentpageindex = foRequest.inPageIndex;
                }
                if (foRequest.inPageIndex > 0)
                    objCampaignViewModel.loVolunteerList = objCampaignViewModel.loVolunteerList.Skip((foRequest.inPageIndex - 1) * foRequest.inPageSize).Take(foRequest.inPageSize).ToList();

            }

            return objCampaignViewModel.loVolunteerList;
        }
    }
}