﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Areas.Company.Models.Company;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Linq.Expressions;

namespace PurpicsWeb.Areas.Company.Controllers
{
    [Authorize(Roles = "Company")]
    public class ManageCompanyController : Controller
    {
        // GET: Company/ManageCompany
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CompanyDetail(int CompanyId = 0, int page = 1, bool IsFromDashboard = false)
        {
            ViewBag.Currentpageindex = page;
            Companies lowebstats = Repository<Companies>.GetEntityListForQuery(x => x.Id == CompanyId).FirstOrDefault();
            AspNetUsers loUser = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == lowebstats.AspNetUserID).FirstOrDefault();
            CompanyViewModel loCompanyViewModel = new CompanyViewModel();
            loCompanyViewModel.Id = Convert.ToInt32(CompanyId);
            loCompanyViewModel.LegalEntityName = lowebstats.LegalEntityName;
            loCompanyViewModel.Status = lowebstats.Status;
            loCompanyViewModel.ContactPerson = lowebstats.ContactPerson;
            //loCompanyViewModel.C501C3Number = lowebstats.C501C3Number;
            loCompanyViewModel.CompanyName = lowebstats.CompanyName;
            loCompanyViewModel.ContactPhone = lowebstats.ContactPhone;
            loCompanyViewModel.About = lowebstats.About;
            loCompanyViewModel.Address = lowebstats.Address;
            loCompanyViewModel.AspNetUsername = loUser.UserName;
            loCompanyViewModel.LogoFilename = lowebstats.LogoFilename;
            loCompanyViewModel.IsManageDashboard = IsFromDashboard;
            return View(loCompanyViewModel);
        }
    }
}