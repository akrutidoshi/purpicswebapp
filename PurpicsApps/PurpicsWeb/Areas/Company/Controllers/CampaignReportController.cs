﻿using PurpicsWeb_DL.Entities;
using PurpicsWeb.Areas.Company.Models.CampaignReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PurpicsWeb_DL.Repositories;
using System.Linq.Expressions;
using PurpicsWeb.Services;
using PurpicsWeb.Services.CommonFunctions;

namespace PurpicsWeb.Areas.Company.Controllers
{
    public class CampaignReportController : Controller
    {
        // GET: Company/CampaignReport
        public ActionResult Index(int CurrpageIndex = 1)
        {
            if (CurrpageIndex != 1)
            {
                ViewBag.Currentpageindex = CurrpageIndex;

            }
            else
            {
                ViewBag.Currentpageindex = 1;
            }
            CampaignReportViewModel foRequest = new CampaignReportViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.inPageIndex = ViewBag.Currentpageindex;
            CampaignReportModel loCampaignReport = getCampaign(foRequest);

            return View(loCampaignReport);
        }
        public CampaignReportModel getCampaign(CampaignReportViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;


            CampaignReportModel objCampaignReportModel = new CampaignReportModel();
            int liRecordCount = 0;

            string userid = HttpContext.User.Identity.GetUserId();
            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();

            DateTime? ldFromDate = null;
            if (!string.IsNullOrEmpty(foRequest.lsFromDate))
                ldFromDate = Convert.ToDateTime(foRequest.lsFromDate + " 00:00:01");

            DateTime? ldToDate = null;
            if (!string.IsNullOrEmpty(foRequest.lsToDate))
                ldToDate = Convert.ToDateTime(foRequest.lsToDate + " 23:59:59");

            Expression<Func<Campaigns, bool>> expression = null;
            if (!string.IsNullOrEmpty(foRequest.lsFromDate) && !string.IsNullOrEmpty(foRequest.lsToDate))
                expression = x => x.CompanyID == company.Id && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignStartDate >= ldFromDate && x.CampaignEndDate <= ldToDate;
            else if (!string.IsNullOrEmpty(foRequest.lsFromDate))
                expression = x => x.CompanyID == company.Id && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignStartDate >= ldFromDate;
            else if (!string.IsNullOrEmpty(foRequest.lsToDate))
                expression = x => x.CompanyID == company.Id && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved && x.CampaignEndDate <= ldToDate;
            else
                expression = x => x.CompanyID == company.Id && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved;

            Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderingFunc =
            query => query.OrderBy(x => x.Id);

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "Likes DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Likes);
                        break;
                    case "Likes ASC":
                        orderingFunc = q => q.OrderBy(s => s.Likes);
                        break;
                    case "PackageLikes DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.PackageLikes);
                        break;
                    case "PackageLikes ASC":
                        orderingFunc = q => q.OrderBy(s => s.PackageLikes);
                        break;
                    case "CampaignStartDate DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignStartDate);
                        break;
                    case "CampaignStartDate ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignStartDate);
                        break;
                    case "CampaignEndDate DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignEndDate);
                        break;
                    case "CampaignEndDate ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignEndDate);
                        break;
                    case "CampaignStatusID ASC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignStatusID).ThenByDescending(s => s.CampaignEndDate);
                        break;
                    case "CampaignStatusID DESC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignStatusID).ThenBy(s => s.CampaignEndDate);
                        break;
                    case "TimeToReachTarget ASC":
                        orderingFunc = q => q.OrderBy(s => (System.Data.Entity.DbFunctions.DiffDays(s.CampaignStartDate, s.CampaignEndDate)));
                        break;
                    case "TimeToReachTarget DESC":
                        orderingFunc = q => q.OrderByDescending(s => (System.Data.Entity.DbFunctions.DiffDays(s.CampaignStartDate, s.CampaignEndDate)));
                        break;
                    default:  // Name ascending 
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }
            }


            //Include 
            List<Expression<Func<Campaigns, Object>>> bilingHistory = new List<Expression<Func<Campaigns, object>>>();


            List<Campaigns> objallCharityHistory = new List<Campaigns>();
            objallCharityHistory = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, bilingHistory, foRequest.inPageIndex, foRequest.inPageSize).ToList();
            liRecordCount = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, bilingHistory).Count();
            if (objallCharityHistory.Count > 0)
            {
                foreach (var history in objallCharityHistory)
                {
                    CampaignReportViewModel objhistory = new CampaignReportViewModel
                    {
                        Id = history.Id,
                        CampaignStatusID = history.CampaignStatusID,
                        Likes = history.Likes,
                        PackageLikes = history.PackageLikes,
                        CampaignStartDate = history.CampaignStartDate,
                        CampaignEndDate = history.CampaignEndDate,
                        CouponRedeem = getCouponRedeem(history.Id),
                        Days = getCampaignTargetReachDays(history.Id)
                    };
                    objCampaignReportModel.loCampaignReportViewModel.Add(objhistory);

                }
            }
            CampaignReportModel loCampaignListModel = new CampaignReportModel();
            loCampaignListModel.inRecordCount = liRecordCount;
            loCampaignListModel.loCampaignReportViewModel = objCampaignReportModel.loCampaignReportViewModel;
            loCampaignListModel.inPageIndex = foRequest.inPageIndex;
            ViewBag.Currentpageindex = foRequest.inPageIndex;
            loCampaignListModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCampaignListModel;
        }
        public string getCouponRedeem(int fiCampaignId)
        {
            string lsCouponRedeem = string.Empty;
            Campaigns campaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == fiCampaignId).FirstOrDefault();
            if (campaign != null)
            {
                if (!string.IsNullOrEmpty(campaign.CouponCode))
                {
                    int liRedeemCoupon = Repository<VolunteerCoupons>.GetEntityListForQuery(x => x.CampaignID == fiCampaignId && x.RedeemptionDate != null).Count();
                    lsCouponRedeem = Convert.ToString(liRedeemCoupon);
                }
                else
                    lsCouponRedeem = "N/A";
            }
            else
                lsCouponRedeem = "N/A";
            return lsCouponRedeem;
        }
        public int getCampaignTargetReachDays(int fiCampaignId)
        {
            int liDays = 0;
            Campaigns campaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == fiCampaignId).FirstOrDefault();
            if (campaign != null && campaign.CampaignEndDate != null && campaign.CampaignStartDate != null)
            {
                liDays = Convert.ToInt32((Convert.ToDateTime(campaign.CampaignEndDate) - Convert.ToDateTime(campaign.CampaignStartDate)).TotalDays);
            }
            return liDays;
        }
        public ActionResult searchCampaign(CampaignReportViewModel foRequest)
        {
            CampaignReportModel loCampaignReport = getCampaign(foRequest);
            return PartialView("~/Areas/Company/Views/CampaignReport/_CampaignReport.cshtml", loCampaignReport);
        }
    }
}