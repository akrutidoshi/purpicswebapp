﻿using Hangfire;
using Braintree;
using Microsoft.AspNet.Identity.Owin;
using PurpicsWeb.Areas.Company.Models;
using PurpicsWeb.Models;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Services;
using System.Text;
using System.Drawing;
using ImageResizer;
using PurpicsWeb.Controllers;
using System.Drawing.Imaging;

namespace PurpicsWeb.Areas.Company.Controllers
{
    
    public class RegistrationController : Controller
    {
        string CompanyImagePath = ConfigurationManager.AppSettings["CompanyImagePath"].ToString();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        //
        // GET: /Company/Registration/
        public ActionResult Index()
        {
            return View();
        }
        [ActionName("Step1")]
        public ActionResult Registration()
        {
            Registration objRegistration = new Registration();
            if (Session["Registration"] != null)
            {
                objRegistration = (Registration)Session["Registration"];
            }
            return View("Register", objRegistration);
        }

        public ActionResult Payment()
        {
            
            return View("Payment");
        }

        [HttpPost]
        [ValidateInput(false)] 
        public async Task<ActionResult> AddCompanyRegistration(Registration objRegistration)
        {
            List<AspNetUsers> users = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == objRegistration.UserName).ToList();
            if (users.Count > 0)
            {
                TempData["Message"] = "The email address " + objRegistration.UserName + " is already registered. Please login.";
                return View("Register", objRegistration);
            }
            string newfilename = UploadCompanyLogo(objRegistration.CompanyLogo);

            objRegistration.TempCompanyFileName = newfilename.ToString();
            objRegistration.CompanyLogo = null;
            Session["Registration"] = objRegistration;
            return RedirectToAction("Step2", "Payment", new { CompanyId = 0 });
        }


        [HttpPost]
        [ValidateInput(false)] 
        public async Task<ActionResult> AddNewCompanyRegistration(FormCollection collection)
        {
            try
            {
                BraintreePayments payment = new BraintreePayments();
                Result<Customer> result = payment.CreateNewCustomer(collection);
                if (result.IsSuccess())
                {

                    Registration objRegistration = (Registration)Session["Registration"];
                    // Create Entry in ASPNetUsers table
                    var user = new ApplicationUser() { UserName = objRegistration.UserName, Email = objRegistration.UserName, PhoneNumber = objRegistration.PhoneNumber1 + "-" + objRegistration.PhoneNumber2 + "-" + objRegistration.PhoneNumber3 };
                    var userresult = await UserManager.CreateAsync(user, objRegistration.Password);
                    if (userresult.Succeeded)
                    {
                        string strCompanyRole = CommonConstants.RoleNames.Company;
                        await UserManager.AddToRoleAsync(user.Id, strCompanyRole);
                        Companies cmpRegistration = new Companies();
                        cmpRegistration.ContactPerson = objRegistration.ContactName;
                        cmpRegistration.ContactPhone = objRegistration.PhoneNumber1 + "-" + objRegistration.PhoneNumber2 + "-" + objRegistration.PhoneNumber3;
                        cmpRegistration.About = objRegistration.AboutCompany;
                        cmpRegistration.LegalEntityName = objRegistration.LegalEntityName;                        
                        cmpRegistration.CompanyName = objRegistration.CompanyName;
                        cmpRegistration.Address = objRegistration.CompanyAddress;
                        cmpRegistration.CreatedOn = DateTime.Now;
                        cmpRegistration.CreatedBy = user.Id;
                        cmpRegistration.AspNetUserID = user.Id;
                        cmpRegistration.IsActive = true;
                        cmpRegistration.BraintreeCustomerId = result.Target.Id;
                        await Repository<Companies>.InsertEntity(cmpRegistration, entity => { return entity.Id; });
                        

                        string TempFilesPath = Server.MapPath("~/Images/TempStoredFiles/") + objRegistration.TempCompanyFileName.ToString();
                        string lsFileExt = Path.GetExtension(TempFilesPath);
                        string savepath = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["CompanyImagePath"]), cmpRegistration.Id.ToString() + lsFileExt);

                        if (System.IO.File.Exists(TempFilesPath))
                        {

                            System.IO.File.Copy(TempFilesPath, savepath);
                            System.IO.File.Delete(TempFilesPath);
                        }

                        //objRegistration.CompanyLogo.SaveAs(savepath);

                        var company = Repository<Companies>.GetEntityListForQuery(r => r.Id == cmpRegistration.Id).FirstOrDefault();
                        company.LogoFilename = cmpRegistration.Id.ToString() + lsFileExt;


                        await Repository<Companies>.UpdateEntity(company, entity => { return entity.Id; });

                        string objRegistrationEmailBody = new SystemEmailController().CompanyRegistration(objRegistration, cmpRegistration.Id);

                        string mailTo = Utility.ExecuteScalar("sp_GetAllAdminEmailId").ToString();
                        try
                        {
                            BackgroundJob.Enqueue(() => EmailHelper.SendEmail("New Company Registration", objRegistrationEmailBody, mailTo));

                        }
                        catch { }
                        Session["Registration"] = null;
                        return View("Thanks");
                    }
                    else
                    {
                        TempData["Message"] = "The email address " + objRegistration.UserName + " is already registered. Please login.";
                        return View("Register", objRegistration);
                    }
                }
                else
                {
                    if (result.Message == "Duplicate card exists in the vault.")
                    {
                        ModelState.AddModelError("CardError", "This credit card is already on file.");
                    }
                    else
                    {
                        ModelState.AddModelError("CardError", result.Message.ToString());
                    }
                    PaymentDTO model = new PaymentDTO();
                    model = GetModel();
                    model.StatesList = CommonFunctions.GetStateList();
                    model.CreditCardExpireMonthList = CommonFunctions.getCreditCardMonth();
                    model.month = "";
                    model.CreditCardExpireYearList = CommonFunctions.getCreditCardYear();
                    model.year = "";
                    model.CreditCardDetails = new CreditCardDetails();
                    model.CreditCardDetails.MaskedNumber = collection["number"];
                    model.CreditCardDetails.CardholderName = collection["card-holder-name"];
                    model.CreditCardDetails.BillingAddress.StreetAddress = collection["street_address"];
                    model.CreditCardDetails.BillingAddress.Locality = collection["locality"];
                    model.CreditCardDetails.BillingAddress.Region = collection["CreditCardDetails.BillingAddress.Region"];
                    model.CreditCardDetails.BillingAddress.PostalCode = collection["postal_code"];
                    model.CreditCardDetails.ExpirationDate = collection["month"] + "/" + collection["year"];                    
                    return View("~/Areas/Company/Views/Payment/AddEditCreditCard.cshtml", model);
                }

            }
            catch (Exception ex)
            {
                PaymentDTO model = new PaymentDTO();
                model.StatesList = CommonFunctions.GetStateList();
                model.CreditCardExpireMonthList = CommonFunctions.getCreditCardMonth();
                model.month = "";
                model.CreditCardExpireYearList = CommonFunctions.getCreditCardYear();
                model.year = "";
                model.CreditCardDetails = new CreditCardDetails();
                model.CreditCardDetails.MaskedNumber = collection["number"];
                model.CreditCardDetails.CardholderName = collection["card-holder-name"];
                model.CreditCardDetails.BillingAddress.StreetAddress = collection["street_address"];
                model.CreditCardDetails.BillingAddress.Locality = collection["locality"];
                model.CreditCardDetails.BillingAddress.Region = collection["CreditCardDetails.BillingAddress.Region"];
                model.CreditCardDetails.BillingAddress.PostalCode = collection["postal_code"];
                model.CreditCardDetails.ExpirationDate = collection["month"] + "/" + collection["year"];
                ModelState.AddModelError("CardError","Sorry an error occured. Please try again.");
                return View("~/Areas/Company/Views/Payment/AddEditCreditCard.cshtml", model);
            }

            return View();

        }

        public PaymentDTO GetModel()
        {
            PaymentDTO model = new PaymentDTO();
            model.StatesList = CommonFunctions.GetStateList();
           
            return model;
        }

        public string UploadCompanyLogo(HttpPostedFileBase companyLogo)
        {

            if (companyLogo != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("~/Images/TempStoredFiles")))
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/Images/TempStoredFiles"));
                }

                string newFilename = Guid.NewGuid().ToString();

                string savepath = Path.Combine(Server.MapPath("~/Images/TempStoredFiles/"), newFilename);




                Dictionary<string, string> loVersions = new Dictionary<string, string>();
                loVersions.Add("", "maxwidth=250&maxheight=300&AutoRotate=true");

                //Generate each version
                foreach (var loKey in loVersions.Keys)
                {
                    Image img = Image.FromStream(companyLogo.InputStream);
                    var ExIfImage = img.PropertyItems.Where(x => x.Id == 0x0112).FirstOrDefault();

                    if (ExIfImage != null)
                    {
                        int orientationValue = img.GetPropertyItem(0x0112).Value[0];
                        RotateFlipType rotateFlipType = CommonFunctions.GetOrientationToFlipType(orientationValue);
                        img.RotateFlip(rotateFlipType);
                    }

                    ImageBuilder.Current.Build(
                             new ImageJob(
                                 img,
                                 savepath,
                                 new Instructions(loVersions[loKey]),
                                 false,
                                 true));
                    string lsFileExt = Path.GetExtension(companyLogo.FileName);

                    string newext = "";
                    if (ImageFormat.Jpeg.Equals(img.RawFormat))
                    {
                        newext = ".JPG";
                    }
                    else if (ImageFormat.Png.Equals(img.RawFormat))
                    {
                        newext = ".PNG";
                    }
                    else if (ImageFormat.Gif.Equals(img.RawFormat))
                    {
                        newext = ".GIF";
                    }
                    newFilename = newFilename.ToString() + newext;
                    return newFilename;
                }

            }
            return "";
        }
    }
}