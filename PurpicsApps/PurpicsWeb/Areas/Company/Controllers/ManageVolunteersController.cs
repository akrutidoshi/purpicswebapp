﻿using PurpicsWeb.Areas.Company.Models;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Areas.Company.Models.Campaigns;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Threading.Tasks;
using System.IO;
using Hangfire;
using PurpicsWeb.Services;
using System.Text;
using PurpicsWeb.Services.CommonFunctions;
using System.Drawing;
using ImageResizer;
using PurpicsWeb.Services;

namespace PurpicsWeb.Areas.Company.Controllers
{
    [Authorize(Roles = "Company")]
    public class ManageVolunteersController : Controller
    {
        // GET: Company/ManageVolunteers
        public ActionResult Index(int CampaignId)
        {
            VolunteerViewModel foRequest = new VolunteerViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.inPageIndex = 1;
            foRequest.CampaignId = CampaignId;

            VolunteersViewModel lovolunteersViewmodel = getVolunteersList(foRequest);
            lovolunteersViewmodel.CampaignId = CampaignId;
            return View("~/Areas/Company/Views/ManageVolunteers/ManageVolunteers.cshtml", lovolunteersViewmodel);
        }

        public VolunteersViewModel getVolunteersList(VolunteerViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            VolunteersViewModel objVolunteersViewModel = new VolunteersViewModel();
            int liRecordCount = 0;

           


            Func<IQueryable<CampaignUploads>, IOrderedQueryable<CampaignUploads>> orderingFunc =
          query => query.OrderBy(x => x.VolunteerID);

            Expression<Func<CampaignUploads, bool>> expression = null;

            if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CampaignID == foRequest.CampaignId && x.Volunteers.Name.ToLower().Contains(foRequest.stSearch.ToLower()) && x.IsDeleted == false;
            else
                expression = x => x.CampaignID == foRequest.CampaignId && x.IsDeleted == false;

            //Include 
            List<Expression<Func<CampaignUploads, Object>>> CampaignUploadsincludes = new List<Expression<Func<CampaignUploads, object>>>();

            Expression<Func<CampaignUploads, object>> volunteersEntity = (volunteers) => volunteers.Volunteers;
            CampaignUploadsincludes.Add(volunteersEntity);
            List<CampaignUploads> loCampaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(expression, null, CampaignUploadsincludes, null, null).ToList();


                if (!string.IsNullOrEmpty(foRequest.stSortColumn))
                {
                    switch (foRequest.stSortColumn)
                    {
                        case "Id DESC":
                            orderingFunc = q => q.OrderByDescending(s => s.Volunteers.Id);
                            break;
                        case "Id ASC":
                            orderingFunc = q => q.OrderBy(s => s.Volunteers.Id);
                            break;
                        case "Name DESC":
                            orderingFunc = q => q.OrderByDescending(s => s.Volunteers.Name);
                            break;
                        case "Name ASC":
                            orderingFunc = q => q.OrderBy(s => s.Volunteers.Name);
                            break;

                        case "ZipCode DESC":
                            orderingFunc = q => q.OrderByDescending(s => s.Volunteers.ZipCode);
                            break;
                        case "ZipCode ASC":
                            orderingFunc = q => q.OrderBy(s => s.Volunteers.ZipCode);
                            break;


                        case "Likes DESC":
                            orderingFunc = q => q.OrderByDescending(s => s.Likes);
                            break;
                        case "Likes ASC":
                            orderingFunc = q => q.OrderBy(s => s.Likes);
                            break;
                        default:  // Name ascending 
                            orderingFunc = q => q.OrderBy(s => s.Volunteers.Id);
                            break;
                    }
                }

                List<CampaignUploads> objCampaignUploads = new List<CampaignUploads>();
                objCampaignUploads = Repository<CampaignUploads>.GetEntityListForQuery(expression, orderingFunc, CampaignUploadsincludes, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.CampaignID == foRequest.CampaignId).ToList();
                if (!string.IsNullOrEmpty(foRequest.stSearch))
                {
                    liRecordCount = Repository<CampaignUploads>.GetEntityListForQuery(null, null, CampaignUploadsincludes).Where(x => x.CampaignID == foRequest.CampaignId && x.IsDeleted == false && x.Volunteers.Name != null && x.Volunteers.Name.ToLower().Contains(foRequest.stSearch.ToLower())).Count();
                }
                else
                {
                    liRecordCount = Repository<CampaignUploads>.GetEntityListForQuery(null).Where(x => x.CampaignID == foRequest.CampaignId && x.IsDeleted == false).Count();
                }

                if (foRequest.inPageIndex > 0)
                {
                    ViewBag.Currentpageindex = foRequest.inPageIndex;
                }
                if (objCampaignUploads.Count > 0)
                {
                    foreach (var upload in objCampaignUploads)
                    {
                        VolunteerViewModel objcampaign = new VolunteerViewModel
                        {
                            Id = upload.VolunteerID,
                            Name = upload.Volunteers.Name,
                            Likes = upload.Likes,
                            ZipCode = upload.Volunteers.ZipCode,
                            
                        };

                        objVolunteersViewModel.loVolunteersList.Add(objcampaign);
                    }
                }

                VolunteersViewModel loVolunteersModel = new VolunteersViewModel();
                loVolunteersModel.inRecordCount = liRecordCount;
                loVolunteersModel.loVolunteersList = objVolunteersViewModel.loVolunteersList;
                loVolunteersModel.inPageIndex = foRequest.inPageIndex;
                loVolunteersModel.CampaignId = foRequest.CampaignId;
                loVolunteersModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
                return loVolunteersModel;


        }

        public ActionResult searchVolunteers(VolunteerViewModel foSearchRequest)
        {
            VolunteersViewModel objVolunteersViewModel = new VolunteersViewModel();
            objVolunteersViewModel = getVolunteersList(foSearchRequest);

            return PartialView("~/Areas/Company/Views/Shared/_VolunteersDetails.cshtml", objVolunteersViewModel);
        }
    }
}