﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Braintree;

namespace PurpicsWeb.Areas.Company.Models
{
    public class PaymentDTO
    {
        public int CharityId { get; set; }
        public int CompanyId { get; set; }
        public string AspNetUserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }


        public string BraintreeCustomerId { get; set; }

        public CreditCard[] CreditCards { get; set; }
        public CreditCardDetails CreditCardDetails { get; set; }
        public List<SelectListItem> StatesList { get; set; }
        public List<SelectListItem> CreditCardList { get; set; }
        public string CreditCardNumber { get; set; }
        public int? Amount { get; set; }
        public string ErrorMessage { get; set; }

        public string year { get; set; }
        public List<SelectListItem> CreditCardExpireYearList { get; set; }
        public string month { get; set; }
        public List<SelectListItem> CreditCardExpireMonthList { get; set; }

        private bool _FromAccountInfo = false;
        public bool FromAccountInfo
        {
            get
            {
                return _FromAccountInfo;
            }
            set
            {
                _FromAccountInfo = value;
            }
        }
    }

    public class CreditCardDetails
    {
        public CreditCardDetails()
        {
            MaskedNumber = string.Empty;
            CardholderName = string.Empty;
            ExpirationDate = string.Empty;
            Month = string.Empty;
            Year = string.Empty;
            BillingAddress = new BillingAddress();
            Token = string.Empty;
            IsDefault = false;
        }

        public string MaskedNumber { get; set; }
        public string CardholderName { get; set; }
        public string ExpirationDate { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public string Token { get; set; }
        public bool IsDefault { get; set; }
    }
    public class BillingAddress
    {
        public BillingAddress()
        {
            StreetAddress = string.Empty;
            Locality = string.Empty;
            PostalCode = string.Empty;
            Region = string.Empty;
        }

        public string StreetAddress { get; set; }
        public string Locality { get; set; }
        public string PostalCode { get; set; }
        public string Region { get; set; }

    }
}