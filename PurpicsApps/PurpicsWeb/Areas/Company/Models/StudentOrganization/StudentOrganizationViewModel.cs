﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Company.Models.StudentOrganization
{
    public class StudentOrganizationViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public string Address { get; set; } // Address
        public string ContactPerson { get; set; } // ContactPerson
        public string ContactPhone { get; set; } // ContactPhone
        public string About { get; set; } // About
        public string LogoFileName { get; set; } // LogoFileName
        public string AspNetUserID { get; set; } // AspNetUserID
        public string AspNetUsername { get; set; }
        public int UserStatusID { get; set; } // UserStatusID
        public int CategoryID { get; set; } // CategoryID
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string stSortColumn { get; set; }
        public string stSearch { get; set; }
        public string Company { get; set; }
        public string Charity { get; set; }
        public string Status { get; set; }
        public int inPageSize { get; set; }
        public int inPageIndex { get; set; }
        public int? STATUS { get; set; } // STATUS
        private bool _IsManageDashboard = false;
        public bool IsManageDashboard
        {
            get
            {
                return _IsManageDashboard;
            }
            set
            {
                _IsManageDashboard = value;
            }
        }

        private bool _ShowBackButton = true;
        public bool ShowBackButton
        {
            get
            {
                return _ShowBackButton;
            }
            set
            {
                _ShowBackButton = value;
            }
        }
    }
    public class StudentOrganizationSearchModel
    {
        public int SoId { get; set; } 
        public string SOName { get; set; }
        public int CharityId { get; set; } 
        public string CharityName { get; set; }
        public string CategoryName { get; set; }
        public string ZipCode { get; set; } 
    }
}