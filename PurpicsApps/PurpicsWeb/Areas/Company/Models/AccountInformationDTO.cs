﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Company.Models
{
    public class AccountInformationDTO
    {
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Please enter Contact Name.")]
        public string ContactName { get; set; }
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter Phone Number.")]
        public string PhoneNumber1 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        public string PhoneNumber2 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(4, ErrorMessage = "You must enter 4 numbers.", MinimumLength = 4)]
        public string PhoneNumber3 { get; set; }
        [Required(ErrorMessage = "Please enter details About Company.")]
        public string AboutCompany { get; set; }
        [Required(ErrorMessage = "Please enter Legal Entity Name.")]
        public string LegalEntityName { get; set; }
        [Required(ErrorMessage = "Please enter 501_C3 Number.")]
        public string Number501_C3 { get; set; }
        [Required(ErrorMessage = "Please enter Company Address.")]
        public string Address { get; set; }        
        [Required(ErrorMessage = "Please enter Company name.")]
        public string CompanyName { get; set; }
        public HttpPostedFileBase CompanyLogo { get; set; }
        public string CompanyLogoFullPath { get; set; }        
        public int CompanyId { get; set; }


    }
}