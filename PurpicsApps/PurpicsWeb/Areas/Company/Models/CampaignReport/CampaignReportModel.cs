﻿using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Company.Models.CampaignReport
{
    public class CampaignReportModel
    {
        public CampaignReportModel()
        {
            loCampaignReportViewModel = new List<CampaignReportViewModel>();
        }
        public List<CampaignReportViewModel> loCampaignReportViewModel;
        public int inRecordCount { get; set; }
        public int inPageIndex { get; set; }
        public Pager Pager { get; set; }	
    }
    public class CampaignReportViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public int CampaignStatusID { get; set; } // CampaignStatusID
        public int Likes { get; set; } // Likes
        public int PackageLikes { get; set; } // PackageLikes
        public DateTime? CampaignStartDate { get; set; } // CampaignStartDate
        public DateTime? CampaignEndDate { get; set; } // CampaignEndDate
        public int Days { get; set; } 
        public string CouponRedeem { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public string lsFromDate { get; set; }
        public string lsToDate { get; set; }
    }
}