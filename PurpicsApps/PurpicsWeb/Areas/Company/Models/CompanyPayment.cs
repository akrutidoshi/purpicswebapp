﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Company.Models
{
    public class CompanyPayment
    {
        public string Name { get; set; }
        public string CardNumber { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
    }
}