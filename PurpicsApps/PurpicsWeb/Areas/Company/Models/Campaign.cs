﻿using PurpicsWeb_DL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using PurpicsWeb.Areas.Company.Models.StudentOrganization;
namespace PurpicsWeb.Areas.Company.Models
{
    public class Campaign
    {
        public Campaign()
        {
            loCharities = new List<Charities>();
            loPackages = new List<Packages>();
            loStudentOrganizationSearchModel = new List<StudentOrganizationSearchModel>();

        }
        
        public List<Packages> loPackages { get; set; }
        public List<Charities> loCharities { get; set; }
        public List<StudentOrganizations> loStudentOrganizations { get; set; }
        public List<StudentOrganizationSearchModel> loStudentOrganizationSearchModel { get; set; }

        public int CharityID { get; set; }
        public int StudentOrganizationId { get; set; }

        public HttpPostedFileBase CampaignImage { get; set; }

        [Required(ErrorMessage = "Please enter Caption.")]
        public string Caption { get; set; }

        [Required(ErrorMessage = "Please enter Campaign Name.")]
        public string CampaignName { get; set; }

        [Required(ErrorMessage = "Please enter Description.")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [MaxLength(10, ErrorMessage="Please enter valid date.")]
        public DateTime? ExpirationDate { get; set; }
        public HttpPostedFileBase CouponCode { get; set; }

        public List<CharityCategories> loCharityCategories { get; set; }
        public List<string> loZipcodes { get; set; }
        public List<string> loSOZipcodes { get; set; }

        public string CharityName { get; set; }
        public string SOName { get; set; }

        public string PackageName { get; set; }
        public double PackagePrice { get; set; }
        public int PackageLikes { get; set; }
        public int CampaignId { get; set; }
        public string CampaignImageFullPath { get; set; }
        public string CouponCodeImageFullPath { get; set; }
        public string CouponTitle { get; set; }
        public string CouponDescription { get; set; }
        private bool _IsManageDashboard = false;
        public bool IsManageDashboard
        {
            get
            {
                return _IsManageDashboard;
            }
            set
            {
                _IsManageDashboard = value;
            }
        }

        public int Currentpageindex { get; set; }
        [Required(ErrorMessage = "Please select Miles.")]
        public int Radius { get; set; } // Radius
        [Required(ErrorMessage = "Please enter Zipcode.")]
        public string ZipCode { get; set; }
    }
}