﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Company.Models
{
    public class AccountInformation
    {
        public AccountInformation()
        {
            CompanyId = 0;
            EmailAddress = string.Empty;
            Password = string.Empty;
            ContactName = string.Empty;
            PhoneNumber = string.Empty;
            AboutCompany = string.Empty;
            LegalEntityName = string.Empty;
            Number501_C3 = string.Empty;
            Address = string.Empty;
            CompanyName = string.Empty;
            CharityLogo = null;            
            CompanyLogoFullPath = string.Empty;
            
            
            Status = string.Empty;
            CreditCardDetails = new CreditCardDetails();
        }

        public int CompanyId { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumber { get; set; }
        public string AboutCompany { get; set; }
        public string LegalEntityName { get; set; }
        public string Number501_C3 { get; set; }
        public string Address { get; set; }
        public string CompanyName { get; set; }
        public HttpPostedFileBase CharityLogo { get; set; }
        
        public string CompanyLogoFullPath { get; set; }
        
        
        public string Status { get; set; }
        public CreditCardDetails CreditCardDetails { get; set; }
    }
}