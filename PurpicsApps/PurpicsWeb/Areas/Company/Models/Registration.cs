﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PurpicsWeb.Areas.Company.Models
{
    public class Registration
    {
        [Required(ErrorMessage="Please enter username.")]
        [EmailAddress(ErrorMessage = "Please enter valid Email Address.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter password.")]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*0-9]{6,}$", ErrorMessage = "Password must have:<br/>Minimum 6 characters<br/>At least one special character<br/>At least one digit (0-9)<br/>At least one upper case letter")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter contact name.")]
        public string ContactName { get; set; }

        [Required(ErrorMessage = "Please enter phone number.")]
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        public string PhoneNumber1 { get; set; }
        [Required(ErrorMessage = "Please enter phone number.")]
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        public string PhoneNumber2 { get; set; }
        [Required(ErrorMessage = "Please enter phone number.")]
        [StringLength(4, ErrorMessage = "You must enter 4 numbers.", MinimumLength = 4)]
        public string PhoneNumber3 { get; set; }

        [Required(ErrorMessage = "Please enter details about company.")]
        public string AboutCompany { get; set; }

        [Required(ErrorMessage = "Please enter legal entity name.")]
        public string LegalEntityName { get; set; }

         [Required(ErrorMessage = "Please enter company name.")]
        public string CompanyName { get; set; }


         [Required(ErrorMessage = "Please enter company address.")]
        public string CompanyAddress { get; set; }

         public string LogoFileName { get; set;         }

         public int Id { get; set; }

        public HttpPostedFileBase CompanyLogo { get; set; }
        public string TempCompanyFileName { get; set; }
    }
}