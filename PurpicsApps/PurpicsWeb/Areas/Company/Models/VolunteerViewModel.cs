﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PurpicsWeb.Services;

namespace PurpicsWeb.Areas.Company.Models
{
    public class VolunteersViewModel
    {
        public VolunteersViewModel()
        {
        loVolunteersList = new List<VolunteerViewModel>();
        }

        public List<VolunteerViewModel> loVolunteersList { get; set; }
        public Int64 inRecordCount { get; set; }
        public int inPageIndex { get; set; }
        public int CampaignId { get; set; }
        public Pager Pager { get; set; }	
    }

    public class VolunteerViewModel
    {
        public int CampaignId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Likes { get; set; }
        public string ZipCode { get; set; }
                
        public string Status { get; set; }
        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
    }
}