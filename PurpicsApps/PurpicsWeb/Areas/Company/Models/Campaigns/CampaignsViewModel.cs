﻿using System;
using System.Collections.Generic;
using PurpicsWeb.Services;

namespace PurpicsWeb.Areas.Company.Models.Campaigns
{
    public class CampaignsViewModel
    {


        public CampaignsViewModel()
        {
            loCampaignsList = new List<CampaignViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public int inPageIndex { get; set; }
        public List<CampaignViewModel> loCampaignsList { get; set; }        
        private bool _IsManageDashboard = false;
        public bool IsManageDashboard
        {
            get
            {
                return _IsManageDashboard;
            }
            set
            {
                _IsManageDashboard = value;
            }
        }
        public Int64 TotalActiveCampaigns { get; set; }
        public Int64 TotalLikes { get; set; }
        public Pager Pager { get; set; }	
    }

    public class CampaignViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public int CharityID { get; set; } // CharityID
        public int CompanyID { get; set; } // CompanyID
        public int PackageID { get; set; } // PackageID
        public int CampaignStatusID { get; set; } // CampaignStatusID
        public string Caption { get; set; } // Caption
        public string CharitySponsor { get; set; } // Caption
        public string CharityName { get; set; } // Caption
        public int? StudentOrganizationId { get; set; } // StudentOrganizationId        
        public string StudentOrganizationName { get; set; } // StudentOrganizationName
        public string ImageFile { get; set; } // ImageFile
        public string CampaignCouponImageFile { get; set; } // ImageFile
        public string CouponCode { get; set; } // CouponCode
        public DateTime? ExpirationDate { get; set; } // ExpirationDate
        public System.Data.Entity.Spatial.DbGeography Latitude { get; set; } // Latitude
        public System.Data.Entity.Spatial.DbGeography Longitude { get; set; } // Longitude
        public int Radius { get; set; } // Radius
        public int Likes { get; set; } // Likes
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string ZipCode { get; set; }
        public double PackagePrice { get; set; } // Price
        public int PackageLikes { get; set; } // Likes

        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public string Status { get; set; }

        private bool _IsManageDashboard = false;
        public string CampaignName { get; set; } 
        public bool IsManageDashboard
        {
            get
            {
                return _IsManageDashboard;
            }
            set
            {
                _IsManageDashboard = value;
            }
        }
        public List<VolunteerModel> loVolunteerList { get; set; }
        public Int64 inRecordCount { get; set; }
        public Int64 hdnVolunteerPageIndex { get; set; }
        public string ConversionRateValue { get; set; }
        public int TotalCoupons { get; set; }
        public int RedeemedCoupons { get; set; }
    }

    public class VolunteerModel
    {
        public int CampaignId { get; set; } 
        public int Id { get; set; } 
        public string Name { get; set; }
        public int Likes { get; set; }
        public string ZipCode { get; set; }

        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public string Status { get; set; }
        public int inRecordCount { get; set; }
        public int inVoluneerPageIndex { get; set; }
    }
}