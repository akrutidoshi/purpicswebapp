﻿//BindTable('load');
function BindTable(foId) {
    $.ajax({
        type: "POST",
        url: '/billing/getCardList',
        content: "application/json; charset=utf-8",
        dataType: "html",
        data: {
        },
        success: function (lodata) {
            if (lodata != null) {
                var divCardList = $('#divCardList');
                divCardList.html('');
                divCardList.html(lodata);
                if ($('#viewbagvalue').val() == "" || $('#viewbagvalue').val() == null)
                { $('#divCreditCardNotFound').show(); }
                else
                { $('#divCreditCardNotFound').hide(); }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (errorThrown == "abort") {
                return;
            }
            else {
                alert(errorThrown);
            }
        }
    });
}

function ConfirmDelete() {
    if (confirm("Are you sure you want to delete this credit card?")) {
        return true;
    }
    return false;
}

function submitwithSkip() {
    debugger;
    var form = $("#frmAddEditCreditCardWithSkip");    
        form.submit();      
        if ($("#loadingbar").length === 0) {
            $("body").append("<div id='loadingbar'></div>")
            $("#divLoader").css("display", "block");
            $("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
            $("#loadingbar").width((50 + Math.random() * 30) + "%");
        }
    return true;
}

function formValidate() {
    var form = $("#frmAddEditCreditCard");
    if (isValidFrom()) {
        form.submit();
        if ($("#loadingbar").length === 0) {
            $("body").append("<div id='loadingbar'></div>")
            $("#divLoader").css("display", "block");
            $("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
            $("#loadingbar").width((50 + Math.random() * 30) + "%");
        }
    }
    return true;
}
function isValidFrom() {
    var IsValid = true;
    if ($('#cardno').val().trim() == '') {
        $("#msgStCard").css("display", "block");
        $('#cardno').addClass('input-validation-error');
        IsValid = false;
    }
    else {
        $("#msgStCard").css("display", "none");
    }

    if ($('#cardno').val().trim() != '') {
        if (parseInt($('#cardno').val().trim().length) < 12) {
            $("#msgStCardLength").css("display", "block");
            $('#cardno').addClass('input-validation-error');
            IsValid = false;
        }
        else {
            $("#msgStCardLength").css("display", "none");
        }
    }

    if ($('#cardholder').val().trim() == '') {
        $("#msgStCardHolder").css("display", "block");
        $('#cardholder').addClass('input-validation-error');
        IsValid = false;

    }
    else {
        $("#msgStCardHolder").css("display", "none");
    }

    if ($('#cardCVV').val().trim() == '') {
        $("#msgStCVV").css("display", "block");
        $('#cardCVV').addClass('input-validation-error');
        IsValid = false;

    }
    else {
        $("#msgStCVV").css("display", "none");
    }

    if ($('#cardCVV').val().trim() != '') {
        if (parseInt($('#cardCVV').val().trim().length) < 3) {

            $("#msgStCVVLength").css("display", "block");
            $('#cardCVV').addClass('input-validation-error');
            IsValid = false;
        }
        else {
            $("#msgStCVVLength").css("display", "none");
        }
    }

    if ($('#expdateMonth').val().trim() == '' || $('#expdateYear').val().trim() == '') {
        $("#msgStExpDate").css("display", "block");
        $('#expdateMonth').addClass('input-validation-error');
        IsValid = false;

    }
    else {
        $("#msgStExpDate").css("display", "none");
    }

    var IsValidDateAndMonth = true;

    if (parseInt($('#expdateMonth').val().trim()) > 12 || parseInt($('#expdateMonth').val().trim()) < 1) {
        $("#msgStExpMonth").css("display", "block");
        $('#expdateMonth').addClass('input-validation-error');
        IsValid = false;
        IsValidDateAndMonth = false;
    }
    else {
        $("#msgStExpMonth").css("display", "none");
    }

    if ($('#expdateYear').val().trim() != '') {
        if (parseInt($('#expdateYear').val().trim().length) != 4) {
            $("#msgStExpYear").css("display", "block");
            $('#expdateYear').addClass('input-validation-error');
            IsValid = false;
            IsValidDateAndMonth = false;
        }
        else {
            $("#msgStExpYear").css("display", "none");
        }
    }
    if ($('#expdateMonth').val().trim() != '' && $('#expdateYear').val().trim() != '' && IsValidDateAndMonth == true) {
        if (IsExpired($('#expdateMonth').val().trim(), $('#expdateYear').val().trim())) {
            $("#msgStExpiredDate").css("display", "block");
            $('#expdateMonth').addClass('input-validation-error');
            IsValid = false;
        }
        else {
            $("#msgStExpiredDate").css("display", "none");
        }
    }
    else {
        $("#msgStExpiredDate").css("display", "none");
    }



    if ($('#address').val().trim() == '') {
        $("#msgStaddress").css("display", "block");
        $('#address').addClass('input-validation-error');
        IsValid = false;

    }
    else {
        $("#msgStaddress").css("display", "none");
    }

    if ($('#city').val().trim() == '') {
        $("#msgStcity").css("display", "block");
        $('#city').addClass('input-validation-error');
        IsValid = false;

    }
    else {
        $("#msgStcity").css("display", "none");
    }

    if ($('#state').val().trim() == '') {
        $("#msgStstate").css("display", "block");
        $('#state').addClass('input-validation-error');
        IsValid = false;

    }
    else {
        $("#msgStstate").css("display", "none");
    }


    if ($('#zip').val().trim() == '') {
        $("#msgStzip").css("display", "block");
        $('#zip').addClass('input-validation-error');
        IsValid = false;

    }
    else {

        if (!isValidUSZip($('#zip').val().trim())) {
            $("#msgStzip").css("display", "block");
            $("#msgStzip").html("Invalid zip.");
            $('#zip').addClass('input-validation-error');
            IsValid = false;
        }
        else {
            $("#msgStzip").css("display", "none");
        }
    }
    return IsValid;
}
function isValidUSZip(sZip) {
    return /^[0-9]*$/.test(sZip);
}

$('#cardholder').change(function () {
    $('#cardholder').removeClass('input-validation-error');
    $("#msgStCardHolder").css("display", "none");
});

$('#address').change(function () {
    $('#address').removeClass('input-validation-error');
    $("#msgStaddress").css("display", "none");
});

$('#cardno').change(function () {
    $('#cardno').removeClass('input-validation-error');
    $("#msgStCard").css("display", "none");
});

$('#expdateMonth').change(function () {
    $('#expdateMonth').removeClass('input-validation-error');
    $("#msgStExpDate").css("display", "none");
    $("#msgStExpMonth").css("display", "none");
    $("#msgStExpiredDate").css("display", "none");
});

$('#expdateYear').change(function () {
    $('#expdateYear').removeClass('input-validation-error');
    $("#msgStExpYear").css("display", "none");
});

$('#city').change(function () {
    $('#city').removeClass('input-validation-error');
    $("#msgStcity").css("display", "none");
});

$('#state').change(function () {
    $('#state').removeClass('input-validation-error');
    $("#msgStstate").css("display", "none");
});

$('#cardCVV').change(function () {
    $('#cardCVV').removeClass('input-validation-error');
    $("#msgStCVV").css("display", "none");
    $("#msgStCVVLength").css("display", "none");
});

$('#zip').change(function () {
    $('#zip').removeClass('input-validation-error');
    $("#msgStzip").css("display", "none");
});


function IsExpired(month, year) {
    var currentDate = new Date(),
        currentYear = currentDate.getFullYear();

    if (currentYear > year) {
        return true;
    } else if (currentYear == year
            && (month - 1) < currentDate.getMonth()) {
        // Months are zero-indexed, Jan = 0, Feb = 1...
        return true;
    } else {
        return false;
    }
}

function clearForms() {
    $('#cardholder').val("");
    $('#cardno').val("");
    $('#cardCVV').val("");
    $('#expdateMonth').val("");
    $('#expdateYear').val("");
    $('#address').val("");
    $('#city').val("");
    $('#state').val("");
    $('#zip').val("");
    $('#zip').val("");
    //$("[id='IsDefault']").bootstrapSwitch('setState', false);
}