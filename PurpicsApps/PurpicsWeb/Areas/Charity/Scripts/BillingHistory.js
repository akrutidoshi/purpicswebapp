﻿$(document).ready(function () {
    setSortingArrow();
    $('#txtSearch').focus();
});

function search() {
    var stSearch = "";
    if ($('#txtFromDate').val() != "") {
        stSearch = $('#txtFromDate').val();
    }
    refeshList('load');
}
function refeshList(foId, pageIndex) {
    //var pageIndex = 0;
    
    var stSortField = "";
    
    if (foId == "load") {
        pageIndex = 1;
    }
    else if (foId == "Id ASC" || foId == "Id DESC" || foId == "ReferenceNumber ASC" || foId == "ReferenceNumber DESC" || foId == "Amount ASC" || foId == "Amount DESC" || foId == "CreatedOn ASC" || foId == "CreatedOn DESC" || foId == "SubscriptionEndDate ASC" || foId == "SubscriptionEndDate DESC") {
        pageIndex = parseInt($('#hdnPageIndex').val());
    }
    var lsFromdate = $('#txtFromDate').val().trim();
    var lsTodate = $('#txtToDate').val().trim();
    $.ajax({
        type: "POST",
        url: lsgetUserList,
        content: "application/json; charset=utf-8",
        dataType: "html",
        data: {
            inPageIndex: pageIndex,
            inPageSize: 10,
            stSortColumn: $('#hdnOrder').val(),
            lsFromDate: lsFromdate,
            lsToDate: lsTodate
        },
        success: function (lodata) {

            if (lodata != null) {
                var divSOReleased = $('#BillingHistorylist');
                divSOReleased.html('');
                divSOReleased.html(lodata);
                $('#hdnPageIndex').val(pageIndex);
                setSortingArrow();
               
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            
            if (errorThrown == "abort") {
                return;
            }
            else {
                alert(errorThrown);
            }
        }
    });
}


function getOrderbyUserList(foOrderedField, tdID) {

    var foFieldwithOrder = "";
    $('#hdnSortingOnColumn').val(tdID);

    if ($('#hdnOrder').val() == "") {
        $('#hdnOrder').val(foOrderedField + " ASC");
        foFieldwithOrder = foOrderedField + " ASC";
        $('#hdnOrder').val(foFieldwithOrder);
        $('#hdnSortingDirection').val("ASC");

    }
    else {
        if ($('#hdnOrder').val() == (foOrderedField + " ASC")) {
            foFieldwithOrder = foOrderedField + " DESC";
            $('#hdnOrder').val(foFieldwithOrder);
            $('#hdnSortingDirection').val("DESC");
        }
        else {
            foFieldwithOrder = foOrderedField + " ASC";
            $('#hdnOrder').val(foFieldwithOrder);
            $('#hdnSortingDirection').val("ASC");
        }
    }
    refeshList(foFieldwithOrder);
}

function setSortingArrow() {

    var sortTD = $('#hdnSortingOnColumn').val();
    var sortDirection = $('#hdnSortingDirection').val();

    if (sortTD != "") {
        $('#' + sortTD).removeClass("sorting");
        if (sortDirection.toUpperCase() == "ASC") {
            $('#' + sortTD).addClass("sorting_asc");
        }
        else if (sortDirection.toUpperCase() == "DESC") {
            $('#' + sortTD).addClass("sorting_desc");
        }
    }
    else {
        $('#sortId').removeClass("sorting");
        $('#sortId').addClass("sorting_asc");
        $('#hdnOrder').val("Id ASC");
    }
}
function clearDate(fsTextBoxId) {
    $('#' + fsTextBoxId).val('');
}
$(function () {

    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true
    });
});