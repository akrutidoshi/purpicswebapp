﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Areas.Charity.Models.Campaigns;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Linq.Expressions;
using PurpicsWeb.Services.CommonFunctions;

namespace PurpicsWeb.Areas.Charity.Controllers
{
    [Authorize(Roles = "Charity")]
    public class ManageDashboardController : Controller
    {
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        string CampaignCouponImagePath = ConfigurationManager.AppSettings["CampaignCouponImagePath"].ToString();
        // GET: Charity/Campaigns
        public ActionResult Index(int CurrpageIndex = 1)
        {
            if (CurrpageIndex != 1)
            {
                ViewBag.Currentpageindex = CurrpageIndex;
            }
            else
            {
                ViewBag.Currentpageindex = 1;
            }

            CampaignViewModel foRequest = new CampaignViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.inPageIndex = ViewBag.Currentpageindex;
            foRequest.inPageSize = 10;

            CampaignsViewModel loCampaignListModel = getCampaignList(foRequest);
            loCampaignListModel.IsManageDashboard = true;

            string userid = HttpContext.User.Identity.GetUserId();
            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            List<PurpicsWeb_DL.Entities.Campaigns> objallCampaigns = Repository<PurpicsWeb_DL.Entities.Campaigns>.GetEntityListForQuery(x => x.CharityID == charity.Id).OrderBy(x => x.Id).Where(x => (x.IsDeleted == false && x.IsActive == true && x.CampaignStatusID == (int)CommonEnums.CampaignApprovalStatus.Approved)).ToList();
            loCampaignListModel.TotalActiveCampaigns = objallCampaigns.Count();
            loCampaignListModel.TotalLikes = objallCampaigns.Select(x => x.Likes).Sum();
            return View("~/Areas/Charity/Views/Dashboard/ManageDashboard.cshtml", loCampaignListModel);
        }

        public CampaignsViewModel getCampaignList(CampaignViewModel foRequest)
        {
            foRequest.inPageSize = 5;
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CampaignsViewModel objCampaignsViewModel = new CampaignsViewModel();
            int liRecordCount = 0;
            string userid = HttpContext.User.Identity.GetUserId();
            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            Expression<Func<Campaigns, bool>> expression = null;

            expression = x => x.CharityID == charity.Id && x.IsDeleted == false && x.IsActive == true && x.CampaignStatusID == (int)CommonEnums.UserStatus.Approved;

            Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderingFunc =
            query => query.OrderBy(x => x.Id);
            //Include 
            List<Expression<Func<Campaigns, Object>>> Campaignsincludes = new List<Expression<Func<Campaigns, object>>>();

            Expression<Func<Campaigns, object>> campaignStatsName = (campaignstatus) => campaignstatus.CampaignStatus;
            Campaignsincludes.Add(campaignStatsName);

            Expression<Func<Campaigns, object>> companyName = (companyname) => companyname.Companies;
            Campaignsincludes.Add(companyName);

            List<Campaigns> objallCampaigns = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, 1, 5).ToList();
            string lsCharityContribution = Convert.ToString(Repository<CharityPaymentHistory>.GetEntityListForQuery(null).Where(x => x.CharityID == charity.Id && x.IsPaid == true).Sum(y => y.EstAmount));
            string lsMonthlyContribution = Convert.ToString(Repository<CharityPaymentHistory>.GetEntityListForQuery(null).Where(x => x.CharityID == charity.Id && x.IsPaid == true && Convert.ToDateTime(x.CampaignEndDate).Month == DateTime.Now.Month).Sum(y => y.EstAmount));
            List<CampaignsWithDetails> objCampaigns = new List<CampaignsWithDetails>();
            CampaignsWithDetails campaignWithDetails = null;
            foreach (Campaigns campaign in objallCampaigns)
            {

                campaignWithDetails = new CampaignsWithDetails();
                campaignWithDetails.Id = campaign.Id;
                campaignWithDetails.Caption = campaign.Caption;
                campaignWithDetails.CampaignName = campaign.CampaignName;
                campaignWithDetails.IsActive = campaign.IsActive;
                campaignWithDetails.Status = campaign.CampaignStatus.Name;
                campaignWithDetails.Likes = campaign.Likes;
                campaignWithDetails.CharitySponsor = campaign.Companies.CompanyName;
                campaignWithDetails.CompanyID = campaign.CompanyID;
                objCampaigns.Add(campaignWithDetails);
            }


            liRecordCount = objCampaigns.Count();
            if (objCampaigns.Count > 0)
            {

                if (objCampaigns != null && objCampaigns.Count > 0)
                {
                    foreach (var campaign in objCampaigns)
                    {
                        CampaignViewModel objCampaign = new CampaignViewModel
                        {
                            Id = campaign.Id,
                            CreatedBy = campaign.CreatedBy,
                            CreatedOn = campaign.CreatedOn,
                            IsActive = campaign.IsActive,
                            IsDeleted = campaign.IsDeleted,
                            Likes = campaign.Likes,
                            ModifiedBy = campaign.ModifiedBy,
                            ModifiedOn = campaign.ModifiedOn,
                            Caption = campaign.Caption,
                            CampaignName = campaign.CampaignName,
                            CharitySponsor = campaign.CharitySponsor,
                            Status = campaign.Status,
                            IsManageDashboard = true,
                            CompanyID = campaign.CompanyID
                        };

                        objCampaignsViewModel.loCampaignsList.Add(objCampaign);
                    }
                }
            }
            CampaignsViewModel loCampaignModel = new CampaignsViewModel();
            loCampaignModel.inRecordCount = liRecordCount;
            loCampaignModel.loCampaignsList = objCampaignsViewModel.loCampaignsList;
            loCampaignModel.IsManageDashboard = true;
            loCampaignModel.TotalCharityContribution = lsCharityContribution;
            loCampaignModel.MonthlyCharityContribution = lsMonthlyContribution;
            return loCampaignModel;
        }

        public ActionResult searchCampaigns(CampaignViewModel foSearchRequest)
        {
            CampaignsViewModel loCampaignModel = getCampaignList(foSearchRequest);
            return PartialView("~/Areas/Charity/Views/Campaigns/_ManageCampaign.cshtml", loCampaignModel);
        }

        public ActionResult CampaignDetails(int Id, int page = 1)
        {
            ViewBag.Currentpageindex = page;
            CampaignViewModel objCampaignViewModel = new CampaignViewModel();


            Campaigns objCampaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();

            objCampaignViewModel = new CampaignViewModel
            {
                Id = objCampaign.Id,
                CreatedBy = objCampaign.CreatedBy,
                CreatedOn = objCampaign.CreatedOn,
                IsActive = objCampaign.IsActive,
                IsDeleted = objCampaign.IsDeleted,
                Likes = objCampaign.Likes,
                ModifiedBy = objCampaign.ModifiedBy,
                ModifiedOn = objCampaign.ModifiedOn,
                Caption = objCampaign.Caption,
                CampaignName = objCampaign.CampaignName,
                Description = objCampaign.Description,
                ExpirationDate = objCampaign.ExpirationDate == null ? "" : Convert.ToDateTime(objCampaign.ExpirationDate).ToShortDateString(),
                CouponTitle = objCampaign.CouponTitle,
                CouponDescription = objCampaign.CouponDescription,
                ImageFile = string.IsNullOrEmpty(objCampaign.ImageFile) ? Url.Content(CampaignImagePath) + "NoPhoto.jpg" : Url.Content(CampaignImagePath) + objCampaign.ImageFile,
                CampaignCouponImageFile = string.IsNullOrEmpty(objCampaign.CouponCode) ? Url.Content(CampaignCouponImagePath) + "NoPhoto.jpg" : Url.Content(CampaignCouponImagePath) + objCampaign.CouponCode,
                PackagePrice = Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(x => x.Id == objCampaign.PackageID).Select(x => x.Price).FirstOrDefault(),
                PackageLikes = Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(x => x.Id == objCampaign.PackageID).Select(x => x.Likes).FirstOrDefault(),
            };
            objCampaignViewModel.IsManageDashboard = true;

            return View("~/Areas/Charity/Views/Campaigns/CampaignDetails.cshtml", objCampaignViewModel);
        }

        public class CampaignsWithDetails : PurpicsWeb_DL.Entities.Campaigns
        {
            public string CharitySponsor { get; set; }
            public string Status { get; set; }
        }
    }
}