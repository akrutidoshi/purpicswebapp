﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Services;
using Braintree;
using PurpicsWeb.Areas.Charity.Models;
using Microsoft.AspNet.Identity;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Services.CommonFunctions;
using System.Threading.Tasks;
using AutoMapper;


namespace PurpicsWeb.Areas.Charity.Controllers
{
    
    public class PaymentController : Controller
    {
        
        // GET: Charity/Payment
        public ActionResult Index()
        {
            return View("");
        }

        [ActionName("Step2")]
        public ActionResult ShowAddEditCreditCardView(int CharityId, bool fromAccountInfo = false)
        {
            AppSettings appSettings = Repository<AppSettings>.GetEntityListForQuery(x => (x.Name == "CharityRegistrationAmount" &&  x.IsDeleted == false)).FirstOrDefault();
            PaymentDTO model = GetModel();
            model.CharityId = CharityId;
            model.CreditCardExpireMonthList = CommonFunctions.getCreditCardMonth();
            model.CreditCardExpireYearList = CommonFunctions.getCreditCardYear();
            model.FromAccountInfo = fromAccountInfo;
            model.SubscriptionFee = appSettings.Value;
            return View("~/Areas/Charity/Views/Payment/AddEditCreditCard.cshtml", model);
        }

        public ActionResult AddEditCreditCard(string token)
        {
            BraintreePayments payment = new BraintreePayments();
            PaymentDTO model = new PaymentDTO();
            string userid = HttpContext.User.Identity.GetUserId();
            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            if (!string.IsNullOrEmpty(token))
            {
               
                CreditCard card = payment.GetCustomerDetails(charity.BraintreeCustomerId).CreditCards.Where(m => m.Token == token).FirstOrDefault();
                model = GetModel();
                model.CharityId = charity.Id;
                model.CreditCardDetails = Mapper.DynamicMap<CreditCardDetails>(card);
                model.FromAccountInfo = true;
                return View("~/Areas/Charity/Views/Payment/AddEditCreditCard.cshtml", model);
            }
            else
            {
                model.StatesList = CommonFunctions.GetStateList();
                model.CharityId = charity.Id;
                return View("~/Areas/Charity/Views/Payment/AddEditCreditCard.cshtml", model);
            }
        }


        [HttpPost]
        public async Task<ActionResult> AddEditCreditCard(FormCollection collection)
        {
            string token = collection["CreditCardDetails.Token"];
            PaymentDTO model = new PaymentDTO();

            BraintreePayments payment = new BraintreePayments();
            int charityId = Convert.ToInt32( collection["CharityId"]);
            model.CharityId = charityId;
            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == charityId).FirstOrDefault();
            string CustomerId = charity.BraintreeCustomerId;
            if (!string.IsNullOrEmpty(token))
            {
                
                Result<PaymentMethod> result = payment.UpdateCreditCard(collection, CustomerId, token);
                if (result.IsSuccess())
                {
                    return RedirectToAction("Index", "AccountInformation");
                }
                else
                {                    
                    if (result.Message == "Duplicate card exists in the vault.")
                    {
                        ModelState.AddModelError("CardError", "This credit card is already on file.");
                    }
                    else
                    {
                        ModelState.AddModelError("CardError", result.Message.ToString());
                    }
                    CreditCard card = payment.GetCustomerDetails(charity.BraintreeCustomerId).CreditCards.Where(m => m.Token == token).FirstOrDefault();
                    model.StatesList = CommonFunctions.GetStateList();
                    model = GetModel();
                    model.CharityId = charity.Id;
                    model.CreditCardDetails = Mapper.DynamicMap<CreditCardDetails>(card);
                    model.FromAccountInfo = true;
                    return View("~/Areas/Charity/Views/Payment/AddEditCreditCard.cshtml", model);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(CustomerId)) // Create New Customer in Braintree
                {
                    Result<Customer> result = payment.CreateNewCustomer(collection);
                    if (result.IsSuccess())
                    {
                        charity.BraintreeCustomerId = result.Target.Id;
                        await Repository<Charities>.UpdateEntity(charity, (entity) => { return entity.Id; });

                        if (Convert.ToBoolean(collection["fromAccountInfo"]) == true)
                        {
                            return RedirectToAction("Index", "AccountInformation");
                        }
                        else
                        { 
                        return View("~/Areas/Charity/Views/Registration/Thanks.cshtml");
                        }
                    }
                    else
                    {                        
                        if (result.Message == "Duplicate card exists in the vault.")
                        {
                            ModelState.AddModelError("CardError", "This credit card is already on file.");
                        }
                        else
                        {
                            ModelState.AddModelError("CardError", result.Message.ToString());
                        }
                        model.StatesList = CommonFunctions.GetStateList();
                        model.FromAccountInfo = Convert.ToBoolean(collection["fromAccountInfo"]);
                        return View("AddEditCreditCard", model);
                    }
                }
                else
                {
                    Result<PaymentMethod> result = payment.CreateCreditCard(collection, CustomerId);
                    if (result.IsSuccess())
                    {
                        return View("AddEditCreditCard", model);
                    }
                    else
                    {
                        model = GetModel();
                        ModelState.AddModelError("CardError", "Credit card number is invalid.");
                        model.StatesList = CommonFunctions.GetStateList();
                        model.FromAccountInfo = Convert.ToBoolean(collection["fromAccountInfo"]);
                        return View("AddEditCreditCard", model);
                    }
                }
            }
        }

        public PaymentDTO GetModel()
        {
            PaymentDTO model = new PaymentDTO();
            model.StatesList = CommonFunctions.GetStateList();
            //string userid = HttpContext.User.Identity.GetUserId();
            //Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            //if (charity.Id > 0)
            //{
               
            //    //model.CreditCardList = GetCreditCardList();
                


            //}
            return model;
        }

        public async Task<ActionResult> DeleteCreditCard(string token, int chrityId)
        {
            try
            {
                BraintreePayments payment = new BraintreePayments();
                payment.DeleteCreditCard(token);
                Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == chrityId).FirstOrDefault();
                charity.BraintreeCustomerId = null;
                await Repository<Charities>.UpdateEntity(charity, (entity) => { return entity.Id; });
                return RedirectToAction("Index", "AccountInformation");
            }
            catch (Exception ex)
            {
                return Json(-1, JsonRequestBehavior.AllowGet);

            }
        }
    }
}