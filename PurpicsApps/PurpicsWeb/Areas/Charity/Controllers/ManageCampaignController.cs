﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb.Areas.Charity.Models.Campaigns;
using PurpicsWeb.Areas.Charity.Models.Company;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Linq.Expressions;
using PurpicsWeb.Services;
using PurpicsWeb.Services.CommonFunctions;

namespace PurpicsWeb.Areas.Charity.Controllers
{
    [Authorize(Roles = "Charity")]
    public class ManageCampaignController : Controller
    {
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        string CampaignCouponImagePath = ConfigurationManager.AppSettings["CampaignCouponImagePath"].ToString();

        // GET: Charity/Campaigns
        public ActionResult Index(int CurrpageIndex = 1)
        {
            if (CurrpageIndex != 1)
            {
                ViewBag.Currentpageindex = CurrpageIndex;

            }
            else
            {
                ViewBag.Currentpageindex = 1;
            }

            CampaignViewModel foRequest = new CampaignViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.inPageIndex = ViewBag.Currentpageindex;
            CampaignsViewModel loCampaignListModel = getCampaignList(foRequest);

            return View("~/Areas/Charity/Views/Campaigns/ManageCampaign.cshtml", loCampaignListModel);
        }

        public CampaignsViewModel getCampaignList(CampaignViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            CampaignsViewModel objCampaignsViewModel = new CampaignsViewModel();
            int liRecordCount = 0;
            string userid = HttpContext.User.Identity.GetUserId();
            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();

            Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderingFunc =
            query => query.OrderBy(x => x.Id);

            Expression<Func<Campaigns, bool>> expression = null;

            if (!string.IsNullOrEmpty(foRequest.stSearch))
                expression = x => x.CampaignStatusID == (int)CommonEnums.CampaignApprovalStatus.Approved && x.CharityID == charity.Id && x.IsDeleted == false && x.IsActive == true && x.CampaignName.ToLower().Contains(foRequest.stSearch.ToLower());
            else
                expression = x => x.CampaignStatusID == (int)CommonEnums.CampaignApprovalStatus.Approved && x.CharityID == charity.Id && x.IsDeleted == false && x.IsActive == true;

            //Include 
            List<Expression<Func<Campaigns, Object>>> Campaignsincludes = new List<Expression<Func<Campaigns, object>>>();

            Expression<Func<Campaigns, object>> campaignStatsName = (campaignstatus) => campaignstatus.CampaignStatus;
            Campaignsincludes.Add(campaignStatsName);

            Expression<Func<Campaigns, object>> companyName = (companyname) => companyname.Companies;
            Campaignsincludes.Add(companyName);

            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "Caption DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignName);
                        break;
                    case "Caption ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignName);
                        break;
                    case "Likes DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Likes);
                        break;
                    case "Likes ASC":
                        orderingFunc = q => q.OrderBy(s => s.Likes);
                        break;
                    case "CharitySponsor DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Companies.CompanyName);
                        break;
                    case "CharitySponsor ASC":
                        orderingFunc = q => q.OrderBy(s => s.Companies.CompanyName);
                        break;
                    case "Status DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CampaignStatus.Name);
                        break;
                    case "Status ASC":
                        orderingFunc = q => q.OrderBy(s => s.CampaignStatus.Name);
                        break;
                    default:  // Name ascending 
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }

                List<Campaigns> objCampaign = new List<Campaigns>();
                objCampaign = Repository<Campaigns>.GetEntityListForQuery(expression, orderingFunc, Campaignsincludes, foRequest.inPageIndex, foRequest.inPageSize).Where(x => x.CharityID == charity.Id && x.IsDeleted == false && x.IsActive == true).ToList();

                liRecordCount = Repository<Campaigns>.GetEntityListForQuery(expression, null, null, null, null).Where(x => x.CharityID == charity.Id && x.IsDeleted == false && x.IsActive == true).Count();

                if (objCampaign.Count > 0)
                {
                    foreach (var campaign in objCampaign)
                    {
                        CampaignViewModel objcampaign = new CampaignViewModel
                        {
                            Id = campaign.Id,
                            CreatedBy = campaign.CreatedBy,
                            CreatedOn = campaign.CreatedOn,
                            IsActive = campaign.IsActive,
                            IsDeleted = campaign.IsDeleted,
                            Likes = campaign.Likes,
                            ModifiedBy = campaign.ModifiedBy,
                            ModifiedOn = campaign.ModifiedOn,
                            Caption = campaign.Caption,
                            CampaignName = campaign.CampaignName,
                            CharitySponsor = campaign.Companies.CompanyName,
                            Status = campaign.CampaignStatus.Name,
                            CompanyID = campaign.CompanyID
                        };

                        objCampaignsViewModel.loCampaignsList.Add(objcampaign);
                    }
                }
            }
            CampaignsViewModel loCampaignModel = new CampaignsViewModel();
            loCampaignModel.inRecordCount = liRecordCount;
            loCampaignModel.loCampaignsList = objCampaignsViewModel.loCampaignsList;
            loCampaignModel.inPageIndex = foRequest.inPageIndex;
            ViewBag.Currentpageindex = foRequest.inPageIndex;
            loCampaignModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCampaignModel;
        }

        public ActionResult searchCampaigns(CampaignViewModel foSearchRequest)
        {
            CampaignsViewModel loCampaignModel = getCampaignList(foSearchRequest);
            return PartialView("~/Areas/Charity/Views/Campaigns/_ManageCampaign.cshtml", loCampaignModel);
        }

        public ActionResult CampaignDetails(int Id, int page = 1)
        {
            ViewBag.Currentpageindex = page;
            CampaignViewModel objCampaignViewModel = new CampaignViewModel();
            Campaigns objCampaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == Id).FirstOrDefault();

            objCampaignViewModel = new CampaignViewModel
            {
                Id = objCampaign.Id,
                CreatedBy = objCampaign.CreatedBy,
                CreatedOn = objCampaign.CreatedOn,
                IsActive = objCampaign.IsActive,
                IsDeleted = objCampaign.IsDeleted,
                Likes = objCampaign.Likes,
                ModifiedBy = objCampaign.ModifiedBy,
                ModifiedOn = objCampaign.ModifiedOn,
                Caption = objCampaign.Caption,
                CampaignName = objCampaign.CampaignName,
                Description=objCampaign.Description,
                ExpirationDate = objCampaign.ExpirationDate == null ? "" : Convert.ToDateTime( objCampaign.ExpirationDate).ToShortDateString(),
                CouponTitle = objCampaign.CouponTitle,
                CouponDescription = objCampaign.CouponDescription,
                ImageFile = string.IsNullOrEmpty(objCampaign.ImageFile) ? Url.Content(CampaignImagePath) + "NoPhoto.jpg" : Url.Content(CampaignImagePath) + objCampaign.ImageFile,
                CampaignCouponImageFile = string.IsNullOrEmpty(objCampaign.CouponCode) ? Url.Content(CampaignCouponImagePath) + "NoPhoto.jpg" : Url.Content(CampaignCouponImagePath) + objCampaign.CouponCode,
                PackagePrice = objCampaign.PackagePrice, //Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(x => x.Id == objCampaign.PackageID).Select(x => x.Price).FirstOrDefault(),
                PackageLikes = objCampaign.PackageLikes//Repository<PurpicsWeb_DL.Entities.Packages>.GetEntityListForQuery(x => x.Id == objCampaign.PackageID).Select(x => x.Likes).FirstOrDefault(),
            };

            return View("~/Areas/Charity/Views/Campaigns/CampaignDetails.cshtml", objCampaignViewModel);
        }

        public class CampaignsWithDetails : PurpicsWeb_DL.Entities.Campaigns
        {
            public string CharitySponsor { get; set; }
            public string Status { get; set; }
        }
    }
}