﻿using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Expressions;
using Microsoft.AspNet.Identity;
using PurpicsWeb.Areas.Charity.Models.Billing;
using PurpicsWeb.Services;

namespace PurpicsWeb.Areas.Charity.Controllers
{
    public class BillingHistoryController : Controller
    {
        //
        // GET: /Charity/BillingHistory/
        public ActionResult Index(int CurrpageIndex = 1)
        {
            if (CurrpageIndex != 1)
            {
                ViewBag.Currentpageindex = CurrpageIndex;

            }
            else
            {
                ViewBag.Currentpageindex = 1;
            }
            BillingHistoryViewModel foRequest = new BillingHistoryViewModel();
            foRequest.stSortColumn = "Id ASC";
            foRequest.inPageIndex = ViewBag.Currentpageindex;
            BillingHistorysViewModel loBillingHistory = getBillingHistory(foRequest);

            return View("~/Areas/Charity/Views/BillingHistory/BillingHistory.cshtml", loBillingHistory);
        }
        public BillingHistorysViewModel getBillingHistory(BillingHistoryViewModel foRequest)
        {
            if (foRequest.inPageSize <= 0)
                foRequest.inPageSize = 10;

            if (foRequest.inPageIndex <= 0)
                foRequest.inPageIndex = 1;

            if (foRequest.stSortColumn == "")
                foRequest.stSortColumn = null;

            if (foRequest.stSearch == "")
                foRequest.stSearch = null;

            BillingHistorysViewModel objBillingHistorysViewModel = new BillingHistorysViewModel();
            int liRecordCount = 0;

            string userid = HttpContext.User.Identity.GetUserId();
            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();

            DateTime? ldFromDate = null;
            if (!string.IsNullOrEmpty(foRequest.lsFromDate))
                ldFromDate = Convert.ToDateTime(foRequest.lsFromDate + " 00:00:01");

            DateTime? ldToDate = null;
            if (!string.IsNullOrEmpty(foRequest.lsToDate))
                ldToDate = Convert.ToDateTime(foRequest.lsToDate + " 23:59:59");

            Expression<Func<CharitySubscriptionTransactions, bool>> expression = null;
            if(!string.IsNullOrEmpty(foRequest.lsFromDate) && !string.IsNullOrEmpty(foRequest.lsToDate))
                expression = x => x.CharityID == charity.Id && x.CreatedOn >= ldFromDate && x.SubscriptionEndDate <= ldToDate;
            else if (!string.IsNullOrEmpty(foRequest.lsFromDate))
                expression = x => x.CharityID == charity.Id && x.CreatedOn >= ldFromDate;
            else if (!string.IsNullOrEmpty(foRequest.lsToDate))
                expression = x => x.CharityID == charity.Id && x.SubscriptionEndDate <= ldToDate;
            else
                expression = x => x.CharityID == charity.Id;

            Func<IQueryable<CharitySubscriptionTransactions>, IOrderedQueryable<CharitySubscriptionTransactions>> orderingFunc =
            query => query.OrderBy(x => x.Id);
            
            if (!string.IsNullOrEmpty(foRequest.stSortColumn))
            {
                switch (foRequest.stSortColumn)
                {
                    case "Id DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Id);
                        break;
                    case "Id ASC":
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                    case "ReferenceNumber DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.ReferenceNumber);
                        break;
                    case "ReferenceNumber ASC":
                        orderingFunc = q => q.OrderBy(s => s.ReferenceNumber);
                        break;
                    case "Amount DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.Amount);
                        break;
                    case "Amount ASC":
                        orderingFunc = q => q.OrderBy(s => s.Amount);
                        break;
                    case "CreatedOn DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.CreatedOn);
                        break;
                    case "CreatedOn ASC":
                        orderingFunc = q => q.OrderBy(s => s.CreatedOn);
                        break;
                    case "SubscriptionEndDate DESC":
                        orderingFunc = q => q.OrderByDescending(s => s.SubscriptionEndDate);
                        break;
                    case "SubscriptionEndDate ASC":
                        orderingFunc = q => q.OrderBy(s => s.SubscriptionEndDate);
                        break;
                    default:  // Name ascending 
                        orderingFunc = q => q.OrderBy(s => s.Id);
                        break;
                }
            }


            //Include 
            List<Expression<Func<CharitySubscriptionTransactions, Object>>> bilingHistory = new List<Expression<Func<CharitySubscriptionTransactions, object>>>();


            List<CharitySubscriptionTransactions> objallCharityHistory = new List<CharitySubscriptionTransactions>();
            objallCharityHistory = Repository<CharitySubscriptionTransactions>.GetEntityListForQuery(expression, orderingFunc, bilingHistory, foRequest.inPageIndex, foRequest.inPageSize).ToList();
            liRecordCount = Repository<CharitySubscriptionTransactions>.GetEntityListForQuery(expression, orderingFunc, bilingHistory).Count();
            if (objallCharityHistory.Count > 0)
            {
                foreach (var history in objallCharityHistory)
                {
                    BillingHistoryViewModel objhistory = new BillingHistoryViewModel
                    {
                        Id = history.Id,
                        ReferenceNumber = history.ReferenceNumber,
                        Amount = history.Amount,
                        CreatedOn = history.CreatedOn,
                        SubscriptionEndDate = history.SubscriptionEndDate
                    };
                    objBillingHistorysViewModel.loBillingHistory.Add(objhistory);

                }
            }
            BillingHistorysViewModel loCampaignListModel = new BillingHistorysViewModel();
            loCampaignListModel.inRecordCount = liRecordCount;
            loCampaignListModel.loBillingHistory = objBillingHistorysViewModel.loBillingHistory;
            loCampaignListModel.inPageIndex = foRequest.inPageIndex;
            ViewBag.Currentpageindex = foRequest.inPageIndex;
            loCampaignListModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
            return loCampaignListModel;
        }
        //public BillingHistorysViewModel getBillingHistory(BillingHistoryViewModel foRequest)
        //{
        //    if (foRequest.inPageSize <= 0)
        //        foRequest.inPageSize = 10;

        //    if (foRequest.inPageIndex <= 0)
        //        foRequest.inPageIndex = 1;

        //    if (foRequest.stSortColumn == "")
        //        foRequest.stSortColumn = null;

        //    if (foRequest.stSearch == "")
        //        foRequest.stSearch = null;

        //    BillingHistorysViewModel objBillingHistorysViewModel = new BillingHistorysViewModel();
        //    int liRecordCount = 0;

        //    string userid = HttpContext.User.Identity.GetUserId();
        //    Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();

        //    Expression<Func<CharityPaymentHistory, bool>> expression = null;
        //    expression = x => x.CharityID == charity.Id;

        //    Func<IQueryable<CharityPaymentHistory>, IOrderedQueryable<CharityPaymentHistory>> orderingFunc =
        //    query => query.OrderBy(x => x.Id);



        //    if (!string.IsNullOrEmpty(foRequest.stSortColumn))
        //    {
        //        switch (foRequest.stSortColumn)
        //        {
        //            case "Id DESC":
        //                orderingFunc = q => q.OrderByDescending(s => s.Id);
        //                break;
        //            case "Id ASC":
        //                orderingFunc = q => q.OrderBy(s => s.Id);
        //                break;
        //            case "Caption DESC":
        //                orderingFunc = q => q.OrderByDescending(s => s.Campaigns.Caption);
        //                break;
        //            case "Caption ASC":
        //                orderingFunc = q => q.OrderBy(s => s.Campaigns.Caption);
        //                break;
        //            case "Notes DESC":
        //                orderingFunc = q => q.OrderByDescending(s => s.Notes);
        //                break;
        //            case "Notes ASC":
        //                orderingFunc = q => q.OrderBy(s => s.Notes);
        //                break;
        //            case "CompanyName DESC":
        //                orderingFunc = q => q.OrderByDescending(s => s.Companies.CompanyName);
        //                break;
        //            case "CompanyName ASC":
        //                orderingFunc = q => q.OrderBy(s => s.Companies.CompanyName);
        //                break;
        //            case "IsPaid DESC":
        //                orderingFunc = q => q.OrderByDescending(s => s.IsPaid);
        //                break;
        //            case "IsPaid ASC":
        //                orderingFunc = q => q.OrderBy(s => s.IsPaid);
        //                break;
        //            case "EstAmount DESC":
        //                orderingFunc = q => q.OrderByDescending(s => s.EstAmount);
        //                break;
        //            case "EstAmount ASC":
        //                orderingFunc = q => q.OrderBy(s => s.EstAmount);
        //                break;
        //            default:  // Name ascending 
        //                orderingFunc = q => q.OrderBy(s => s.Id);
        //                break;
        //        }
        //    }


        //    //Include 
        //    List<Expression<Func<CharityPaymentHistory, Object>>> bilingHistory = new List<Expression<Func<CharityPaymentHistory, object>>>();

        //    Expression<Func<CharityPaymentHistory, object>> campaignName = (campaignname) => campaignname.Campaigns;
        //    bilingHistory.Add(campaignName);


        //    Expression<Func<CharityPaymentHistory, object>> companyName = (companyname) => companyname.Companies;
        //    bilingHistory.Add(companyName);

        //    List<CharityPaymentHistory> objallCharityHistory=new List<CharityPaymentHistory>();
        //    objallCharityHistory = Repository<CharityPaymentHistory>.GetEntityListForQuery(expression, orderingFunc, bilingHistory, foRequest.inPageIndex, foRequest.inPageSize).ToList();
        //    liRecordCount = Repository<CharityPaymentHistory>.GetEntityListForQuery(expression, orderingFunc, bilingHistory).Count();
        //    if (objallCharityHistory.Count > 0)
        //    {
        //        foreach (var history in objallCharityHistory)
        //        {
        //            BillingHistoryViewModel objhistory = new BillingHistoryViewModel
        //            {
        //                Id = history.Id,
        //                IsPaid = history.IsPaid,
        //                CampaingCaption = history.Campaigns.Caption,
        //                CompanyName = history.Companies.CompanyName,
        //                CompanyID = history.CompanyID,
        //                CharityID=history.CharityID,
        //                CampaignID=history.CampaignID,
        //                CampaignEndDate=history.CampaignEndDate,
        //                EstAmount=history.EstAmount,
        //                Notes=history.Notes
        //            };
        //            objBillingHistorysViewModel.loBillingHistory.Add(objhistory);
                    
        //        }
        //    }
        //    BillingHistorysViewModel loCampaignListModel = new BillingHistorysViewModel();
        //    loCampaignListModel.inRecordCount = liRecordCount;
        //    loCampaignListModel.loBillingHistory = objBillingHistorysViewModel.loBillingHistory;
        //    loCampaignListModel.inPageIndex = foRequest.inPageIndex;
        //    ViewBag.Currentpageindex = foRequest.inPageIndex;
        //    loCampaignListModel.Pager = new Pager(liRecordCount, foRequest.inPageIndex);
        //    return loCampaignListModel;
        //}
        public ActionResult searchBillingHistory(BillingHistoryViewModel foRequest)
        {
            BillingHistorysViewModel loBillingHistory = getBillingHistory(foRequest);
            return PartialView("~/Areas/Charity/Views/BillingHistory/_BillingHistory.cshtml", loBillingHistory);
        }

    }

}