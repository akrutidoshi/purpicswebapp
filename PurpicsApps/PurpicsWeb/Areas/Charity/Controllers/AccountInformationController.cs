﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Areas.Charity.Models;
using PurpicsWeb.Models;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Threading.Tasks;
using PurpicsWeb.Services.CommonFunctions;
using AutoMapper;
using Braintree;
using PurpicsWeb.Services;
using System.Drawing;
using ImageResizer;
using System.Drawing.Imaging;

namespace PurpicsWeb.Areas.Charity.Controllers
{
    [Authorize(Roles = "Charity")]
    public class AccountInformationController : Controller
    {
        // GET: Charity/AccountInformation
        public ActionResult Index()
        {
            AccountInformation objAccountInformtion = new AccountInformation();
            string userid = HttpContext.User.Identity.GetUserId();
            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == charity.AspNetUserID).FirstOrDefault();
            objAccountInformtion.CharityId = charity.Id;
            objAccountInformtion.EmailAddress = user.UserName;
            objAccountInformtion.ContactName = charity.ContactPerson;
            objAccountInformtion.PhoneNumber = charity.ContactPhone;
            objAccountInformtion.AboutCharity = charity.About;
            objAccountInformtion.LegalEntityName = charity.LegalEntityName;
            objAccountInformtion.Number501_C3 = charity.C501C3Number;
            objAccountInformtion.CharityName = charity.CharityName;
            objAccountInformtion.Address = charity.Address;
            objAccountInformtion.City = charity.City;
            objAccountInformtion.State = charity.State;
            objAccountInformtion.Zip = charity.Zip;
            CharityCategories charityCategoy = Repository<CharityCategories>.GetEntityListForQuery(x => x.Id == charity.CategoryID).FirstOrDefault();
            objAccountInformtion.CategoryName = charityCategoy.Name;
            if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"] + charity.LogoFileName)))
            {
                objAccountInformtion.CharityLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"].Remove(0, 1) + charity.LogoFileName;
            }
            int value = charity.UserStatusID;
            CommonEnums.UserStatus enumUserStatus = (CommonEnums.UserStatus)value;
            string stringUserStatus = enumUserStatus.ToString();
            objAccountInformtion.Status = stringUserStatus;


            string CustomerId = charity.BraintreeCustomerId;
            if (!string.IsNullOrEmpty(CustomerId))
            {
                BraintreePayments payment = new BraintreePayments();
                CreditCard CreditCard = payment.GetCustomerDetails(CustomerId).CreditCards.FirstOrDefault();
                objAccountInformtion.CreditCardDetails = Mapper.DynamicMap<CreditCardDetails>(CreditCard);
            }



            return View("AccountInformation", objAccountInformtion);
        }


        public ActionResult EditAccountInformation(int CharityId)
        {
            AccountInformationDTO objAccountInformtion = new AccountInformationDTO();
            string userid = HttpContext.User.Identity.GetUserId();
            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == CharityId).FirstOrDefault();
            AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == charity.AspNetUserID).FirstOrDefault();
            objAccountInformtion.CharityId = charity.Id;
            objAccountInformtion.EmailAddress = user.UserName;
            objAccountInformtion.ContactName = charity.ContactPerson;

            string[] splitPhoneNumber = charity.ContactPhone.Split('-');


            objAccountInformtion.PhoneNumber1 = splitPhoneNumber[0];
            objAccountInformtion.PhoneNumber2 = splitPhoneNumber[1];
            objAccountInformtion.PhoneNumber3 = splitPhoneNumber[2];


            objAccountInformtion.AboutCharity = charity.About;
            objAccountInformtion.LegalEntityName = charity.LegalEntityName;
            objAccountInformtion.Number501_C3 = charity.C501C3Number;
            objAccountInformtion.CharityName = charity.CharityName;
            objAccountInformtion.Address = charity.Address;
            objAccountInformtion.City = charity.City;
            objAccountInformtion.State = charity.State;
            objAccountInformtion.Zip = charity.Zip;
            CharityCategories charityCategoy = Repository<CharityCategories>.GetEntityListForQuery(x => x.Id == charity.CategoryID).FirstOrDefault();
            if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"] + charity.LogoFileName)))
            {
                objAccountInformtion.CharityLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"].Remove(0, 1) + charity.LogoFileName;
            }
            else
            {
                objAccountInformtion.CharityLogoFullPath = System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"].Remove(0, 1) + "NoPhoto.jpg";
            }


            List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
            CharityCategories objcategory = new CharityCategories();
            SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
            objAccountInformtion.Categories = objmodeldata.ToList();
            objAccountInformtion.CategoryID = charity.CategoryID;

            objAccountInformtion.States = CommonFunctions.GetStateList();
            return View("EditAccountInfo", objAccountInformtion);
        }

        [ValidateInput(false)] 
        public async Task<ActionResult> UpateAccountInfo(AccountInformationDTO objAccountInformation)
        {
            string userid = HttpContext.User.Identity.GetUserId();
            var charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == objAccountInformation.CharityId).FirstOrDefault();
            charity.ContactPerson = objAccountInformation.ContactName;
            charity.ContactPhone = objAccountInformation.PhoneNumber1 + "-" + objAccountInformation.PhoneNumber2 + "-" + objAccountInformation.PhoneNumber3; ;
            charity.About = objAccountInformation.AboutCharity;
            charity.LegalEntityName = objAccountInformation.LegalEntityName;
            charity.C501C3Number = objAccountInformation.Number501_C3;
            charity.CharityName = objAccountInformation.CharityName;
            charity.Address = objAccountInformation.Address;
            charity.City = objAccountInformation.City;
            charity.State = objAccountInformation.State;
            charity.Zip = objAccountInformation.Zip;
            charity.CategoryID = objAccountInformation.CategoryID;
            charity.ContactPerson = objAccountInformation.ContactName;
            charity.ModifiedBy = userid;
            charity.ModifiedOn = DateTime.Now;

            string newfilename = UploadCharityLogo(objAccountInformation.CharityLogo, objAccountInformation.CharityId);
            if (newfilename != "")
            {
                charity.LogoFileName = newfilename;
            }


            await Repository<Charities>.UpdateEntity(charity, (entity) => { return entity.Id; });
            return Redirect("Index");
        }



        public string UploadCharityLogo(HttpPostedFileBase charityLogo, int CharityId)
        {
            string newfileName = "";
            if (charityLogo != null)
            {

                var charity = Repository<Charities>.GetEntityListForQuery(x => x.Id == CharityId).FirstOrDefault();
                string savepath = Path.Combine(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"]), CharityId.ToString());
                if (System.IO.File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"]) + charity.LogoFileName))
                {
                    System.IO.File.Delete(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["CharityImagePath"]) + charity.LogoFileName);
                }


                Dictionary<string, string> loVersions = new Dictionary<string, string>();
                loVersions.Add("", "maxwidth=250&maxheight=300&AutoRotate=true");

                //Generate each version
                foreach (var loKey in loVersions.Keys)
                {
                    Image img = Image.FromStream(charityLogo.InputStream);
                    var ExIfImage = img.PropertyItems.Where(x => x.Id == 0x0112).FirstOrDefault();

                    if (ExIfImage != null)
                    {
                        int orientationValue = img.GetPropertyItem(0x0112).Value[0];
                        RotateFlipType rotateFlipType = CommonFunctions.GetOrientationToFlipType(orientationValue);
                        img.RotateFlip(rotateFlipType);
                    }

                    ImageBuilder.Current.Build(
                             new ImageJob(
                                 img,
                                 savepath,
                                 new Instructions(loVersions[loKey]),
                                 false,
                                 true));
                    //string lsFileExt = Path.GetExtension(charityLogo.FileName);
                    string newext = "";
                    if (ImageFormat.Jpeg.Equals(img.RawFormat))
                    {
                        newext = ".JPG";
                    }
                    else if (ImageFormat.Png.Equals(img.RawFormat))
                    {
                        newext = ".PNG";
                    }
                    else if (ImageFormat.Gif.Equals(img.RawFormat))
                    {
                        newext = ".GIF";
                    }
                    newfileName = CharityId.ToString() + newext;
                    return newfileName;
                }
                
            }
            return "";
        }
    }


}