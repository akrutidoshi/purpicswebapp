﻿using Hangfire;
using Microsoft.AspNet.Identity.Owin;
using PurpicsWeb.Areas.Charity.Models;
using PurpicsWeb.Models;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PurpicsWeb.Services;
using PurpicsWeb.Areas.Charity.Models;
using Braintree;
using System.IO;
using System.Drawing;
using ImageResizer;
using PurpicsWeb.Controllers;
using System.Drawing.Imaging;


namespace PurpicsWeb.Areas.Charity.Controllers
{

    public class RegistrationController : Controller
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            if (User != null)
            {
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction("", "AdminDashBoard", new { area = "Admin" });
                }
                else if (User.IsInRole("Company"))
                {
                    return RedirectToAction("", "ManageDashboard", new { area = "Company" });
                }
                else if (User.IsInRole("Charity"))
                {
                    return RedirectToAction("", "ManageDashboard", new { area = "Charity" });
                }
            }
            return View();
        }



        [ActionName("Step1")]
        public ActionResult Registration()
        {
            CharityRegistration objCharityRegistration = new CharityRegistration();
            if (Session["CharityRegistration"] != null)
            {
                objCharityRegistration = (CharityRegistration)Session["CharityRegistration"];
            }
            List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
            CharityCategories objcategory = new CharityCategories();
            SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
            objCharityRegistration.Categories = objmodeldata.ToList();
            if (Session["CharityRegistration"] == null)
            {
                objCharityRegistration.CategoryID = 0;
            }


            objCharityRegistration.States = CommonFunctions.GetStateList();


            return View("Register", objCharityRegistration);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> AddCharityRegistration(CharityRegistration objCharityRegistration)
        {
            List<AspNetUsers> users = Repository<AspNetUsers>.GetEntityListForQuery(x => x.UserName == objCharityRegistration.EmailAddress).ToList();
            if (users.Count > 0)
            {
                TempData["Message"] = "The email address " + objCharityRegistration.EmailAddress + " is already registered. Please login.";
                List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
                CharityCategories objcategory = new CharityCategories();
                SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
                objCharityRegistration.Categories = objmodeldata.ToList();

                objCharityRegistration.States = CommonFunctions.GetStateList();
                return View("Register", objCharityRegistration);
            }


            string newfilename = UploadCharityLogo(objCharityRegistration.CharityLogo);

            objCharityRegistration.TempCharityFileName = newfilename.ToString();
            objCharityRegistration.CharityLogo = null;
            Session["CharityRegistration"] = objCharityRegistration;


            return RedirectToAction("Step2", "Payment", new { CharityId = 0 });
        }

        public async Task<ActionResult> SaveCharity(string CustomerId)
        {
            try
            {
                CharityRegistration objCharityRegistration = new CharityRegistration();

                if (Session["CharityRegistration"] == null)
                {
                    List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
                    CharityCategories objcategory = new CharityCategories();
                    SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
                    objCharityRegistration.Categories = objmodeldata.ToList();
                    objCharityRegistration.States = CommonFunctions.GetStateList();
                    return View("Register", objCharityRegistration);
                }

                objCharityRegistration = (CharityRegistration)Session["CharityRegistration"];
                // Create Entry in ASPNetUsers table
                var user = new ApplicationUser() { UserName = objCharityRegistration.EmailAddress, Email = objCharityRegistration.EmailAddress, PhoneNumber = objCharityRegistration.PhoneNumber1 + "-" + objCharityRegistration.PhoneNumber2 + "-" + objCharityRegistration.PhoneNumber3 };
                var userresult = await UserManager.CreateAsync(user, objCharityRegistration.Password);
                if (userresult.Succeeded)
                {
                    string strChariyRole = CommonConstants.RoleNames.Charity;
                    await UserManager.AddToRoleAsync(user.Id, strChariyRole);

                    Charities charities = new Charities();
                    charities.LegalEntityName = objCharityRegistration.LegalEntityName;
                    charities.C501C3Number = objCharityRegistration.Number501_C3;
                    charities.CharityName = objCharityRegistration.CharityName;
                    charities.Address = objCharityRegistration.Address;
                    charities.City = objCharityRegistration.City;
                    charities.State = objCharityRegistration.State;
                    charities.Zip = objCharityRegistration.Zip;
                    charities.ContactPerson = objCharityRegistration.ContactName;
                    charities.ContactPhone = objCharityRegistration.PhoneNumber1 + "-" + objCharityRegistration.PhoneNumber2 + "-" + objCharityRegistration.PhoneNumber3;
                    charities.About = objCharityRegistration.AboutCharity;
                    charities.AspNetUserID = user.Id;
                    charities.IsActive = true;
                    charities.CategoryID = objCharityRegistration.CategoryID;

                    charities.CreatedOn = DateTime.Now;
                    charities.CreatedBy = user.Id;
                    charities.UserStatusID = (int)CommonEnums.UserStatus.Pending;
                    charities.BraintreeCustomerId = CustomerId;
                    await Repository<Charities>.InsertEntity(charities, entity => { return entity.Id; });

                    string lsUploadPath = Server.MapPath(ConfigurationManager.AppSettings["CharityImagePath"]);
                    int charityid = charities.Id;

                    string TempFilesPath = Server.MapPath("~/Images/TempStoredFiles/") + objCharityRegistration.TempCharityFileName.ToString();
                    string lsFileExt = Path.GetExtension(TempFilesPath);
                    string savepath = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["CharityImagePath"]), charityid.ToString() + lsFileExt);

                    if (System.IO.File.Exists(TempFilesPath))
                    {
                        System.IO.File.Copy(TempFilesPath, savepath);
                        System.IO.File.Delete(TempFilesPath);
                    }

                    var charity = Repository<Charities>.GetEntityListForQuery(r => r.Id == charityid).FirstOrDefault();
                    charity.LogoFileName = charityid.ToString() + lsFileExt;
                    await Repository<Charities>.UpdateEntity(charity, entity => { return entity.Id; });

                    CharityCategories category = Repository<CharityCategories>.GetEntityListForQuery(x => x.Id == objCharityRegistration.CategoryID).FirstOrDefault();
                    string domainURL = ConfigurationManager.AppSettings["domainURL"] ?? "http://www.Purpics.com/";

                    string objRegistrationEmailBody = new SystemEmailController().CharityRegistration(objCharityRegistration, category, charity.Id);

                    string mailTo = Utility.ExecuteScalar("sp_GetAllAdminEmailId").ToString();
                    try
                    {
                        BackgroundJob.Enqueue(() => EmailHelper.SendEmail("New Charity Registration", objRegistrationEmailBody, mailTo));

                    }
                    catch { }
                    Session["CharityRegistration"] = null;
                    return View("Thanks");
                }
                else
                {
                    TempData["Message"] = "The email address " + objCharityRegistration.EmailAddress + " is already registered. Please login.";
                    return View("Register", objCharityRegistration);
                }
            }
            catch (Exception ex)
            {
                PaymentDTO model = new PaymentDTO();
                model.StatesList = CommonFunctions.GetStateList();
                ModelState.AddModelError("CardError", "Sorry an error occured. Please try again.");
                return View("~/Areas/Charity/Views/Payment/AddEditCreditCard.cshtml", model);
            }

            return View();

        }


        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> AddNewCharityRegistration(FormCollection collection)
        {

            try
            {
                if (Session["CharityRegistration"] == null)
                {
                    CharityRegistration objCharityRegistration = new CharityRegistration();
                    List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
                    CharityCategories objcategory = new CharityCategories();
                    SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
                    objCharityRegistration.Categories = objmodeldata.ToList();
                    objCharityRegistration.States = CommonFunctions.GetStateList();
                    return View("Register", objCharityRegistration);
                }

                BraintreePayments payment = new BraintreePayments();
                Result<Customer> result = payment.CreateNewCustomer(collection);
                if (result.IsSuccess())
                {
                    await SaveCharity(result.Target.Id);
                    return View("Thanks");
                }
                else
                {
                    if (result.Message == "Duplicate card exists in the vault.")
                    {
                        ModelState.AddModelError("CardError", "This credit card is already on file.");
                    }
                    else
                    {
                        ModelState.AddModelError("CardError", result.Message.ToString());
                    }
                    PaymentDTO model = new PaymentDTO();
                    model = GetModel();
                    model.StatesList = CommonFunctions.GetStateList();
                    model.CreditCardExpireMonthList = CommonFunctions.getCreditCardMonth();
                    model.month = "";
                    model.CreditCardExpireYearList = CommonFunctions.getCreditCardYear();
                    model.year = "";

                    AppSettings appSettings = Repository<AppSettings>.GetEntityListForQuery(x => (x.Name == "CharityRegistrationAmount" && x.IsDeleted == false)).FirstOrDefault();
                    model.SubscriptionFee = appSettings.Value;
                    model.CreditCardDetails = new CreditCardDetails();
                    model.CreditCardDetails.MaskedNumber = collection["number"];
                    model.CreditCardDetails.CardholderName = collection["card-holder-name"];
                    model.CreditCardDetails.BillingAddress.StreetAddress = collection["street_address"];
                    model.CreditCardDetails.BillingAddress.Locality = collection["locality"];
                    model.CreditCardDetails.BillingAddress.Region = collection["CreditCardDetails.BillingAddress.Region"];
                    model.CreditCardDetails.BillingAddress.PostalCode = collection["postal_code"];
                    model.CreditCardDetails.ExpirationDate = collection["month"] + "/" + collection["year"];
                    return View("~/Areas/Charity/Views/Payment/AddEditCreditCard.cshtml", model);
                }

            }
            catch (Exception ex)
            {
                PaymentDTO model = new PaymentDTO();
                model.StatesList = CommonFunctions.GetStateList();
                AppSettings appSettings = Repository<AppSettings>.GetEntityListForQuery(x => (x.Name == "CharityRegistrationAmount" && x.IsDeleted == false)).FirstOrDefault();
                model.SubscriptionFee = appSettings.Value;
                model.CreditCardExpireMonthList = CommonFunctions.getCreditCardMonth();
                model.month = "";
                model.CreditCardExpireYearList = CommonFunctions.getCreditCardYear();
                model.year = "";
                model.CreditCardDetails = new CreditCardDetails();
                model.CreditCardDetails.MaskedNumber = collection["number"];
                model.CreditCardDetails.CardholderName = collection["card-holder-name"];
                model.CreditCardDetails.BillingAddress.StreetAddress = collection["street_address"];
                model.CreditCardDetails.BillingAddress.Locality = collection["locality"];
                model.CreditCardDetails.BillingAddress.Region = collection["CreditCardDetails.BillingAddress.Region"];
                model.CreditCardDetails.BillingAddress.PostalCode = collection["postal_code"];
                model.CreditCardDetails.ExpirationDate = collection["month"] + "/" + collection["year"];
                ModelState.AddModelError("CardError", "Sorry an error occured. Please try again.");
                return View("~/Areas/Charity/Views/Payment/AddEditCreditCard.cshtml", model);
            }

            return View();

        }


        [HttpPost]
        public async Task<ActionResult> AddNewCharityRegistrationWithSkip()
        {

            try
            {
                if (Session["CharityRegistration"] == null)
                {
                    CharityRegistration objCharityRegistration = new CharityRegistration();
                    List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(null).OrderBy(x => x.Name).ToList();
                    CharityCategories objcategory = new CharityCategories();
                    SelectList objmodeldata = new SelectList(charityCategories, "ID", "Name", 0);
                    objCharityRegistration.Categories = objmodeldata.ToList();
                    objCharityRegistration.States = CommonFunctions.GetStateList();
                    return View("Register", objCharityRegistration);
                }
                    await SaveCharity("");
                    return View("Thanks");               
               

            }
            catch (Exception ex)
            {
                PaymentDTO model = new PaymentDTO();
                model.StatesList = CommonFunctions.GetStateList();
                AppSettings appSettings = Repository<AppSettings>.GetEntityListForQuery(x => (x.Name == "CharityRegistrationAmount" && x.IsDeleted == false)).FirstOrDefault();
                model.SubscriptionFee = appSettings.Value;
                model.CreditCardExpireMonthList = CommonFunctions.getCreditCardMonth();
                model.month = "";
                model.CreditCardExpireYearList = CommonFunctions.getCreditCardYear();
                model.year = "";
                //model.CreditCardDetails = new CreditCardDetails();

                ModelState.AddModelError("CardError", "Something wrong!! Please try after sometime");
                return View("~/Areas/Charity/Views/Payment/AddEditCreditCard.cshtml", model);
            }

            return View();

        }

        public PaymentDTO GetModel()
        {
            PaymentDTO model = new PaymentDTO();
            model.StatesList = CommonFunctions.GetStateList();

            return model;
        }

        private async void Insert(Charities charity)
        {
            await Repository<Charities>.InsertEntity(charity, (entity) => { return entity.Id; });
        }

        public string UploadCharityLogo(HttpPostedFileBase charityLogo)
        {

            if (charityLogo != null)
            {
                if (!System.IO.Directory.Exists(Server.MapPath("~/Images/TempStoredFiles")))
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/Images/TempStoredFiles"));
                }

                string newFilename = Guid.NewGuid().ToString();

                string savepath = Path.Combine(Server.MapPath("~/Images/TempStoredFiles/"), newFilename);




                Dictionary<string, string> loVersions = new Dictionary<string, string>();
                loVersions.Add("", "maxwidth=250&maxheight=300&AutoRotate=true");

                //Generate each version
                foreach (var loKey in loVersions.Keys)
                {
                    Image img = Image.FromStream(charityLogo.InputStream);
                    var ExIfImage = img.PropertyItems.Where(x => x.Id == 0x0112).FirstOrDefault();

                    if (ExIfImage != null)
                    {
                        int orientationValue = img.GetPropertyItem(0x0112).Value[0];
                        RotateFlipType rotateFlipType = CommonFunctions.GetOrientationToFlipType(orientationValue);
                        img.RotateFlip(rotateFlipType);
                    }

                    ImageBuilder.Current.Build(
                             new ImageJob(
                                 img,
                                 savepath,
                                 new Instructions(loVersions[loKey]),
                                 false,
                                 true));
                    string newext = "";
                    if (ImageFormat.Jpeg.Equals(img.RawFormat))
                    {
                        newext = ".JPG";
                    }
                    else if (ImageFormat.Png.Equals(img.RawFormat))
                    {
                        newext = ".PNG";
                    }
                    else if (ImageFormat.Gif.Equals(img.RawFormat))
                    {
                        newext = ".GIF";
                    }


                    string lsFileExt = Path.GetExtension(charityLogo.FileName);
                    newFilename = newFilename.ToString() + newext;
                    return newFilename;
                }

            }
            return "";
        }
    }
}
