﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Charity.Models.Company
{
    public class CompanyViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string LegalEntityName { get; set; } // LegalEntityName
        public string Address { get; set; } // Address
        public string ContactPerson { get; set; } // ContactPerson
        public string ContactPhone { get; set; } // ContactPhone
        public string About { get; set; } // About
        public string LogoFilename { get; set; } // LogoFilename
        public string AspNetUserID { get; set; } // AspNetUserID
        public int Status { get; set; } // Status
        public bool IsDeleted { get; set; } // IsDeleted
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime CreatedOn { get; set; } // CreatedOn
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedOn { get; set; } // ModifiedOn
        public string C501C3Number { get; set; } // 501C3Number
        public string CompanyName { get; set; } // CompanyName
        public bool IsActive { get; set; } // IsActive
        public int ActiveCampaigns { get; set; } // Status
        public string stSearch { get; set; }
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public string AspNetUsername { get; set; } // AspNetUserID
        private bool _IsManageDashboard = false;
        public bool IsManageDashboard
        {
            get
            {
                return _IsManageDashboard;
            }
            set
            {
                _IsManageDashboard = value;
            }
        }
    }
}