﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace PurpicsWeb.Areas.Charity.Models
{
    public class AccountInformationDTO        
    {
        public string EmailAddress { get; set; }        
        [Required(ErrorMessage = "Please enter Contact Name.")]
        public string ContactName { get; set; }
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter Phone Number.")]
        public string PhoneNumber1 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(3, ErrorMessage = "You must enter 3 numbers.", MinimumLength = 3)]
        public string PhoneNumber2 { get; set; }
        [Required(ErrorMessage = "Please enter Phone Number.")]
        [StringLength(4, ErrorMessage = "You must enter 4 numbers.", MinimumLength = 4)]
        public string PhoneNumber3 { get; set; }
        [Required(ErrorMessage = "Please enter About Charity.")]
        public string AboutCharity { get; set; }
        [Required(ErrorMessage = "Please enter Legal Entity Name.")]
        public string LegalEntityName { get; set; }
        [Required(ErrorMessage = "Please enter 501_C3 Number.")]
        public string Number501_C3 { get; set; }
        [Required(ErrorMessage = "Please enter Charity Address.")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please enter City.")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please select State.")]
        public string State { get; set; }
        [Required(ErrorMessage = "Please enter zip.")]
        public string Zip { get; set; }
        [Required(ErrorMessage = "Please select Category.")]
        public int? CategoryID { get; set; }
        [Required(ErrorMessage = "Please enter Charity Name.")]
        public string CharityName { get; set; }
        public HttpPostedFileBase CharityLogo { get; set; }
        public string CharityLogoFullPath { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public List<SelectListItem> States { get; set; }
        public int CharityId { get; set; }


    }
}
