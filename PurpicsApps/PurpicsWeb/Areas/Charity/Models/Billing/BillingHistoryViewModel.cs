﻿using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurpicsWeb.Areas.Charity.Models.Billing
{
    public class BillingHistorysViewModel
    {
        public List<BillingHistoryViewModel> loBillingHistory { get; set; }
        public BillingHistorysViewModel()
        {
            loBillingHistory = new List<BillingHistoryViewModel>();
        }
        public Int64 inRecordCount { get; set; }
        public Pager Pager { get; set; }
        public int inPageIndex { get; set; }

        
    } 
    //public class BillingHistoryViewModel
    //{
    //    public int Id { get; set; } // Id (Primary key)
    //    public int? CharityID { get; set; } // CharityID
    //    public int? CompanyID { get; set; } // CompanyID
    //    public int? CampaignID { get; set; } // CampaignID
    //    public DateTime? CampaignEndDate { get; set; } // CampaignEndDate
    //    public string Notes { get; set; } // Notes
    //    public double? EstAmount { get; set; } // EstAmount
    //    public bool? IsPaid { get; set; } // IsPaid
    //    public string CampaingCaption { get; set; }
    //    public string CompanyName { get; set; }
    //    public int inPageIndex { get; set; }
    //    public int inPageSize { get; set; }
    //    public string stSearch { get; set; }
    //    public string stSortColumn { get; set; }
    //    public Int64 inRowNumber { get; set; }
    //}
    public class BillingHistoryViewModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string AspNetUserID { get; set; } // AspNetUserID
        public int? CharityID { get; set; } // CharityID
        public string ReferenceNumber { get; set; } // ReferenceNumber
        public string Notes { get; set; } // Notes
        public double? Amount { get; set; } // Amount
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime? CreatedOn { get; set; } // CreatedOn
        public DateTime? SubscriptionEndDate { get; set; } // SubscriptionEndDate
        public int inPageIndex { get; set; }
        public int inPageSize { get; set; }
        public string stSearch { get; set; }
        public string stSortColumn { get; set; }
        public Int64 inRowNumber { get; set; }
        public string lsFromDate { get; set; }
        public string lsToDate { get; set; } 
    }
}