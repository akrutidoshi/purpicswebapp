﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Charity.Models
{
    public class CharityBankInfo
    {
        public int CharityId { get; set; }
        [Required(ErrorMessage = "Please enter Legal Bank Name.")]
        public string BankName { get; set; }
        [Required(ErrorMessage = "Please enter Account Type.")]
        public string AccountType { get; set; }
        [Required(ErrorMessage = "Please enter Routing Number.")]
        public string RoutingNumber { get; set; }
        [Required(ErrorMessage = "Please enter Account Number.")]
        public string AccountNumber { get; set; }        
    }
}