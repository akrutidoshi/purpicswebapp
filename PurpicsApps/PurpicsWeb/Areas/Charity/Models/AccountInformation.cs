﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PurpicsWeb.Areas.Charity.Models
{
    public class AccountInformation
    {
        public AccountInformation()
        {
            CharityId = 0;
            EmailAddress = string.Empty;
            Password = string.Empty;
            ContactName = string.Empty;
            PhoneNumber = string.Empty;
            AboutCharity = string.Empty;
            LegalEntityName = string.Empty;
            Number501_C3 = string.Empty;
            Address = string.Empty;
            CharityName = string.Empty;
            CharityLogo = null;

            CategoryName = string.Empty;
            CharityLogoFullPath = string.Empty;
            CategoryId = 0;
            Categories = new List<SelectListItem>();
            Status = string.Empty;
            CreditCardDetails = new CreditCardDetails();
        }

        public int CharityId { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumber { get; set; }
        public string AboutCharity { get; set; }
        public string LegalEntityName { get; set; }
        public string Number501_C3 { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CharityName { get; set; }
        public HttpPostedFileBase CharityLogo { get; set; }

        public string CategoryName { get; set; }
        public string CharityLogoFullPath { get; set; }
        public int CategoryId { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public string Status { get; set; }
        public CreditCardDetails CreditCardDetails { get; set; }

    }

}