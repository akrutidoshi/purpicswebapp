﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using NLog;
using PurpicsWeb.Services.CommonFunctions;
namespace PurpicsWeb.Controllers.API
{

    /// <summary>
    /// Charity APIs
    /// </summary>
    [RoutePrefix("api")]
    public class CharityAPIController : BaseAPIController
    {
        string CharityImagePath = ConfigurationManager.AppSettings["CharityImagePath"].ToString();
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Get Charity details.
        /// </summary>
        /// <remarks>
        /// Pass 0 to get all record.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("charity/{id:int?}")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> CharityDetails(int? id = 0)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                Expression<Func<Charities, bool>> expression = null;

                if (id > 0)
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false && x.Id == id;
                }
                else
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false && x.UserStatusID == (int)CommonEnums.CharityApprovalStatus.Approved;
                }

                //Order By
                Func<IQueryable<Charities>, IOrderedQueryable<Charities>> orderBy = x => x.OrderBy(y => y.CharityName);

                //Include CharityCategories
                List<Expression<Func<Charities, Object>>> includes = new List<Expression<Func<Charities, object>>>();
                Expression<Func<Charities, object>> charitieEntity = (roles) => roles.CharityCategories;
                includes.Add(charitieEntity);

                Expression<Func<Charities, object>> campaignsEntity = (roles) => roles.Campaigns;
                includes.Add(campaignsEntity);

                List<Charities> charities = Repository<Charities>.GetEntityListForQuery(expression, orderBy, includes, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    charities =
                        from charity in charities
                        select new
                        {
                            Id = charity.Id,
                            LegalEntityName = charity.LegalEntityName,
                            CharityName = charity.CharityName,
                            C501C3Number = charity.C501C3Number,
                            Address = charity.Address,
                            ContactPerson = charity.ContactPerson,
                            ContactPhone = charity.ContactPhone,
                            About = charity.About,
                            LogoFileName = Url.Content(CharityImagePath) + charity.LogoFileName,
                            UserStatusID = charity.UserStatusID,
                            CategoryID = charity.CategoryID,
                            CategoryName = charity.CharityCategories == null ? "" : charity.CharityCategories.Name,
                            Campaigns =
                                from campaign in charity.Campaigns
                                where campaign.CharityID == charity.Id
                                    //&& campaign.Likes < campaign.PackageLikes
                                    //&& campaign.CampaignStatusID ==(int) CommonEnums.CampaignApprovalStatus.Approved
                                && campaign.IsActive == true
                                && campaign.IsDeleted == false
                                select new
                                {
                                    CampaignId = campaign.Id,
                                    CampaignCaption = campaign.Caption,
                                    ImageFile = Url.Content(CampaignImagePath) + campaign.ImageFile,
                                    ExpirationDate = campaign.ExpirationDate,
                                    CampaignRunningStatus = campaign.Likes >= campaign.PackageLikes ?
                                    StringEnum.GetStringValue(CommonEnums.CampaignRunningStatus.Completed) :
                                    StringEnum.GetStringValue(CommonEnums.CampaignRunningStatus.OnGoing)
                                }
                        }
                });

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }

            return GetOkResult(loJObjResult);
        }

    }

}
