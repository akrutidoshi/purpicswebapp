﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using NLog;
using PurpicsWeb.Services.CommonFunctions;
namespace PurpicsWeb.Controllers.API
{

    /// <summary>
    /// StudentOrganiation APIs
    /// </summary>
    [RoutePrefix("api")]
    public class StudentOrganiationAPIController : BaseAPIController
    {
        string CharityImagePath = ConfigurationManager.AppSettings["CharityImagePath"].ToString();
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        string StudentOrganizationImagePath = ConfigurationManager.AppSettings["StudentOrganizationImagePath"].ToString();
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Get student organization details.
        /// </summary>
        /// <remarks>
        /// Pass 0 to get all record.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("studentorganization/{id:int?}")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> StudentOrganizationDetails(int? id = 0)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                Expression<Func<StudentOrganizations, bool>> expression = null;

                if (id > 0)
                {
                    expression = x => x.Id == id;
                }
                else
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false && x.UserStatusID == (int)CommonEnums.StudentOrganizationApprovalStatus.Approved;
                }

                //Order By
                Func<IQueryable<StudentOrganizations>, IOrderedQueryable<StudentOrganizations>> orderBy = x => x.OrderBy(y => y.Name);

                List<Expression<Func<StudentOrganizations, Object>>> includes = new List<Expression<Func<StudentOrganizations, object>>>();

                Expression<Func<StudentOrganizations, object>> campaignsEntity = (so) => so.Campaigns;
                includes.Add(campaignsEntity);

                List<StudentOrganizations> studentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(expression, orderBy, includes, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    studentorganizations =
                        from so in studentOrganizations
                        select new
                        {
                            Id = so.Id,
                            Name = so.Name,
                            Address = so.Address,
                            ContactPerson = so.ContactPerson,
                            ContactPhone = so.ContactPhone,
                            LogoFileName = Url.Content(StudentOrganizationImagePath) + so.LogoFileName,
                            UserStatusID = so.UserStatusID,
                            Campaigns =
                                from campaign in so.Campaigns
                                where campaign.StudentOrganizationId == so.Id
                                && campaign.IsActive == true
                                && campaign.IsDeleted == false
                                select new
                                {
                                    CampaignId = campaign.Id,
                                    CampaignCaption = campaign.Caption,
                                    ImageFile = Url.Content(CampaignImagePath) + campaign.ImageFile,
                                    ExpirationDate = campaign.ExpirationDate,
                                    CampaignRunningStatus = campaign.Likes >= campaign.PackageLikes ?
                                    StringEnum.GetStringValue(CommonEnums.CampaignRunningStatus.Completed) :
                                    StringEnum.GetStringValue(CommonEnums.CampaignRunningStatus.OnGoing)
                                }
                        }
                });

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }

            return GetOkResult(loJObjResult);
        }
        /// <summary>
        /// Get student organization list.
        /// </summary>
        /// <remarks>
        /// Pass 0 to get all record.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("studentorganization/list")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> GetStudentOrganizations()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                Expression<Func<StudentOrganizations, bool>> expression =  x => x.IsActive == true && x.IsDeleted == false && x.UserStatusID == (int)CommonEnums.StudentOrganizationApprovalStatus.Approved;

                //Order By
                Func<IQueryable<StudentOrganizations>, IOrderedQueryable<StudentOrganizations>> orderBy = x => x.OrderBy(y => y.Name);

                List<Expression<Func<StudentOrganizations, Object>>> includes = new List<Expression<Func<StudentOrganizations, object>>>();

                List<StudentOrganizations> studentOrganizations = Repository<StudentOrganizations>.GetEntityListForQuery(expression, orderBy, null, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    studentorganizations =
                        from so in studentOrganizations
                        select new
                        {
                            Id = so.Id,
                            Name = so.Name,
                            Address = so.Address,
                            ContactPerson = so.ContactPerson,
                            ContactPhone = so.ContactPhone,
                            LogoFileName = Url.Content(StudentOrganizationImagePath) + so.LogoFileName,
                            UserStatusID = so.UserStatusID
                        }
                });

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }

            return GetOkResult(loJObjResult);
        }
    }

}
