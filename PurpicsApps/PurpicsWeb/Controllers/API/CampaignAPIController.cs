﻿using Hangfire;
using LinqKit;
using Newtonsoft.Json.Linq;
using NLog;
using Purpics.Common;
using PurpicsWeb.Controllers.API;
using PurpicsWeb.Models;
using PurpicsWeb.Services;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace PurpicsWeb.Controllers
{

    /// <summary>
    /// Campaign APIs
    /// </summary>
    [RoutePrefix("api")]
    public class CampaignAPIController : BaseAPIController
    {
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        string CharityImagePath = ConfigurationManager.AppSettings["CharityImagePath"].ToString();
        string CampaignCouponImagePath = ConfigurationManager.AppSettings["CampaignCouponImagePath"].ToString();
        string StudentOrganizationImagePath = ConfigurationManager.AppSettings["StudentOrganizationImagePath"].ToString();
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Get campaign details with charitie and volunteerCoupons
        /// </summary>
        /// <remarks>
        /// Pass 0 to get all records.
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("campaign/{id:int}")]
        [AcceptVerbs("POST")]
        public async Task<IHttpActionResult> CampaignDetails(CampaignFilterModel model, int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;
            try
            {
                Expression<Func<Campaigns, bool>> expression = null;
                Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderBy = GetSortExpression(model.SortBy);
                if (id > 0)
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false && x.Id == id;
                }
                else
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false
                        && x.CampaignStatusID == (int)CommonEnums.CampaignApprovalStatus.Approved
                        && x.Likes < x.PackageLikes;

                    if (model.SearchType == "CurrentLocation")
                    {
                        if (model.Latitude == null || model.Longitude == null || model.Latitude == 0 || model.Longitude == 0)
                        {
                            loJObjResult = new JObject(
                               new JProperty("id", null),
                               new JProperty("message", "Location inuput is required"),
                               new JProperty("status", false)
                           );
                            return GetOkResult(loJObjResult);
                        }

                        //Chose By Filter
                        if (model.Latitude != null && model.Longitude != null && model.Latitude != 0 && model.Longitude != 0)
                        {
                            string NearestZip = PurpicsWeb.Services.GeoLocation.GetNearestZipCode((double)model.Latitude, (double)model.Longitude);
                            expression = GetCampaignScopeAreaExpression(expression, NearestZip, string.Empty);
                        }
                    }
                    else if (model.SearchType == "RegisteredLocation")
                    {
                        Volunteers volunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == model.VolunteersId).FirstOrDefault();
                        if (volunteer != null && !string.IsNullOrEmpty(volunteer.ZipCode))
                        {
                            expression = GetCampaignScopeAreaExpression(expression, volunteer.ZipCode, string.Empty);
                        }
                    }
                    else if (model.SearchType == "Both")
                    {
                        if (model.Latitude == null || model.Longitude == null || model.Latitude == 0 || model.Longitude == 0)
                        {
                            loJObjResult = new JObject(
                               new JProperty("id", null),
                               new JProperty("message", "Location inuput is required"),
                               new JProperty("status", false)
                           );
                            return GetOkResult(loJObjResult);
                        }

                        //For Current Location
                        string NearestZip = string.Empty;
                        if (model.Latitude != null && model.Longitude != null && model.Latitude != 0 && model.Longitude != 0)
                        {
                            NearestZip = PurpicsWeb.Services.GeoLocation.GetNearestZipCode((double)model.Latitude, (double)model.Longitude);
                        }

                        //For Regstered Location of Volunteer
                        Volunteers volunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == model.VolunteersId).FirstOrDefault();

                        //Now get expression for both location
                        if (volunteer != null)
                        {
                            expression = GetCampaignScopeAreaExpression(expression, NearestZip, volunteer.ZipCode);
                        }
                    }

                    if (model.CharityIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length > 0)
                    {
                        IEnumerable<int> charityIds = model.CharityIds.Split(',').Select(str => int.Parse(str));

                        Expression<Func<Campaigns, bool>> expCharity = x => charityIds.Contains(x.CharityID);
                        expression = PredicateBuilder.And(expression, expCharity);
                    }


                    if (model.SOIds != null && model.SOIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length > 0)
                    {
                        IEnumerable<int> studentOrgaIds = model.SOIds.Split(',').Select(str => int.Parse(str));

                        Expression<Func<Campaigns, bool>> expSO = x => studentOrgaIds.Contains((int)x.StudentOrganizationId);
                        expression = PredicateBuilder.And(expression, expSO);
                    }

                    if (model.CompanyIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length > 0)
                    {
                        IEnumerable<int> companyIds = model.CompanyIds.Split(',').Select(str => int.Parse(str));

                        Expression<Func<Campaigns, bool>> expCompanyId = x => companyIds.Contains(x.CompanyID);
                        expression = PredicateBuilder.And(expression, expCompanyId);
                    }
                }

                //Include Charities , volunteerCoupons, package
                List<Expression<Func<Campaigns, Object>>> includes = GetIncludeExpression();

                model.PageNumber = model.PageNumber == 0 ? 1 : model.PageNumber;
                model.PageSize = model.PageSize == 0 ? 20 : model.PageSize;

                List<Campaigns> campaigns = Repository<Campaigns>.GetEntityListForQuery(expression, orderBy, includes, model.PageNumber, model.PageSize).ToList();
                loJObjResult = GetCampaignJsonFormat(campaigns);

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }

            return GetOkResult(loJObjResult);
        }


        private static Expression<Func<Campaigns, bool>> GetCampaignScopeAreaExpression(Expression<Func<Campaigns, bool>> expression, string zip, string zip2)
        {
            Expression<Func<ZipCode, bool>> expressionZip = null;
            expressionZip = x => x.Zipcode == zip;
            ZipCode zipCode = Repository<ZipCode>.GetEntityListForQuery(expressionZip, null, null, null, null).FirstOrDefault();

            Expression<Func<ZipCode, bool>> expressionZip2 = null;
            expressionZip2 = x => x.Zipcode == zip2;
            ZipCode zipCode2 = Repository<ZipCode>.GetEntityListForQuery(expressionZip2, null, null, null, null).FirstOrDefault();

            if (zipCode != null && zipCode2 == null)
            {
                Expression<Func<Campaigns, bool>> expCampaignScope = x => x.CampaignScopeAreas.Any(y => y.ZipcodeId == zipCode.Id);
                expression = PredicateBuilder.And(expression, expCampaignScope);
                return expression;

            }
            else if (zipCode2 != null && zipCode == null)
            {
                Expression<Func<Campaigns, bool>> expCampaignScope = x => x.CampaignScopeAreas.Any(y => y.ZipcodeId == zipCode2.Id);
                expression = PredicateBuilder.And(expression, expCampaignScope);
                return expression;

            }
            else if (zipCode2 != null && zipCode != null)
            {
                Expression<Func<Campaigns, bool>> expCampaignScope = x => x.CampaignScopeAreas.Any(y => y.ZipcodeId == zipCode2.Id || y.ZipcodeId == zipCode.Id);
                expression = PredicateBuilder.And(expression, expCampaignScope);
                return expression;

            }
            else if (zipCode2 == null && zipCode == null)
            {
                Expression<Func<Campaigns, bool>> expCampaignScope = x => x.CampaignScopeAreas.Any(y => 1 == 2);
                expression = PredicateBuilder.And(expression, expCampaignScope);
                return expression;

            }
            return expression;
        }

        /// <summary>
        ///  Upload campaign.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("campaign/{id}/upload")]
        [AcceptVerbs("POST")]
        public async Task<IHttpActionResult> CampaignUpload(CampaignUploadModel model)
        {
            string trackingId = string.Empty;
            string trackingIdOnly = string.Empty;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject jsonresult = null;
            try
            {
                CampaignUploads campaignUpload = new CampaignUploads();
                campaignUpload.CampaignID = model.CampaignID;
                campaignUpload.VolunteerID = model.VolunteerID;
                campaignUpload.IsDeleted = false;
                campaignUpload.CreatedOn = DateTime.Now;
                campaignUpload.CreatedBy = model.VolunteerID.ToString();
                await Repository<CampaignUploads>.InsertEntity<int>(campaignUpload, (entity) => { return entity.Id; });

                Campaigns campaign = Repository<Campaigns>.GetEntityListForQuery(x => x.Id == model.CampaignID).FirstOrDefault();
                trackingIdOnly = TrackingIdService.NewId(campaignUpload.VolunteerID, campaignUpload.Id);
                if (campaign != null)
                {
                    trackingId = "#PurPics" + trackingIdOnly;
                }
                else
                {
                    trackingId = "#PurPics" + trackingIdOnly;
                }
                CampaignUploads objCampaignUploads = Repository<CampaignUploads>.GetEntityListForQuery(x => x.Id == campaignUpload.Id).FirstOrDefault();
                objCampaignUploads.TrackingID = trackingIdOnly;
                await Repository<CampaignUploads>.UpdateEntity<int>(objCampaignUploads, (entity) => { return entity.Id; });

                Volunteers volunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == campaignUpload.VolunteerID).FirstOrDefault();
                AppSettings setting = Repository<AppSettings>.GetEntityListForQuery(x => x.Name == "CampaignUploadPoints").FirstOrDefault();
                volunteer.CreditPoints = volunteer.CreditPoints + Convert.ToInt32(setting.Value);
                await Repository<Volunteers>.UpdateEntity<int>(volunteer, (entity) => { return entity.Id; });

                VolunteerCoupons coupen = Repository<VolunteerCoupons>.GetEntityListForQuery(x => x.VolunteerID == campaignUpload.VolunteerID && x.CampaignID == model.CampaignID).FirstOrDefault();
                if (coupen == null)
                {
                    VolunteerCoupons volunteerCoupons = new VolunteerCoupons();
                    volunteerCoupons.CampaignID = model.CampaignID;
                    volunteerCoupons.VolunteerID = model.VolunteerID;
                    volunteerCoupons.IsDeleted = false;
                    volunteerCoupons.CreatedOn = DateTime.Now;
                    volunteerCoupons.CreatedBy = model.VolunteerID.ToString();
                    await Repository<VolunteerCoupons>.InsertEntity<int>(volunteerCoupons, (entity) => { return entity.Id; });
                }

                InstagramSyncService objInstagramSyncService = new InstagramSyncService();
                BackgroundJob.Schedule(() => objInstagramSyncService.UpdateMediaIdByTrackingId(objCampaignUploads.TrackingID), TimeSpan.FromMinutes(2));

                jsonresult = new JObject(
                     new JProperty("id", campaignUpload.Id),
                     new JProperty("message", "Campaign uploaded successfully."),
                     new JProperty("trackingId", trackingId),
                     new JProperty("status", true)
                 );
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                jsonresult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("trackingId", null),
                       new JProperty("status", false)
                   );
                return GetOkResult(jsonresult);
            }

            return GetOkResult(jsonresult);
        }

        /// <summary>
        /// Get Tracking Id.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("trackingid")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> GetTrackingId()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                string trackingId = TrackingId.Generate();
                loJObjResult = new JObject(
                      new JProperty("TrackingId", TrackingId.Generate())
                  );
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }

            return GetOkResult(loJObjResult);
        }

        /// <summary>
        /// Get featured campaign details with charitie and volunteerCoupons
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("campaign/featured")]
        [AcceptVerbs("POST")]
        public async Task<IHttpActionResult> FeaturedCampaignDetails(FeaturedCampaignFilterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;
            try
            {
                Expression<Func<Campaigns, bool>> expression = expression = x => x.IsActive == true && x.IsDeleted == false
                    && x.CampaignStatusID == (int)CommonEnums.CampaignApprovalStatus.Approved
                    && x.Likes < x.PackageLikes
                    && x.IsFeaturedCampaign == true;

                //Order By
                Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderBy = GetSortExpression(model.SortBy);

                List<Expression<Func<Campaigns, Object>>> includes = GetIncludeExpression();

                model.PageNumber = model.PageNumber == 0 ? 1 : model.PageNumber;
                model.PageSize = model.PageSize == 0 ? 20 : model.PageSize;

                List<Campaigns> campaigns = Repository<Campaigns>.GetEntityListForQuery(expression, orderBy, includes, model.PageNumber, model.PageSize).ToList();
                loJObjResult = GetCampaignJsonFormat(campaigns);

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }

            return GetOkResult(loJObjResult);
        }

        private static List<Expression<Func<Campaigns, object>>> GetIncludeExpression()
        {
            List<Expression<Func<Campaigns, Object>>> includes = new List<Expression<Func<Campaigns, object>>>();
            Expression<Func<Campaigns, object>> charities = (roles) => roles.Charities;
            includes.Add(charities);
            Expression<Func<Campaigns, object>> studentOrganizations = (roles) => roles.StudentOrganizations;
            includes.Add(studentOrganizations);
            Expression<Func<Campaigns, object>> volunteerCoupons = (roles) => roles.VolunteerCoupons;
            includes.Add(volunteerCoupons);
            Expression<Func<Campaigns, object>> charityCategories = (roles) => roles.Charities.CharityCategories;
            includes.Add(charityCategories);
            Expression<Func<Campaigns, object>> zip = (roles) => roles.CampaignScopeAreas;
            includes.Add(zip);
            Expression<Func<Campaigns, object>> companies = (roles) => roles.Companies;
            includes.Add(companies);
            return includes;
        }

        private static Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> GetSortExpression(string SortBy)
        {
            Func<IQueryable<Campaigns>, IOrderedQueryable<Campaigns>> orderBy = null;
            switch (SortBy)
            {
                case "TargetLikes":
                    orderBy = x => x.OrderByDescending(y => y.PackageLikes);
                    break;
                case "CompanyName":
                    orderBy = x => x.OrderBy(y => y.Companies.CompanyName);
                    break;
                case "CharityName":
                    orderBy = x => x.OrderBy(y => y.Charities.CharityName);
                    break;
                case "StudentOrganizationName":
                    orderBy = x => x.OrderBy(y => y.StudentOrganizations.Name);
                    break;
                case "TotalLikesReceived":
                    orderBy = x => x.OrderByDescending(y => y.Likes);
                    break;
                default:
                    orderBy = x => x.OrderBy(y => y.Companies.CompanyName);
                    break;
            }
            return orderBy;
        }

        private JObject GetCampaignJsonFormat(List<Campaigns> campaigns)
        {

            JObject loJObjResult = JObject.FromObject(new
            {
                campaigns =
                    from campaign in campaigns
                    select new
                    {
                        Id = campaign.Id,
                        CharityID = campaign.CharityID,
                        StudentOrganizationId = campaign.StudentOrganizationId,
                        CompanyID = campaign.CompanyID,
                        CompanyName = campaign.Companies.CompanyName,
                        //Address = campaign.Companies.Address,
                        Address = campaign.StudentOrganizations == null ? "" : campaign.StudentOrganizations.Address,
                        PackageID = campaign.PackageID,
                        CampaignStatusID = campaign.CampaignStatusID,
                        Caption = campaign.Caption,
                        CampaignName = campaign.CampaignName,
                        Description = campaign.Description,
                        ImageFile = Url.Content(CampaignImagePath) + campaign.ImageFile,
                        CouponCode = campaign.CouponCode,
                        CouponImage = Url.Content(CampaignCouponImagePath) + campaign.CouponCode,
                        CoupenExpirationDate = campaign.ExpirationDate,
                        CouponTitle = campaign.CouponTitle,
                        CouponDescription = campaign.CouponDescription,
                        ExpirationDate = campaign.ExpirationDate,
                        Latitude = campaign.Latitude,
                        Longitude = campaign.Longitude,
                        Zipcode = campaign.Zipcode,
                        Radius = campaign.Radius,
                        Likes = campaign.Likes,
                        PackageLikes = campaign.PackageLikes,
                        CategoryId = campaign.Charities.CategoryID,
                        CategoryName = campaign.Charities.CharityCategories == null ? "": campaign.Charities.CharityCategories.Name,
                        charitie =
                            new
                            {
                                LegalEntityName = campaign.Charities.LegalEntityName,
                                C501C3Number = campaign.Charities.C501C3Number,
                                CharityName = campaign.Charities.CharityName,
                                Address = campaign.Charities.Address,
                                ContactPerson = campaign.Charities.ContactPerson,
                                ContactPhone = campaign.Charities.ContactPhone,
                                About = campaign.Charities.About,
                                LogoFileName = Url.Content(CharityImagePath) + campaign.Charities.LogoFileName,
                                UserStatusID = campaign.Charities.UserStatusID,
                                CategoryID = campaign.Charities.CategoryID
                            },
                        studentorganization =
                            new
                            {
                                Name = campaign.StudentOrganizations == null ? "" : campaign.StudentOrganizations.Name,
                                Address = campaign.StudentOrganizations == null ? "" : campaign.StudentOrganizations.Address,
                                ContactPerson = campaign.StudentOrganizations == null ? "" : campaign.StudentOrganizations.ContactPerson,
                                ContactPhone = campaign.StudentOrganizations == null ? "" : campaign.StudentOrganizations.ContactPhone,
                                LogoFileName = campaign.StudentOrganizations == null ? "" : Url.Content(StudentOrganizationImagePath) + campaign.StudentOrganizations.LogoFileName,
                                UserStatusID = campaign.StudentOrganizations == null ? 0 : campaign.StudentOrganizations.UserStatusID
                            },
                        volunteerCoupons =
                            from coupons in campaigns
                            where coupons.Id == campaign.Id
                            select new
                            {
                                VolunteerID = 0,
                                RedeemptionDate = DateTime.Now,
                                CoupenId = 0,
                                CouponImage = Url.Content(CampaignCouponImagePath) + campaign.CouponCode,
                                CoupenExpirationDate = campaign.ExpirationDate,
                                CouponTitle = campaign.CouponTitle,
                                CouponDescription = campaign.CouponDescription
                            }
                    }
            });
            return loJObjResult;
        }
    }
}
