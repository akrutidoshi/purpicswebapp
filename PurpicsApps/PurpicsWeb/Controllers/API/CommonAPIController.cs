﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using NLog;
using PurpicsWeb.Services.CommonFunctions;
namespace PurpicsWeb.Controllers.API
{

    /// <summary>
    /// Common APIs
    /// </summary>
    [RoutePrefix("api")]
    public class CommonAPIController : BaseAPIController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Get list of charity categories
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("category/list")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> CategoryList()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;
            try
            {
                Expression<Func<CharityCategories, bool>> expression = null;

                //Order By
                Func<IQueryable<CharityCategories>, IOrderedQueryable<CharityCategories>> orderBy = x => x.OrderBy(y => y.Name);

                //Include 
                List<Expression<Func<CharityCategories, Object>>> includes = new List<Expression<Func<CharityCategories, object>>>();

                List<CharityCategories> charityCategories = Repository<CharityCategories>.GetEntityListForQuery(expression, orderBy, includes, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    categories =
                        from category in charityCategories
                        select new
                        {
                            Id = category.Id,
                            Name = category.Name,
                        }
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }
            return GetOkResult(loJObjResult);
        }
    }
}
