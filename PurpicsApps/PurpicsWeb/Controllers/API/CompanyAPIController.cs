﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using NLog;
using PurpicsWeb.Services.CommonFunctions;
namespace PurpicsWeb.Controllers.API
{

    /// <summary>
    /// Company APIs
    /// </summary>
    [RoutePrefix("api")]
    public class CompanyAPIController : BaseAPIController
    {
        string CompanyImagePath = ConfigurationManager.AppSettings["CompanyImagePath"].ToString();
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Get Comapny details.
        /// </summary>
        /// <remarks>
        /// Pass 0 to get all records.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("company/{id:int?}")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> CompanyDetails(int? id = 0)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                Expression<Func<Companies, bool>> expression = null;

                if (id > 0)
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false && x.Id == id;
                }
                else
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false;

                }

                //Order By
                Func<IQueryable<Companies>, IOrderedQueryable<Companies>> orderBy = x => x.OrderBy(y => y.CompanyName);

                //Include 
                List<Expression<Func<Companies, Object>>> includes = new List<Expression<Func<Companies, object>>>();
                Expression<Func<Companies, object>> campaignsEntity = (roles) => roles.Campaigns;
                includes.Add(campaignsEntity);

                List<Companies> companies = Repository<Companies>.GetEntityListForQuery(expression, orderBy, includes, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    companies =
                        from company in companies
                        select new
                        {
                            Id = company.Id,
                            LegalEntityName = company.LegalEntityName,
                            Address = company.Address,
                            ContactPerson = company.ContactPerson,
                            ContactPhone = company.ContactPhone,
                            About = company.About,
                            LogoFileName = Url.Content(CompanyImagePath) + company.LogoFilename,
                            //C501C3Number = company.C501C3Number,
                            CompanyName = company.CompanyName,
                            Campaigns =
                               from campaign in company.Campaigns
                               where campaign.CompanyID == company.Id
                                //&& campaign.Likes < campaign.PackageLikes
                                //&& campaign.CampaignStatusID == (int)CommonEnums.CampaignApprovalStatus.Approved
                                && campaign.IsActive == true
                                && campaign.IsDeleted == false
                               select new
                               {
                                   CampaignId = campaign.Id,
                                   CampaignCaption = campaign.Caption,
                                   ImageFile = Url.Content(CampaignImagePath) + campaign.ImageFile,
                                   ExpirationDate = campaign.ExpirationDate,
                                   CampaignRunningStatus = campaign.Likes >= campaign.PackageLikes ?
                                    StringEnum.GetStringValue(CommonEnums.CampaignRunningStatus.Completed) :
                                    StringEnum.GetStringValue(CommonEnums.CampaignRunningStatus.OnGoing)
                               }
                        }
                });

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }
            return GetOkResult(loJObjResult);
        }

        /// <summary>
        /// Get Company's campaign details.
        /// </summary>
        /// <remarks>
        /// Pass 0 to get all records.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("company/{id}/Campaigns")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> CompanyCampaignsDetails(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                Expression<Func<Companies, bool>> expression = null;

                if (id > 0)
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false && x.Id == id;
                }
                else
                {
                    expression = x => x.IsActive == true && x.IsDeleted == false;

                }

                //Order By
                Func<IQueryable<Companies>, IOrderedQueryable<Companies>> orderBy = x => x.OrderBy(y => y.CompanyName);

                //Include 
                List<Expression<Func<Companies, Object>>> includes = new List<Expression<Func<Companies, object>>>();
                Expression<Func<Companies, object>> campaignsEntity = (roles) => roles.Campaigns;
                includes.Add(campaignsEntity);

                List<Companies> companies = Repository<Companies>.GetEntityListForQuery(expression, orderBy, includes, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    companies =
                        from company in companies
                        select new
                        {
                            Id = company.Id,
                            LegalEntityName = company.LegalEntityName,
                            Address = company.Address,
                            ContactPerson = company.ContactPerson,
                            ContactPhone = company.ContactPhone,
                            About = company.About,
                            LogoFileName = Url.Content(CompanyImagePath) + company.LogoFilename,
                            //C501C3Number = company.C501C3Number,
                            CompanyName = company.CompanyName,
                            campaigns =
                                from campaign in company.Campaigns
                                select new
                                {
                                    Id = campaign.Id,
                                    CharityID = campaign.CharityID,
                                    StudentOrganizationId = campaign.StudentOrganizationId,
                                    CompanyID = campaign.CompanyID,
                                    PackageID = campaign.PackageID,
                                    CampaignStatusID = campaign.CampaignStatusID,
                                    Caption = campaign.Caption,
                                    ImageFile = Url.Content(CampaignImagePath) + campaign.ImageFile,
                                    CouponCode = campaign.CouponCode,
                                    ExpirationDate = campaign.ExpirationDate,
                                    Latitude = campaign.Latitude,
                                    Longitude = campaign.Longitude,
                                    Radius = campaign.Radius,
                                    Likes = campaign.Likes,
                                }
                        }
                });

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }
            return GetOkResult(loJObjResult);
        }
    }
}
