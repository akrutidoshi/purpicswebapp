﻿using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using PurpicsWeb.Models;
using PurpicsWeb_DL.Repositories;
using PurpicsWeb_DL.Entities;

using System.Linq;
using System.IO;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using System.Linq.Expressions;
using System.Collections.Generic;
using PurpicsWeb.Controllers.API;
using PurpicsWeb.Services.CommonFunctions;
using NLog;
using LinqKit;
namespace PurpicsWeb.Controllers
{

    /// <summary>
    /// Volunteers APIs
    /// </summary>
    [RoutePrefix("api")]
    public class VolunteerAPIController : BaseAPIController
    {
        private static readonly string VolunteerImagePath = ConfigurationManager.AppSettings["VolunteerImagePath"].ToString();
        string CampaignImagePath = ConfigurationManager.AppSettings["CampaignImagePath"].ToString();
        string CampaignCouponImagePath = ConfigurationManager.AppSettings["CampaignCouponImagePath"].ToString();
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Create/Update student profile
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("volunteer/saveprofile")]
        [AcceptVerbs("POST")]
        public async Task<IHttpActionResult> AddEditProfile(VolunteersProfile model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject jsonresult = null;

            try
            {
                if (model.Id != null && model.Id > 0) // Update Profile
                {
                    Volunteers volunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == model.Id).FirstOrDefault();
                    volunteer.Name = string.IsNullOrEmpty(model.FullName) ? volunteer.Name : model.FullName;
                    volunteer.Email = string.IsNullOrEmpty(model.Email) ? volunteer.Email : model.Email;
                    volunteer.CurrentGrade = string.IsNullOrEmpty(model.CurrentGrade) ? volunteer.CurrentGrade : model.CurrentGrade;
                    volunteer.GiftCardPreference = model.GiftCardPreference == null ? volunteer.GiftCardPreference : model.GiftCardPreference;
                    volunteer.ZipCode = model.ZipCode;
                    volunteer.ModifiedOn = DateTime.Now;
                    volunteer.ModifiedBy = Convert.ToString(model.Id);

                    await Repository<Volunteers>.UpdateEntity<int>(volunteer, (entity) => { return entity.Id; });

                    //Delete existing records
                    Expression<Func<VolunteersSO, bool>> expVolunteersSO = x => x.VolunteerId == volunteer.Id;
                    List<VolunteersSO> volSos = Repository<VolunteersSO>.GetEntityListForQuery(expVolunteersSO, null, null, null, null).ToList();
                    await Task.Run(() => Repository<VolunteersSO>.DeleteRange(volSos));

                    if (!string.IsNullOrEmpty(model.StudentOrganizationIds))
                    {
                        IEnumerable<int> studentOrgaIds = model.StudentOrganizationIds.Split(',').Select(str => int.Parse(str));
                        foreach (var soId in studentOrgaIds)
                        {
                            VolunteersSO existingVolunteerSO = Repository<VolunteersSO>.GetEntityListForQuery(x => x.VolunteerId == volunteer.Id && x.StudentOrganizationId == soId).FirstOrDefault();

                            if (existingVolunteerSO == null)
                            {
                                VolunteersSO volunteerSO = new VolunteersSO();
                                volunteerSO.VolunteerId = volunteer.Id;
                                volunteerSO.StudentOrganizationId = soId;
                                volunteerSO.CreatedOn = DateTime.Now;
                                volunteerSO.CreatedBy = "0";
                                await Repository<VolunteersSO>.InsertEntity<int>(volunteerSO, (entity) => { return entity.Id; });
                            }
                        }
                    }

                    jsonresult = new JObject(
                         new JProperty("id", volunteer.Id),
                         new JProperty("message", "Profile saved successfully."),
                         new JProperty("status", true)
                     );
                }
                else // Create Profile
                {
                    Volunteers volunteer = new Volunteers();
                    volunteer.CurrentGrade = model.CurrentGrade;
                    volunteer.ZipCode = model.ZipCode;
                    volunteer.CreditPoints = 0;
                    volunteer.InstagramUserId = model.InstagramUserId;
                    volunteer.AspNetUserID = null;
                    volunteer.ImageFile = model.InstagramProfileImageURL;
                    volunteer.Name = model.FullName;
                    volunteer.Email = model.Email;
                    volunteer.InstagramToken = model.InstagramToken;
                    volunteer.InstagramUserName = model.InstagramUserName;
                    volunteer.CreatedOn = DateTime.Now;
                    volunteer.CreatedBy = "0";
                    await Repository<Volunteers>.InsertEntity<int>(volunteer, (entity) => { return entity.Id; });

                    if (!string.IsNullOrEmpty(model.StudentOrganizationIds))
                    {
                        IEnumerable<int> studentOrgaIds = model.StudentOrganizationIds.Split(',').Select(str => int.Parse(str));
                        foreach (var soId in studentOrgaIds)
                        {
                            VolunteersSO existingVolunteerSO = Repository<VolunteersSO>.GetEntityListForQuery(x => x.VolunteerId == volunteer.Id && x.StudentOrganizationId == soId).FirstOrDefault();

                            if (existingVolunteerSO == null)
                            {
                                VolunteersSO volunteerSO = new VolunteersSO();
                                volunteerSO.VolunteerId = volunteer.Id;
                                volunteerSO.StudentOrganizationId = soId;
                                volunteerSO.CreatedOn = DateTime.Now;
                                volunteerSO.CreatedBy = "0";
                                await Repository<VolunteersSO>.InsertEntity<int>(volunteerSO, (entity) => { return entity.Id; });
                            }
                        }
                    }

                    jsonresult = new JObject(
                         new JProperty("id", volunteer.Id),
                         new JProperty("message", "Volunteer registered successfully."),
                         new JProperty("status", true)
                     );
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                jsonresult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
                return GetOkResult(jsonresult);
            }

            return GetOkResult(jsonresult);
        }

        /// <summary>
        /// Update Instagram Authentication token after every Login
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("volunteer/validate")]
        [AcceptVerbs("POST")]
        public async Task<IHttpActionResult> UpdateInstagramToken(UpdateInstagramTokenModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject jsonresult = null;

            try
            {
                Volunteers volunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.InstagramUserId == model.InstagramUserId).FirstOrDefault();
                if (volunteer == null)
                {
                    jsonresult = new JObject(
                                            new JProperty("id", "0"),
                                            new JProperty("message", "Volunteer not exists."),
                                            new JProperty("status", false)
                                        );
                }
                else
                {
                    volunteer.InstagramToken = string.IsNullOrEmpty(model.InstagramToken) ? volunteer.InstagramToken : model.InstagramToken;
                    volunteer.ModifiedOn = DateTime.Now;
                    volunteer.ModifiedBy = Convert.ToString(model.InstagramUserId);

                    await Repository<Volunteers>.UpdateEntity<int>(volunteer, (entity) => { return entity.Id; });

                    jsonresult = new JObject(
                         new JProperty("id", volunteer.Id),
                         new JProperty("message", "Volunteer exists."),
                         new JProperty("status", true)
                     );
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                jsonresult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
                return GetOkResult(jsonresult);
            }

            return GetOkResult(jsonresult);
        }

        /// <summary>
        /// Get coupon details.
        /// </summary>
        /// <remarks>
        /// Pass CouponId=0 to get all records.
        /// </remarks>
        /// <param name="VolunteerID"></param>
        /// <param name="CouponId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("volunteer/{VolunteerID}/coupons/{CouponId:int?}")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> CouponDetails(int VolunteerID, int CouponId = 0)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                Expression<Func<VolunteerCoupons, bool>> expression = null;
                DateTime todayDate = DateTime.Now.Date;

                if (CouponId > 0)
                {
                    expression = x => x.Id == CouponId
                        && x.VolunteerID == VolunteerID
                        && x.IsDeleted == false
                        && x.RedeemptionDate == null
                        && System.Data.Entity.DbFunctions.TruncateTime(x.Campaigns.ExpirationDate) >= todayDate;
                }
                else
                {
                    expression = x => x.VolunteerID == VolunteerID
                        && x.IsDeleted == false
                        && x.RedeemptionDate == null
                        && System.Data.Entity.DbFunctions.TruncateTime(x.Campaigns.ExpirationDate) >= todayDate;
                }

                //Order By
                Func<IQueryable<VolunteerCoupons>, IOrderedQueryable<VolunteerCoupons>> orderBy = x => x.OrderBy(y => y.Id);

                //Include 
                List<Expression<Func<VolunteerCoupons, Object>>> includes = new List<Expression<Func<VolunteerCoupons, object>>>();
                Expression<Func<VolunteerCoupons, object>> campaign = (roles) => roles.Campaigns;
                includes.Add(campaign);

                List<VolunteerCoupons> volunteerCoupons = Repository<VolunteerCoupons>.GetEntityListForQuery(expression, orderBy, includes, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    coupons =
                        from coupon in volunteerCoupons
                        select new
                        {
                            Id = coupon.Id,
                            CouponImage = Url.Content(CampaignCouponImagePath) + coupon.Campaigns.CouponCode,
                            CoupenExpirationDate = coupon.Campaigns.ExpirationDate,
                            CompanyID = coupon.VolunteerID,
                            PackageID = coupon.CampaignID,
                            RedeemptionDate = coupon.RedeemptionDate,
                            CouponTitle = coupon.Campaigns.CouponTitle,
                            CouponDescription = coupon.Campaigns.CouponDescription
                        }
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }

            return GetOkResult(loJObjResult);
        }

        /// <summary>
        /// Update Redemption Date
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("coupon/{id}/redeemed")]
        [AcceptVerbs("POST")]
        public async Task<IHttpActionResult> MarkAsRedeemed(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject jsonresult = null;

            try
            {
                VolunteerCoupons coupen = Repository<VolunteerCoupons>.GetEntityListForQuery(x => x.Id == id).FirstOrDefault();
                if (coupen != null)
                {
                    coupen.RedeemptionDate = DateTime.Now;
                    coupen.ModifiedOn = DateTime.Now;

                    await Repository<VolunteerCoupons>.UpdateEntity<int>(coupen, (entity) => { return entity.Id; });

                    jsonresult = new JObject(
                        new JProperty("id", coupen.Id),
                        new JProperty("message", "Coupon redeemed successfully."),
                        new JProperty("status", true)
                    );
                }
                else
                {
                    jsonresult = new JObject(
                        new JProperty("id", coupen.Id),
                        new JProperty("message", "Sorry, there was an error processing your request.Please try again."),
                        new JProperty("status", false)
                    );
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                jsonresult = new JObject(
                       new JProperty("id", null),
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
                return GetOkResult(jsonresult);
            }

            return GetOkResult(jsonresult);
        }

        [AllowAnonymous]
        [Route("coupenpreference/list")]
        [AcceptVerbs("POST")]
        public async Task<IHttpActionResult> CoupenPreferenceList(CoupenPreferenceModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                Volunteers volunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == model.VolunteerId).FirstOrDefault();


                Expression<Func<GiftCards, bool>> expression = null;
                expression = x => x.IsActive == true && x.IsDeleted == false;

                if (model.Latitude != null && model.Longitude != null && model.Latitude != 0 && model.Longitude != 0)
                {
                    string NearestZip = PurpicsWeb.Services.GeoLocation.GetNearestZipCode((double)model.Latitude, (double)model.Longitude);

                    Expression<Func<ZipCode, bool>> expressionZip = null;
                    expressionZip = x => x.Zipcode == NearestZip;
                    ZipCode zipCode = Repository<ZipCode>.GetEntityListForQuery(expressionZip, null, null, null, null).FirstOrDefault();

                    if (zipCode != null)
                    {
                        Expression<Func<GiftCards, bool>> expGiftCards = x => x.GiftCardScopeAreas.Any(y => y.ZipcodeId == zipCode.Id);
                        expression = PredicateBuilder.And(expression, expGiftCards);
                    }
                }

                Expression<Func<GiftCards, bool>> expSelectedGiftCards = x => x.Id == volunteer.GiftCardPreference && x.IsActive == true && x.IsDeleted == false;
                expression = PredicateBuilder.Or(expression, expSelectedGiftCards);

                //Order By
                Func<IQueryable<GiftCards>, IOrderedQueryable<GiftCards>> orderBy = x => x.OrderBy(y => y.Name);

                //Include CharityCategories
                List<Expression<Func<GiftCards, Object>>> includes = new List<Expression<Func<GiftCards, object>>>();

                List<GiftCards> preferences = Repository<GiftCards>.GetEntityListForQuery(expression, orderBy, includes, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    preferences =
                        from preference in preferences
                        select new
                        {
                            Id = preference.Id,
                            Name = preference.Name,
                            CharityName = preference.Name
                        }
                });

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }
            return GetOkResult(loJObjResult);
        }

        /// <summary>
        /// Get volunetter details.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("volunteer/{id}/profile")]
        [AcceptVerbs("GET")]
        public async Task<IHttpActionResult> GetProfile(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JObject loJObjResult = null;

            try
            {
                Expression<Func<Volunteers, bool>> expression = null;
                expression = x => x.Id == id;

                //Order By
                Func<IQueryable<Volunteers>, IOrderedQueryable<Volunteers>> orderBy = x => x.OrderBy(y => y.Id);

                //Include 
                List<Expression<Func<Volunteers, Object>>> includes = new List<Expression<Func<Volunteers, object>>>();
                Expression<Func<Volunteers, object>> campaignUploadsEntity = (cu) => cu.CampaignUploads;
                includes.Add(campaignUploadsEntity);

                Expression<Func<Volunteers, object>> incldeSOEntity = (so) => so.VolunteersSOes;
                includes.Add(incldeSOEntity);

                Expression<Func<Volunteers, object>> incldeSudentEntity = (s) => s.VolunteersSOes.Select(y=>y.StudentOrganizations);
                includes.Add(incldeSudentEntity);

                Expression<Func<Volunteers, object>> campaignsEntity = (c) => c.CampaignUploads.Select(x => x.Campaigns);
                includes.Add(campaignsEntity);

                List<Volunteers> volunteers = Repository<Volunteers>.GetEntityListForQuery(expression, orderBy, includes, null, null).ToList();

                loJObjResult = JObject.FromObject(new
                {
                    volunteer =
                        from volunteer in volunteers
                        select new
                        {
                            Id = volunteer.Id,
                            CurrentGrade = volunteer.CurrentGrade,
                            ZipCode = volunteer.ZipCode,
                            CreditPoints = volunteer.CreditPoints,
                            InstagramUserId = volunteer.InstagramUserId,
                            GiftCardPreference = volunteer.GiftCardPreference,
                            ImageFile = volunteer.ImageFile,
                            TotalLike = volunteer.CampaignUploads.Sum(x => x.Likes),
                            RedeemPoints = volunteer.CreditPoints,
                            Email = volunteer.Email,
                            Name = volunteer.Name,
                            InstagramUserName = volunteer.InstagramUserName,
                            organizations =
                                from so in volunteer.VolunteersSOes
                                select new
                                {
                                    StudentOrganizationId = so.StudentOrganizationId,
                                    StudentOrganizationName = so.StudentOrganizations.Name,
                                },
                            campaigns =
                                from campaign in volunteer.CampaignUploads.Where(a => a.TrackingID != null && a.IsDeleted == false).Select(x => x.Campaigns).Where(y => y.IsActive == true && y.IsDeleted == false)
                                select new
                                {
                                    Id = campaign.Id,
                                    CharityID = campaign.CharityID,
                                    StudentOrganizationId = campaign.StudentOrganizationId,
                                    CompanyID = campaign.CompanyID,
                                    CampaignStatusID = campaign.CampaignStatusID,
                                    Caption = campaign.Caption,
                                    ImageFile = Url.Content(CampaignImagePath) + campaign.ImageFile,
                                    ExpirationDate = campaign.ExpirationDate,
                                    Likes = campaign.Likes,
                                    CampaignRunningStatus = campaign.Likes >= campaign.PackageLikes ?
                                    StringEnum.GetStringValue(CommonEnums.CampaignRunningStatus.Completed) :
                                    StringEnum.GetStringValue(CommonEnums.CampaignRunningStatus.OnGoing)
                                }
                        }
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                loJObjResult = new JObject(
                       new JProperty("message", "Sorry, there was an error processing your request. Please try again."),
                       new JProperty("status", false)
                   );
            }

            return GetOkResult(loJObjResult);
        }
    }

    //public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    //{
    //    string Id = string.Empty;
    //    public CustomMultipartFormDataStreamProvider(string path, string id)
    //        : base(path)
    //    {
    //        Id = id;
    //    }

    //    public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
    //    {
    //        var name = Id + ".png";
    //        headers.ContentDisposition.FileName = name;
    //        return name.Replace("\"", string.Empty);
    //    }
    //}

}
