﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Controllers
{
    public class VolunteerController : Controller
    {
        // GET: Volunteer
        public ActionResult Login()
        {
            return View();
        }

        public JsonResult Logout()
        {
            return Json(new { loggedOut = true });
        }
    }
}