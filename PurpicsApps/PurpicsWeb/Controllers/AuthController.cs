﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using InstaSharp;
using InstaSharp.Models.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using PurpicsWeb.Models;

namespace PurpicsWeb.Controllers
{
    public class AuthController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ActionResult Index()
        {
            return View(new OauthRequestViewModel());
        }


        // GET: OAuth
        public ActionResult Complete(OAuthResponse response = null)
        {
            return View(response);
        }

        //if(Request.QueryString["aspxerrorpath"]!=null)
        //{
        //    //your code that depends on aspxerrorpath here
        //}


        // GET: OAuth
        public async Task<ActionResult> OAuth()
        {

            //error: access_denied
            //error_reason: user_denied
            //error_description: The user denied your request
            if (Request.QueryString["error"] != null)
            {
                var error = Request.QueryString["error"];
                var errorReason = Request.QueryString["error_reason"];
                var errorDescription = Request.QueryString["error_description"];
                logger.Warn("Error retrieving OAuth code: {0} - {1} : {2}", error, errorReason, errorDescription);

                return RedirectToAction("Complete", new OauthResponseModel());
            }

            var code = Request.QueryString["code"].Trim();
            

            logger.Debug("Instagram Auth Called");
            logger.Debug("Code = {0}", code);

            var oauthResponse = GetInstagramOauthTokenResponse(code);
            
            // all done, lets redirect to the home controller which will send some intial data to the app
            return RedirectToAction("Complete", oauthResponse);
        }

        //// GET: OAuth
        //public async Task<ActionResult> OAuth(string code, int? applicationId = null)
        //{
        //    //var clientId = ConfigurationManager.AppSettings["client_id"];
        //    //var clientSecret = ConfigurationManager.AppSettings["client_secret"];
        //    //var redirectUri = ConfigurationManager.AppSettings["redirect_uri"];
        //    //var realtimeUri = "";
        //    var clientId = "433528627562442ebc4d768a45d48c48";
        //    var clientSecret = "812425692a8e4f4e8349d7fde2fd0487";
        //    var redirectUri = "	http://purpicsapitest.azurewebsites.net/auth/oauth";
        //    var realtimeUri = "";

        //    logger.Debug("Instagram Auth Called");
        //    logger.Debug("Code = {0}", code);
        //    logger.Debug("Application Id = {0}", applicationId.HasValue ? applicationId : -1);

        //    InstagramConfig config = new InstagramConfig(clientId, clientSecret, redirectUri, realtimeUri);

        //    // add this code to the auth object
        //    var auth = new OAuth(config);

        //    // now we have to call back to instagram and include the code they gave us
        //    // along with our client secret
        //    var oauthResponse = await auth.RequestToken(code);

        //    // both the client secret and the token are considered sensitive data, so we won't be
        //    // sending them back to the browser. we'll only store them temporarily.  If a user's session times
        //    // out, they will have to click on the authenticate button again - sorry bout yer luck.
        //    Session.Add("InstaSharp.AuthInfo", oauthResponse);

        //    if (oauthResponse.AccessToken != null)
        //    {
        //        logger.Debug("Token: {0}", oauthResponse.AccessToken);
        //        logger.Debug("User: {0}", oauthResponse.User.FullName);
        //    }

        //    // all done, lets redirect to the home controller which will send some intial data to the app
        //    return RedirectToAction("Complete", oauthResponse);
        //}

        private OauthResponseModel GetInstagramOauthTokenResponse(string code)
        {
            var parameters = new NameValueCollection
            {
                {"client_id", ConfigurationManager.AppSettings["instagram.clientid"].ToString()},
                {"client_secret", ConfigurationManager.AppSettings["instagram.clientsecret"].ToString()},
                {"grant_type", "authorization_code"},
                {"redirect_uri", ConfigurationManager.AppSettings["instagram.redirecturi"].ToString()},
                {"code", code}
            };

            var client = new WebClient();
            var result = client.UploadValues("https://api.instagram.com/oauth/access_token", "POST", parameters);
            var response = System.Text.Encoding.Default.GetString(result);

            // deserializing nested JSON string to object  
            var jsResult = (JObject)JsonConvert.DeserializeObject(response);
          
            logger.Debug("JSON Result = {0}", jsResult);

            return new OauthResponseModel(jsResult);
        }
    }
}