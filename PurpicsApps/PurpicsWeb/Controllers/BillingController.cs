﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Braintree;
using AutoMapper;
using PurpicsWeb.Services;
using PurpicsWeb.Services.CommonFunctions;
using PurpicsWeb.Models.DTO;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Threading.Tasks;
using Braintree;

namespace PurpicsWeb.Controllers
{
    [Authorize(Roles="Company, Charity")] 
    public class BillingController : Controller
    {

        #region Manage Billling
        public ActionResult ManageBilling()
        {
            TempData["CSEKey"] = System.Configuration.ConfigurationManager.AppSettings["CSEKey"].ToString();
            return View(GetModel());
        }
        public BillingDTO GetModel()
        {
            BillingDTO model = new BillingDTO();
            model.StatesList = CommonFunctions.GetStateList("--Select--");
            model.YearList = CommonFunctions.getCreditCardYear();
            model.MonthList = CommonFunctions.getCreditCardMonth();
            model.CreditCardList = GetCreditCardList();
            if(User.IsInRole("Charity"))
            {
                Charities lsCharity = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.AspNetUserID == User.Identity.GetUserId()).FirstOrDefault();
                if(lsCharity!=null)
                {
                    model.IsAutoRefillOK =(bool) lsCharity.IsAutoPay;
                }
            }
            return model;
        }

        public ActionResult AddEditCreditCard(string token)
        {
            BraintreePayments payment = new BraintreePayments();
            BillingDTO model = new BillingDTO();
            string CustomerId = GetCustomerId();
            if (!string.IsNullOrEmpty(token))
            {
                model = GetModel();
                CreditCard card = payment.GetCustomerDetails(CustomerId).CreditCards.Where(m => m.Token == token).FirstOrDefault();
                model.CreditCardDetails = Mapper.DynamicMap<CreditCardDetails>(card);
                ViewBag.ShowAddBlock = true;
                return View("ManageBilling", model);
            }
            else
            {
                model.StatesList = CommonFunctions.GetStateList("--Select--");
                return View("AddEditCreditCard", model);
            }
        }
        public List<SelectListItem> GetCreditCardList()
        {
            string CustomerId = GetCustomerId();
            BraintreePayments payment = new BraintreePayments();
            List<SelectListItem> loCreditCards = new List<SelectListItem>();
            loCreditCards.Add(new SelectListItem { Value = "0", Text = "Select" });
            if (!string.IsNullOrEmpty(CustomerId) && CustomerId != "0")
            {
                CreditCard[] CreditCards = payment.GetCustomerDetails(CustomerId).CreditCards;

                foreach (var card in CreditCards)
                {
                    loCreditCards.Add(new SelectListItem { Value = card.Token, Text = card.MaskedNumber.GetCreditCardLastDigit() });
                }
            }
            return loCreditCards;
        }
        [HttpPost]
        public ActionResult AddEditCreditCard(FormCollection collection)
        {
            string token = collection["CreditCardDetails.Token"];
            BillingDTO model = new BillingDTO();
            string CustomerId = GetCustomerId();
            BraintreePayments payment = new BraintreePayments();
            if (!string.IsNullOrEmpty(token))
            {
                Result<PaymentMethod> result = payment.UpdateCreditCard(collection, CustomerId, token);
                if (result.IsSuccess())
                {
                    return View("ManageBilling", GetModel());
                }
                else
                {
                    model = GetModel();
                    if (result.Message == "Duplicate card exists in the vault.")
                    {
                        ModelState.AddModelError("CardError", "This credit card is already on file.");
                    }
                    else
                    {
                        ModelState.AddModelError("CardError", result.Message.ToString());
                    }
                    model.StatesList = CommonFunctions.GetStateList("--Select--");
                    ViewBag.ShowAddBlock = true;
                    return View("ManageBilling", model);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(CustomerId)) // Create New Customer in Braintree
                {
                    Result<Customer> result = payment.CreateNewCustomer(collection);
                    if (result.IsSuccess())
                    {
                        string userid = HttpContext.User.Identity.GetUserId();
                        if (User.IsInRole("Charity"))
                        {
                            Charities charity = Repository<Charities>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();

                            charity.BraintreeCustomerId = result.Target.Id;
                            charity.ModifiedOn = DateTime.Now;

                            Repository<Charities>.UpdateEntity<int>(charity, (entity) => { return entity.Id; });
                        }
                        else if (User.IsInRole("Company"))
                        {
                            Companies company = Repository<Companies>.GetEntityListForQuery(x => x.AspNetUserID == userid).FirstOrDefault();

                            company.BraintreeCustomerId = result.Target.Id;
                            company.ModifiedOn = DateTime.Now;

                            Repository<Companies>.UpdateEntity<int>(company, (entity) => { return entity.Id; });
                        }
                        return View("ManageBilling", GetModel());
                    }
                    else
                    {
                        model = GetModel();
                        if (result.Message == "Duplicate card exists in the vault.")
                        {
                            ModelState.AddModelError("CardError", "This credit card is already on file.");
                        }
                        else
                        {
                            ModelState.AddModelError("CardError", result.Message.ToString());
                        }
                        model.StatesList = CommonFunctions.GetStateList();
                        ViewBag.ShowAddBlock = true;
                        return View("ManageBilling", model);
                    }
                }
                else
                {
                    Result<PaymentMethod> result = payment.CreateCreditCard(collection, CustomerId);
                    if (result.IsSuccess())
                    {
                        return View("ManageBilling", GetModel());
                    }
                    else
                    {
                        model = GetModel();
                        ModelState.AddModelError("CardError", "Credit card number is invalid.");
                        model.StatesList = CommonFunctions.GetStateList("--Select--");
                        
                        model.CreditCardDetails = new CreditCardDetails();
                        model.CreditCardDetails.MaskedNumber = collection["number"];
                        model.CreditCardDetails.CardholderName = collection["card-holder-name"];
                        model.CreditCardDetails.BillingAddress.StreetAddress = collection["street_address"];
                        model.CreditCardDetails.BillingAddress.Locality = collection["locality"];
                        model.CreditCardDetails.BillingAddress.Region = collection["CreditCardDetails.BillingAddress.Region"];
                        model.CreditCardDetails.BillingAddress.PostalCode = collection["postal_code"];
                        model.CreditCardDetails.ExpirationDate = collection["month"] + "/" + collection["year"];
                        model.CreditCardDetails.IsDefault = collection["IsDefault"] == "on" ? true : false;
                        ViewBag.ShowAddBlock = true;
                        return View("ManageBilling", model);
                    }
                }
            }
        }

        [HttpPost]
        public ActionResult getCardList()
        {
            string CustomerId = GetCustomerId();
            BraintreePayments payment = new BraintreePayments();
            BillingDTO model = new BillingDTO();
            model.StatesList = CommonFunctions.GetStateList("--Select--");
            if (!string.IsNullOrEmpty(CustomerId) && CustomerId != "0")
            {
                model.CreditCards = payment.GetCustomerDetails(CustomerId).CreditCards;
            }
            return PartialView("~/Views/Billing/_CreditCardList.cshtml", model);
        }

        public ActionResult DeleteCreditCard(string token)
        {
            try
            {
                BillingDTO model = new BillingDTO();
                BraintreePayments payment = new BraintreePayments();
                payment.DeleteCreditCard(token);
                model = GetModel();
                model.StatesList = CommonFunctions.GetStateList("--Select--");
                ViewBag.ShowAddBlock = false;
                return View("ManageBilling", model);
            }
            catch (Exception ex)
            {
                return Json(-1, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public string paynow(string Token)
        {
            string lsMsg = string.Empty;
            try
            {
                string CharityId = User.Identity.GetUserId();
                Charities loCharity = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.AspNetUserID == CharityId).FirstOrDefault();
                var lsResult = new BraintreePayments().PayCharityActivationAmount(loCharity.Id, Token);
                if (lsResult != null && lsResult.IsSuccess())
                {
                    TempData["SuccessMsg"] = "Your subscription is activated now.";
                }
                else
                {
                    TempData["ErrorMsg"] = lsResult.Message;
                }
            }
            catch(Exception)
            {
                TempData["ErrorMsg"] = "Something wrong!! Please try after sometime";
            }
            lsMsg = "Success";
            
            return lsMsg;
        }
        [HttpPost]
        public ActionResult SaveAutoRefillSettings(bool IsAutoRefillOK)
        {
            Charities lsCharity = Repository<Charities>.GetEntityListForQuery(null).Where(x => x.AspNetUserID == User.Identity.GetUserId()).FirstOrDefault();
            if (lsCharity != null)
            {
                lsCharity.IsAutoPay = IsAutoRefillOK;
                Repository<Charities>.UpdateEntity(lsCharity, (entity) => { return entity.Id; });
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public string GetCustomerId()
        {
            string CustomerId = string.Empty;
            string userid = HttpContext.User.Identity.GetUserId();
            if (User.IsInRole("Charity"))
            {
                Charities charity = userid.GetCharityDetails();
                CustomerId = charity.BraintreeCustomerId;
            }
            else if (User.IsInRole("Company"))
            {
                Companies company = userid.GetCompanyDetails();
                CustomerId = company.BraintreeCustomerId;
            }
            return CustomerId;
        }
        #endregion
    }
}