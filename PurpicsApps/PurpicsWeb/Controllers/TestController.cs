﻿using InstaSharp;
using InstaSharp.Models.Responses;
using PurpicsWeb.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Controllers
{
    public class TestController : Controller
    {
        static string clientId = ConfigurationManager.AppSettings["instagram.clientid"];
        static string clientSecret = ConfigurationManager.AppSettings["instagram.clientsecret"];
        static string redirectUri = ConfigurationManager.AppSettings["Instagram.redirecturi"];

        InstagramConfig config = new InstagramConfig(clientId, clientSecret, redirectUri, "");

        // GET: Home
        public ActionResult Index()
        {
            InstagramSyncService obj = new InstagramSyncService();
            obj.SyncCampaignLikes();
            return View();
        }

        public ActionResult Login()
        {
            var scopes = new List<OAuth.Scope>();
            scopes.Add(InstaSharp.OAuth.Scope.Basic);
            scopes.Add(InstaSharp.OAuth.Scope.Public_Content);
            scopes.Add(InstaSharp.OAuth.Scope.Follower_List);
            scopes.Add(InstaSharp.OAuth.Scope.Comments);
            scopes.Add(InstaSharp.OAuth.Scope.Relationships);
            scopes.Add(InstaSharp.OAuth.Scope.Likes);

            var link = InstaSharp.OAuth.AuthLink(config.OAuthUri + "authorize", config.ClientId, config.RedirectUri, scopes, InstaSharp.OAuth.ResponseType.Code);

            return Redirect(link);
        }

        public async Task<ActionResult> MyFeed()
        {
            var oAuthResponse = Session["InstaSharp.AuthInfo"] as OAuthResponse;

            if (oAuthResponse == null)
            {
                return RedirectToAction("Login");
            }

            var users = new InstaSharp.Endpoints.Users(config, oAuthResponse);

            var feed = await users.Feed(null, null, null);

            return View(feed.Data);
        }

        public async Task<ActionResult> OAuth(string code)
        {
            // add this code to the auth object
            var auth = new OAuth(config);

            // now we have to call back to instagram and include the code they gave us
            // along with our client secret
            var oauthResponse = await auth.RequestToken(code);

            // both the client secret and the token are considered sensitive data, so we won't be
            // sending them back to the browser. we'll only store them temporarily.  If a user's session times
            // out, they will have to click on the authenticate button again - sorry bout yer luck.
            Session.Add("InstaSharp.AuthInfo", oauthResponse);


            var likes = new InstaSharp.Endpoints.Likes(config, oauthResponse);
            await likes.Post("1232342185245059305_3093344265");

            OAuthResponse Auth;
            Auth = new OAuthResponse()
            {
                AccessToken = "3093344265.7987dca.07e5fafdf991482c997fedfa3e3a7c83"
            };
            var objLike = new InstaSharp.Endpoints.Likes(config, Auth);
            var reponse = await objLike.Get("1232342185245059305_3093344265");

            // all done, lets redirect to the home controller which will send some intial data to the app
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Sync()
        {
            InstagramSyncService obj = new InstagramSyncService();

            await Task.Run(() =>
                {
                    obj.UpdateMediaIds();
                });
            return View();
        }
    }
}