﻿using PurpicsWeb.Areas.Charity.Models;
using PurpicsWeb.Areas.Company.Models;
using PurpicsWeb.Areas.StudentOrganization.Models;
using PurpicsWeb_DL.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PurpicsWeb.Controllers
{
    public class SystemEmailController : Controller
    {
        /// <summary>
        /// Created by Nikunj Balar on 10-05-2016 for send email to admin for charity registration notification
        /// </summary>
        /// <param name="objCharityRegistration"></param>
        /// <param name="objCharityCategories"></param>
        /// <returns></returns>
        public string CharityRegistration(CharityRegistration objCharityRegistration, CharityCategories objCharityCategories, int objCharityID)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CharityRegistration.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##CharityName##", objCharityRegistration.CharityName);
            objEmailBody = objEmailBody.Replace("##RegistrationApprovalLink##", ConfigurationManager.AppSettings["domainURL"] + "Admin/ManageCharity/CharityDetail?fiCharityId=" + objCharityID);

            return objEmailBody;
        }

        public string CharityRegistration(PurpicsWeb.Areas.Admin.ViewModels.Charity.CharityRegistration objCharityRegistration, int objCharityID)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CharityRegistration.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##CharityName##", objCharityRegistration.CharityName);
            objEmailBody = objEmailBody.Replace("##RegistrationApprovalLink##", ConfigurationManager.AppSettings["domainURL"] + "Admin/ManageCharity/CharityDetail?fiCharityId=" + objCharityID);

            return objEmailBody;
        }

        /// <summary>
        /// Created by Nikunj Balar on 13-05-2016 for send email to user when charity approvaled by admin
        /// </summary>
        /// <param name="objCharityName"></param>
        /// <returns></returns>
        public string CharityRegistrationApproval(string objCharityName)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CharityRegistrationApproval.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##CharityName##", objCharityName);
            objEmailBody = objEmailBody.Replace("##LoginLink##", ConfigurationManager.AppSettings["domainURL"] + "/Account/Login");

            return objEmailBody;
        }

        public string SORegistrationApproval(string strSOName)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/SORegistrationApproval.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##SOName##", strSOName);
            objEmailBody = objEmailBody.Replace("##LoginLink##", ConfigurationManager.AppSettings["domainURL"] + "/Account/Login");

            return objEmailBody;
        }
        
        /// <summary>
        /// Created by Nikunj Balar on 13-05-2016 for send email to admin for charity rejected from admin
        /// </summary>
        /// <returns></returns>
        public string CharityRegistrationRejected()
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CharityRegistrationRejected.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            return objEmailBody;
        }

        public string SORegistrationRejected()
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/SORegistrationRejected.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }
            return objEmailBody;
        }

        /// <summary>
        /// Created by Nikunj Balar on 10-05-2016 for send email to admin for campaign registration notification
        /// </summary>
        /// <param name="lbjCharityName"></param>
        /// <param name="objPakageName"></param>
        /// <param name="objPackagePrice"></param>
        /// <param name="objCaption"></param>
        /// <param name="objExpirationDate"></param>
        /// <returns></returns>
        public string CampaignRegistration(string objCaption, string objCompanyName, int objCapaignID)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CampaignRegistration.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##Caption##", objCaption);
            objEmailBody = objEmailBody.Replace("##CompanyName##", objCompanyName);
            objEmailBody = objEmailBody.Replace("##RegistrationApprovalLink##", ConfigurationManager.AppSettings["domainURL"] + "Admin/ManageCampaigns/CampaignDetail?fiCampaignId=" + objCapaignID);


            return objEmailBody;
        }

        /// <summary>
        /// Created by Nikunj Balar on 13-05-2016 for send email to user when compaign approvaled by admin
        /// </summary>
        /// <param name="objCampaignName"></param>
        /// <returns></returns>
        public string CampaignRegistrationApproval(string objCampaignName)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CampaignRegistrationApproval.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##CampaignName##", objCampaignName);
            objEmailBody = objEmailBody.Replace("##LoginLink##", ConfigurationManager.AppSettings["domainURL"] + "/Account/Login");

            return objEmailBody;
        }

        /// <summary>
        /// Created by Nikunj Balar on 13-05-2016 for send email to admin for campaign rejected from admin
        /// </summary>
        /// <returns></returns>
        public string CampaignRegistrationRejected()
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CampaignRegistrationRejected.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            return objEmailBody;
        }

        /// <summary>
        /// Created by Nikunj Balar on 10-05-2016 for send email to admin for company registration notification
        /// </summary>
        /// <param name="objRegistration"></param>
        /// <returns></returns>
        public string CompanyRegistration(Registration objRegistration, int objCompanyID)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CompanyRegistration.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##CompanyName##", objRegistration.CompanyName);
            objEmailBody = objEmailBody.Replace("##RegistrationApprovalLink##", ConfigurationManager.AppSettings["domainURL"] + "Admin/ManageCompany/CompanyDetail?fiCompanyId=" + objCompanyID);

            return objEmailBody;
        }

        /// <summary>
        /// Created by Nikunj Balar on 13-05-2016 for send email to user when compaign approvaled by admin
        /// </summary>
        /// <param name="objCompanyName"></param>
        /// <returns></returns>
        public string CompanyRegistrationApproval(string objCompanyName)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CompanyRegistrationApproval.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##CompanyName##", objCompanyName);
            objEmailBody = objEmailBody.Replace("##LoginLink##", ConfigurationManager.AppSettings["domainURL"] + "/Account/Login");

            return objEmailBody;
        }

        /// <summary>
        /// Created by Nikunj Balar on 13-05-2016 for send email to admin for company rejected from admin
        /// </summary>
        /// <returns></returns>
        public string CompanyRegistrationRejected()
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CompanyRegistrationRejected.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            return objEmailBody;
        }

        /// <summary>
        /// Created By Nikunj Balar on 14-05-2016 for gift card volunterr notification
        /// </summary>
        /// <param name="objVolunteerID"></param>
        /// <param name="objVolunteerName"></param>
        /// <param name="objGiftCardType"></param>
        /// <returns></returns>
        public string GiftCardCompanyAdminNotification(string objVolunteerID, string objVolunteerName, string objGiftCardType)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/GiftCardCompanyAdminNotification.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }


            objEmailBody = objEmailBody.Replace("##VolunteerID##", objVolunteerID);
            objEmailBody = objEmailBody.Replace("##VolunteerName##", objVolunteerName);
            objEmailBody = objEmailBody.Replace("##GiftCardType##", objGiftCardType);
            objEmailBody = objEmailBody.Replace("##LoginLink##", ConfigurationManager.AppSettings["domainURL"] + "/Account/Login");

            return objEmailBody;
        }


        /// <summary>
        /// Created by Nikunj Balar on 14-05-2016 for send email to admin for gift card company admin notfication
        /// </summary>
        /// <param name="objVia"></param>
        /// <param name="objExpectedDeliveryDays"></param>
        /// <param name="objExceedDelieveryDays"></param>
        /// <returns></returns>
        public string GiftCardVolunteerNotification(string objVia, string objExpectedDeliveryDays, string objExceedDelieveryDays)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/GiftCardVolunteerNotification.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }



            objEmailBody = objEmailBody.Replace("##via##", objVia);
            objEmailBody = objEmailBody.Replace("##ExpectedDeliveryDays##", objExpectedDeliveryDays);
            objEmailBody = objEmailBody.Replace("##ExceedDeliveryDays##", objExceedDelieveryDays);
            return objEmailBody;
        }

        public string CharitySubscriptionSuccessNotification()
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CharitySubscriptionSuccess.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            return objEmailBody;
        }

        public string CharitySubscriptionFailedNotification()
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CharitySubscriptionFailed.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            return objEmailBody;
        }
        public string CharitySubscriptionExpiringNotification(Charities charity, DateTime expritingOn)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/CharitySubscriptionExpiring.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }
            objEmailBody = objEmailBody.Replace("##RefID##", charity.Id.ToString());
            objEmailBody = objEmailBody.Replace("##CharityName##", charity.CharityName);
            objEmailBody = objEmailBody.Replace("##ExpiringOn##", expritingOn.ToString("dd-MMM-yyyy"));
            return objEmailBody;
        }


        /// <summary>
        /// Created by Jignasha Kanara on 06-11-2016 to send gift card dispatched details to volunteer
        /// </summary>
        /// <returns></returns>
        public string SendGiftCardEmail()
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/SendGiftCardToVolunteer.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }
            return objEmailBody;
        }

        public string SORegistration(StudentOrganizationRegistration objCharityRegistration, int objCharityID)
        {
            string objEmailBody = string.Empty;

            using (StreamReader objStreamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/SORegistration.html")))
            {
                string lsLine = string.Empty;
                while ((lsLine = objStreamReader.ReadLine()) != null)
                {
                    objEmailBody += lsLine;
                }
            }

            objEmailBody = objEmailBody.Replace("##OrgName##", objCharityRegistration.Name);
            objEmailBody = objEmailBody.Replace("##RegistrationApprovalLink##", ConfigurationManager.AppSettings["domainURL"] + "Admin/ManageCharity/CharityDetail?fiCharityId=" + objCharityID);

            return objEmailBody;
        }

    }
}