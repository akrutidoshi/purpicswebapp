﻿using Hangfire;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire.Dashboard;
using PurpicsWeb.Services;
namespace PurpicsWeb
{
    public class Hangfire
    {
        public static void ConfigureHangfire(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");

            app.UseHangfire(config =>
            {
                config.UseAuthorizationFilters(new AuthorizationFilter
                {
                    Roles = "Admin" // allow only Admin roles
                });
            });

            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }

        public static void InitializeJobs()
        {
            InstagramSyncService objInstagramSyncService = new InstagramSyncService();
            CharityPaymentScheduler objCharityPaymentScheduler = new CharityPaymentScheduler();
            RecurringJob.AddOrUpdate("Update MediaId", () => objInstagramSyncService.UpdateMediaIds(), Cron.Hourly);
            RecurringJob.AddOrUpdate("Sync Instagram Likes Count", () => objInstagramSyncService.SyncCampaignLikes(), Cron.Hourly);
            RecurringJob.AddOrUpdate("Process charity payment", () => objCharityPaymentScheduler.ProcessCharitySubscription(), "00 01 * * MON,TUE,WED,THU,FRI,SAT,SUN");
            RecurringJob.AddOrUpdate("Sync Follower Count", () => objInstagramSyncService.SyncVolunteersFollowerCount(), Cron.Daily(1, 0));
        }
    }
}