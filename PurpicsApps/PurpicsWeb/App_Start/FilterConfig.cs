﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NLog;
namespace PurpicsWeb
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new HandleAntiforgeryTokenErrorAttribute() { ExceptionType = typeof(HttpAntiForgeryException) });
        }
    }

    public class HandleAntiforgeryTokenErrorAttribute : HandleErrorAttribute
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            Log.Error(filterContext.Exception.Message);
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(new { action = "Login", controller = "Account", area = "" }));
        }
    }
}
