﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PurpicsWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();
          
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "WebLogin", id = UrlParameter.Optional }
            );
            routes.MapRoute(
              "Admin",
              "Admin/{controller}/{action}",
              new { area = "Admin", controller = "Account", action = "Login" }// URL with parameters
              );
        }
    }
}
