# Purpics README

Author: [Ken Taylor](mailto:ktaylor@gotechark.com?subject=Purpics)  
Date Created: 2016-04-28  
Last Updated: 2016-04-28  

# Summary

The Purpics application consists of a web application, RESTful API and iOS mobile application which currently integrates with Instagram.

## Installation

TBD

## Contributing

First make sure you have the developer dependencies installed.
In particular, you will need the [EF Reverse POCO Generator][revpoco] installed. After installing the generator, you will need to open the `Purpics_Database.tt` T4 file in the `PurpicsWeb_DL` project and save it (no real changes needed). This will regenerate the code first classes.

### Build

### Developer Dependencies

* [EF Reverse POCO Generator][revpoco]
* SendGrid

---

[revpoco]: https://visualstudiogallery.msdn.microsoft.com/ee4fcff9-0c4c-4179-afd9-7a2fb90f5838