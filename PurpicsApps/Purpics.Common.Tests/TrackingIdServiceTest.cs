﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Purpics.Common.Tests
{
    [TestClass]
    public class TrackingIdServiceTest
    {
        private const int TestVolunteerId1 = 1;
        private const int TestVolunteerId2 = 2;
        private const int TestUploadId1 = 1;
        private const int TestUploadId2 = 2;

        [TestMethod]
        public void GetCode_GivenParams_GeneratesCode()
        {
            // Arrange
            // Act
            var code = TrackingIdService.NewId(TestVolunteerId1, TestUploadId1);
            // Assert
            code.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public void GetCode_GivenParams_ReturnsCodeSixCharactersLong()
        {
            // Arrange
            // Act
            var code = TrackingIdService.NewId(TestVolunteerId1, TestUploadId1);
            // Assert
            code.Length.Should().Be(6);
        }

        [TestMethod]
        public void GetCode_GivenTwoDifferentParamSets_GeneratesTwoDifferentIds()
        {
            // Arrange
            // Act
            var code1 = TrackingIdService.NewId(TestVolunteerId1, TestUploadId1);
            var code2 = TrackingIdService.NewId(TestVolunteerId2, TestUploadId2);
            // Assert
            code1.Should().NotBe(code2);
        }

        [TestMethod]
        public void GetCode_GivenSameSetOfParamsTwice_GeneratesSameIds()
        {
            // Arrange
            // Act
            var code1 = TrackingIdService.NewId(TestVolunteerId1, TestUploadId1);
            var code2 = TrackingIdService.NewId(TestVolunteerId1, TestUploadId1);
            // Assert
            code2.Should().Be(code1);
        }
    }
}
