using System;
using NLog;

namespace Purpics.Instagram
{
    public class InstagramService
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly IInstagramUploadRepository _repo;
        private readonly IInstagramApiClient _client;

        public InstagramService(IInstagramUploadRepository repo, IInstagramApiClient client)
        {
            _repo = repo;
            _client = client;
        }

        public void UpdateMediaId(string trackingId)
        {
            Log.Debug("Updating media id for trackingId {0}", trackingId);
            var upload = _repo.GetUploadByTrackingId(trackingId);
            if (upload != null)
            {
                var instagramUpload = _client.GetMediaId(upload);
                if (instagramUpload != null && !String.IsNullOrEmpty(instagramUpload.MediaId))
                {
                    _repo.UpdateMediaId(instagramUpload);
                }
            }
            else
            {
                Log.Warn("CampaignUpload not found for trackingID {0}", trackingId);
            }
        }

        public void UpdateMediaIds()
        {
            Log.Debug("Updating media ids");
            var uploads = _repo.GetUploadsWithNoMediaId();
            foreach (var instagramUpload in uploads)
            {
                var uploadWithMediaId = _client.GetMediaId(instagramUpload);
                if (uploadWithMediaId != null && !String.IsNullOrEmpty(uploadWithMediaId.MediaId))
                {
                    _repo.UpdateMediaId(uploadWithMediaId);
                }
                else // Delete record for which Intagrampost leave by user
                {
                    _repo.DeleteCampaignUpload(instagramUpload);
                }
            }
        }

        public void UpdateLikeCounts()
        {
            Log.Debug("Updating like counts");
            var uploads = _repo.GetUploadsWithMediaId();
            foreach (var instagramUpload in uploads)
            {
                var uploadWithLikes = _client.GetLikeCount(instagramUpload);
                if (uploadWithLikes != null && uploadWithLikes.LikeCount > 0)
                {
                    _repo.UpdateLikeCount(uploadWithLikes);
                }
            }
            _repo.UpdateTotalLikeCount(); // Update total likes of single campaign uploaded by volunteers.
        }

        public void UpdateLikeCountByMediaId(string mediaId)
        {
            Log.Debug("Updating like count for Media Id {0}", mediaId);
            var upload = _repo.GetUploadByMediaId(mediaId);
            if (upload != null)
            {
                var uploadWithLikes = _client.GetLikeCount(upload);
                if (uploadWithLikes != null && uploadWithLikes.LikeCount > 0)
                {
                    _repo.UpdateLikeCount(uploadWithLikes);
                }
            }
            else
            {
                Log.Warn("Media Id {0} not found!", mediaId);
            }
        }

        public void UpdateFollowerCount()
        {
            var volunteers = _repo.GetVolunteers();
            foreach (var volunteer in volunteers)
            {
                Log.Debug("Updating follower count for user Id {0}", volunteer.InstagramUserId);

                var volunteerDetails = _client.GetFollowerCount(volunteer);
                if (volunteerDetails != null)
                {
                    _repo.UpdateFollowerCount(volunteerDetails);
                }
            }
        }
    }
}