﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;

namespace Purpics.Instagram
{
    // Borrowed from: https://gist.github.com/rdingwall/1884642
    public class DynamicJsonDeserializer : IDeserializer
    {
        public string RootElement { get; set; }
        public string Namespace { get; set; }
        public string DateFormat { get; set; }

        public T Deserialize<T>(RestResponse response) where T : new()
        {
            return JsonConvert.DeserializeObject<dynamic>(response.Content);
        }


        public T Deserialize<T>(IRestResponse response)
        {
            return JsonConvert.DeserializeObject<dynamic>(response.Content);
        }
    }
}
