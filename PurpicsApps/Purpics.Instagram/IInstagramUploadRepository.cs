using System.Collections.Generic;

namespace Purpics.Instagram
{
    public interface IInstagramUploadRepository
    {
        InstagramUpload GetUploadByTrackingId(string trackingId);
        void UpdateMediaId(InstagramUpload upload);
        IEnumerable<InstagramUpload> GetUploadsWithNoMediaId();
        IEnumerable<InstagramUpload> GetUploadsWithMediaId();
        void UpdateLikeCount(InstagramUpload upload);
        void UpdateTotalLikeCount();
        InstagramUpload GetUploadByMediaId(string mediaId);
        void DeleteCampaignUpload(InstagramUpload upload);
        IEnumerable<InstagramUser> GetVolunteers();
        void UpdateFollowerCount(InstagramUser user);
    }
}