namespace Purpics.Instagram
{
    public interface IInstagramApiClient
    {
        InstagramUpload GetMediaId(InstagramUpload upload);
        InstagramUpload GetLikeCount(InstagramUpload upload);
        InstagramUser GetFollowerCount(InstagramUser user);
    }
}