﻿using System;
using System.Configuration;
using NLog;
using RestSharp;

namespace Purpics.Instagram
{
    public class InstagramApiClient : IInstagramApiClient
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private RestClient _client;

        public InstagramApiClient()
        {
            _client = new RestClient(ConfigurationManager.AppSettings["InstagramUri"]);
            _client.AddHandler("application/json", new DynamicJsonDeserializer());
            Log.Debug("Instagram URI = {0}", ConfigurationManager.AppSettings["InstagramUri"]);
        }

        public InstagramUpload GetMediaId(InstagramUpload upload)
        {
            bool hasFoundMedia = false;
            try
            {
                var request = new RestRequest(ConfigurationManager.AppSettings["InstagramGetRecentMediaEndpoint"], Method.GET);
                request.AddUrlSegment("userid", upload.InstagramUserId.Trim());
                request.AddParameter("access_token", upload.InstagramToken.Trim());

                var response = _client.Execute<dynamic>(request);
                Log.Debug(response);

                // Data returned as dynamic object!
                if (response.Data != null)
                {
                    dynamic media = response.Data.data;

                    foreach (var mediaRecord in media)
                    {
                        //if (mediaRecord.caption.text == upload.TrackingId.Trim())
                        string caption = mediaRecord.caption.text;
                        if (caption.Contains(upload.TrackingId.Trim()))
                        {
                            upload.MediaId = mediaRecord.id;
                            hasFoundMedia = true;
                        }
                    }
                }
            }
            catch
            { }
            return (hasFoundMedia) ? upload : null;
        }

        public InstagramUpload GetLikeCount(InstagramUpload upload)
        {
            var request = new RestRequest(ConfigurationManager.AppSettings["InstagramGetMediaEndpoint"], Method.GET);
            request.AddUrlSegment("mediaid", upload.MediaId);
            request.AddParameter("access_token", upload.InstagramToken);

            var response = _client.Execute<dynamic>(request);
            Log.Debug(response);

            if (response == null
                || response.Data == null
                || response.Data.meta == null
                || response.Data.meta.code == 400)
            {
                return null; // If not found, return null
            }
            //if (response.Data.meta.code == 400) return null; // If not found, return null

            // Data returned as dynamic object!
            dynamic media = response.Data.data;

            upload.LikeCount = media.likes.count;

            return upload;
        }

        public InstagramUser GetFollowerCount(InstagramUser user)
        {
            var request = new RestRequest(ConfigurationManager.AppSettings["InstagramGetUserDetailsEndpoint"], Method.GET);
            request.AddUrlSegment("userid", user.InstagramUserId.Trim());
            request.AddParameter("access_token", user.InstagramToken.Trim());

            var response = _client.Execute<dynamic>(request);
            Log.Debug(response);

            if (response == null
                || response.Data == null
                || response.Data.meta == null
                || response.Data.meta.code == 400)
            {
                return null; // If not found, return null
            }

            user.InstagramUserDetails = response.Data.data;
            return user;
        }
    }
}