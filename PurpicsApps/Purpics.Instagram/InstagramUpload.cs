namespace Purpics.Instagram
{
    /// <summary>
    /// The InstagramUpload is a DTO class which combines data from the Volunteer record (instagram userid and token)
    /// and a related CampaignUpload record in order to retrieve data (mediaid/like count) from the Instagram API
    /// </summary>
    public class InstagramUpload
    {
        public int CampaignUploadId { get; set; }
        public string InstagramUserId { get; set; }
        public string InstagramToken { get; set; }
        public int? LikeCount { get; set; }
        public string TrackingId { get; set; }
        public string MediaId { get; set; }
        public int CampaignID { get; set; }
        public int VolunteerId { get; set; }
    }
}