﻿using System.Threading.Tasks;
using System.Configuration;
using InstaSharp.Models.Responses;

// THIS WILL BE DEPRECATED!
namespace Purpics.Instagram
{
    public static class InstagramHelper
    {
        private static string _clientId = ConfigurationManager.AppSettings["instagram.clientid"];
        private static string _clientSecret = ConfigurationManager.AppSettings["instagram.clientsecret"];
        private static string _websiteUrl = ConfigurationManager.AppSettings["instagram.redirecturi"];
        private static string _redirectUri = ConfigurationManager.AppSettings["instagram.redirecturi"];


        public static async Task<UsersResponse> GetLikes(string MediaId, string AuthToken)
        {
            var connector = new InstagramConnector(_clientId, _clientSecret, _websiteUrl, _redirectUri);
            OAuthResponse Auth;

            Auth = new OAuthResponse()
            {
                AccessToken = AuthToken
            };

            var objLike = new InstaSharp.Endpoints.Likes(connector._config, Auth);
            var reponse = await objLike.Get(MediaId);
            return reponse;
        }
    }
}
