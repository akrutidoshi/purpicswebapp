using System.Collections.Generic;
using InstaSharp;

// THIS WILL BE DEPRECATED!
namespace Purpics.Instagram
{
    public class InstagramConnector
    {
        public InstagramConfig _config;

        public InstagramConnector(string clientId, string clientSecret, string websiteUrl, string redirectUri)
            : this(clientId, clientSecret, websiteUrl, redirectUri, "") { }

        public InstagramConnector(string clientId, string clientSecret, string websiteUrl, string redirectUri,
            string callbackUri)
        {
            _config = new InstagramConfig(clientId, clientSecret, redirectUri, callbackUri);
        }

        public string GetLoginLink()
        {
            var scopes = new List<OAuth.Scope>();
            scopes.Add(InstaSharp.OAuth.Scope.Likes);
            scopes.Add(InstaSharp.OAuth.Scope.Comments);

            var link = InstaSharp.OAuth.AuthLink(_config.OAuthUri + "authorize", _config.ClientId, _config.RedirectUri, scopes, InstaSharp.OAuth.ResponseType.Code);

            return link;
        }
    }
}