﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using PurpicsWeb_DL.Entities;
using PurpicsWeb_DL.Repositories;
using NLog.Fluent;
using System.IO;
using System.Configuration;
using Purpics.Common;

namespace Purpics.Instagram
{
    public class InstagramUploadRepository : IInstagramUploadRepository
    {
        public InstagramUpload GetUploadByTrackingId(string trackingId)
        {
            Expression<Func<CampaignUploads, bool>> expression;
            expression = x => x.TrackingID == trackingId;

            //Order By
            Func<IQueryable<CampaignUploads>, IOrderedQueryable<CampaignUploads>> orderBy = x => x.OrderBy(y => y.TrackingID);

            //Include Volunteers
            List<Expression<Func<CampaignUploads, Object>>> includes = new List<Expression<Func<CampaignUploads, object>>>();
            Expression<Func<CampaignUploads, object>> volunteersEntity = (field) => field.Volunteers;
            includes.Add(volunteersEntity);

            var upload = Repository<CampaignUploads>.GetEntityListForQuery(expression, orderBy, includes, null, null).FirstOrDefault();

            if (upload == null) return null;

            return new InstagramUpload
            {
                CampaignUploadId = upload.Id,
                LikeCount = upload.Likes,
                MediaId = upload.MediaID,
                TrackingId = upload.TrackingID,
                InstagramUserId = upload.Volunteers.InstagramUserId,
                InstagramToken = upload.Volunteers.InstagramToken
            };
        }

        public void UpdateMediaId(InstagramUpload upload)
        {
            var campaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(x => x.Id == upload.CampaignUploadId).FirstOrDefault();
            if (campaignUpload == null) return;

            campaignUpload.MediaID = upload.MediaId;
            campaignUpload.ModifiedOn = DateTime.Now;
            campaignUpload.ModifiedBy = upload.VolunteerId.ToString();
            Repository<CampaignUploads>.UpdateEntity(campaignUpload, (entity) => entity.Id);
        }

        public void DeleteCampaignUpload(InstagramUpload upload)
        {
            var campaignUpload = Repository<CampaignUploads>.GetEntityListForQuery(x => x.Id == upload.CampaignUploadId).FirstOrDefault();
            if (campaignUpload == null) return;

            campaignUpload.IsDeleted = true;
            campaignUpload.ModifiedOn = DateTime.Now;
            campaignUpload.ModifiedBy = upload.VolunteerId.ToString();
            Repository<CampaignUploads>.UpdateEntity(campaignUpload, (entity) => entity.Id);
        }

        public IEnumerable<InstagramUpload> GetUploadsWithNoMediaId()
        {
            Expression<Func<CampaignUploads, bool>> expression;
            expression = x => x.MediaID == null;// && x.IsDeleted == false;

            //Order By
            Func<IQueryable<CampaignUploads>, IOrderedQueryable<CampaignUploads>> orderBy = x => x.OrderBy(y => y.TrackingID);

            //Include Volunteers
            List<Expression<Func<CampaignUploads, Object>>> includes = new List<Expression<Func<CampaignUploads, object>>>();
            Expression<Func<CampaignUploads, object>> volunteersEntity = (field) => field.Volunteers;
            includes.Add(volunteersEntity);

            var campaignUploads = Repository<CampaignUploads>.GetEntityListForQuery(expression, orderBy, includes, null, null);

            var uploadsNoMediaId = from u in campaignUploads
                                   select new InstagramUpload
                                   {
                                       CampaignUploadId = u.Id,
                                       InstagramToken = u.Volunteers.InstagramToken,
                                       InstagramUserId = u.Volunteers.InstagramUserId,
                                       LikeCount = u.Likes,
                                       MediaId = u.MediaID,
                                       TrackingId = u.TrackingID
                                   };

            return uploadsNoMediaId;
        }

        public IEnumerable<InstagramUpload> GetUploadsWithMediaId()
        {
            Expression<Func<CampaignUploads, bool>> expression;
            expression = x => x.MediaID != null && x.IsDeleted == false;

            //Order By
            Func<IQueryable<CampaignUploads>, IOrderedQueryable<CampaignUploads>> orderBy = x => x.OrderBy(y => y.TrackingID);

            //Include Volunteers
            List<Expression<Func<CampaignUploads, Object>>> includes = new List<Expression<Func<CampaignUploads, object>>>();
            Expression<Func<CampaignUploads, object>> volunteersEntity = (field) => field.Volunteers;
            includes.Add(volunteersEntity);

            var campaignUploads = Repository<CampaignUploads>.GetEntityListForQuery(expression, orderBy, includes, null, null);

            var uploadsWithMediaId = from u in campaignUploads
                                     select new InstagramUpload
                                     {
                                         CampaignUploadId = u.Id,
                                         InstagramToken = u.Volunteers.InstagramToken,
                                         InstagramUserId = u.Volunteers.InstagramUserId,
                                         LikeCount = u.Likes,
                                         MediaId = u.MediaID,
                                         TrackingId = u.TrackingID,
                                         CampaignID = u.CampaignID,
                                         VolunteerId = u.VolunteerID
                                     };

            return uploadsWithMediaId;
        }

        public void UpdateLikeCount(InstagramUpload upload)
        {
            if (upload != null && upload.LikeCount > 0)
            {
                CampaignUploads objCampaignUploads = Repository<CampaignUploads>.GetEntityListForQuery(x => x.Id == upload.CampaignUploadId).FirstOrDefault();
                int NewLikeDifference = Convert.ToInt32(upload.LikeCount) - objCampaignUploads.Likes;
                objCampaignUploads.Likes = (int)upload.LikeCount;
                Repository<CampaignUploads>.UpdateEntity<int>(objCampaignUploads, (entity) => { return entity.Id; });

                // Update PointsEarned by Volunteers based on Likes
                Volunteers volunveers = Repository<Volunteers>.GetEntityListForQuery(x => x.Id == upload.VolunteerId).FirstOrDefault();
                AppSettings setting = Repository<AppSettings>.GetEntityListForQuery(x => x.Name == "CampaignPointAwarded").FirstOrDefault();
                int CreditPoints = volunveers.CreditPoints + (NewLikeDifference * Convert.ToInt32(setting.Value));
                if (CreditPoints > 0)
                {
                    volunveers.CreditPoints = CreditPoints;
                    Repository<Volunteers>.UpdateEntity<int>(volunveers, (entity) => { return entity.Id; });
                }
            }
        }
        public async void UpdateTotalLikeCount()
        {
            Log.Warn("Update Total Like Count");
            Expression<Func<Campaigns, bool>> expression;
            expression = x => x.IsActive == true && x.IsDeleted == false && x.Likes < x.PackageLikes;

            var campaigns = Repository<Campaigns>.GetEntityListForQuery(expression, null, null, null, null);
            foreach (var campaign in campaigns)
            {
                List<CampaignUploads> objCampaignUploads = Repository<CampaignUploads>.GetEntityListForQuery(x => x.CampaignID == campaign.Id).ToList();
                int TotalCampaignLikes = objCampaignUploads.Sum(x => x.Likes);

                campaign.Likes = TotalCampaignLikes;
                if (campaign.Likes >= campaign.PackageLikes)
                {
                    // Insert entry into CharityPaymentHistory when campgin likes completed
                    CharityPaymentHistory charityPaymentHistory = Repository<CharityPaymentHistory>.GetEntityListForQuery(x => x.CampaignID == campaign.Id).FirstOrDefault();
                    if (charityPaymentHistory == null)
                    {
                        CharityPaymentHistory charityPayment = new CharityPaymentHistory();
                        charityPayment.CharityID = campaign.CharityID;
                        charityPayment.CompanyID = campaign.CompanyID;
                        charityPayment.CampaignID = campaign.Id;
                        charityPayment.CampaignEndDate = DateTime.Now;
                        charityPayment.EstAmount = (campaign.PackagePrice / 100) * Convert.ToDouble(campaign.CharityPercentage);
                        charityPayment.IsPaid = false;
                        charityPayment.CreatedOn = DateTime.Now;
                        charityPayment.CreatedBy = "0";
                        Repository<CharityPaymentHistory>.InsertEntity<int>(charityPayment, (entity) => { return entity.Id; });
                    }

                    // Send email to company when campgin likes completed
                    Companies company = Repository<Companies>.GetEntityListForQuery(x => x.Id == campaign.CompanyID).FirstOrDefault();
                    AspNetUsers user = Repository<AspNetUsers>.GetEntityListForQuery(x => x.Id == company.AspNetUserID).FirstOrDefault();
                    if (user != null && !string.IsNullOrEmpty(user.Email))
                    {
                        System.Net.WebClient client = new System.Net.WebClient();
                        byte[] data = client.DownloadData(ConfigurationManager.AppSettings["domainURL"] + "EmailTemplate/CampaignCompletionNotification.html");
                        string EmailBody = System.Text.Encoding.UTF8.GetString(data);
                        //EmailBody = EmailBody.Replace("##RefID##", campaign.Id.ToString());
                        //EmailBody = EmailBody.Replace("##Caption##", campaign.Caption);
                        //EmailBody = EmailBody.Replace("##Likes##", Convert.ToString(campaign.Likes));
                        EmailService.SendEmail("Campaign completed", EmailBody, user.Email);
                    }

                    //Update CampaignEndDate
                    campaign.CampaignEndDate = DateTime.Now;
                }
               await Repository<Campaigns>.UpdateEntity<int>(campaign, (entity) => { return entity.Id; });
            }
        }
        public InstagramUpload GetUploadByMediaId(string mediaId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<InstagramUser> GetVolunteers()
        {
            Expression<Func<Volunteers, bool>> expression;
            expression = x => x.InstagramUserId != null && x.InstagramToken != null && x.IsDeleted == false;

            var campaignUploads = Repository<Volunteers>.GetEntityListForQuery(expression, null, null, null, null);

            var uploadsWithMediaId = from u in campaignUploads
                                     select new InstagramUser
                                     {
                                         Id = u.Id,
                                         InstagramToken = u.InstagramToken,
                                         InstagramUserId = u.InstagramUserId,
                                     };

            return uploadsWithMediaId;
        }

        public void UpdateFollowerCount(InstagramUser user)
        {
            if (!string.IsNullOrEmpty(user.InstagramUserId))
            {
                Volunteers volunteer = Repository<Volunteers>.GetEntityListForQuery(x => x.InstagramUserId == user.InstagramUserId).FirstOrDefault();

                if (volunteer != null)
                {
                    if (user.InstagramUserDetails != null)
                    {
                        volunteer.FollowerCount = user.InstagramUserDetails.counts.followed_by;
                        volunteer.ModifiedOn = DateTime.Now;
                        Repository<Volunteers>.UpdateEntity<int>(volunteer, (entity) => { return entity.Id; });
                    }
                }
            }
        }
    }
}
