﻿using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Purpics.Instagram.IntegrationTests
{
    [TestClass]
    public class InstagramUploadRepositoryTest
    {
        private readonly InstagramUploadRepository _repo = new InstagramUploadRepository();
        
        [TestMethod]
        public void GetUploadByTrackingId_GivenValidTrackingId_ReturnsInstagramUpload()
        {
            // Arrange
            const string trackingIdInDatabase = "1232342185245059305_3093344265";
            const int expectedCampaignUploadId = 3;
            // Act
            var upload = _repo.GetUploadByTrackingId(trackingIdInDatabase);
            // Assert
            upload.CampaignUploadId.Should().Be(expectedCampaignUploadId);
        }

        [TestMethod]
        public void GetUploadByTrackingId_GivenInvalidTrackingId_ReturnsNull()
        {
            // Arrange
            string nonExistantTrackingId = "1def316b-7bed-4660-8442-034616155e76";
            // Act
            var upload = _repo.GetUploadByTrackingId(nonExistantTrackingId);
            // Assert
            upload.Should().BeNull();
        }

        [TestMethod]
        public void UpdateMediaId_GivenValidUpload_UpdatesMediaId()
        {
            // Arrange
            var upload = new InstagramUpload
            {
                CampaignUploadId = 3,
                MediaId = "TESTMEDIAID",
                TrackingId = "1232342185245059305_3093344265"
            };
            
            // Act
            _repo.UpdateMediaId(upload);
            var returnUpload = _repo.GetUploadByTrackingId(upload.TrackingId);
            // Assert
            returnUpload.MediaId.Should().Be(upload.MediaId);
        }

        [TestMethod]
        public void GetUploadsWithNoMediaId_WhenCalled_ReturnsUploads()
        {
            // Arrange
            // Act
            var uploads = _repo.GetUploadsWithNoMediaId();
            // Assert
            uploads.Count().Should().BeGreaterThan(0);
        }

        [TestMethod]
        public void GetUploadsWithMediaId_WhenCalled_ReturnsUploads()
        {
            // Arrange
            // Act
            var uploads = _repo.GetUploadsWithMediaId();
            // Assert
            uploads.Count().Should().BeGreaterThan(0);
        }        
    }  
}
