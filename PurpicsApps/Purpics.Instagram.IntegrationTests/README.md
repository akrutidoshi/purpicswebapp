﻿# Purpics.Instagram.IntegrationTests

Author: [Ken Taylor](mailto:ktaylor@gotechark.com?subject=Purpics.Instagram.IntegrationTests)  
Date Created: 2016-05-05  
Last Updated: 2016-05-05  

## Summary

This is an integration test library. BEWARE: Integration tests are brittle and rely on databases, apis and such.
They are NOT unit tests.
These should be run on occasion, but may need tweeking.

## Installation

---