﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Purpics.Instagram.IntegrationTests
{
    [TestClass]
    public class InstagramApiClientTest
    {
        private string _instagramUserId = "2718940373";
        private string _instagramToken = "2718940373.1fb234f.7dcef28a6cef414f87f250dc7bff222d";
        private string _mediaId = "1203890351354796607_2718940373";

        [TestMethod]
        public void GetLikeCount_GivenValidUpload_ReturnsProperLikeCount()
        {
            // Arrange
            var apiClient = new InstagramApiClient();
            var upload = new InstagramUpload()
            {
                CampaignUploadId = 1,
                InstagramToken = _instagramToken,
                InstagramUserId = _instagramUserId,
                MediaId = _mediaId,
                TrackingId = "TEST"
            };
            // Act
            var returnUpload = apiClient.GetLikeCount(upload);
            // Assert
            returnUpload.LikeCount.Should().Be(10);
        }

        [TestMethod]
        public void GetLikeCount_GivenInvalidUpload_ReturnsNull()
        {
            // Arrange
            var apiClient = new InstagramApiClient();
            var upload = new InstagramUpload()
            {
                CampaignUploadId = 1,
                InstagramToken = _instagramToken,
                InstagramUserId = _instagramUserId,
                MediaId = "DOESNOTEXIST",
                TrackingId = "TEST"
            };
            // Act
            var returnUpload = apiClient.GetLikeCount(upload);
            // Assert
            returnUpload.Should().BeNull();
        }

        [TestMethod]
        public void GetMediaId_GivenValidUpload_ReturnsProperMediaId()
        {
            // Arrange
            var apiClient = new InstagramApiClient();
            var upload = new InstagramUpload()
            {
                CampaignUploadId = 1,
                InstagramToken = _instagramToken,
                InstagramUserId = _instagramUserId,
                TrackingId = "The pets are busy watching the latest show on their new TV..."
            };
            // Act
            var returnUpload = apiClient.GetMediaId(upload);
            // Assert
            returnUpload.MediaId.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public void GetMediaId_GivenInvalidUpload_ReturnsNull()
        {
            // Arrange
            var apiClient = new InstagramApiClient();
            var upload = new InstagramUpload()
            {
                CampaignUploadId = 1,
                InstagramToken = _instagramToken,
                InstagramUserId = _instagramUserId,
                TrackingId = "NON-EXISTANT-TRACKING-ID"
            };
            // Act
            var returnUpload = apiClient.GetMediaId(upload);
            // Assert
            returnUpload.Should().BeNull();
        }
    }
}
